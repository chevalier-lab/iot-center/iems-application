package com.aitekteam.developer.iemscontrolling.core.data.network.response.ticketing

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class Ticketing(
    @field:SerializedName("id")
    val id: String? = null,

    @field:SerializedName("full_name")
    val fullName: String? = null,

    @field:SerializedName("email")
    val email: String? = null,

    @field:SerializedName("uri")
    val uri: String? = null,

    @field:SerializedName("file_name")
    val fileName: String? = null,

    @field:SerializedName("phone_number")
    val phoneNumber: String? = null,

    @field:SerializedName("title")
    val title: String? = null,

    @field:SerializedName("description")
    val description: String? = null,

    @field:SerializedName("status")
    val status: String? = null,

    @field:SerializedName("created_at")
    val date: String? = null
) : Parcelable
