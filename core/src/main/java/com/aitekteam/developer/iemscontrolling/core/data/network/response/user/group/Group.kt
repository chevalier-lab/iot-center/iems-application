package com.aitekteam.developer.iemscontrolling.core.data.network.response.user.group

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class Group(

    @field:SerializedName("id")
    val id: String? = null,

    @field:SerializedName("group_label")
    val label: String? = null,

    @field:SerializedName("phone_number")
    val number: String? = null,

    @field:SerializedName("total_device")
    val total: Int? = null
) : Parcelable
