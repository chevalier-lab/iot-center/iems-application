package com.aitekteam.developer.iemscontrolling.core.data.network.source

import com.aitekteam.developer.iemscontrolling.core.data.network.response.ticketing.CreateTicketing
import com.aitekteam.developer.iemscontrolling.core.data.network.response.ticketing.ReplyTicket
import com.aitekteam.developer.iemscontrolling.core.data.network.response.ticketing.Ticketing
import com.aitekteam.developer.iemscontrolling.core.data.network.service.TicketingService
import com.aitekteam.developer.iemscontrolling.core.utils.vo.ApiResponse
import com.google.gson.JsonObject
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import timber.log.Timber

class RemoteTicketingDataSource(
    private val ticketingService: TicketingService
) {

    fun createComplain(
        authorization: String,
        jsonObject: JsonObject
    ): Flow<ApiResponse<CreateTicketing>> = flow {
        try {
            val response = ticketingService.createComplain(authorization, jsonObject)
            val data = response.data

            data?.let {
                when (response.code) {
                    200 -> emit(ApiResponse.Success(it))
                    401 -> emit(ApiResponse.Error(response.message!!))
                    else -> emit(ApiResponse.Empty)
                }
            }
        } catch (e: Exception) {
            emit(ApiResponse.Error(e.toString()))
            Timber.e("onFailure : $e")
        }
    }.flowOn(Dispatchers.IO)

    fun getListComplain(authorization: String): Flow<ApiResponse<List<Ticketing>>> = flow {
        try {
            val response = ticketingService.getListComplain(authorization)
            val data = response.data

            data?.let {
                when (response.code) {
                    200 -> emit(ApiResponse.Success(it))
                    401 -> emit(ApiResponse.Error(response.message!!))
                    else -> emit(ApiResponse.Empty)
                }
            }
        } catch (e: Exception) {
            emit(ApiResponse.Error(e.toString()))
            Timber.e("onFailure : $e")
        }
    }.flowOn(Dispatchers.IO)

    fun replyComplaint(
        authorization: String,
        ticketId: Int,
        jsonObject: JsonObject
    ): Flow<ApiResponse<ReplyTicket>> = flow {
        try {
            val response = ticketingService.replyComplaint(authorization, ticketId, jsonObject)
            val data = response.data

            data?.let {
                when (response.code) {
                    200 -> emit(ApiResponse.Success(it))
                    else -> emit(ApiResponse.Error(response.message!!))
                }
            }
        } catch (e: Exception) {
            emit(ApiResponse.Error(e.toString()))
            Timber.e("onFailure : $e")
        }
    }.flowOn(Dispatchers.IO)

    fun getListReply(
        authorization: String,
        ticketId: Int
    ): Flow<ApiResponse<List<ReplyTicket>>> = flow {
        try {
            val response = ticketingService.getListReply(authorization, ticketId)
            val data = response.data

            data?.let {
                when (response.code) {
                    200 -> emit(ApiResponse.Success(it))
                    else -> emit(ApiResponse.Error(response.message!!))
                }
            }
        } catch (e: Exception) {
            emit(ApiResponse.Error(e.toString()))
            Timber.e("onFailure : $e")
        }
    }.flowOn(Dispatchers.IO)
}