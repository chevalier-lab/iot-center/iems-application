package com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.sensor

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class SingleSensorResponse(
    @field:SerializedName("code")
    val code: Int? = null,

    @field:SerializedName("message")
    val message: String? = null,

    @field:SerializedName("data")
    val data: Sensor? = Sensor()
) : Parcelable
