package com.aitekteam.developer.iemscontrolling.core.data.network.response.operator

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class ActivationDevice(
    @field:SerializedName("phone_number")
    val phoneNumber: String? = null,

    @field:SerializedName("device_name")
    val deviceName: String? = null,

    @field:SerializedName("device_token")
    val deviceToken: String? = null
) : Parcelable
