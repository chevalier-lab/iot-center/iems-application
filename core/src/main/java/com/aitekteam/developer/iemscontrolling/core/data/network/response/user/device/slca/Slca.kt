package com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.slca

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class Slca(
    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("id_m_devices")
    val deviceId: Int? = null,

    @field:SerializedName("iac")
    val iac: Double? = null,

    @field:SerializedName("power")
    val power: Double? = null,

    @field:SerializedName("date")
    val date: String? = null,

    @field:SerializedName("time")
    val time: String? = null,

    @field:SerializedName("status")
    val status: SlcaStatus? = SlcaStatus()
) : Parcelable
