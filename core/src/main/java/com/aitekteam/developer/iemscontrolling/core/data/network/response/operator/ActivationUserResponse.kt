package com.aitekteam.developer.iemscontrolling.core.data.network.response.operator

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class ActivationUserResponse(
    @field:SerializedName("code")
    val code: Int? = null,

    @field:SerializedName("message")
    val message: String? = null,

    @field:SerializedName("data")
    val data: ActivationUser? = ActivationUser()
) : Parcelable
