package com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.calculate

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class TotalKwhResponse(
    @field:SerializedName("code")
    val code: Int? = null,

    @field:SerializedName("message")
    val message: String? = null,

    @field:SerializedName("data")
    val data: TotalKwh? = TotalKwh()
) : Parcelable
