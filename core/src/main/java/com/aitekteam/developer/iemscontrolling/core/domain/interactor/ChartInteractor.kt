package com.aitekteam.developer.iemscontrolling.core.domain.interactor

import com.aitekteam.developer.iemscontrolling.core.data.Resource
import com.aitekteam.developer.iemscontrolling.core.data.network.response.chart.kwh1.ChartKwh1
import com.aitekteam.developer.iemscontrolling.core.data.network.response.chart.kwh3.ChartKwh3
import com.aitekteam.developer.iemscontrolling.core.data.repository.ChartRepository
import com.aitekteam.developer.iemscontrolling.core.domain.usecase.ChartUseCase
import kotlinx.coroutines.flow.Flow

class ChartInteractor(
    private val chartRepository: ChartRepository
) : ChartUseCase {

    override fun getChartKwh1Day(
        deviceToken: String,
        param: String,
        date: String,
        time: String
    ): Flow<Resource<List<ChartKwh1>>> = chartRepository.getChartKwh1Day(deviceToken, param, date, time)

    override fun getChartKwh1Month(
        deviceToken: String,
        param: String,
        date: String
    ): Flow<Resource<List<ChartKwh1>>> = chartRepository.getChartKwh1Month(deviceToken, param, date)

    override fun getChartKwh1Year(
        deviceToken: String,
        param: String
    ): Flow<Resource<List<ChartKwh1>>> = chartRepository.getChartKwh1Year(deviceToken, param)

    override fun getChartKwh1Realtime(
        deviceToken: String,
        param: String
    ): Flow<Resource<List<ChartKwh1>>> = chartRepository.getChartKwh1Realtime(deviceToken, param)

    override fun getChartKwh3Day(
        deviceToken: String,
        param: String,
        date: String,
        time: String
    ): Flow<Resource<List<ChartKwh3>>> = chartRepository.getChartKwh3Day(deviceToken, param, date, time)

    override fun getChartKwh3Month(
        deviceToken: String,
        param: String,
        date: String
    ): Flow<Resource<List<ChartKwh3>>> = chartRepository.getChartKwh3Month(deviceToken, param, date)

    override fun getChartKwh3Year(
        deviceToken: String,
        param: String
    ): Flow<Resource<List<ChartKwh3>>> = chartRepository.getChartKwh3Year(deviceToken, param)

    override fun getChartKwh3Realtime(
        deviceToken: String,
        param: String
    ): Flow<Resource<List<ChartKwh3>>> = chartRepository.getChartKwh3Realtime(deviceToken, param)
}