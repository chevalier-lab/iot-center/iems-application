package com.aitekteam.developer.iemscontrolling.core.utils

object Constants {
    // base url
    const val BASE_URL = "http://213.190.4.40/iems/iems-api/index.php/"

    // auth routes
    const val ROUTE_LOGIN = "public/auth/signIn"
    const val ROUTE_REGISTER = "public/auth/signUp"

    // user routes
    const val ROUTE_GET_PROFILE = "services/profile"
    const val ROUTE_UPDATE_PROFILE = "services/profile/update"
    const val ROUTE_UPDATE_PHOTO_PROFILE = "services/profile/photo"
    const val ROUTE_USER_INFO = "services/user/info"
    const val ROUTE_CHANGE_USER_INFO = "services/user/info/changeInfo"

    // master routes
    const val ROUTE_CREATE_MEDIA = "services/master/medias/create"

    // ticketing routes
    const val ROUTE_CREATE_COMPLAIN = "services/ticketing/create"
    const val ROUTE_GET_LIST_COMPLAIN = "services/ticketing/my"
    const val ROUTE_REPLY_COMPLAIN = "services/ticketing/reply/{ticket_id}"
    const val ROUTE_GET_LIST_REPLY = "services/ticketing/detail/{ticket_id}"

    // manage devices
    const val ROUTE_LIST_DEVICE = "services/user/manage/devices"
    const val ROUTE_UPDATE_DEVICE = "services/user/manage/devices/update/{id}"
    const val ROUTE_LAST_DEVICE = "public/devices/last"
    const val ROUTE_PULL_DEVICE = "public/devices/pull"
    const val ROUTE_PUSH_DEVICE = "public/devices/push"
    const val ROUTE_INFO_DEVICE = "public/devices/info"
    const val ROUTE_PREVIEW_DEVICE = "public/devices/preview"
    const val ROUTE_CHANGE_DEVICE_SSID = "services/user/manage/devices/updateSSID/{device_id}"

    // device notification
    const val ROUTE_DEVICE_USER_NOTIFICATION = "services/user/notification"
    const val ROUTE_DEVICE_GUEST_NOTIFICATION = "services/guest/notification"

    // chart
    const val ROUTE_CHART_KWH1_DAY = "public/data-devices/KWH1/day"
    const val ROUTE_CHART_KWH1_MONTH = "public/data-devices/KWH1/month"
    const val ROUTE_CHART_KWH1_YEAR = "public/data-devices/KWH1/year"
    const val ROUTE_CHART_KWH3_DAY = "public/data-devices/KWH3/day"
    const val ROUTE_CHART_KWH3_MONTH = "public/data-devices/KWH3/month"
    const val ROUTE_CHART_KWH3_YEAR = "public/data-devices/KWH3/year"

    // operator (customer)
    const val ROUTE_LIST_CUSTOMER = "services/operator/manage/customer"
    const val ROUTE_LIST_ACTIVE_CUSTOMER = "services/operator/manage/customer/my"
    const val ROUTE_ACTIVATE_CUSTOMER = "services/operator/manage/customer/activate"
    const val ROUTE_ACTIVATE_DEVICE = "services/operator/manage/devices/activate"

    // guest
    const val ROUTE_SUBS_GUEST = "services/guest/subscribe"
    const val ROUTE_LIST_GUEST = "services/user/manage/guest"
    const val ROUTE_INVITE_GUEST = "services/user/manage/guest/invited"
    const val ROUTE_DETAIL_GUEST = "services/user/manage/guest/detail"
    const val ROUTE_LIST_DEVICE_GUEST = "services/user/manage/deviceGuest"
    const val ROUTE_ADD_DEVICE_GUEST = "services/user/manage/deviceGuest/invited"
    const val ROUTE_GET_LIST_DEVICE_GUEST = "services/guest/manage/devices"

    // building (kwh)
    const val ROUTE_TOTAL_KWH = "public/data-devices/calculate/getTotal"
    const val ROUTE_LIST_KWH = "public/data-devices/calculate/listKWH"
    const val ROUTE_TOTAL_KWH_ON_GROUP = "public/data-devices/calculate/getTotalOnGroup/{group_id}"
    const val ROUTE_LIST_KWH_ON_GROUP = "public/data-devices/calculate/listKWHGroup/{group_id}"
    const val ROUTE_CALCULATE_KWH = "public/data-devices/calculate/getTotalOnce/{device_token}"

    // building (group)
    const val ROUTE_LIST_GROUP = "services/user/group"
    const val ROUTE_CREATE_GROUP = "services/user/group/create"
    const val ROUTE_UPDATE_GROUP = "services/user/group/update/{group_id}"
    const val ROUTE_DETAIL_GROUP = "services/user/group/detail/{group_id}"
    const val ROUTE_FILTER_DEVICE_GROUP = "services/user/group/filterPreviewDevice/{group_id}"
    const val ROUTE_APPEND_DEVICE_GROUP = "services/user/group/appendDevice/{group_id}/{device_id}"
    const val ROUTE_REMOVE_DEVICE_GROUP = "services/user/group/popDevice/{group_id}/{device_id}"

    // scheduling (slca)
    const val ROUTE_LIST_SCHEDULER = "services/schedule/SLCA/listsSchedule/{device_id}"
    const val ROUTE_CREATE_SCHEDULER = "services/schedule/SLCA/addSchedule/{device_id}"
    const val ROUTE_DELETE_SCHEDULER = "services/schedule/SLCA/removeSchedule/{scheduler_id}"
}