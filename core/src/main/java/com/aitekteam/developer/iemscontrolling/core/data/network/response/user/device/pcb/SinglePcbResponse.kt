package com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.pcb

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class SinglePcbResponse(
    @field:SerializedName("code")
    val code: Int? = null,

    @field:SerializedName("message")
    val message: String? = null,

    @field:SerializedName("data")
    val data: Pcb? = Pcb()
) : Parcelable
