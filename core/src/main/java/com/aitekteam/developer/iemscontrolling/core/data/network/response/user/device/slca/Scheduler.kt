package com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.slca

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class Scheduler(
    @field:SerializedName("id")
    var id: String? = null,

    @field:SerializedName("id_m_devices")
    var device_id: String? = null,

    @field:SerializedName("action")
    var action: String? = null,

    @field:SerializedName("action_time")
    var action_time: String? = null
) : Parcelable
