package com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.general

import android.os.Parcelable
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.general.Device
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class DeviceResponse(
    @field:SerializedName("code")
    val code: Int? = null,

    @field:SerializedName("message")
    val message: String? = null,

    @field:SerializedName("data")
    val data: List<Device>? = listOf()
) : Parcelable
