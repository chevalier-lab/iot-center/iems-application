package com.aitekteam.developer.iemscontrolling.core.data.network.source

import com.aitekteam.developer.iemscontrolling.core.data.network.response.chart.kwh1.ChartKwh1
import com.aitekteam.developer.iemscontrolling.core.data.network.response.chart.kwh3.ChartKwh3
import com.aitekteam.developer.iemscontrolling.core.data.network.service.ChartService
import com.aitekteam.developer.iemscontrolling.core.utils.vo.ApiResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import timber.log.Timber

class RemoteChartDataSource(
    private val chartService: ChartService
) {

    fun getChartKwh1Day(
        deviceToken: String,
        param: String,
        date: String,
        time: String
    ): Flow<ApiResponse<List<ChartKwh1>>> = flow {
        try {
            val response = chartService.getChartKwh1Day(deviceToken, param, date, time)
            val data = response.data

            data?.let {
                if (response.code == 200)
                    emit(ApiResponse.Success(it))
                else
                    emit(ApiResponse.Error(response.message!!))
            }
        } catch (e: Exception) {
            emit(ApiResponse.Error(e.toString()))
            Timber.e("onFailure : $e")
        }
    }.flowOn(Dispatchers.IO)

    fun getChartKwh1Month(
        deviceToken: String,
        param: String,
        date: String
    ): Flow<ApiResponse<List<ChartKwh1>>> = flow {
        try {
            val response = chartService.getChartKwh1Month(deviceToken, param, date)
            val data = response.data

            data?.let {
                if (response.code == 200)
                    emit(ApiResponse.Success(it))
                else
                    emit(ApiResponse.Error(response.message!!))
            }
        } catch (e: Exception) {
            emit(ApiResponse.Error(e.toString()))
            Timber.e("onFailure : $e")
        }
    }.flowOn(Dispatchers.IO)

    fun getChartKwh1Year(
        deviceToken: String,
        param: String
    ): Flow<ApiResponse<List<ChartKwh1>>> = flow {
        try {
            val response = chartService.getChartKwh1Year(deviceToken, param)
            val data = response.data

            data?.let {
                if (response.code == 200)
                    emit(ApiResponse.Success(it))
                else
                    emit(ApiResponse.Error(response.message!!))
            }
        } catch (e: Exception) {
            emit(ApiResponse.Error(e.toString()))
            Timber.e("onFailure : $e")
        }
    }.flowOn(Dispatchers.IO)

    fun getChartKwh1Realtime(
        deviceToken: String,
        param: String
    ): Flow<ApiResponse<List<ChartKwh1>>> = flow {
        try {
            val response = chartService.getChartKwh1Realtime(deviceToken, param)
            val data = response.data

            data?.let {
                if (response.code == 200)
                    emit(ApiResponse.Success(it))
                else
                    emit(ApiResponse.Error(response.message!!))
            }
        } catch (e: Exception) {
            emit(ApiResponse.Error(e.toString()))
            Timber.e("onFailure : $e")
        }
    }.flowOn(Dispatchers.IO)

    fun getChartKwh3Day(
        deviceToken: String,
        param: String,
        date: String,
        time: String
    ): Flow<ApiResponse<List<ChartKwh3>>> = flow {
        try {
            val response = chartService.getChartKwh3Day(deviceToken, param, date, time)
            val data = response.data

            data?.let {
                if (response.code == 200)
                    emit(ApiResponse.Success(it))
                else
                    emit(ApiResponse.Error(response.message!!))
            }
        } catch (e: Exception) {
            emit(ApiResponse.Error(e.toString()))
            Timber.e("onFailure : $e")
        }
    }.flowOn(Dispatchers.IO)

    fun getChartKwh3Month(
        deviceToken: String,
        param: String,
        date: String
    ): Flow<ApiResponse<List<ChartKwh3>>> = flow {
        try {
            val response = chartService.getChartKwh3Month(deviceToken, param, date)
            val data = response.data

            data?.let {
                if (response.code == 200)
                    emit(ApiResponse.Success(it))
                else
                    emit(ApiResponse.Error(response.message!!))
            }
        } catch (e: Exception) {
            emit(ApiResponse.Error(e.toString()))
            Timber.e("onFailure : $e")
        }
    }.flowOn(Dispatchers.IO)

    fun getChartKwh3Year(
        deviceToken: String,
        param: String
    ): Flow<ApiResponse<List<ChartKwh3>>> = flow {
        try {
            val response = chartService.getChartKwh3Year(deviceToken, param)
            val data = response.data

            data?.let {
                if (response.code == 200)
                    emit(ApiResponse.Success(it))
                else
                    emit(ApiResponse.Error(response.message!!))
            }
        } catch (e: Exception) {
            emit(ApiResponse.Error(e.toString()))
            Timber.e("onFailure : $e")
        }
    }.flowOn(Dispatchers.IO)

    fun getChartKwh3Realtime(
        deviceToken: String,
        param: String
    ): Flow<ApiResponse<List<ChartKwh3>>> = flow {
        try {
            val response = chartService.getChartKwh3Realtime(deviceToken, param)
            val data = response.data

            data?.let {
                if (response.code == 200)
                    emit(ApiResponse.Success(it))
                else
                    emit(ApiResponse.Error(response.message!!))
            }
        } catch (e: Exception) {
            emit(ApiResponse.Error(e.toString()))
            Timber.e("onFailure : $e")
        }
    }.flowOn(Dispatchers.IO)
}