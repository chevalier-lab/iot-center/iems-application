package com.aitekteam.developer.iemscontrolling.core.utils.adapter

import androidx.recyclerview.widget.RecyclerView

interface ReusableAdapterInterface<T> {

    // set layout
    fun setLayout(layout: Int): ReusableAdapter<T>

    // filterable
    fun filterable(): ReusableAdapter<T>

    // append data
    fun addData(items: List<T>): ReusableAdapter<T>

    // clear data
    fun clearData(): ReusableAdapter<T>
    fun deleteData(item: T): ReusableAdapter<T>
    fun addMoreData(items: List<T>): ReusableAdapter<T>

    // realtime change
    fun updateData(item: T): ReusableAdapter<T>

    // adapter callback
    fun adapterCallback(adapterCallback: AdapterCallback<T>): ReusableAdapter<T>

    // layout orientation
    fun isVerticalView(): ReusableAdapter<T>
    fun isHorizontalView(): ReusableAdapter<T>
    fun isGridView(spanCount: Int): ReusableAdapter<T>

    // build view
    fun build(recyclerView: RecyclerView): ReusableAdapter<T>

}