package com.aitekteam.developer.iemscontrolling.core.utils.helper

import com.aitekteam.developer.iemscontrolling.core.utils.Constants.BASE_URL
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class NetworkHelper<S>(
    okHttpClient: OkHttpClient,
    serviceClass: Class<S>
) {

    private val retrofit by lazy {
        val gson = GsonBuilder()
            .setLenient()
            .create()

        Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .client(okHttpClient)
            .build()
    }

    val createService : S by lazy { retrofit.create(serviceClass) }

}