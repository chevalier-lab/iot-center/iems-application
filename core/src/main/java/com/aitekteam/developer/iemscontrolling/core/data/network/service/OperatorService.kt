package com.aitekteam.developer.iemscontrolling.core.data.network.service

import com.aitekteam.developer.iemscontrolling.core.data.network.response.operator.ActivationDeviceResponse
import com.aitekteam.developer.iemscontrolling.core.data.network.response.operator.ActivationUserResponse
import com.aitekteam.developer.iemscontrolling.core.data.network.response.operator.ListUserResponse
import com.aitekteam.developer.iemscontrolling.core.utils.Constants.ROUTE_ACTIVATE_CUSTOMER
import com.aitekteam.developer.iemscontrolling.core.utils.Constants.ROUTE_ACTIVATE_DEVICE
import com.aitekteam.developer.iemscontrolling.core.utils.Constants.ROUTE_LIST_ACTIVE_CUSTOMER
import com.aitekteam.developer.iemscontrolling.core.utils.Constants.ROUTE_LIST_CUSTOMER
import com.google.gson.JsonObject
import retrofit2.Response
import retrofit2.http.*

interface OperatorService {

    @GET(ROUTE_LIST_CUSTOMER)
    suspend fun getListCustomer(
        @Header("Authorization") token: String
    ): ListUserResponse

    @GET(ROUTE_LIST_CUSTOMER)
    suspend fun getListCustomerPaging(
        @Header("Authorization") token: String,
        @Query("page") page: Int
    ): Response<ListUserResponse>

    @GET(ROUTE_LIST_ACTIVE_CUSTOMER)
    suspend fun getListActiveCustomer(
        @Header("Authorization") token: String
    ): ListUserResponse

    @GET(ROUTE_LIST_ACTIVE_CUSTOMER)
    suspend fun getListActiveCustomerPaging(
        @Header("Authorization") token: String,
        @Query("page") page: Int
    ): Response<ListUserResponse>

    @POST(ROUTE_ACTIVATE_CUSTOMER)
    suspend fun activateCustomer(
        @Header("Authorization") token: String,
        @Body jsonObject: JsonObject
    ): ActivationUserResponse

    @POST(ROUTE_ACTIVATE_DEVICE)
    suspend fun activateDevice(
        @Header("Authorization") token: String,
        @Body jsonObject: JsonObject
    ): ActivationDeviceResponse
}