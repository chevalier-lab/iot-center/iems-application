package com.aitekteam.developer.iemscontrolling.core.data.network.service

import com.aitekteam.developer.iemscontrolling.core.data.network.response.chart.kwh1.ChartKwh1Response
import com.aitekteam.developer.iemscontrolling.core.data.network.response.chart.kwh3.ChartKwh3Response
import com.aitekteam.developer.iemscontrolling.core.utils.Constants.ROUTE_CHART_KWH1_DAY
import com.aitekteam.developer.iemscontrolling.core.utils.Constants.ROUTE_CHART_KWH1_MONTH
import com.aitekteam.developer.iemscontrolling.core.utils.Constants.ROUTE_CHART_KWH1_YEAR
import com.aitekteam.developer.iemscontrolling.core.utils.Constants.ROUTE_CHART_KWH3_DAY
import com.aitekteam.developer.iemscontrolling.core.utils.Constants.ROUTE_CHART_KWH3_MONTH
import com.aitekteam.developer.iemscontrolling.core.utils.Constants.ROUTE_CHART_KWH3_YEAR
import retrofit2.http.GET
import retrofit2.http.Query

interface ChartService {

    @GET(ROUTE_CHART_KWH1_DAY)
    suspend fun getChartKwh1Day(
        @Query("device_token") deviceToken: String,
        @Query("param") param: String,
        @Query("date") date: String,
        @Query("time") time: String
    ) : ChartKwh1Response

    @GET(ROUTE_CHART_KWH1_MONTH)
    suspend fun getChartKwh1Month(
        @Query("device_token") deviceToken: String,
        @Query("param") param: String,
        @Query("date") date: String
    ) : ChartKwh1Response

    @GET(ROUTE_CHART_KWH1_YEAR)
    suspend fun getChartKwh1Year(
        @Query("device_token") deviceToken: String,
        @Query("param") param: String
    ) : ChartKwh1Response

    @GET(ROUTE_CHART_KWH1_DAY)
    suspend fun getChartKwh1Realtime(
        @Query("device_token") deviceToken: String,
        @Query("param") param: String
    ) : ChartKwh1Response

    @GET(ROUTE_CHART_KWH3_DAY)
    suspend fun getChartKwh3Day(
        @Query("device_token") deviceToken: String,
        @Query("param") param: String,
        @Query("date") date: String,
        @Query("time") time: String
    ) : ChartKwh3Response

    @GET(ROUTE_CHART_KWH3_MONTH)
    suspend fun getChartKwh3Month(
        @Query("device_token") deviceToken: String,
        @Query("param") param: String,
        @Query("date") date: String
    ) : ChartKwh3Response

    @GET(ROUTE_CHART_KWH3_YEAR)
    suspend fun getChartKwh3Year(
        @Query("device_token") deviceToken: String,
        @Query("param") param: String
    ) : ChartKwh3Response

    @GET(ROUTE_CHART_KWH3_DAY)
    suspend fun getChartKwh3Realtime(
        @Query("device_token") deviceToken: String,
        @Query("param") param: String
    ) : ChartKwh3Response
}