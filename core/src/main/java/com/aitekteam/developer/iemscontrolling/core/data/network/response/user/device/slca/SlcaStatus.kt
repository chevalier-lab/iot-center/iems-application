package com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.slca

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class SlcaStatus(
    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("mode")
    val mode: String? = null
) : Parcelable
