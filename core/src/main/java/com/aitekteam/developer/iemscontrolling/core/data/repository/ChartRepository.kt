package com.aitekteam.developer.iemscontrolling.core.data.repository

import com.aitekteam.developer.iemscontrolling.core.data.Resource
import com.aitekteam.developer.iemscontrolling.core.data.network.response.chart.kwh1.ChartKwh1
import com.aitekteam.developer.iemscontrolling.core.data.network.response.chart.kwh3.ChartKwh3
import com.aitekteam.developer.iemscontrolling.core.data.network.source.RemoteChartDataSource
import com.aitekteam.developer.iemscontrolling.core.domain.repository.IChartRepository
import com.aitekteam.developer.iemscontrolling.core.utils.vo.ApiResponse
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flow

class ChartRepository(
    private val remoteChartDataSource: RemoteChartDataSource
) : IChartRepository {

    override fun getChartKwh1Day(
        deviceToken: String,
        param: String,
        date: String,
        time: String
    ): Flow<Resource<List<ChartKwh1>>> = flow {
        emit(Resource.loading<List<ChartKwh1>>())
        val response = remoteChartDataSource.getChartKwh1Day(deviceToken, param, date, time)

        when (val apiResponse = response.first()) {
            is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
            is ApiResponse.Empty -> emit(Resource.success(listOf<ChartKwh1>()))
            is ApiResponse.Error -> emit(Resource.error<List<ChartKwh1>>(apiResponse.errorMessage))
        }
    }

    override fun getChartKwh1Month(
        deviceToken: String,
        param: String,
        date: String
    ): Flow<Resource<List<ChartKwh1>>> = flow {
        emit(Resource.loading<List<ChartKwh1>>())
        val response = remoteChartDataSource.getChartKwh1Month(deviceToken, param, date)

        when (val apiResponse = response.first()) {
            is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
            is ApiResponse.Empty -> emit(Resource.success(listOf<ChartKwh1>()))
            is ApiResponse.Error -> emit(Resource.error<List<ChartKwh1>>(apiResponse.errorMessage))
        }
    }

    override fun getChartKwh1Year(
        deviceToken: String,
        param: String
    ): Flow<Resource<List<ChartKwh1>>> = flow {
        emit(Resource.loading<List<ChartKwh1>>())
        val response = remoteChartDataSource.getChartKwh1Year(deviceToken, param)

        when (val apiResponse = response.first()) {
            is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
            is ApiResponse.Empty -> emit(Resource.success(listOf<ChartKwh1>()))
            is ApiResponse.Error -> emit(Resource.error<List<ChartKwh1>>(apiResponse.errorMessage))
        }
    }

    override fun getChartKwh1Realtime(
        deviceToken: String,
        param: String
    ): Flow<Resource<List<ChartKwh1>>> = flow {
        emit(Resource.loading<List<ChartKwh1>>())
        val response = remoteChartDataSource.getChartKwh1Realtime(deviceToken, param)

        when (val apiResponse = response.first()) {
            is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
            is ApiResponse.Empty -> emit(Resource.success(listOf<ChartKwh1>()))
            is ApiResponse.Error -> emit(Resource.error<List<ChartKwh1>>(apiResponse.errorMessage))
        }
    }

    override fun getChartKwh3Day(
        deviceToken: String,
        param: String,
        date: String,
        time: String
    ): Flow<Resource<List<ChartKwh3>>> = flow {
        emit(Resource.loading<List<ChartKwh3>>())
        val response = remoteChartDataSource.getChartKwh3Day(deviceToken, param, date, time)

        when (val apiResponse = response.first()) {
            is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
            is ApiResponse.Empty -> emit(Resource.success(listOf<ChartKwh3>()))
            is ApiResponse.Error -> emit(Resource.error<List<ChartKwh3>>(apiResponse.errorMessage))
        }
    }

    override fun getChartKwh3Month(
        deviceToken: String,
        param: String,
        date: String
    ): Flow<Resource<List<ChartKwh3>>> = flow {
        emit(Resource.loading<List<ChartKwh3>>())
        val response = remoteChartDataSource.getChartKwh3Month(deviceToken, param, date)

        when (val apiResponse = response.first()) {
            is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
            is ApiResponse.Empty -> emit(Resource.success(listOf<ChartKwh3>()))
            is ApiResponse.Error -> emit(Resource.error<List<ChartKwh3>>(apiResponse.errorMessage))
        }
    }

    override fun getChartKwh3Year(
        deviceToken: String,
        param: String
    ): Flow<Resource<List<ChartKwh3>>> = flow {
        emit(Resource.loading<List<ChartKwh3>>())
        val response = remoteChartDataSource.getChartKwh3Year(deviceToken, param)

        when (val apiResponse = response.first()) {
            is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
            is ApiResponse.Empty -> emit(Resource.success(listOf<ChartKwh3>()))
            is ApiResponse.Error -> emit(Resource.error<List<ChartKwh3>>(apiResponse.errorMessage))
        }
    }

    override fun getChartKwh3Realtime(
        deviceToken: String,
        param: String
    ): Flow<Resource<List<ChartKwh3>>> = flow {
        emit(Resource.loading<List<ChartKwh3>>())
        val response = remoteChartDataSource.getChartKwh3Realtime(deviceToken, param)

        when (val apiResponse = response.first()) {
            is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
            is ApiResponse.Empty -> emit(Resource.success(listOf<ChartKwh3>()))
            is ApiResponse.Error -> emit(Resource.error<List<ChartKwh3>>(apiResponse.errorMessage))
        }
    }
}