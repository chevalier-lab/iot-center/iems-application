package com.aitekteam.developer.iemscontrolling.core.domain.interactor

import com.aitekteam.developer.iemscontrolling.core.data.Resource
import com.aitekteam.developer.iemscontrolling.core.data.network.response.auth.Login
import com.aitekteam.developer.iemscontrolling.core.data.network.response.auth.Register
import com.aitekteam.developer.iemscontrolling.core.data.repository.AuthRepository
import com.aitekteam.developer.iemscontrolling.core.domain.usecase.AuthUseCase
import com.google.gson.JsonObject
import kotlinx.coroutines.flow.Flow

class AuthInteractor(
    private val authRepository: AuthRepository
) : AuthUseCase {

    override fun login(jsonObject: JsonObject): Flow<Resource<Login>> =
        authRepository.login(jsonObject)

    override fun register(jsonObject: JsonObject): Flow<Resource<Register>> =
        authRepository.register(jsonObject)
}