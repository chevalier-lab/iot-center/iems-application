package com.aitekteam.developer.iemscontrolling.core.domain.usecase

import com.aitekteam.developer.iemscontrolling.core.data.Resource
import com.aitekteam.developer.iemscontrolling.core.data.network.response.operator.ActivationDevice
import com.aitekteam.developer.iemscontrolling.core.data.network.response.operator.ActivationUser
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.profile.User
import com.google.gson.JsonObject
import kotlinx.coroutines.flow.Flow

interface OperatorUseCase {

    fun getListCustomer(token: String): Flow<Resource<List<User>>>

    fun getListActiveCustomer(token: String): Flow<Resource<List<User>>>

    fun activateCustomer(token: String, jsonObject: JsonObject): Flow<Resource<ActivationUser>>

    fun activateDevice(token: String, jsonObject: JsonObject): Flow<Resource<ActivationDevice>>
}