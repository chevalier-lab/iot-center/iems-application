package com.aitekteam.developer.iemscontrolling.core.domain.usecase

import com.aitekteam.developer.iemscontrolling.core.data.Resource
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.calculate.TotalKwh
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.general.Device
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.kwh1.Kwh1
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.kwh3.Kwh3
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.notification.DeviceNotification
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.pcb.Pcb
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.sensor.Sensor
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.slca.Scheduler
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.slca.SingleSchedulerResponse
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.slca.Slca
import com.google.gson.JsonObject
import kotlinx.coroutines.flow.Flow

interface DeviceUseCase {

    fun getListDevice(token: String): Flow<Resource<List<Device>>>

    fun getListDeviceGuest(token: String): Flow<Resource<List<Device>>>

    fun updateDevice(token: String, id: Int, jsonObject: JsonObject): Flow<Resource<Device>>

    fun getLastPcb(token: String): Flow<Resource<Pcb>>

    fun getHistoryPcb(token: String): Flow<Resource<List<Pcb>>>

    fun getLastSensor(token: String): Flow<Resource<Sensor>>

    fun getHistorySensor(token: String): Flow<Resource<List<Sensor>>>

    fun getLastSlca(token: String): Flow<Resource<Slca>>

    fun getHistorySlca(token: String): Flow<Resource<List<Slca>>>

    fun getLastKwh1(token: String): Flow<Resource<Kwh1>>

    fun getLastKwh3(token: String): Flow<Resource<Kwh3>>

    fun getDeviceInfo(token: String): Flow<Resource<Device>>

    fun previewDevice(token: String): Flow<Resource<Device>>

    fun pushDevice(token: String, jsonObject: JsonObject): Flow<Resource<Device>>

    fun changeDeviceSsid(
        token: String,
        deviceId: Int,
        jsonObject: JsonObject
    ): Flow<Resource<Device>>

    fun totalKwh(token: String): Flow<Resource<TotalKwh>>

    fun totalKwhOnGroup(token: String, groupId: Int): Flow<Resource<TotalKwh>>

    fun getListKwh(token: String): Flow<Resource<List<Device>>>

    fun getListKwhOnGroup(token: String, groupId: Int): Flow<Resource<List<Device>>>

    fun calculateKwh(token: String, deviceToken: String): Flow<Resource<TotalKwh>>

    fun getDeviceUserNotification(token: String): Flow<Resource<List<DeviceNotification>>>

    fun getDeviceGuestNotification(token: String): Flow<Resource<List<DeviceNotification>>>

    fun getListScheduler(token: String, deviceId: String): Flow<Resource<List<Scheduler>>>

    fun createScheduler(
        token: String,
        deviceId: String,
        jsonObject: JsonObject
    ): Flow<Resource<Scheduler>>

    fun deleteScheduler(token: String, schedulerId: String): Flow<Resource<SingleSchedulerResponse>>

}