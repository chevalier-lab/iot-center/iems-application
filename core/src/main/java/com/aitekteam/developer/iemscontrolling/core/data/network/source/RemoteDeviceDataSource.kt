package com.aitekteam.developer.iemscontrolling.core.data.network.source

import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.calculate.TotalKwh
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.general.Device
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.kwh1.Kwh1
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.kwh3.Kwh3
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.notification.DeviceNotification
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.pcb.Pcb
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.sensor.Sensor
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.slca.Scheduler
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.slca.SingleSchedulerResponse
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.slca.Slca
import com.aitekteam.developer.iemscontrolling.core.data.network.service.DeviceService
import com.aitekteam.developer.iemscontrolling.core.utils.vo.ApiResponse
import com.google.gson.JsonObject
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import timber.log.Timber

class RemoteDeviceDataSource(
    private val deviceService: DeviceService
) {

    fun getListDevice(token: String): Flow<ApiResponse<List<Device>>> = flow {
        try {
            val response = deviceService.getListDevice(token)
            val data = response.data

            data?.let {
                when (response.code) {
                    200 -> emit(ApiResponse.Success(it))
                    else -> emit(ApiResponse.Error(response.message!!))
                }
            }
        } catch (e: Exception) {
            emit(ApiResponse.Error(e.toString()))
            Timber.e("onFailure : $e")
        }
    }.flowOn(Dispatchers.IO)

    fun getListDeviceGuest(token: String): Flow<ApiResponse<List<Device>>> = flow {
        try {
            val response = deviceService.getListDeviceGuest(token)
            val data = response.data

            data?.let {
                when (response.code) {
                    200 -> emit(ApiResponse.Success(it))
                    else -> emit(ApiResponse.Error(response.message!!))
                }
            }
        } catch (e: Exception) {
            emit(ApiResponse.Error(e.toString()))
            Timber.e("onFailure : $e")
        }
    }.flowOn(Dispatchers.IO)

    fun updateDevice(token: String, id: Int, jsonObject: JsonObject): Flow<ApiResponse<Device>> = flow {
        try {
            val response = deviceService.updateDevice(token, id, jsonObject)
            val data = response.data

            data?.let {
                when (response.code) {
                    200 -> emit(ApiResponse.Success(it))
                    else -> emit(ApiResponse.Error(response.message!!))
                }
            }
        } catch (e: Exception) {
            emit(ApiResponse.Error(e.toString()))
            Timber.e("onFailure : $e")
        }
    }.flowOn(Dispatchers.IO)

    fun getLastPcb(token: String): Flow<ApiResponse<Pcb>> = flow {
        try {
            val response = deviceService.getLastPcb(token)
            val data = response.data

            data?.let {
                when (response.code) {
                    200 -> emit(ApiResponse.Success(it))
                    else -> emit(ApiResponse.Error(response.message!!))
                }
            }
        } catch (e: Exception) {
            emit(ApiResponse.Error(e.toString()))
            Timber.e("onFailure : $e")
        }
    }.flowOn(Dispatchers.IO)

    fun getHistoryPcb(token: String): Flow<ApiResponse<List<Pcb>>> = flow {
        try {
            val response = deviceService.getHistoryPcb(token)
            val data = response.data

            data?.let {
                when (response.code) {
                    200 -> emit(ApiResponse.Success(it))
                    else -> emit(ApiResponse.Error(response.message!!))
                }
            }
        } catch (e: Exception) {
            emit(ApiResponse.Error(e.toString()))
            Timber.e("onFailure : $e")
        }
    }.flowOn(Dispatchers.IO)

    fun getLastSensor(token: String): Flow<ApiResponse<Sensor>> = flow {
        try {
            val response = deviceService.getLastSensor(token)
            val data = response.data

            data?.let {
                when (response.code) {
                    200 -> emit(ApiResponse.Success(it))
                    else -> emit(ApiResponse.Error(response.message!!))
                }
            }
        } catch (e: Exception) {
            emit(ApiResponse.Error(e.toString()))
            Timber.e("onFailure : $e")
        }
    }.flowOn(Dispatchers.IO)

    fun getHistorySensor(token: String): Flow<ApiResponse<List<Sensor>>> = flow {
        try {
            val response = deviceService.getHistorySensor(token)
            val data = response.data

            data?.let {
                when (response.code) {
                    200 -> emit(ApiResponse.Success(it))
                    else -> emit(ApiResponse.Error(response.message!!))
                }
            }
        } catch (e: Exception) {
            emit(ApiResponse.Error(e.toString()))
            Timber.e("onFailure : $e")
        }
    }.flowOn(Dispatchers.IO)

    fun getLastSlca(token: String): Flow<ApiResponse<Slca>> = flow {
        try {
            val response = deviceService.getLastSlca(token)
            val data = response.data

            data?.let {
                when (response.code) {
                    200 -> emit(ApiResponse.Success(it))
                    else -> emit(ApiResponse.Error(response.message!!))
                }
            }
        } catch (e: Exception) {
            emit(ApiResponse.Error(e.toString()))
            Timber.e("onFailure : $e")
        }
    }.flowOn(Dispatchers.IO)

    fun getHistorySlca(token: String): Flow<ApiResponse<List<Slca>>> = flow {
        try {
            val response = deviceService.getHistorySlca(token)
            val data = response.data

            data?.let {
                when (response.code) {
                    200 -> emit(ApiResponse.Success(it))
                    else -> emit(ApiResponse.Error(response.message!!))
                }
            }
        } catch (e: Exception) {
            emit(ApiResponse.Error(e.toString()))
            Timber.e("onFailure : $e")
        }
    }.flowOn(Dispatchers.IO)

    fun getLastKwh1(token: String): Flow<ApiResponse<Kwh1>> = flow {
        try {
            val response = deviceService.getLastKwh1(token)
            val data = response.data

            data?.let {
                when (response.code) {
                    200 -> emit(ApiResponse.Success(it))
                    else -> emit(ApiResponse.Error(response.message!!))
                }
            }
        } catch (e: Exception) {
            emit(ApiResponse.Error(e.toString()))
            Timber.e("onFailure : $e")
        }
    }.flowOn(Dispatchers.IO)

    fun getLastKwh3(token: String): Flow<ApiResponse<Kwh3>> = flow {
        try {
            val response = deviceService.getLastKwh3(token)
            val data = response.data

            data?.let {
                when (response.code) {
                    200 -> emit(ApiResponse.Success(it))
                    else -> emit(ApiResponse.Error(response.message!!))
                }
            } ?: run {
                when (response.code) {
                    200 -> emit(ApiResponse.Success(Kwh3()))
                    else -> emit(ApiResponse.Error(response.message!!))
                }
            }
        } catch (e: Exception) {
            emit(ApiResponse.Error(e.toString()))
            Timber.e("onFailure : $e")
        }
    }.flowOn(Dispatchers.IO)

    fun getDeviceInfo(token: String): Flow<ApiResponse<Device>> = flow {
        try {
            val response = deviceService.getDeviceInfo(token)
            val data = response.data

            data?.let {
                when (response.code) {
                    200 -> emit(ApiResponse.Success(it))
                    else -> emit(ApiResponse.Error(response.message!!))
                }
            }
        } catch (e: Exception) {
            emit(ApiResponse.Error(e.toString()))
            Timber.e("onFailure : $e")
        }
    }.flowOn(Dispatchers.IO)

    fun previewDevice(token: String): Flow<ApiResponse<Device>> = flow {
        try {
            val response = deviceService.previewDevice(token)
            val data = response.data

            data?.let {
                when (response.code) {
                    200 -> emit(ApiResponse.Success(it))
                    else -> emit(ApiResponse.Error(response.message!!))
                }
            }
        } catch (e: Exception) {
            emit(ApiResponse.Error(e.toString()))
            Timber.e("onFailure : $e")
        }
    }.flowOn(Dispatchers.IO)

    fun pushDevice(token: String, jsonObject: JsonObject): Flow<ApiResponse<Device>> = flow {
        try {
         val response = deviceService.pushDevice(token, jsonObject)
         val data = response.data

            data?.let {
                when (response.code) {
                    200 -> emit(ApiResponse.Success(it))
                    else -> emit(ApiResponse.Error(response.message!!))
                }
            }
        } catch (e: Exception) {
            emit(ApiResponse.Error(e.toString()))
            Timber.e("onFailure : $e")
        }
    }.flowOn(Dispatchers.IO)

    fun changeDeviceSsid(token: String, deviceId: Int, jsonObject: JsonObject): Flow<ApiResponse<Device>> = flow {
        try {
            val response = deviceService.changeDeviceSsid(token, deviceId, jsonObject)
            val data = response.data

            data?.let {
                when (response.code) {
                    200 -> emit(ApiResponse.Success(it))
                    else -> emit(ApiResponse.Error(response.message!!))
                }
            }
        } catch (e: Exception) {
            emit(ApiResponse.Error(e.toString()))
            Timber.e("onFailure : $e")
        }
    }.flowOn(Dispatchers.IO)

    fun totalKwh(token: String): Flow<ApiResponse<TotalKwh>> = flow {
        try {
            val response = deviceService.totalKwh(token)
            val data = response.data

            data?.let {
                when (response.code) {
                    200 -> emit(ApiResponse.Success(it))
                    else -> emit(ApiResponse.Error(response.message!!))
                }
            }
        } catch (e: Exception) {
            emit(ApiResponse.Error(e.toString()))
            Timber.e("onFailure : $e")
        }
    }.flowOn(Dispatchers.IO)

    fun totalKwhOnGroup(token: String, groupId: Int): Flow<ApiResponse<TotalKwh>> = flow {
        try {
            val response = deviceService.totalKwhOnGroup(token, groupId)
            val data = response.data

            data?.let {
                when (response.code) {
                    200 -> emit(ApiResponse.Success(it))
                    else -> emit(ApiResponse.Error(response.message!!))
                }
            }
        } catch (e: Exception) {
            emit(ApiResponse.Error(e.toString()))
            Timber.e("onFailure : $e")
        }
    }.flowOn(Dispatchers.IO)

    fun getListKwh(token: String): Flow<ApiResponse<List<Device>>> = flow {
        try {
            val response = deviceService.listKwh(token)
            val data = response.data

            data?.let {
                when (response.code) {
                    200 -> emit(ApiResponse.Success(it))
                    else -> emit(ApiResponse.Error(response.message!!))
                }
            }
        } catch (e: Exception) {
            emit(ApiResponse.Error(e.toString()))
            Timber.e("onFailure : $e")
        }
    }.flowOn(Dispatchers.IO)

    fun getListKwhOnGroup(token: String, groupId: Int): Flow<ApiResponse<List<Device>>> = flow {
        try {
            val response = deviceService.listKwhOnGroup(token, groupId)
            val data = response.data

            data?.let {
                when (response.code) {
                    200 -> emit(ApiResponse.Success(it))
                    else -> emit(ApiResponse.Error(response.message!!))
                }
            }
        } catch (e: Exception) {
            emit(ApiResponse.Error(e.toString()))
            Timber.e("onFailure : $e")
        }
    }.flowOn(Dispatchers.IO)

    fun calculateKwh(token: String, deviceToken: String): Flow<ApiResponse<TotalKwh>> = flow {
        try {
            val response = deviceService.calculateKwh(token, deviceToken)
            val data = response.data

            data?.let {
                when (response.code) {
                    200 -> emit(ApiResponse.Success(it))
                    else -> emit(ApiResponse.Error(response.message!!))
                }
            }
        } catch (e: Exception) {
            emit(ApiResponse.Error(e.toString()))
            Timber.e("onFailure : $e")
        }
    }.flowOn(Dispatchers.IO)

    fun getDeviceUserNotification(token: String): Flow<ApiResponse<List<DeviceNotification>>> = flow {
        try {
            val response = deviceService.getDeviceUserNotification(token)
            val data = response.data

            data?.let {
                when (response.code) {
                    200 -> emit(ApiResponse.Success(it))
                    else -> emit(ApiResponse.Error(response.message!!))
                }
            }
        } catch (e: Exception) {
            emit(ApiResponse.Error(e.toString()))
            Timber.e("onFailure : $e")
        }
    }.flowOn(Dispatchers.IO)

    fun getDeviceGuestNotification(token: String): Flow<ApiResponse<List<DeviceNotification>>> = flow {
        try {
            val response = deviceService.getDeviceGuestNotification(token)
            val data = response.data

            data?.let {
                when (response.code) {
                    200 -> emit(ApiResponse.Success(it))
                    else -> emit(ApiResponse.Error(response.message!!))
                }
            }
        } catch (e: Exception) {
            emit(ApiResponse.Error(e.toString()))
            Timber.e("onFailure : $e")
        }
    }.flowOn(Dispatchers.IO)

    fun getListScheduler(token: String, deviceId: String): Flow<ApiResponse<List<Scheduler>>> = flow {
        try {
            val response = deviceService.getListScheduler(token, deviceId)
            val data = response.data

            data?.let {
                when (response.code) {
                    200 -> emit(ApiResponse.Success(it))
                    else -> emit(ApiResponse.Error(response.message!!))
                }
            }
        } catch (e: Exception) {
            emit(ApiResponse.Error(e.toString()))
            Timber.e("onFailure : $e")
        }
    }.flowOn(Dispatchers.IO)

    fun createScheduler(token: String, deviceId: String, jsonObject: JsonObject): Flow<ApiResponse<Scheduler>> = flow {
        try {
            val response = deviceService.createScheduler(token, deviceId, jsonObject)
            val data = response.data

            data?.let {
                when (response.code) {
                    200 -> emit(ApiResponse.Success(it))
                    else -> emit(ApiResponse.Error(response.message!!))
                }
            }
        } catch (e: Exception) {
            emit(ApiResponse.Error(e.toString()))
            Timber.e("onFailure : $e")
        }
    }.flowOn(Dispatchers.IO)

    fun deleteScheduler(token: String, schedulerId: String): Flow<ApiResponse<SingleSchedulerResponse>> = flow {
        try {
            val response = deviceService.deleteScheduler(token, schedulerId)

            response.let {
                when (response.code) {
                    200 -> emit(ApiResponse.Success(it))
                    else -> emit(ApiResponse.Error(response.message!!))
                }
            }
        } catch (e: Exception) {
            emit(ApiResponse.Error(e.toString()))
            Timber.e("onFailure : $e")
        }
    }.flowOn(Dispatchers.IO)
}