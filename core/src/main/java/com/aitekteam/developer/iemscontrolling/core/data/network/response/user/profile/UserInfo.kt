package com.aitekteam.developer.iemscontrolling.core.data.network.response.user.profile

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class UserInfo(
    @field:SerializedName("phone_number")
    val phoneNumber: String? = null,

    @field:SerializedName("ssid_name")
    val ssid: String? = null,

    @field:SerializedName("ssid_password")
    val ssidPassword: String? = null,

    @field:SerializedName("installed_by")
    val installedBy: String? = null,
) : Parcelable
