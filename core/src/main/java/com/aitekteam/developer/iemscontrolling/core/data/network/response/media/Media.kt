package com.aitekteam.developer.iemscontrolling.core.data.network.response.media

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class Media(
    @field:SerializedName("id")
    val id: String? = null,

    @field:SerializedName("uri")
    val uri: String? = null,

    @field:SerializedName("file_name")
    val fileName: String? = null
) : Parcelable
