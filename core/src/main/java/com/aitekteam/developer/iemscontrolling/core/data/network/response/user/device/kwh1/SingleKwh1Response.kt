package com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.kwh1

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class SingleKwh1Response(
    @field:SerializedName("code")
    val code: Int? = null,

    @field:SerializedName("message")
    val message: String? = null,

    @field:SerializedName("data")
    val data: Kwh1? = Kwh1()
) : Parcelable
