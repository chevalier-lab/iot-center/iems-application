package com.aitekteam.developer.iemscontrolling.core.domain.interactor

import com.aitekteam.developer.iemscontrolling.core.data.Resource
import com.aitekteam.developer.iemscontrolling.core.data.network.response.media.Media
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.general.Device
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.group.Group
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.invite.Invite
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.profile.User
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.profile.UserInfo
import com.aitekteam.developer.iemscontrolling.core.data.repository.UserRepository
import com.aitekteam.developer.iemscontrolling.core.domain.usecase.UserUseCase
import com.google.gson.JsonObject
import kotlinx.coroutines.flow.Flow
import okhttp3.MultipartBody

class UserInteractor(
    private val userRepository: UserRepository
) : UserUseCase {

    override fun getData(authorization: String): Flow<Resource<User>> =
        userRepository.getData(authorization)

    override fun updateProfile(
        authorization: String,
        jsonObject: JsonObject
    ): Flow<Resource<User>> = userRepository.updateProfile(authorization, jsonObject)

    override fun updatePhotoProfile(authorization: String, coverId: String): Flow<Resource<User>> =
        userRepository.updatePhotoProfile(authorization, coverId)

    override fun getUserInfo(token: String): Flow<Resource<UserInfo>> =
        userRepository.getUserInfo(token)

    override fun changeUserInfo(token: String, jsonObject: JsonObject): Flow<Resource<UserInfo>> =
        userRepository.changeUserInfo(token, jsonObject)

    override fun requestActivate(authorization: String, requestType: String): Flow<Resource<User>> =
        userRepository.requestActivate(authorization, requestType)

    override fun inviteGuest(token: String, jsonObject: JsonObject): Flow<Resource<Invite>> =
        userRepository.inviteGuest(token, jsonObject)

    override fun listGuest(token: String): Flow<Resource<List<User>>> =
        userRepository.listGuest(token)

    override fun detailGuest(token: String, phoneNumber: String): Flow<Resource<User>> =
        userRepository.detailGuest(token, phoneNumber)

    override fun getListGroup(token: String): Flow<Resource<List<Group>>> =
        userRepository.getListGroup(token)

    override fun createGroup(token: String, jsonObject: JsonObject): Flow<Resource<Group>> =
        userRepository.createGroup(token, jsonObject)

    override fun updateGroup(
        token: String,
        groupId: Int,
        jsonObject: JsonObject
    ): Flow<Resource<Group>> = userRepository.updateGroup(token, groupId, jsonObject)

    override fun getDetailGroup(token: String, groupId: Int): Flow<Resource<List<Device>>> =
        userRepository.getDetailGroup(token, groupId)

    override fun filterDeviceGroup(
        token: String,
        groupId: Int,
        search: String,
        page: String,
        type: String,
        isAdded: String,
        direction: String
    ): Flow<Resource<List<Device>>> =
        userRepository.filterDeviceGroup(token, groupId, search, page, type, isAdded, direction)

    override fun appendDevice(token: String, groupId: Int, deviceId: Int): Flow<Resource<Group>> =
        userRepository.appendDevice(token, groupId, deviceId)

    override fun removeDevice(token: String, groupId: Int, deviceId: Int): Flow<Resource<Group>> =
        userRepository.removeDevice(token, groupId, deviceId)

    override fun createMedia(token: String, photo: MultipartBody.Part): Flow<Resource<Media>> =
        userRepository.createMedia(token, photo)

    override fun getDeviceGuest(token: String, phoneNumber: String): Flow<Resource<List<Device>>> =
        userRepository.getDeviceGuest(token, phoneNumber)

    override fun addDeviceGuest(token: String, jsonObject: JsonObject): Flow<Resource<Device>> =
        userRepository.addDeviceGuest(token, jsonObject)
}