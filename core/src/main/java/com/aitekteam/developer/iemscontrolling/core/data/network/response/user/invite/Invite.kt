package com.aitekteam.developer.iemscontrolling.core.data.network.response.user.invite

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class Invite(
    @field:SerializedName("phone_number_parent")
    val parent: String? = null,

    @field:SerializedName("phone_number_child")
    val child: String? = null
) : Parcelable
