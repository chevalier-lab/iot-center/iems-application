package com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.pcb

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class Pcb(
    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("device_name")
    val name: String? = null,

    @field:SerializedName("mode")
    val mode: String? = null,

    @field:SerializedName("date")
    val date: String? = null,

    @field:SerializedName("time")
    val time: String? = null
) : Parcelable
