package com.aitekteam.developer.iemscontrolling.core.data.network.source

import com.aitekteam.developer.iemscontrolling.core.data.network.response.media.Media
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.general.Device
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.group.Group
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.invite.Invite
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.profile.User
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.profile.UserInfo
import com.aitekteam.developer.iemscontrolling.core.data.network.service.UserService
import com.aitekteam.developer.iemscontrolling.core.utils.vo.ApiResponse
import com.google.gson.JsonObject
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import okhttp3.MultipartBody
import timber.log.Timber

class RemoteUserDataSource(
    private val userService: UserService
) {

    fun getProfile(authorization: String): Flow<ApiResponse<User>> = flow {
        try {
            val response = userService.getProfile(authorization)
            val data = response.data

            data?.let {
                when (response.code) {
                    200 -> emit(ApiResponse.Success(it))
                    401 -> emit(ApiResponse.Error(response.message!!))
                    500 -> emit(ApiResponse.Error(response.message!!))
                    else -> emit(ApiResponse.Empty)
                }
            }
        } catch (e: Exception) {
            emit(ApiResponse.Error(e.toString()))
            Timber.e("onFailure : $e")
        }
    }.flowOn(Dispatchers.IO)

    fun updateProfile(authorization: String, jsonObject: JsonObject): Flow<ApiResponse<User>> = flow {
        try {
            val response = userService.updateProfile(authorization, jsonObject)
            val data = response.data

            data?.let {
                when (response.code) {
                    200 -> emit(ApiResponse.Success(it))
                    401 -> emit(ApiResponse.Error(response.message!!))
                    500 -> emit(ApiResponse.Error(response.message!!))
                    else -> emit(ApiResponse.Empty)
                }
            }
        } catch (e: Exception) {
            emit(ApiResponse.Error(e.toString()))
            Timber.e("onFailure : $e")
        }
    }.flowOn(Dispatchers.IO)

    fun updatePhotoProfile(authorization: String, coverId: String): Flow<ApiResponse<User>> = flow {
        try {
            val response = userService.updatePhotoProfile(authorization, coverId)
            val data = response.data

            data?.let {
                when (response.code) {
                    200 -> emit(ApiResponse.Success(it))
                    401 -> emit(ApiResponse.Error(response.message!!))
                    500 -> emit(ApiResponse.Error(response.message!!))
                    else -> emit(ApiResponse.Empty)
                }
            }
        } catch (e: Exception) {
            emit(ApiResponse.Error(e.toString()))
            Timber.e("onFailure : $e")
        }
    }.flowOn(Dispatchers.IO)

    fun getUserInfo(token: String): Flow<ApiResponse<UserInfo>> = flow {
        try {
            val response = userService.getUserInfo(token)
            val data = response.data

            data?.let {
                when (response.code) {
                    200 -> emit(ApiResponse.Success(it))
                    else -> emit(ApiResponse.Error(response.message!!))
                }
            }
        } catch (e: Exception) {
            emit(ApiResponse.Error(e.toString()))
            Timber.e("onFailure : $e")
        }
    }.flowOn(Dispatchers.IO)

    fun changeUserInfo(token: String, jsonObject: JsonObject): Flow<ApiResponse<UserInfo>> = flow {
        try {
            val response = userService.changeUserInfo(token, jsonObject)
            val data = response.data

            data?.let {
                when (response.code) {
                    200 -> emit(ApiResponse.Success(it))
                    else -> emit(ApiResponse.Error(response.message!!))
                }
            }
        } catch (e: Exception) {
            emit(ApiResponse.Error(e.toString()))
            Timber.e("onFailure : $e")
        }
    }.flowOn(Dispatchers.IO)

    fun requestActivate(authorization: String, requestType: String): Flow<ApiResponse<User>> =
        flow {
            try {
                val response = userService.requestActivate(authorization, requestType)
                val data = response.data

                data?.let {
                    when (response.code) {
                        200 -> emit(ApiResponse.Success(it))
                        401 -> emit(ApiResponse.Error(response.message!!))
                        else -> emit(ApiResponse.Empty)
                    }
                }
            } catch (e: Exception) {
                emit(ApiResponse.Error(e.toString()))
                Timber.e("onFailure : $e")
            }
        }.flowOn(Dispatchers.IO)

    fun inviteGuest(token: String, jsonObject: JsonObject): Flow<ApiResponse<Invite>> = flow {
        try {
            val response = userService.inviteGuest(token, jsonObject)
            val data = response.data

            data?.let {
                when (response.code) {
                    200 -> emit(ApiResponse.Success(it))
                    else -> emit(ApiResponse.Error(response.message!!))
                }
            }
        } catch (e: Exception) {
            emit(ApiResponse.Error(e.toString()))
            Timber.e("onFailure : $e")
        }
    }.flowOn(Dispatchers.IO)

    fun listGuest(token: String): Flow<ApiResponse<List<User>>> = flow {
        try {
            val response = userService.listGuest(token)
            val data = response.data

            data?.let {
                when (response.code) {
                    200 -> emit(ApiResponse.Success(it))
                    else -> emit(ApiResponse.Error(response.message!!))
                }
            }
        } catch (e: Exception) {
            emit(ApiResponse.Error(e.toString()))
            Timber.e("onFailure : $e")
        }
    }.flowOn(Dispatchers.IO)

    fun detailGuest(token: String, phoneNumber: String): Flow<ApiResponse<User>> = flow {
        try {
            val response = userService.detailGuest(token, phoneNumber)
            val data = response.data

            data?.let {
                when (response.code) {
                    200 -> emit(ApiResponse.Success(it))
                    else -> emit(ApiResponse.Error(response.message!!))
                }
            }
        } catch (e: Exception) {
            emit(ApiResponse.Error(e.toString()))
            Timber.e("onFailure : $e")
        }
    }.flowOn(Dispatchers.IO)

    fun getListGroup(token: String): Flow<ApiResponse<List<Group>>> = flow {
        try {
            val response = userService.getListGroup(token)
            val data = response.data

            data?.let {
                when (response.code) {
                    200 -> emit(ApiResponse.Success(it))
                    else -> emit(ApiResponse.Error(response.message!!))
                }
            }
        } catch (e: Exception) {
            emit(ApiResponse.Error(e.toString()))
            Timber.e("onFailure : $e")
        }
    }.flowOn(Dispatchers.IO)

    fun createGroup(token: String, jsonObject: JsonObject): Flow<ApiResponse<Group>> = flow {
        try {
            val response = userService.createGroup(token, jsonObject)
            val data = response.data

            data?.let {
                when (response.code) {
                    200 -> emit(ApiResponse.Success(it))
                    else -> emit(ApiResponse.Error(response.message!!))
                }
            }
        } catch (e: Exception) {
            emit(ApiResponse.Error(e.toString()))
            Timber.e("onFailure : $e")
        }
    }.flowOn(Dispatchers.IO)

    fun updateGroup(token: String, groupId: Int, jsonObject: JsonObject): Flow<ApiResponse<Group>> = flow {
        try {
            val response = userService.updateGroup(token, groupId, jsonObject)
            val data = response.data

            data?.let {
                when (response.code) {
                    200 -> emit(ApiResponse.Success(it))
                    else -> emit(ApiResponse.Error(response.message!!))
                }
            }
        } catch (e: Exception) {
            emit(ApiResponse.Error(e.toString()))
            Timber.e("onFailure : $e")
        }
    }.flowOn(Dispatchers.IO)

    fun getDetailGroup(token: String, groupId: Int): Flow<ApiResponse<List<Device>>> = flow {
        try {
            val response = userService.getDetailGroup(token, groupId)
            val data = response.data

            data?.let {
                when (response.code) {
                    200 -> emit(ApiResponse.Success(it))
                    else -> emit(ApiResponse.Error(response.message!!))
                }
            }
        } catch (e: Exception) {
            emit(ApiResponse.Error(e.toString()))
            Timber.e("onFailure : $e")
        }
    }.flowOn(Dispatchers.IO)

    fun filterDeviceGroup(
        token: String,
        groupId: Int,
        search: String,
        page: String,
        type: String,
        isAdded: String,
        direction: String
    ): Flow<ApiResponse<List<Device>>> = flow {
        try {
            val response = userService.filterDeviceGroup(token, groupId, search, page, type, isAdded, direction)
            val data = response.data

            data?.let {
                when (response.code) {
                    200 -> emit(ApiResponse.Success(it))
                    else -> emit(ApiResponse.Error(response.message!!))
                }
            }
        } catch (e: Exception) {
            emit(ApiResponse.Error(e.toString()))
            Timber.e("onFailure : $e")
        }
    }.flowOn(Dispatchers.IO)

    fun appendDevice(token: String, groupId: Int, deviceId: Int): Flow<ApiResponse<Group>> = flow {
        try {
            val response = userService.appendDevice(token, groupId, deviceId)
            val data = response.data

            data?.let {
                when (response.code) {
                    200 -> emit(ApiResponse.Success(it))
                    else -> emit(ApiResponse.Error(response.message!!))
                }
            }
        } catch (e: Exception) {
            emit(ApiResponse.Error(e.toString()))
            Timber.e("onFailure : $e")
        }
    }.flowOn(Dispatchers.IO)

    fun removeDevice(token: String, groupId: Int, deviceId: Int): Flow<ApiResponse<Group>> = flow {
        try {
            val response = userService.removeDevice(token, groupId, deviceId)
            val data = response.data

            data?.let {
                when (response.code) {
                    200 -> emit(ApiResponse.Success(it))
                    else -> emit(ApiResponse.Error(response.message!!))
                }
            }
        } catch (e: Exception) {
            emit(ApiResponse.Error(e.toString()))
            Timber.e("onFailure : $e")
        }
    }.flowOn(Dispatchers.IO)

    fun createMedia(token: String, photo: MultipartBody.Part): Flow<ApiResponse<Media>> = flow {
        try {
            val response = userService.createMedia(token, photo)
            val data = response.data

            data?.let {
                when (response.code) {
                    200 -> emit(ApiResponse.Success(it))
                    else -> emit(ApiResponse.Error(response.message!!))
                }
            }
        } catch (e: Exception) {
            emit(ApiResponse.Error(e.toString()))
            Timber.e("onFailure : $e")
        }
    }.flowOn(Dispatchers.IO)

    fun getDeviceGuest(token: String, phoneNumber: String): Flow<ApiResponse<List<Device>>> = flow {
        try {
            val response = userService.getDeviceGuest(token, phoneNumber)
            val data = response.data

            data?.let {
                when (response.code) {
                    200 -> emit(ApiResponse.Success(it))
                    else -> emit(ApiResponse.Error(response.message!!))
                }
            }
        } catch (e: Exception) {
            emit(ApiResponse.Error(e.toString()))
            Timber.e("onFailure : $e")
        }
    }.flowOn(Dispatchers.IO)

    fun adDeviceGuest(token: String, jsonObject: JsonObject): Flow<ApiResponse<Device>> = flow {
        try {
            val response = userService.addDeviceGuest(token, jsonObject)
            val data = response.data

            data?.let {
                when (response.code) {
                    200 -> emit(ApiResponse.Success(it))
                    else -> emit(ApiResponse.Error(response.message!!))
                }
            }
        } catch (e: Exception) {
            emit(ApiResponse.Error(e.toString()))
            Timber.e("onFailure : $e")
        }
    }.flowOn(Dispatchers.IO)
}