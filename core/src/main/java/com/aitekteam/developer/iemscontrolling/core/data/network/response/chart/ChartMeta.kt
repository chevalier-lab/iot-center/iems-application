package com.aitekteam.developer.iemscontrolling.core.data.network.response.chart

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class ChartMeta(
    @field:SerializedName("first_date")
    val firstDate: String? = null,

    @field:SerializedName("last_date")
    val lastDate: String? = null,

    @field:SerializedName("param")
    val param: String? = null
) : Parcelable
