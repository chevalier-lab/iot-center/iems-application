package com.aitekteam.developer.iemscontrolling.core.domain.repository

import com.aitekteam.developer.iemscontrolling.core.data.Resource
import com.aitekteam.developer.iemscontrolling.core.data.network.response.ticketing.CreateTicketing
import com.aitekteam.developer.iemscontrolling.core.data.network.response.ticketing.ReplyTicket
import com.aitekteam.developer.iemscontrolling.core.data.network.response.ticketing.Ticketing
import com.google.gson.JsonObject
import kotlinx.coroutines.flow.Flow

interface ITicketingRepository {

    fun createComplain(
        authorization: String,
        jsonObject: JsonObject
    ): Flow<Resource<CreateTicketing>>

    fun getListComplain(authorization: String): Flow<Resource<List<Ticketing>>>

    fun replyComplaint(
        authorization: String,
        ticketId: Int,
        jsonObject: JsonObject
    ): Flow<Resource<ReplyTicket>>

    fun getListReply(
        authorization: String,
        ticketId: Int
    ): Flow<Resource<List<ReplyTicket>>>
}
