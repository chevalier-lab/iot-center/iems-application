package com.aitekteam.developer.iemscontrolling.core.data.network.response.chart.kwh3

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class ChartKwh3(
    @field:SerializedName("date")
    val date: String? = null,

    @field:SerializedName("time")
    val time: String? = null,

    @field:SerializedName("sum")
    val sum: ChartKwh3Child? = ChartKwh3Child(),

    @field:SerializedName("avg")
    val avg: ChartKwh3Child? = ChartKwh3Child(),

    @field:SerializedName("last")
    val last: ChartKwh3Child? = ChartKwh3Child(),

    @field:SerializedName("first")
    val first: ChartKwh3Child? = ChartKwh3Child(),

    @field:SerializedName("first_day_of_month")
    val firstDayOfMonth: ChartKwh3Child? = ChartKwh3Child()
) : Parcelable
