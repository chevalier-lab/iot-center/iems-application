package com.aitekteam.developer.iemscontrolling.core.data.repository

import com.aitekteam.developer.iemscontrolling.core.data.Resource
import com.aitekteam.developer.iemscontrolling.core.data.network.response.media.Media
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.general.Device
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.group.Group
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.invite.Invite
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.profile.User
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.profile.UserInfo
import com.aitekteam.developer.iemscontrolling.core.data.network.source.RemoteUserDataSource
import com.aitekteam.developer.iemscontrolling.core.domain.repository.IUserRepository
import com.aitekteam.developer.iemscontrolling.core.utils.vo.ApiResponse
import com.google.gson.JsonObject
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flow
import okhttp3.MultipartBody

class UserRepository(
    private val remoteUserDataSource: RemoteUserDataSource
) : IUserRepository {

    override fun getData(authorization: String): Flow<Resource<User>> = flow {
        emit(Resource.loading<User>())
        val response = remoteUserDataSource.getProfile(authorization)

        when (val apiResponse = response.first()) {
            is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
            is ApiResponse.Empty -> emit(Resource.success(User()))
            is ApiResponse.Error -> emit(Resource.error<User>(apiResponse.errorMessage))
        }
    }

    override fun updateProfile(
        authorization: String,
        jsonObject: JsonObject
    ): Flow<Resource<User>> = flow {
        emit(Resource.loading<User>())
        val response = remoteUserDataSource.updateProfile(authorization, jsonObject)

        when (val apiResponse = response.first()) {
            is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
            is ApiResponse.Empty -> emit(Resource.success(User()))
            is ApiResponse.Error -> emit(Resource.error<User>(apiResponse.errorMessage))
        }
    }

    override fun updatePhotoProfile(authorization: String, coverId: String): Flow<Resource<User>> =
        flow {
            emit(Resource.loading<User>())
            val response = remoteUserDataSource.updatePhotoProfile(authorization, coverId)

            when (val apiResponse = response.first()) {
                is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
                is ApiResponse.Empty -> emit(Resource.success(User()))
                is ApiResponse.Error -> emit(Resource.error<User>(apiResponse.errorMessage))
            }
        }

    override fun getUserInfo(token: String): Flow<Resource<UserInfo>> = flow {
        emit(Resource.loading<UserInfo>())
        val response = remoteUserDataSource.getUserInfo(token)

        when (val apiResponse = response.first()) {
            is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
            is ApiResponse.Empty -> emit(Resource.success(UserInfo()))
            is ApiResponse.Error -> emit(Resource.error<UserInfo>(apiResponse.errorMessage))
        }
    }

    override fun changeUserInfo(token: String, jsonObject: JsonObject): Flow<Resource<UserInfo>> =
        flow {
            emit(Resource.loading<UserInfo>())
            val response = remoteUserDataSource.changeUserInfo(token, jsonObject)

            when (val apiResponse = response.first()) {
                is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
                is ApiResponse.Empty -> emit(Resource.success(UserInfo()))
                is ApiResponse.Error -> emit(Resource.error<UserInfo>(apiResponse.errorMessage))
            }
        }

    override fun requestActivate(authorization: String, requestType: String): Flow<Resource<User>> =
        flow {
            emit(Resource.loading<User>())
            val response = remoteUserDataSource.requestActivate(authorization, requestType)

            when (val apiResponse = response.first()) {
                is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
                is ApiResponse.Empty -> emit(Resource.success(User()))
                is ApiResponse.Error -> emit(Resource.error<User>(apiResponse.errorMessage))
            }
        }

    override fun inviteGuest(token: String, jsonObject: JsonObject): Flow<Resource<Invite>> = flow {
        emit(Resource.loading<Invite>())
        val response = remoteUserDataSource.inviteGuest(token, jsonObject)

        when (val apiResponse = response.first()) {
            is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
            is ApiResponse.Empty -> emit(Resource.success(Invite()))
            is ApiResponse.Error -> emit(Resource.error<Invite>(apiResponse.errorMessage))
        }
    }

    override fun listGuest(token: String): Flow<Resource<List<User>>> = flow {
        emit(Resource.loading<List<User>>())
        val response = remoteUserDataSource.listGuest(token)

        when (val apiResponse = response.first()) {
            is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
            is ApiResponse.Empty -> emit(Resource.success(listOf<User>()))
            is ApiResponse.Error -> emit(Resource.error<List<User>>(apiResponse.errorMessage))
        }
    }

    override fun detailGuest(token: String, phoneNumber: String): Flow<Resource<User>> = flow {
        emit(Resource.loading<User>())
        val response = remoteUserDataSource.detailGuest(token, phoneNumber)

        when (val apiResponse = response.first()) {
            is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
            is ApiResponse.Empty -> emit(Resource.success(User()))
            is ApiResponse.Error -> emit(Resource.error<User>(apiResponse.errorMessage))
        }
    }

    override fun getListGroup(token: String): Flow<Resource<List<Group>>> = flow {
        emit(Resource.loading<List<Group>>())
        val response = remoteUserDataSource.getListGroup(token)

        when (val apiResponse = response.first()) {
            is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
            is ApiResponse.Empty -> emit(Resource.success(listOf<Group>()))
            is ApiResponse.Error -> emit(Resource.error<List<Group>>(apiResponse.errorMessage))
        }
    }

    override fun createGroup(token: String, jsonObject: JsonObject): Flow<Resource<Group>> = flow {
        emit(Resource.loading<Group>())
        val response = remoteUserDataSource.createGroup(token, jsonObject)

        when (val apiResponse = response.first()) {
            is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
            is ApiResponse.Empty -> emit(Resource.success(Group()))
            is ApiResponse.Error -> emit(Resource.error<Group>(apiResponse.errorMessage))
        }
    }

    override fun updateGroup(
        token: String,
        groupId: Int,
        jsonObject: JsonObject
    ): Flow<Resource<Group>> = flow {
        emit(Resource.loading<Group>())
        val response = remoteUserDataSource.updateGroup(token, groupId, jsonObject)

        when (val apiResponse = response.first()) {
            is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
            is ApiResponse.Empty -> emit(Resource.success(Group()))
            is ApiResponse.Error -> emit(Resource.error<Group>(apiResponse.errorMessage))
        }
    }

    override fun getDetailGroup(token: String, groupId: Int): Flow<Resource<List<Device>>> = flow {
        emit(Resource.loading<List<Device>>())
        val response = remoteUserDataSource.getDetailGroup(token, groupId)

        when (val apiResponse = response.first()) {
            is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
            is ApiResponse.Empty -> emit(Resource.success(listOf<Device>()))
            is ApiResponse.Error -> emit(Resource.error<List<Device>>(apiResponse.errorMessage))
        }
    }

    override fun filterDeviceGroup(
        token: String,
        groupId: Int,
        search: String,
        page: String,
        type: String,
        isAdded: String,
        direction: String
    ): Flow<Resource<List<Device>>> = flow {
        emit(Resource.loading<List<Device>>())
        val response = remoteUserDataSource.filterDeviceGroup(
            token,
            groupId,
            search,
            page,
            type,
            isAdded,
            direction
        )

        when (val apiResponse = response.first()) {
            is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
            is ApiResponse.Empty -> emit(Resource.success(listOf<Device>()))
            is ApiResponse.Error -> emit(Resource.error<List<Device>>(apiResponse.errorMessage))
        }
    }

    override fun appendDevice(token: String, groupId: Int, deviceId: Int): Flow<Resource<Group>> =
        flow {
            emit(Resource.loading<Group>())
            val response = remoteUserDataSource.appendDevice(token, groupId, deviceId)

            when (val apiResponse = response.first()) {
                is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
                is ApiResponse.Empty -> emit(Resource.success(Group()))
                is ApiResponse.Error -> emit(Resource.error<Group>(apiResponse.errorMessage))
            }
        }

    override fun removeDevice(token: String, groupId: Int, deviceId: Int): Flow<Resource<Group>> =
        flow {
            emit(Resource.loading<Group>())
            val response = remoteUserDataSource.removeDevice(token, groupId, deviceId)

            when (val apiResponse = response.first()) {
                is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
                is ApiResponse.Empty -> emit(Resource.success(Group()))
                is ApiResponse.Error -> emit(Resource.error<Group>(apiResponse.errorMessage))
            }
        }

    override fun createMedia(token: String, photo: MultipartBody.Part): Flow<Resource<Media>> =
        flow {
            emit(Resource.loading<Media>())
            val response = remoteUserDataSource.createMedia(token, photo)

            when (val apiResponse = response.first()) {
                is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
                is ApiResponse.Empty -> emit(Resource.success(Media()))
                is ApiResponse.Error -> emit(Resource.error<Media>(apiResponse.errorMessage))
            }
        }

    override fun getDeviceGuest(token: String, phoneNumber: String): Flow<Resource<List<Device>>> =
        flow {
            emit(Resource.loading<List<Device>>())
            val response = remoteUserDataSource.getDeviceGuest(token, phoneNumber)

            when (val apiResponse = response.first()) {
                is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
                is ApiResponse.Empty -> emit(Resource.success(listOf<Device>()))
                is ApiResponse.Error -> emit(Resource.error<List<Device>>(apiResponse.errorMessage))
            }
        }

    override fun addDeviceGuest(token: String, jsonObject: JsonObject): Flow<Resource<Device>> =
        flow {
            emit(Resource.loading<Device>())
            val response = remoteUserDataSource.adDeviceGuest(token, jsonObject)

            when (val apiResponse = response.first()) {
                is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
                is ApiResponse.Empty -> emit(Resource.success(Device()))
                is ApiResponse.Error -> emit(Resource.error<Device>(apiResponse.errorMessage))
            }
        }
}