package com.aitekteam.developer.iemscontrolling.core.data.network.response.user.profile

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class User(
    @field:SerializedName("phone_number")
    val phoneNumber: String? = null,

    @field:SerializedName("full_name")
    val fullName: String? = null,

    @field:SerializedName("email")
    val email: String? = null,

    @field:SerializedName("cover")
    val cover: String? = null,

    @field:SerializedName("type")
    val type: String? = null,

    @field:SerializedName("request_type")
    val requestType: String? = null,

    @field:SerializedName("status")
    val status: String? = null,

    @field:SerializedName("token")
    val token: String? = null
) : Parcelable