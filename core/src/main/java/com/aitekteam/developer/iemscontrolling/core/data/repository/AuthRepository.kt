package com.aitekteam.developer.iemscontrolling.core.data.repository

import com.aitekteam.developer.iemscontrolling.core.data.Resource
import com.aitekteam.developer.iemscontrolling.core.data.network.response.auth.Login
import com.aitekteam.developer.iemscontrolling.core.data.network.response.auth.Register
import com.aitekteam.developer.iemscontrolling.core.data.network.source.RemoteAuthDataSource
import com.aitekteam.developer.iemscontrolling.core.domain.repository.IAuthRepository
import com.aitekteam.developer.iemscontrolling.core.utils.vo.ApiResponse
import com.google.gson.JsonObject
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flow

class AuthRepository(
    private val remoteAuthDataSource: RemoteAuthDataSource
) : IAuthRepository {

    override fun login(jsonObject: JsonObject): Flow<Resource<Login>> = flow {
        emit(Resource.loading<Login>())
        val response = remoteAuthDataSource.login(jsonObject)

        when (val apiResponse = response.first()) {
            is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
            is ApiResponse.Empty -> emit(Resource.success(Login()))
            is ApiResponse.Error -> emit(Resource.error<Login>(apiResponse.errorMessage))
        }
    }

    override fun register(jsonObject: JsonObject): Flow<Resource<Register>> = flow {
        emit(Resource.loading<Register>())
        val response = remoteAuthDataSource.register(jsonObject)

        when (val apiResponse = response.first()) {
            is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
            is ApiResponse.Empty -> emit(Resource.success(Register()))
            is ApiResponse.Error -> emit(Resource.error<Register>(apiResponse.errorMessage))
        }
    }
}