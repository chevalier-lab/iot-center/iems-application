package com.aitekteam.developer.iemscontrolling.core.data.network.response.chart.kwh1

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class FirstDetail(
    @field:SerializedName("kwh")
    val kwh: Float? = null,

    @field:SerializedName("date")
    val date: String? = null,

    @field:SerializedName("time")
    val time: String? = null
) : Parcelable
