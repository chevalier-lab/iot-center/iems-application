package com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.kwh1

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class Kwh1(
    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("id_m_devices")
    val deviceId: Int? = null,

    @field:SerializedName("i")
    val current: String? = null,

    @field:SerializedName("v")
    val voltage: String? = null,

    @field:SerializedName("pa")
    val power: String? = null,

    @field:SerializedName("pr")
    val pr: String? = null,

    @field:SerializedName("ap")
    val ap: String? = null,

    @field:SerializedName("pf")
    val powerFactor: String? = null,

    @field:SerializedName("f")
    val frequency: String? = null,

    @field:SerializedName("q")
    val q: String? = null,

    @field:SerializedName("s")
    val s: String? = null,

    @field:SerializedName("kwh")
    val kwh: String? = null,

    @field:SerializedName("date")
    val date: String? = null,

    @field:SerializedName("time")
    val time: String? = null
) : Parcelable
