package com.aitekteam.developer.iemscontrolling.core.utils.vo

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}