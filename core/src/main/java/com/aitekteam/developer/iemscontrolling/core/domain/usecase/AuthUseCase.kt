package com.aitekteam.developer.iemscontrolling.core.domain.usecase

import com.aitekteam.developer.iemscontrolling.core.data.Resource
import com.aitekteam.developer.iemscontrolling.core.data.network.response.auth.Login
import com.aitekteam.developer.iemscontrolling.core.data.network.response.auth.Register
import com.google.gson.JsonObject
import kotlinx.coroutines.flow.Flow

interface AuthUseCase {

    fun login(jsonObject: JsonObject): Flow<Resource<Login>>

    fun register(jsonObject: JsonObject): Flow<Resource<Register>>
}