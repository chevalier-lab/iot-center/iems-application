package com.aitekteam.developer.iemscontrolling.core.data.network.response.ticketing

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class ReplyTicket(
    @field:SerializedName("phone_number")
    val phoneNumber: String? = null,

    @field:SerializedName("reply")
    val reply: String? = null,

    @field:SerializedName("id_m_ticketing")
    val ticketId: Int? = null,

    @field:SerializedName("created_at")
    val date: String? = null
) : Parcelable
