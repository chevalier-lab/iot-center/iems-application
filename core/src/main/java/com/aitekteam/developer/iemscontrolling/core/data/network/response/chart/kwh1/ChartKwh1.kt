package com.aitekteam.developer.iemscontrolling.core.data.network.response.chart.kwh1

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class ChartKwh1(
    @field:SerializedName("date")
    val date: String? = null,

    @field:SerializedName("time")
    val time: String? = null,

    @field:SerializedName("sum")
    val sum: Float? = null,

    @field:SerializedName("avg")
    val avg: Float? = null,

    @field:SerializedName("last")
    var last: Float? = null,

    @field:SerializedName("first")
    val first: Float? = null,

    @field:SerializedName("first_day_of_month")
    val firstDayOfMonth: Float? = null,

    @field:SerializedName("first_detail")
    val firstDetail: List<FirstDetail>? = listOf(),

    @field:SerializedName("items")
    val items: List<FirstDetail>? = listOf()
) : Parcelable
