package com.aitekteam.developer.iemscontrolling.core.data.network.response.operator

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class ActivationUser(
    @field:SerializedName("phone_number")
    val phoneNumber: String? = null,

    @field:SerializedName("ssid_name")
    val ssid: String? = null,

    @field:SerializedName("ssid_password")
    val password: String? = null,

    @field:SerializedName("api_key")
    val apiKey: String? = null,

    @field:SerializedName("installed_by")
    val installedBy: String? = null
) : Parcelable
