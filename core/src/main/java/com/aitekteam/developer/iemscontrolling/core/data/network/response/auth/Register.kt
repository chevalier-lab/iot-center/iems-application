package com.aitekteam.developer.iemscontrolling.core.data.network.response.auth

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class Register(
    @field:SerializedName("full_name")
    val fullName: String? = null,

    @field:SerializedName("token")
    val token: String? = null,

    @field:SerializedName("type")
    val type: String? = null,

    @field:SerializedName("status")
    val status: String? = null,
) : Parcelable
