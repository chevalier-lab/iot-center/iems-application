package com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.kwh3

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class Kwh3(
    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("id_m_devices")
    val deviceId: Int? = null,

    @field:SerializedName("va")
    val va: String? = null,

    @field:SerializedName("vb")
    val vb: String? = null,

    @field:SerializedName("vc")
    val vc: String? = null,

    @field:SerializedName("vab")
    val vab: String? = null,

    @field:SerializedName("vbc")
    val vbc: String? = null,

    @field:SerializedName("vca")
    val vca: String? = null,

    @field:SerializedName("ia")
    val ia: String? = null,

    @field:SerializedName("ib")
    val ib: String? = null,

    @field:SerializedName("ic")
    val ic: String? = null,

    @field:SerializedName("pa")
    val pa: String? = null,

    @field:SerializedName("pb")
    val pb: String? = null,

    @field:SerializedName("pc")
    val pc: String? = null,

    @field:SerializedName("pt")
    val pt: String? = null,

    @field:SerializedName("qa")
    val qa: String? = null,

    @field:SerializedName("qb")
    val qb: String? = null,

    @field:SerializedName("qc")
    val qc: String? = null,

    @field:SerializedName("qt")
    val qt: String? = null,

    @field:SerializedName("sa")
    val sa: String? = null,

    @field:SerializedName("sb")
    val sb: String? = null,

    @field:SerializedName("sc")
    val sc: String? = null,

    @field:SerializedName("st")
    val st: String? = null,

    @field:SerializedName("pfa")
    val pfa: String? = null,

    @field:SerializedName("pfb")
    val pfb: String? = null,

    @field:SerializedName("pfc")
    val pfc: String? = null,

    @field:SerializedName("freq")
    val freq: String? = null,

    @field:SerializedName("ep")
    val ep: String? = null,

    @field:SerializedName("eq")
    val eq: String? = null,

    @field:SerializedName("date")
    val date: String? = null,

    @field:SerializedName("time")
    val time: String? = null
) : Parcelable
