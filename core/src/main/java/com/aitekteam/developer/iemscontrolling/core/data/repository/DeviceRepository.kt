package com.aitekteam.developer.iemscontrolling.core.data.repository

import com.aitekteam.developer.iemscontrolling.core.data.Resource
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.calculate.TotalKwh
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.general.Device
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.kwh1.Kwh1
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.kwh3.Kwh3
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.notification.DeviceNotification
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.pcb.Pcb
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.sensor.Sensor
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.slca.Scheduler
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.slca.SingleSchedulerResponse
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.slca.Slca
import com.aitekteam.developer.iemscontrolling.core.data.network.source.RemoteDeviceDataSource
import com.aitekteam.developer.iemscontrolling.core.domain.repository.IDeviceRepository
import com.aitekteam.developer.iemscontrolling.core.utils.vo.ApiResponse
import com.google.gson.JsonObject
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flow

class DeviceRepository(
    private val remoteDeviceDataSource: RemoteDeviceDataSource
) : IDeviceRepository {

    override fun getListDevice(token: String): Flow<Resource<List<Device>>> = flow {
        emit(Resource.loading<List<Device>>())
        val response = remoteDeviceDataSource.getListDevice(token)

        when (val apiResponse = response.first()) {
            is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
            is ApiResponse.Empty -> emit(Resource.success(listOf<Device>()))
            is ApiResponse.Error -> emit(Resource.error<List<Device>>(apiResponse.errorMessage))
        }
    }

    override fun getListDeviceGuest(token: String): Flow<Resource<List<Device>>> = flow {
        emit(Resource.loading<List<Device>>())
        val response = remoteDeviceDataSource.getListDeviceGuest(token)

        when (val apiResponse = response.first()) {
            is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
            is ApiResponse.Empty -> emit(Resource.success(listOf<Device>()))
            is ApiResponse.Error -> emit(Resource.error<List<Device>>(apiResponse.errorMessage))
        }
    }

    override fun updateDevice(
        token: String,
        id: Int,
        jsonObject: JsonObject
    ): Flow<Resource<Device>> =
        flow {
            emit(Resource.loading<Device>())
            val response = remoteDeviceDataSource.updateDevice(token, id, jsonObject)

            when (val apiResponse = response.first()) {
                is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
                is ApiResponse.Empty -> emit(Resource.success(Device()))
                is ApiResponse.Error -> emit(Resource.error<Device>(apiResponse.errorMessage))
            }
        }

    override fun getLastPcb(token: String): Flow<Resource<Pcb>> = flow {
        emit(Resource.loading<Pcb>())
        val response = remoteDeviceDataSource.getLastPcb(token)

        when (val apiResponse = response.first()) {
            is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
            is ApiResponse.Empty -> emit(Resource.success(Pcb()))
            is ApiResponse.Error -> emit(Resource.error<Pcb>(apiResponse.errorMessage))
        }
    }

    override fun getHistoryPcb(token: String): Flow<Resource<List<Pcb>>> = flow {
        emit(Resource.loading<List<Pcb>>())
        val response = remoteDeviceDataSource.getHistoryPcb(token)

        when (val apiResponse = response.first()) {
            is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
            is ApiResponse.Empty -> emit(Resource.success(listOf<Pcb>()))
            is ApiResponse.Error -> emit(Resource.error<List<Pcb>>(apiResponse.errorMessage))
        }
    }

    override fun getLastSensor(token: String): Flow<Resource<Sensor>> = flow {
        emit(Resource.loading<Sensor>())
        val response = remoteDeviceDataSource.getLastSensor(token)

        when (val apiResponse = response.first()) {
            is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
            is ApiResponse.Empty -> emit(Resource.success(Sensor()))
            is ApiResponse.Error -> emit(Resource.error<Sensor>(apiResponse.errorMessage))
        }
    }

    override fun getHistorySensor(token: String): Flow<Resource<List<Sensor>>> = flow {
        emit(Resource.loading<List<Sensor>>())
        val response = remoteDeviceDataSource.getHistorySensor(token)

        when (val apiResponse = response.first()) {
            is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
            is ApiResponse.Empty -> emit(Resource.success(listOf<Sensor>()))
            is ApiResponse.Error -> emit(Resource.error<List<Sensor>>(apiResponse.errorMessage))
        }
    }

    override fun getLastSlca(token: String): Flow<Resource<Slca>> = flow {
        emit(Resource.loading<Slca>())
        val response = remoteDeviceDataSource.getLastSlca(token)

        when (val apiResponse = response.first()) {
            is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
            is ApiResponse.Empty -> emit(Resource.success(Slca()))
            is ApiResponse.Error -> emit(Resource.error<Slca>(apiResponse.errorMessage))
        }
    }

    override fun getHistorySlca(token: String): Flow<Resource<List<Slca>>> = flow {
        emit(Resource.loading<List<Slca>>())
        val response = remoteDeviceDataSource.getHistorySlca(token)

        when (val apiResponse = response.first()) {
            is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
            is ApiResponse.Empty -> emit(Resource.success(listOf<Slca>()))
            is ApiResponse.Error -> emit(Resource.error<List<Slca>>(apiResponse.errorMessage))
        }
    }

    override fun getLastKwh1(token: String): Flow<Resource<Kwh1>> = flow {
        emit(Resource.loading<Kwh1>())
        val response = remoteDeviceDataSource.getLastKwh1(token)

        when (val apiResponse = response.first()) {
            is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
            is ApiResponse.Empty -> emit(Resource.success(Kwh1()))
            is ApiResponse.Error -> emit(Resource.error<Kwh1>(apiResponse.errorMessage))
        }
    }

    override fun getLastKwh3(token: String): Flow<Resource<Kwh3>> = flow {
        emit(Resource.loading<Kwh3>())
        val response = remoteDeviceDataSource.getLastKwh3(token)

        when (val apiResponse = response.first()) {
            is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
            is ApiResponse.Empty -> emit(Resource.success(Kwh3()))
            is ApiResponse.Error -> emit(Resource.error<Kwh3>(apiResponse.errorMessage))
        }
    }

    override fun getDeviceInfo(token: String): Flow<Resource<Device>> = flow {
        emit(Resource.loading<Device>())
        val response = remoteDeviceDataSource.getDeviceInfo(token)

        when (val apiResponse = response.first()) {
            is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
            is ApiResponse.Empty -> emit(Resource.success(Device()))
            is ApiResponse.Error -> emit(Resource.error<Device>(apiResponse.errorMessage))
        }
    }

    override fun previewDevice(token: String): Flow<Resource<Device>> = flow {
        emit(Resource.loading<Device>())
        val response = remoteDeviceDataSource.previewDevice(token)

        when (val apiResponse = response.first()) {
            is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
            is ApiResponse.Empty -> emit(Resource.success(Device()))
            is ApiResponse.Error -> emit(Resource.error<Device>(apiResponse.errorMessage))
        }
    }

    override fun pushDevice(token: String, jsonObject: JsonObject): Flow<Resource<Device>> = flow {
        emit(Resource.loading<Device>())
        val response = remoteDeviceDataSource.pushDevice(token, jsonObject)

        when (val apiResponse = response.first()) {
            is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
            is ApiResponse.Empty -> emit(Resource.success(Device()))
            is ApiResponse.Error -> emit(Resource.error<Device>(apiResponse.errorMessage))
        }
    }

    override fun changeDeviceSsid(
        token: String,
        deviceId: Int,
        jsonObject: JsonObject
    ): Flow<Resource<Device>> = flow {
        emit(Resource.loading<Device>())
        val response = remoteDeviceDataSource.changeDeviceSsid(token, deviceId, jsonObject)

        when (val apiResponse = response.first()) {
            is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
            is ApiResponse.Empty -> emit(Resource.success(Device()))
            is ApiResponse.Error -> emit(Resource.error<Device>(apiResponse.errorMessage))
        }
    }

    override fun totalKwh(token: String): Flow<Resource<TotalKwh>> = flow {
        emit(Resource.loading<TotalKwh>())
        val response = remoteDeviceDataSource.totalKwh(token)

        when (val apiResponse = response.first()) {
            is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
            is ApiResponse.Empty -> emit(Resource.success(TotalKwh()))
            is ApiResponse.Error -> emit(Resource.error<TotalKwh>(apiResponse.errorMessage))
        }
    }

    override fun totalKwhOnGroup(token: String, groupId: Int): Flow<Resource<TotalKwh>> = flow {
        emit(Resource.loading<TotalKwh>())
        val response = remoteDeviceDataSource.totalKwhOnGroup(token, groupId)

        when (val apiResponse = response.first()) {
            is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
            is ApiResponse.Empty -> emit(Resource.success(TotalKwh()))
            is ApiResponse.Error -> emit(Resource.error<TotalKwh>(apiResponse.errorMessage))
        }
    }

    override fun getListKwh(token: String): Flow<Resource<List<Device>>> = flow {
        emit(Resource.loading<List<Device>>())
        val response = remoteDeviceDataSource.getListKwh(token)

        when (val apiResponse = response.first()) {
            is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
            is ApiResponse.Empty -> emit(Resource.success(listOf<Device>()))
            is ApiResponse.Error -> emit(Resource.error<List<Device>>(apiResponse.errorMessage))
        }
    }

    override fun getListKwhOnGroup(token: String, groupId: Int): Flow<Resource<List<Device>>> =
        flow {
            emit(Resource.loading<List<Device>>())
            val response = remoteDeviceDataSource.getListKwhOnGroup(token, groupId)

            when (val apiResponse = response.first()) {
                is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
                is ApiResponse.Empty -> emit(Resource.success(listOf<Device>()))
                is ApiResponse.Error -> emit(Resource.error<List<Device>>(apiResponse.errorMessage))
            }
        }

    override fun calculateKwh(token: String, deviceToken: String): Flow<Resource<TotalKwh>> = flow {
        emit(Resource.loading<TotalKwh>())
        val response = remoteDeviceDataSource.calculateKwh(token, deviceToken)

        when (val apiResponse = response.first()) {
            is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
            is ApiResponse.Empty -> emit(Resource.success(TotalKwh()))
            is ApiResponse.Error -> emit(Resource.error<TotalKwh>(apiResponse.errorMessage))
        }
    }

    override fun getDeviceUserNotification(token: String): Flow<Resource<List<DeviceNotification>>> =
        flow {
            emit(Resource.loading<List<DeviceNotification>>())
            val response = remoteDeviceDataSource.getDeviceUserNotification(token)

            when (val apiResponse = response.first()) {
                is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
                is ApiResponse.Empty -> emit(Resource.success(listOf<DeviceNotification>()))
                is ApiResponse.Error -> emit(Resource.error<List<DeviceNotification>>(apiResponse.errorMessage))
            }
        }

    override fun getDeviceGuestNotification(token: String): Flow<Resource<List<DeviceNotification>>> =
        flow {
            emit(Resource.loading<List<DeviceNotification>>())
            val response = remoteDeviceDataSource.getDeviceGuestNotification(token)

            when (val apiResponse = response.first()) {
                is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
                is ApiResponse.Empty -> emit(Resource.success(listOf<DeviceNotification>()))
                is ApiResponse.Error -> emit(Resource.error<List<DeviceNotification>>(apiResponse.errorMessage))
            }
        }

    override fun getListScheduler(token: String, deviceId: String): Flow<Resource<List<Scheduler>>> =
        flow {
            emit(Resource.loading<List<Scheduler>>())
            val response = remoteDeviceDataSource.getListScheduler(token, deviceId)

            when (val apiResponse = response.first()) {
                is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
                is ApiResponse.Empty -> emit(Resource.success(listOf<Scheduler>()))
                is ApiResponse.Error -> emit(Resource.error<List<Scheduler>>(apiResponse.errorMessage))
            }
        }

    override fun createScheduler(
        token: String,
        deviceId: String,
        jsonObject: JsonObject
    ): Flow<Resource<Scheduler>> = flow {
        emit(Resource.loading<Scheduler>())
        val response = remoteDeviceDataSource.createScheduler(token, deviceId, jsonObject)

        when (val apiResponse = response.first()) {
            is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
            is ApiResponse.Empty -> emit(Resource.success(Scheduler()))
            is ApiResponse.Error -> emit(Resource.error<Scheduler>(apiResponse.errorMessage))
        }
    }

    override fun deleteScheduler(
        token: String,
        schedulerId: String
    ): Flow<Resource<SingleSchedulerResponse>> = flow {
        emit(Resource.loading<SingleSchedulerResponse>())
        val response = remoteDeviceDataSource.deleteScheduler(token, schedulerId)

        when (val apiResponse = response.first()) {
            is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
            is ApiResponse.Empty -> emit(Resource.success(SingleSchedulerResponse()))
            is ApiResponse.Error -> emit(Resource.error<SingleSchedulerResponse>(apiResponse.errorMessage))
        }
    }
}