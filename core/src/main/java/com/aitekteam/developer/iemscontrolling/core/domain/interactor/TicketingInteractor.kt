package com.aitekteam.developer.iemscontrolling.core.domain.interactor

import com.aitekteam.developer.iemscontrolling.core.data.Resource
import com.aitekteam.developer.iemscontrolling.core.data.network.response.ticketing.CreateTicketing
import com.aitekteam.developer.iemscontrolling.core.data.network.response.ticketing.ReplyTicket
import com.aitekteam.developer.iemscontrolling.core.data.network.response.ticketing.Ticketing
import com.aitekteam.developer.iemscontrolling.core.data.repository.TicketingRepository
import com.aitekteam.developer.iemscontrolling.core.domain.usecase.TicketingUseCase
import com.google.gson.JsonObject
import kotlinx.coroutines.flow.Flow

class TicketingInteractor(
    private val ticketingRepository: TicketingRepository
) : TicketingUseCase {

    override fun createComplain(
        authorization: String,
        jsonObject: JsonObject
    ): Flow<Resource<CreateTicketing>> =
        ticketingRepository.createComplain(authorization, jsonObject)

    override fun getListComplain(authorization: String): Flow<Resource<List<Ticketing>>> =
        ticketingRepository.getListComplain(authorization)

    override fun replyComplaint(
        authorization: String,
        ticketId: Int,
        jsonObject: JsonObject
    ): Flow<Resource<ReplyTicket>> =
        ticketingRepository.replyComplaint(authorization, ticketId, jsonObject)

    override fun getListReply(
        authorization: String,
        ticketId: Int
    ): Flow<Resource<List<ReplyTicket>>> = ticketingRepository.getListReply(authorization, ticketId)
}