package com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.notification

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class DeviceNotification(
    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("phone_number")
    val phone: String? = null,

    @field:SerializedName("id_m_devices")
    val deviceId: Int? = null,

    @field:SerializedName("type")
    val type: String? = null,

    @field:SerializedName("title")
    val title: String? = null,

    @field:SerializedName("description")
    val description: String? = null,

    @field:SerializedName("created_at")
    val date: String? = null
) : Parcelable
