package com.aitekteam.developer.iemscontrolling.core.data.network.response.ticketing

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class ReplyTicketResponse(
    @field:SerializedName("code")
    val code: Int? = null,

    @field:SerializedName("message")
    val message: String? = null,

    @field:SerializedName("data")
    val data: List<ReplyTicket>? = listOf()
) : Parcelable
