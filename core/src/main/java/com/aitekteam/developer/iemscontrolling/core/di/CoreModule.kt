package com.aitekteam.developer.iemscontrolling.core.di

import com.aitekteam.developer.iemscontrolling.core.data.network.service.*
import com.aitekteam.developer.iemscontrolling.core.data.network.source.*
import com.aitekteam.developer.iemscontrolling.core.data.repository.*
import com.aitekteam.developer.iemscontrolling.core.utils.helper.AppExecutors
import com.aitekteam.developer.iemscontrolling.core.utils.helper.NetworkHelper
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import java.util.concurrent.TimeUnit

val networkModule = module {
    single {
        OkHttpClient.Builder()
            .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
            .connectTimeout(15, TimeUnit.SECONDS)
            .readTimeout(15, TimeUnit.SECONDS)
            .writeTimeout(30, TimeUnit.SECONDS)
            .build()
    }

    single { NetworkHelper(get(), AuthService::class.java).createService }
    single { NetworkHelper(get(), UserService::class.java).createService }
    single { NetworkHelper(get(), ChartService::class.java).createService }
    single { NetworkHelper(get(), DeviceService::class.java).createService }
    single { NetworkHelper(get(), OperatorService::class.java).createService }
    single { NetworkHelper(get(), TicketingService::class.java).createService }
}

val dataSourceModule = module {
    single { RemoteAuthDataSource(get()) }
    single { RemoteUserDataSource(get()) }
    single { RemoteDeviceDataSource(get()) }
    single { RemoteChartDataSource(get()) }
    single { RemoteOperatorDataSource(get()) }
    single { RemoteTicketingDataSource(get()) }
}

val repositoryModule = module {
    factory { AppExecutors() }
    single { AuthRepository(get()) }
    single { UserRepository(get()) }
    single { ChartRepository(get()) }
    single { DeviceRepository(get()) }
    single { OperatorRepository(get()) }
    single { TicketingRepository(get()) }
}