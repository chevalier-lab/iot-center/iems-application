package com.aitekteam.developer.iemscontrolling.core.domain.interactor

import com.aitekteam.developer.iemscontrolling.core.data.Resource
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.calculate.TotalKwh
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.general.Device
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.kwh1.Kwh1
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.kwh3.Kwh3
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.notification.DeviceNotification
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.pcb.Pcb
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.sensor.Sensor
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.slca.Scheduler
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.slca.SingleSchedulerResponse
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.slca.Slca
import com.aitekteam.developer.iemscontrolling.core.data.repository.DeviceRepository
import com.aitekteam.developer.iemscontrolling.core.domain.usecase.DeviceUseCase
import com.google.gson.JsonObject
import kotlinx.coroutines.flow.Flow

class DeviceInteractor(
    private val deviceRepository: DeviceRepository
) : DeviceUseCase {

    override fun getListDevice(token: String): Flow<Resource<List<Device>>> =
        deviceRepository.getListDevice(token)

    override fun getListDeviceGuest(token: String): Flow<Resource<List<Device>>> =
        deviceRepository.getListDeviceGuest(token)

    override fun updateDevice(
        token: String,
        id: Int,
        jsonObject: JsonObject
    ): Flow<Resource<Device>> =
        deviceRepository.updateDevice(token, id, jsonObject)

    override fun getLastPcb(token: String): Flow<Resource<Pcb>> = deviceRepository.getLastPcb(token)

    override fun getHistoryPcb(token: String): Flow<Resource<List<Pcb>>> =
        deviceRepository.getHistoryPcb(token)

    override fun getLastSensor(token: String): Flow<Resource<Sensor>> =
        deviceRepository.getLastSensor(token)

    override fun getHistorySensor(token: String): Flow<Resource<List<Sensor>>> =
        deviceRepository.getHistorySensor(token)

    override fun getLastSlca(token: String): Flow<Resource<Slca>> =
        deviceRepository.getLastSlca(token)

    override fun getHistorySlca(token: String): Flow<Resource<List<Slca>>> =
        deviceRepository.getHistorySlca(token)

    override fun getLastKwh1(token: String): Flow<Resource<Kwh1>> =
        deviceRepository.getLastKwh1(token)

    override fun getLastKwh3(token: String): Flow<Resource<Kwh3>> =
        deviceRepository.getLastKwh3(token)

    override fun getDeviceInfo(token: String): Flow<Resource<Device>> =
        deviceRepository.getDeviceInfo(token)

    override fun previewDevice(token: String): Flow<Resource<Device>> =
        deviceRepository.previewDevice(token)

    override fun pushDevice(token: String, jsonObject: JsonObject): Flow<Resource<Device>> =
        deviceRepository.pushDevice(token, jsonObject)

    override fun changeDeviceSsid(
        token: String,
        deviceId: Int,
        jsonObject: JsonObject
    ): Flow<Resource<Device>> = deviceRepository.changeDeviceSsid(token, deviceId, jsonObject)

    override fun totalKwh(token: String): Flow<Resource<TotalKwh>> =
        deviceRepository.totalKwh(token)

    override fun totalKwhOnGroup(token: String, groupId: Int): Flow<Resource<TotalKwh>> =
        deviceRepository.totalKwhOnGroup(token, groupId)

    override fun getListKwh(token: String): Flow<Resource<List<Device>>> =
        deviceRepository.getListKwh(token)

    override fun getListKwhOnGroup(token: String, groupId: Int): Flow<Resource<List<Device>>> =
        deviceRepository.getListKwhOnGroup(token, groupId)

    override fun calculateKwh(token: String, deviceToken: String): Flow<Resource<TotalKwh>> =
        deviceRepository.calculateKwh(token, deviceToken)

    override fun getDeviceUserNotification(token: String): Flow<Resource<List<DeviceNotification>>> =
        deviceRepository.getDeviceUserNotification(token)

    override fun getDeviceGuestNotification(token: String): Flow<Resource<List<DeviceNotification>>> =
        deviceRepository.getDeviceGuestNotification(token)

    override fun getListScheduler(
        token: String,
        deviceId: String
    ): Flow<Resource<List<Scheduler>>> = deviceRepository.getListScheduler(token, deviceId)

    override fun createScheduler(
        token: String,
        deviceId: String,
        jsonObject: JsonObject
    ): Flow<Resource<Scheduler>> = deviceRepository.createScheduler(token, deviceId, jsonObject)

    override fun deleteScheduler(
        token: String,
        schedulerId: String
    ): Flow<Resource<SingleSchedulerResponse>> = deviceRepository.deleteScheduler(token, schedulerId)
}