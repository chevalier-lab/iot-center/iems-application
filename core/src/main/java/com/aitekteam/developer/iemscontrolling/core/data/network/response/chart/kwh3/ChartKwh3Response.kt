package com.aitekteam.developer.iemscontrolling.core.data.network.response.chart.kwh3

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class ChartKwh3Response(
    @field:SerializedName("code")
    val code: Int? = null,

    @field:SerializedName("message")
    val message: String? = null,

    @field:SerializedName("data")
    val data: List<ChartKwh3>? = listOf()
) : Parcelable
