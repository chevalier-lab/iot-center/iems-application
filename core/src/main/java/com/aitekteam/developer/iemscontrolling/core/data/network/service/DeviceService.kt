package com.aitekteam.developer.iemscontrolling.core.data.network.service

import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.calculate.TotalKwhResponse
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.general.DeviceResponse
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.general.SingleDeviceResponse
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.kwh1.SingleKwh1Response
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.kwh3.SingleKwh3Response
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.notification.DeviceNotificationResponse
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.pcb.PcbResponse
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.pcb.SinglePcbResponse
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.sensor.SensorResponse
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.sensor.SingleSensorResponse
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.slca.SchedulerResponse
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.slca.SingleSchedulerResponse
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.slca.SingleSlcaResponse
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.slca.SlcaResponse
import com.aitekteam.developer.iemscontrolling.core.utils.Constants.ROUTE_CALCULATE_KWH
import com.aitekteam.developer.iemscontrolling.core.utils.Constants.ROUTE_CHANGE_DEVICE_SSID
import com.aitekteam.developer.iemscontrolling.core.utils.Constants.ROUTE_CREATE_SCHEDULER
import com.aitekteam.developer.iemscontrolling.core.utils.Constants.ROUTE_DELETE_SCHEDULER
import com.aitekteam.developer.iemscontrolling.core.utils.Constants.ROUTE_DEVICE_GUEST_NOTIFICATION
import com.aitekteam.developer.iemscontrolling.core.utils.Constants.ROUTE_DEVICE_USER_NOTIFICATION
import com.aitekteam.developer.iemscontrolling.core.utils.Constants.ROUTE_GET_LIST_DEVICE_GUEST
import com.aitekteam.developer.iemscontrolling.core.utils.Constants.ROUTE_INFO_DEVICE
import com.aitekteam.developer.iemscontrolling.core.utils.Constants.ROUTE_LAST_DEVICE
import com.aitekteam.developer.iemscontrolling.core.utils.Constants.ROUTE_LIST_DEVICE
import com.aitekteam.developer.iemscontrolling.core.utils.Constants.ROUTE_LIST_KWH
import com.aitekteam.developer.iemscontrolling.core.utils.Constants.ROUTE_LIST_KWH_ON_GROUP
import com.aitekteam.developer.iemscontrolling.core.utils.Constants.ROUTE_LIST_SCHEDULER
import com.aitekteam.developer.iemscontrolling.core.utils.Constants.ROUTE_PREVIEW_DEVICE
import com.aitekteam.developer.iemscontrolling.core.utils.Constants.ROUTE_PULL_DEVICE
import com.aitekteam.developer.iemscontrolling.core.utils.Constants.ROUTE_PUSH_DEVICE
import com.aitekteam.developer.iemscontrolling.core.utils.Constants.ROUTE_TOTAL_KWH
import com.aitekteam.developer.iemscontrolling.core.utils.Constants.ROUTE_TOTAL_KWH_ON_GROUP
import com.aitekteam.developer.iemscontrolling.core.utils.Constants.ROUTE_UPDATE_DEVICE
import com.google.gson.JsonObject
import retrofit2.Response
import retrofit2.http.*

interface DeviceService {

    @GET(ROUTE_LIST_DEVICE)
    suspend fun getListDevice(
        @Header("Authorization") auth: String
    ): DeviceResponse

    @GET(ROUTE_GET_LIST_DEVICE_GUEST)
    suspend fun getListDeviceGuest(
        @Header("Authorization") auth: String
    ): DeviceResponse

    @POST(ROUTE_UPDATE_DEVICE)
    suspend fun updateDevice(
        @Header("Authorization") auth: String,
        @Path("id") id: Int,
        @Body jsonObject: JsonObject
    ): SingleDeviceResponse

    @GET(ROUTE_LAST_DEVICE)
    suspend fun getLastPcb(
        @Query("device_token") deviceToken: String
    ): SinglePcbResponse

    @GET(ROUTE_PULL_DEVICE)
    suspend fun getHistoryPcb(
        @Query("device_token") deviceToken: String
    ): PcbResponse

    @GET(ROUTE_PULL_DEVICE)
    suspend fun getHistoryPcbPaging(
        @Query("device_token") deviceToken: String,
        @Query("page") page: Int
    ): Response<PcbResponse>

    @GET(ROUTE_LAST_DEVICE)
    suspend fun getLastSensor(
        @Query("device_token") deviceToken: String
    ): SingleSensorResponse

    @GET(ROUTE_PULL_DEVICE)
    suspend fun getHistorySensor(
        @Query("device_token") deviceToken: String
    ): SensorResponse

    @GET(ROUTE_PULL_DEVICE)
    suspend fun getHistorySensorPaging(
        @Query("device_token") deviceToken: String,
        @Query("page") page: Int
    ): Response<SensorResponse>

    @GET(ROUTE_LAST_DEVICE)
    suspend fun getLastSlca(
        @Query("device_token") deviceToken: String
    ): SingleSlcaResponse

    @GET(ROUTE_PULL_DEVICE)
    suspend fun getHistorySlca(
        @Query("device_token") deviceToken: String
    ): SlcaResponse

    @GET(ROUTE_PULL_DEVICE)
    suspend fun getHistorySlcaPaging(
        @Query("device_token") deviceToken: String,
        @Query("page") page: Int
    ): Response<SlcaResponse>

    @GET(ROUTE_LAST_DEVICE)
    suspend fun getLastKwh1(
        @Query("device_token") deviceToken: String
    ): SingleKwh1Response

    @GET(ROUTE_LAST_DEVICE)
    suspend fun getLastKwh3(
        @Query("device_token") deviceToken: String
    ): SingleKwh3Response

    @GET(ROUTE_INFO_DEVICE)
    suspend fun getDeviceInfo(
        @Query("device_token") deviceToken: String
    ): SingleDeviceResponse

    @GET(ROUTE_PREVIEW_DEVICE)
    suspend fun previewDevice(
        @Query("device_token") deviceToken: String
    ): SingleDeviceResponse

    @POST(ROUTE_PUSH_DEVICE)
    suspend fun pushDevice(
        @Query("device_token") deviceToken: String,
        @Body jsonObject: JsonObject
    ): SingleDeviceResponse

    @POST(ROUTE_CHANGE_DEVICE_SSID)
    suspend fun changeDeviceSsid(
        @Header("Authorization") auth: String,
        @Path("device_id") deviceId: Int,
        @Body jsonObject: JsonObject
    ): SingleDeviceResponse

    @GET(ROUTE_TOTAL_KWH)
    suspend fun totalKwh(
        @Header("Authorization") auth: String
    ): TotalKwhResponse

    @GET(ROUTE_LIST_KWH)
    suspend fun listKwh(
        @Header("Authorization") auth: String
    ): DeviceResponse

    @GET(ROUTE_LIST_KWH)
    suspend fun listKwhPaging(
        @Header("Authorization") auth: String,
        @Query("page") page: Int
    ): Response<DeviceResponse>

    @GET(ROUTE_TOTAL_KWH_ON_GROUP)
    suspend fun totalKwhOnGroup(
        @Header("Authorization") auth: String,
        @Path("group_id") id: Int
    ): TotalKwhResponse

    @GET(ROUTE_LIST_KWH_ON_GROUP)
    suspend fun listKwhOnGroup(
        @Header("Authorization") auth: String,
        @Path("group_id") id: Int
    ): DeviceResponse

    @GET(ROUTE_LIST_KWH_ON_GROUP)
    suspend fun listKwhOnGroupPaging(
        @Header("Authorization") auth: String,
        @Path("group_id") id: Int,
        @Query("page") page: Int
    ): Response<DeviceResponse>

    @GET(ROUTE_CALCULATE_KWH)
    suspend fun calculateKwh(
        @Header("Authorization") auth: String,
        @Path("device_token") token: String
    ): TotalKwhResponse

    @GET(ROUTE_DEVICE_USER_NOTIFICATION)
    suspend fun getDeviceUserNotification(
        @Header("Authorization") auth: String
    ): DeviceNotificationResponse

    @GET(ROUTE_DEVICE_GUEST_NOTIFICATION)
    suspend fun getDeviceGuestNotification(
        @Header("Authorization") auth: String
    ): DeviceNotificationResponse

    @GET(ROUTE_LIST_SCHEDULER)
    suspend fun getListScheduler(
        @Header("Authorization") auth: String,
        @Path("device_id") deviceId: String,
    ): SchedulerResponse

    @POST(ROUTE_CREATE_SCHEDULER)
    suspend fun createScheduler(
        @Header("Authorization") auth: String,
        @Path("device_id") deviceId: String,
        @Body jsonObject: JsonObject
    ): SingleSchedulerResponse

    @GET(ROUTE_DELETE_SCHEDULER)
    suspend fun deleteScheduler(
        @Header("Authorization") auth: String,
        @Path("scheduler_id") schedulerId: String
    ): SingleSchedulerResponse
}