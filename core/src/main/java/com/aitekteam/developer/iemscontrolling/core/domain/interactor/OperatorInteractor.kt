package com.aitekteam.developer.iemscontrolling.core.domain.interactor

import com.aitekteam.developer.iemscontrolling.core.data.Resource
import com.aitekteam.developer.iemscontrolling.core.data.network.response.operator.ActivationDevice
import com.aitekteam.developer.iemscontrolling.core.data.network.response.operator.ActivationUser
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.profile.User
import com.aitekteam.developer.iemscontrolling.core.data.repository.OperatorRepository
import com.aitekteam.developer.iemscontrolling.core.domain.usecase.OperatorUseCase
import com.google.gson.JsonObject
import kotlinx.coroutines.flow.Flow

class OperatorInteractor(
    private val operatorRepository: OperatorRepository
) : OperatorUseCase {
    override fun getListCustomer(token: String): Flow<Resource<List<User>>> =
        operatorRepository.getListCustomer(token)

    override fun getListActiveCustomer(token: String): Flow<Resource<List<User>>> =
        operatorRepository.getListActiveCustomer(token)

    override fun activateCustomer(
        token: String,
        jsonObject: JsonObject
    ): Flow<Resource<ActivationUser>> = operatorRepository.activateCustomer(token, jsonObject)

    override fun activateDevice(
        token: String,
        jsonObject: JsonObject
    ): Flow<Resource<ActivationDevice>> = operatorRepository.activateDevice(token, jsonObject)
}