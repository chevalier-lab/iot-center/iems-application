package com.aitekteam.developer.iemscontrolling.core.data.network.service

import com.aitekteam.developer.iemscontrolling.core.data.network.response.ticketing.CreateTicketingResponse
import com.aitekteam.developer.iemscontrolling.core.data.network.response.ticketing.ListTicketingResponse
import com.aitekteam.developer.iemscontrolling.core.data.network.response.ticketing.ReplyTicketResponse
import com.aitekteam.developer.iemscontrolling.core.data.network.response.ticketing.SingleReplyTicketResponse
import com.aitekteam.developer.iemscontrolling.core.utils.Constants.ROUTE_CREATE_COMPLAIN
import com.aitekteam.developer.iemscontrolling.core.utils.Constants.ROUTE_GET_LIST_COMPLAIN
import com.aitekteam.developer.iemscontrolling.core.utils.Constants.ROUTE_GET_LIST_REPLY
import com.aitekteam.developer.iemscontrolling.core.utils.Constants.ROUTE_REPLY_COMPLAIN
import com.google.gson.JsonObject
import retrofit2.Response
import retrofit2.http.*

interface TicketingService {

    @POST(ROUTE_CREATE_COMPLAIN)
    suspend fun createComplain(
        @Header("Authorization") auth: String,
        @Body jsonObject: JsonObject
    ) : CreateTicketingResponse

    @GET(ROUTE_GET_LIST_COMPLAIN)
    suspend fun getListComplain(
        @Header("Authorization") auth: String
    ) : ListTicketingResponse

    @GET(ROUTE_GET_LIST_COMPLAIN)
    suspend fun getListComplainPaging(
        @Header("Authorization") auth: String,
        @Query("order-direction") direction: String,
        @Query("page") page: Int
    ) : Response<ListTicketingResponse>

    @POST(ROUTE_REPLY_COMPLAIN)
    suspend fun replyComplaint(
        @Header("Authorization") auth: String,
        @Path("ticket_id") id: Int,
        @Body jsonObject: JsonObject
    ) : SingleReplyTicketResponse

    @GET(ROUTE_GET_LIST_REPLY)
    suspend fun getListReply(
        @Header("Authorization") auth: String,
        @Path("ticket_id") id: Int
    ) : ReplyTicketResponse
}