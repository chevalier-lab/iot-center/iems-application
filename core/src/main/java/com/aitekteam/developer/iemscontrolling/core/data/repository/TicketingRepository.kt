package com.aitekteam.developer.iemscontrolling.core.data.repository

import com.aitekteam.developer.iemscontrolling.core.data.Resource
import com.aitekteam.developer.iemscontrolling.core.data.network.response.ticketing.CreateTicketing
import com.aitekteam.developer.iemscontrolling.core.data.network.response.ticketing.ReplyTicket
import com.aitekteam.developer.iemscontrolling.core.data.network.response.ticketing.Ticketing
import com.aitekteam.developer.iemscontrolling.core.data.network.source.RemoteTicketingDataSource
import com.aitekteam.developer.iemscontrolling.core.domain.repository.ITicketingRepository
import com.aitekteam.developer.iemscontrolling.core.utils.vo.ApiResponse
import com.google.gson.JsonObject
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flow

class TicketingRepository(
    private val remoteTicketingDataSource: RemoteTicketingDataSource
) : ITicketingRepository {

    override fun createComplain(
        authorization: String,
        jsonObject: JsonObject
    ): Flow<Resource<CreateTicketing>> = flow {
        emit(Resource.loading<CreateTicketing>())
        val response = remoteTicketingDataSource.createComplain(authorization, jsonObject)

        when (val apiResponse = response.first()) {
            is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
            is ApiResponse.Empty -> emit(Resource.success(CreateTicketing()))
            is ApiResponse.Error -> emit(Resource.error<CreateTicketing>(apiResponse.errorMessage))
        }
    }

    override fun getListComplain(authorization: String): Flow<Resource<List<Ticketing>>> = flow {
        emit(Resource.loading<List<Ticketing>>())
        val response = remoteTicketingDataSource.getListComplain(authorization)

        when (val apiResponse = response.first()) {
            is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
            is ApiResponse.Empty -> emit(Resource.success(listOf<Ticketing>()))
            is ApiResponse.Error -> emit(Resource.error<List<Ticketing>>(apiResponse.errorMessage))
        }
    }

    override fun replyComplaint(
        authorization: String,
        ticketId: Int,
        jsonObject: JsonObject
    ): Flow<Resource<ReplyTicket>> = flow {
        emit(Resource.loading<ReplyTicket>())
        val response = remoteTicketingDataSource.replyComplaint(authorization, ticketId, jsonObject)

        when (val apiResponse = response.first()) {
            is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
            is ApiResponse.Empty -> emit(Resource.success(ReplyTicket()))
            is ApiResponse.Error -> emit(Resource.error<ReplyTicket>(apiResponse.errorMessage))
        }
    }

    override fun getListReply(
        authorization: String,
        ticketId: Int
    ): Flow<Resource<List<ReplyTicket>>> = flow {
        emit(Resource.loading<List<ReplyTicket>>())
        val response = remoteTicketingDataSource.getListReply(authorization, ticketId)

        when (val apiResponse = response.first()) {
            is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
            is ApiResponse.Empty -> emit(Resource.success(listOf<ReplyTicket>()))
            is ApiResponse.Error -> emit(Resource.error<List<ReplyTicket>>(apiResponse.errorMessage))
        }
    }
}