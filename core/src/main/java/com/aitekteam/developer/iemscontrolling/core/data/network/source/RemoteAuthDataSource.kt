package com.aitekteam.developer.iemscontrolling.core.data.network.source

import com.aitekteam.developer.iemscontrolling.core.data.network.response.auth.Login
import com.aitekteam.developer.iemscontrolling.core.data.network.response.auth.Register
import com.aitekteam.developer.iemscontrolling.core.data.network.service.AuthService
import com.aitekteam.developer.iemscontrolling.core.utils.vo.ApiResponse
import com.google.gson.JsonObject
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import timber.log.Timber

class RemoteAuthDataSource(
    private val authService: AuthService
) {

    fun login(jsonObject: JsonObject): Flow<ApiResponse<Login>> = flow {
        try {
            val response = authService.login(jsonObject)
            val data = response.data

            data?.let {
                if (response.code == 200)
                    emit(ApiResponse.Success(it))
                else
                    emit(ApiResponse.Error(response.message!!))
            }
        } catch (e: Exception) {
            emit(ApiResponse.Error(e.toString()))
            Timber.e("onFailure : $e")
        }
    }.flowOn(Dispatchers.IO)

    fun register(jsonObject: JsonObject): Flow<ApiResponse<Register>> = flow {
        try {
            val response = authService.register(jsonObject)
            val data = response.data

            data?.let {
                if (response.code == 200)
                    emit(ApiResponse.Success(it))
                else
                    emit(ApiResponse.Error(response.message!!))
            }
        } catch (e: Exception) {
            emit(ApiResponse.Error(e.toString()))
            Timber.e("onFailure : $e")
        }
    }.flowOn(Dispatchers.IO)
}