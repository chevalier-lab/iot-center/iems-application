package com.aitekteam.developer.iemscontrolling.core.data.network.response.ticketing

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class CreateTicketing(
    @field:SerializedName("phone_number")
    val phoneNumber: String? = null,

    @field:SerializedName("title")
    val title: String? = null,

    @field:SerializedName("description")
    val description: String? = null,

    @field:SerializedName("status")
    val status: String? = null
) : Parcelable
