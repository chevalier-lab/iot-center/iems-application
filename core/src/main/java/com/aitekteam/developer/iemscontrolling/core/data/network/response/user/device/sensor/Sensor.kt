package com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.sensor

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class Sensor(
    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("id_m_devices")
    val deviceId: Int? = null,

    @field:SerializedName("pir")
    val pir: Int? = null,

    @field:SerializedName("temp")
    val temp: Int? = null,

    @field:SerializedName("hum")
    val hum: Int? = null,

    @field:SerializedName("lux")
    val lux: Int? = null,

    @field:SerializedName("date")
    val date: String? = null,

    @field:SerializedName("time")
    val time: String? = null
) : Parcelable
