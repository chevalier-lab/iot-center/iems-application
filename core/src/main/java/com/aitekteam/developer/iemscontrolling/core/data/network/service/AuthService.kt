package com.aitekteam.developer.iemscontrolling.core.data.network.service

import com.aitekteam.developer.iemscontrolling.core.data.network.response.auth.LoginResponse
import com.aitekteam.developer.iemscontrolling.core.data.network.response.auth.RegisterResponse
import com.aitekteam.developer.iemscontrolling.core.utils.Constants.ROUTE_LOGIN
import com.aitekteam.developer.iemscontrolling.core.utils.Constants.ROUTE_REGISTER
import com.google.gson.JsonObject
import retrofit2.http.Body
import retrofit2.http.POST

interface AuthService {
    @POST(ROUTE_LOGIN)
    suspend fun login(
        @Body jsonObject: JsonObject
    ) : LoginResponse

    @POST(ROUTE_REGISTER)
    suspend fun register(
        @Body jsonObject: JsonObject
    ) : RegisterResponse
}