package com.aitekteam.developer.iemscontrolling.core.data.network.service

import com.aitekteam.developer.iemscontrolling.core.data.network.response.media.MediaResponse
import com.aitekteam.developer.iemscontrolling.core.data.network.response.operator.ListUserResponse
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.general.DeviceResponse
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.general.SingleDeviceResponse
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.group.GroupResponse
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.group.SingleGroupResponse
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.invite.InviteResponse
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.profile.UserInfoResponse
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.profile.UserResponse
import com.aitekteam.developer.iemscontrolling.core.utils.Constants.ROUTE_ADD_DEVICE_GUEST
import com.aitekteam.developer.iemscontrolling.core.utils.Constants.ROUTE_APPEND_DEVICE_GROUP
import com.aitekteam.developer.iemscontrolling.core.utils.Constants.ROUTE_CHANGE_USER_INFO
import com.aitekteam.developer.iemscontrolling.core.utils.Constants.ROUTE_CREATE_GROUP
import com.aitekteam.developer.iemscontrolling.core.utils.Constants.ROUTE_CREATE_MEDIA
import com.aitekteam.developer.iemscontrolling.core.utils.Constants.ROUTE_DETAIL_GROUP
import com.aitekteam.developer.iemscontrolling.core.utils.Constants.ROUTE_DETAIL_GUEST
import com.aitekteam.developer.iemscontrolling.core.utils.Constants.ROUTE_FILTER_DEVICE_GROUP
import com.aitekteam.developer.iemscontrolling.core.utils.Constants.ROUTE_GET_PROFILE
import com.aitekteam.developer.iemscontrolling.core.utils.Constants.ROUTE_INVITE_GUEST
import com.aitekteam.developer.iemscontrolling.core.utils.Constants.ROUTE_LIST_DEVICE_GUEST
import com.aitekteam.developer.iemscontrolling.core.utils.Constants.ROUTE_LIST_GROUP
import com.aitekteam.developer.iemscontrolling.core.utils.Constants.ROUTE_LIST_GUEST
import com.aitekteam.developer.iemscontrolling.core.utils.Constants.ROUTE_REMOVE_DEVICE_GROUP
import com.aitekteam.developer.iemscontrolling.core.utils.Constants.ROUTE_SUBS_GUEST
import com.aitekteam.developer.iemscontrolling.core.utils.Constants.ROUTE_UPDATE_GROUP
import com.aitekteam.developer.iemscontrolling.core.utils.Constants.ROUTE_UPDATE_PHOTO_PROFILE
import com.aitekteam.developer.iemscontrolling.core.utils.Constants.ROUTE_UPDATE_PROFILE
import com.aitekteam.developer.iemscontrolling.core.utils.Constants.ROUTE_USER_INFO
import com.google.gson.JsonObject
import okhttp3.MultipartBody
import retrofit2.Response
import retrofit2.http.*

interface UserService {

    @GET(ROUTE_GET_PROFILE)
    suspend fun getProfile(
        @Header("Authorization") auth: String
    ) : UserResponse

    @POST(ROUTE_UPDATE_PROFILE)
    suspend fun updateProfile(
        @Header("Authorization") auth: String,
        @Body jsonObject: JsonObject
    ) : UserResponse

    @POST(ROUTE_UPDATE_PHOTO_PROFILE)
    suspend fun updatePhotoProfile(
        @Header("Authorization") auth: String,
        @Query("id_cover") coverId: String
    ) : UserResponse

    @GET(ROUTE_USER_INFO)
    suspend fun getUserInfo(
        @Header("Authorization") auth: String
    ) : UserInfoResponse

    @POST(ROUTE_CHANGE_USER_INFO)
    suspend fun changeUserInfo(
        @Header("Authorization") auth: String,
        @Body jsonObject: JsonObject
    ) : UserInfoResponse

    @GET(ROUTE_SUBS_GUEST)
    suspend fun requestActivate(
        @Header("Authorization") auth: String,
        @Query("request_type") deviceToken: String
    ) : UserResponse

    @POST(ROUTE_INVITE_GUEST)
    suspend fun inviteGuest(
        @Header("Authorization") auth: String,
        @Body jsonObject: JsonObject
    ) : InviteResponse

    @GET(ROUTE_LIST_GUEST)
    suspend fun listGuest(
        @Header("Authorization") auth: String
    ) : ListUserResponse

    @GET(ROUTE_LIST_GUEST)
    suspend fun listGuestPaging(
        @Header("Authorization") auth: String,
        @Query("page") page: Int
    ) : Response<ListUserResponse>

    @GET(ROUTE_DETAIL_GUEST)
    suspend fun detailGuest(
        @Header("Authorization") auth: String,
        @Query("phone_number") phoneNumber: String
    ) : UserResponse

    @GET(ROUTE_LIST_GROUP)
    suspend fun getListGroup(
        @Header("Authorization") auth: String
    ) : GroupResponse

    @GET(ROUTE_LIST_GROUP)
    suspend fun getListGroupPaging(
        @Header("Authorization") auth: String,
        @Query("page") page: Int
    ) : Response<GroupResponse>

    @POST(ROUTE_CREATE_GROUP)
    suspend fun createGroup(
        @Header("Authorization") auth: String,
        @Body jsonObject: JsonObject
    ) : SingleGroupResponse

    @POST(ROUTE_UPDATE_GROUP)
    suspend fun updateGroup(
        @Header("Authorization") auth: String,
        @Path("group_id") groupId: Int,
        @Body jsonObject: JsonObject
    ) : SingleGroupResponse

    @GET(ROUTE_DETAIL_GROUP)
    suspend fun getDetailGroup(
        @Header("Authorization") auth: String,
        @Path("group_id") groupId: Int
    ) : DeviceResponse

    @GET(ROUTE_FILTER_DEVICE_GROUP)
    suspend fun filterDeviceGroup(
        @Header("Authorization") auth: String,
        @Path("group_id") groupId: Int,
        @Query("search") search: String,
        @Query("page") page: String,
        @Query("type") type: String,
        @Query("isAlreadyAdded") isAdded: String,
        @Query("orderDirection") direction: String
    ) : DeviceResponse

    @GET(ROUTE_FILTER_DEVICE_GROUP)
    suspend fun filterDeviceGroupPaging(
        @Header("Authorization") auth: String,
        @Path("group_id") groupId: Int,
        @Query("search") search: String,
        @Query("page") page: Int,
        @Query("type") type: String,
        @Query("isAlreadyAdded") isAdded: String,
        @Query("orderDirection") direction: String
    ) : Response<DeviceResponse>

    @GET(ROUTE_APPEND_DEVICE_GROUP)
    suspend fun appendDevice(
        @Header("Authorization") auth: String,
        @Path("group_id") groupId: Int,
        @Path("device_id") deviceId: Int
    ) : SingleGroupResponse

    @GET(ROUTE_REMOVE_DEVICE_GROUP)
    suspend fun removeDevice(
        @Header("Authorization") auth: String,
        @Path("group_id") groupId: Int,
        @Path("device_id") deviceId: Int
    ) : SingleGroupResponse

    @Multipart
    @POST(ROUTE_CREATE_MEDIA)
    suspend fun createMedia(
        @Header("Authorization") authorization: String,
        @Part photo: MultipartBody.Part
    ) : MediaResponse

    @GET(ROUTE_LIST_DEVICE_GUEST)
    suspend fun getDeviceGuest(
        @Header("Authorization") authorization: String,
        @Query("phone_number") phoneNumber: String
    ) : DeviceResponse

    @GET(ROUTE_LIST_DEVICE_GUEST)
    suspend fun getDeviceGuestPaging(
        @Header("Authorization") authorization: String,
        @Query("phone_number") phoneNumber: String,
        @Query("page") page: Int
    ) : Response<DeviceResponse>

    @POST(ROUTE_ADD_DEVICE_GUEST)
    suspend fun addDeviceGuest(
        @Header("Authorization") authorization: String,
        @Body jsonObject: JsonObject
    ) : SingleDeviceResponse
}