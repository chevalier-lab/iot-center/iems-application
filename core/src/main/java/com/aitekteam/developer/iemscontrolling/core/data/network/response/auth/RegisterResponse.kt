package com.aitekteam.developer.iemscontrolling.core.data.network.response.auth

import android.os.Parcelable
import com.aitekteam.developer.iemscontrolling.core.data.network.response.auth.Register
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class RegisterResponse(
    @field:SerializedName("code")
    val code: Int? = null,

    @field:SerializedName("message")
    val message: String? = null,

    @field:SerializedName("data")
    val data: Register? = Register(),
) : Parcelable
