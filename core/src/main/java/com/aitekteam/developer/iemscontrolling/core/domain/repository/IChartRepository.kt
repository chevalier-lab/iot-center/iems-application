package com.aitekteam.developer.iemscontrolling.core.domain.repository

import com.aitekteam.developer.iemscontrolling.core.data.Resource
import com.aitekteam.developer.iemscontrolling.core.data.network.response.chart.kwh1.ChartKwh1
import com.aitekteam.developer.iemscontrolling.core.data.network.response.chart.kwh3.ChartKwh3
import kotlinx.coroutines.flow.Flow

interface IChartRepository {

    fun getChartKwh1Day(
        deviceToken: String,
        param: String,
        date: String,
        time: String
    ): Flow<Resource<List<ChartKwh1>>>

    fun getChartKwh1Month(
        deviceToken: String,
        param: String,
        date: String
    ): Flow<Resource<List<ChartKwh1>>>

    fun getChartKwh1Year(
        deviceToken: String,
        param: String
    ): Flow<Resource<List<ChartKwh1>>>

    fun getChartKwh1Realtime(
        deviceToken: String,
        param: String
    ): Flow<Resource<List<ChartKwh1>>>

    fun getChartKwh3Day(
        deviceToken: String,
        param: String,
        date: String,
        time: String
    ): Flow<Resource<List<ChartKwh3>>>

    fun getChartKwh3Month(
        deviceToken: String,
        param: String,
        date: String
    ): Flow<Resource<List<ChartKwh3>>>

    fun getChartKwh3Year(
        deviceToken: String,
        param: String
    ): Flow<Resource<List<ChartKwh3>>>

    fun getChartKwh3Realtime(
        deviceToken: String,
        param: String
    ): Flow<Resource<List<ChartKwh3>>>
}