package com.aitekteam.developer.iemscontrolling.core.data.network.response.chart.kwh1

import android.os.Parcelable
import com.aitekteam.developer.iemscontrolling.core.data.network.response.chart.ChartMeta
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class ChartKwh1Response(
    @field:SerializedName("code")
    val code: Int? = null,

    @field:SerializedName("message")
    val message: String? = null,

    @field:SerializedName("data")
    val data: List<ChartKwh1>? = listOf(),

    @field:SerializedName("meta")
    val meta: ChartMeta? = ChartMeta()
) : Parcelable
