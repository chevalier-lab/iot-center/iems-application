package com.aitekteam.developer.iemscontrolling.core.data.repository

import com.aitekteam.developer.iemscontrolling.core.data.Resource
import com.aitekteam.developer.iemscontrolling.core.data.network.response.operator.ActivationDevice
import com.aitekteam.developer.iemscontrolling.core.data.network.response.operator.ActivationUser
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.profile.User
import com.aitekteam.developer.iemscontrolling.core.data.network.source.RemoteOperatorDataSource
import com.aitekteam.developer.iemscontrolling.core.domain.repository.IOperatoryRepository
import com.aitekteam.developer.iemscontrolling.core.utils.vo.ApiResponse
import com.google.gson.JsonObject
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flow

class OperatorRepository(
    private val remoteOperatorDataSource: RemoteOperatorDataSource
) : IOperatoryRepository {

    override fun getListCustomer(token: String): Flow<Resource<List<User>>> = flow {
        emit(Resource.loading<List<User>>())
        val response = remoteOperatorDataSource.getListCustomer(token)

        when (val apiResponse = response.first()) {
            is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
            is ApiResponse.Empty -> emit(Resource.success(listOf<User>()))
            is ApiResponse.Error -> emit(Resource.error<List<User>>(apiResponse.errorMessage))
        }
    }

    override fun getListActiveCustomer(token: String): Flow<Resource<List<User>>> = flow {
        emit(Resource.loading<List<User>>())
        val response = remoteOperatorDataSource.getListActiveCustomer(token)

        when (val apiResponse = response.first()) {
            is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
            is ApiResponse.Empty -> emit(Resource.success(listOf<User>()))
            is ApiResponse.Error -> emit(Resource.error<List<User>>(apiResponse.errorMessage))
        }
    }

    override fun activateCustomer(
        token: String,
        jsonObject: JsonObject
    ): Flow<Resource<ActivationUser>> = flow {
        emit(Resource.loading<ActivationUser>())
        val response = remoteOperatorDataSource.activateCustomer(token, jsonObject)

        when (val apiResponse = response.first()) {
            is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
            is ApiResponse.Empty -> emit(Resource.success(ActivationUser()))
            is ApiResponse.Error -> emit(Resource.error<ActivationUser>(apiResponse.errorMessage))
        }
    }

    override fun activateDevice(
        token: String,
        jsonObject: JsonObject
    ): Flow<Resource<ActivationDevice>> = flow {
        emit(Resource.loading<ActivationDevice>())
        val response = remoteOperatorDataSource.activateDevice(token, jsonObject)

        when (val apiResponse = response.first()) {
            is ApiResponse.Success -> emit(Resource.success(apiResponse.data))
            is ApiResponse.Empty -> emit(Resource.success(ActivationDevice()))
            is ApiResponse.Error -> emit(Resource.error<ActivationDevice>(apiResponse.errorMessage))
        }
    }
}