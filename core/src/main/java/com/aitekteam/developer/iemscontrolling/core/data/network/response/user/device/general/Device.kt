package com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.general

import android.os.Parcelable
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.calculate.TotalKwh
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class Device(
    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("device_name")
    val name: String? = null,

    @field:SerializedName("device_shortname")
    val shortName: String? = null,

    @field:SerializedName("device_token")
    val token: String? = null,

    @field:SerializedName("device_type")
    val type: String? = null,

    @field:SerializedName("device_status")
    val status: String? = null,

    @field:SerializedName("device_is_connect")
    val isConnect: String? = null,

    @field:SerializedName("created_at")
    val date: String? = null,

    @field:SerializedName("updated_at")
    val lastDate: String? = null,

    @field:SerializedName("ssid_name_device")
    val deviceSsid: String? = null,

    @field:SerializedName("ssid_password_device")
    val devicePassword: String? = null,

    @field:SerializedName("ssid_name")
    val userSsid: String? = null,

    @field:SerializedName("ssid_password")
    val userPassword: String? = null,

    @field:SerializedName("id_group")
    val groupId: String? = null,

    @field:SerializedName("is_already_added")
    val isAdded: String? = null,

    @field:SerializedName("preview")
    val preview: TotalKwh? = TotalKwh()
) : Parcelable
