package com.aitekteam.developer.iemscontrolling.core.domain.repository

import com.aitekteam.developer.iemscontrolling.core.data.Resource
import com.aitekteam.developer.iemscontrolling.core.data.network.response.media.Media
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.general.Device
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.group.Group
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.invite.Invite
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.profile.User
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.profile.UserInfo
import com.google.gson.JsonObject
import kotlinx.coroutines.flow.Flow
import okhttp3.MultipartBody

interface IUserRepository {

    fun getData(authorization: String): Flow<Resource<User>>

    fun updateProfile(authorization: String, jsonObject: JsonObject): Flow<Resource<User>>

    fun updatePhotoProfile(authorization: String, coverId: String): Flow<Resource<User>>

    fun getUserInfo(token: String): Flow<Resource<UserInfo>>

    fun changeUserInfo(token: String, jsonObject: JsonObject): Flow<Resource<UserInfo>>

    fun requestActivate(authorization: String, requestType: String): Flow<Resource<User>>

    fun inviteGuest(token: String, jsonObject: JsonObject): Flow<Resource<Invite>>

    fun listGuest(token: String): Flow<Resource<List<User>>>

    fun detailGuest(token: String, phoneNumber: String): Flow<Resource<User>>

    fun getListGroup(token: String): Flow<Resource<List<Group>>>

    fun createGroup(token: String, jsonObject: JsonObject): Flow<Resource<Group>>

    fun updateGroup(token: String, groupId: Int, jsonObject: JsonObject): Flow<Resource<Group>>

    fun getDetailGroup(token: String, groupId: Int): Flow<Resource<List<Device>>>

    fun filterDeviceGroup(
        token: String,
        groupId: Int,
        search: String,
        page: String,
        type: String,
        isAdded: String,
        direction: String
    ): Flow<Resource<List<Device>>>

    fun appendDevice(token: String, groupId: Int, deviceId: Int): Flow<Resource<Group>>

    fun removeDevice(token: String, groupId: Int, deviceId: Int): Flow<Resource<Group>>

    fun createMedia(token: String, photo: MultipartBody.Part): Flow<Resource<Media>>

    fun getDeviceGuest(token: String, phoneNumber: String): Flow<Resource<List<Device>>>

    fun addDeviceGuest(token: String, jsonObject: JsonObject): Flow<Resource<Device>>
}