package com.aitekteam.developer.iemscontrolling.core.data.network.response.chart.kwh3

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class ChartKwh3Child(
    @field:SerializedName("va")
    val va: Float? = null,

    @field:SerializedName("vb")
    val vb: Float? = null,

    @field:SerializedName("vc")
    val vc: Float? = null,

    @field:SerializedName("vab")
    val vab: Float? = null,

    @field:SerializedName("vbc")
    val vbc: Float? = null,

    @field:SerializedName("vca")
    val vca: Float? = null,

    @field:SerializedName("ia")
    val ia: Float? = null,

    @field:SerializedName("ib")
    val ib: Float? = null,

    @field:SerializedName("ic")
    val ic: Float? = null,

    @field:SerializedName("pa")
    val pa: Float? = null,

    @field:SerializedName("pb")
    val pb: Float? = null,

    @field:SerializedName("pc")
    val pc: Float? = null,

    @field:SerializedName("pt")
    val pt: Float? = null,

    @field:SerializedName("qa")
    val qa: Float? = null,

    @field:SerializedName("qb")
    val qb: Float? = null,

    @field:SerializedName("qc")
    val qc: Float? = null,

    @field:SerializedName("qt")
    val qt: Float? = null,

    @field:SerializedName("sa")
    val sa: Float? = null,

    @field:SerializedName("sb")
    val sb: Float? = null,

    @field:SerializedName("sc")
    val sc: Float? = null,

    @field:SerializedName("st")
    val st: Float? = null,

    @field:SerializedName("pfa")
    val pfa: Float? = null,

    @field:SerializedName("pfb")
    val pfb: Float? = null,

    @field:SerializedName("pfc")
    val pfc: Float? = null,

    @field:SerializedName("freq")
    val freq: Float? = null,

    @field:SerializedName("ep")
    var ep: Float? = null,

    @field:SerializedName("eq")
    var eq: Float? = null
) : Parcelable
