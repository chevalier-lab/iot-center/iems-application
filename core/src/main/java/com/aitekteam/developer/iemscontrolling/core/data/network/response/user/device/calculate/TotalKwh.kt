package com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.calculate

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class TotalKwh(
    @field:SerializedName("total_kwh")
    val total: Double? = null,

    @field:SerializedName("price")
    val price: Double? = null,

    @field:SerializedName("price_str")
    val priceStr: String? = null
) : Parcelable
