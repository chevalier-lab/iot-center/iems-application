package com.aitekteam.developer.iemscontrolling.ui.user.complaint

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.aitekteam.developer.iemscontrolling.R
import com.aitekteam.developer.iemscontrolling.core.utils.vo.Status
import com.aitekteam.developer.iemscontrolling.databinding.FragmentComplainBinding
import com.aitekteam.developer.iemscontrolling.utils.dialog.CustomDialogFragment
import com.aitekteam.developer.iemscontrolling.utils.helper.Constants.AUTH_TOKEN
import com.aitekteam.developer.iemscontrolling.utils.helper.SharedPrefsUtils
import com.github.dhaval2404.imagepicker.ImagePicker
import com.google.android.material.snackbar.Snackbar
import com.google.gson.JsonObject
import kotlinx.android.synthetic.main.activity_host.*
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import org.koin.android.viewmodel.ext.android.viewModel
import java.io.File

class ComplainFragment : Fragment() {
    private var _binding: FragmentComplainBinding? = null
    private val binding get() = _binding!!
    private val viewModel: ComplainViewModel by viewModel()

    // utils
    private var titleValid = false
    private var descriptionValid = false
    private var imgComplaintFile: File? = null
    private lateinit var sharedPrefs: SharedPrefsUtils

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentComplainBinding.inflate(inflater, container, false)

        // init var
        sharedPrefs = SharedPrefsUtils()
        sharedPrefs.start(requireActivity(), AUTH_TOKEN)

        activity?.toolbar_title?.text = getString(R.string.complain_title)

        setupUI()
        // Inflate the layout for this fragment
        return binding.root
    }

    private fun setupUI() {
        // validate input
        binding.etTitle.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun afterTextChanged(p0: Editable?) {}

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                titleValid = p0.toString().trim().isNotEmpty()
                inputCheck()
            }
        })

        binding.etDescription.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun afterTextChanged(p0: Editable?) {}

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                descriptionValid = p0.toString().trim().isNotEmpty()
                inputCheck()
            }
        })

        binding.btnPickImage.setOnClickListener { pickGalleryImage() }

        binding.btnSubmit.setOnClickListener { showDialogToProcess() }
    }

    private fun inputCheck() {
        if (titleValid && descriptionValid) {
            binding.btnSubmit.isEnabled = true
            binding.btnSubmit.setBackgroundColor(
                ContextCompat.getColor(
                    requireContext(),
                    R.color.colorPrimary
                )
            )
        } else {
            binding.btnSubmit.isEnabled = false
            binding.btnSubmit.setBackgroundColor(
                ContextCompat.getColor(
                    requireContext(),
                    R.color.divider
                )
            )
        }
    }

    private fun pickGalleryImage() {
        ImagePicker.with(this)
            .cropSquare()
            .galleryOnly()
            .galleryMimeTypes(
                mimeTypes = arrayOf(
                    "image/png",
                    "image/jpg",
                    "image/jpeg"
                )
            )
            .maxResultSize(1080, 1920)
            .start(GALLERY_IMAGE_REQ_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (resultCode) {
            Activity.RESULT_OK -> {
                // File object will not be null for RESULT_OK
                val file = ImagePicker.getFile(data)!!
                when (requestCode) {
                    GALLERY_IMAGE_REQ_CODE -> {
                        imgComplaintFile = file
                        binding.etPhoto.setText(file.name)
                    }
                }
            }
            ImagePicker.RESULT_ERROR -> {
                Toast.makeText(requireContext(), ImagePicker.getError(data), Toast.LENGTH_SHORT)
                    .show()
            }
        }
    }

    private fun showDialogToProcess() {
        CustomDialogFragment(
            getString(R.string.perhatian),
            getString(R.string.submit_complain_question)
        ) {
            if (imgComplaintFile !== null)
                createComplainWithImage()
            else createComplain(null)
        }.show(parentFragmentManager, "customDialog")
    }

    private fun createComplainWithImage() {
        if (imgComplaintFile != null) {
            val image = imgComplaintFile!!.asRequestBody()
            val dataImage = MultipartBody.Part.createFormData("photo", imgComplaintFile?.name, image)

            sharedPrefs.get(AUTH_TOKEN)?.let { token ->
                viewModel.createMedia(token, dataImage).observe(viewLifecycleOwner) {
                    when (it.status) {
                        Status.LOADING -> {
                            binding.loading.visibility = View.VISIBLE
                            binding.btnSubmit.visibility = View.GONE
                        }
                        Status.SUCCESS -> {
                            it.data?.let { media ->
                                createComplain(media.id)
                            }
                        }
                        Status.ERROR -> {
                            binding.loading.visibility = View.GONE
                            binding.btnSubmit.visibility = View.VISIBLE
                            Snackbar.make(
                                requireView(),
                                "Failed to upload an image!",
                                Snackbar.LENGTH_SHORT
                            ).show()
                        }
                    }
                }
            }
        }
    }

    private fun createComplain(mediaId: String?) {
        val dataRaw = if (mediaId == null)
            JsonObject().apply {
                addProperty("title", binding.etTitle.text.toString())
                addProperty("description", binding.etDescription.text.toString())
            }
        else
            JsonObject().apply {
                addProperty("title", binding.etTitle.text.toString())
                addProperty("description", binding.etDescription.text.toString())
                addProperty("id_m_medias", mediaId.toString())
            }

        sharedPrefs.get(AUTH_TOKEN)?.let { token ->
            viewModel.createComplain(token, dataRaw).observe(viewLifecycleOwner, {
                when (it.status) {
                    Status.LOADING -> {
                        binding.loading.visibility = View.VISIBLE
                        binding.btnSubmit.visibility = View.GONE
                    }
                    Status.SUCCESS -> {
                        binding.etTitle.setText("")
                        binding.etPhoto.setText("")
                        binding.etDescription.setText("")
                        binding.loading.visibility = View.GONE
                        binding.btnSubmit.visibility = View.VISIBLE
                        Snackbar.make(
                            requireView(),
                            "Success submit the complaint!",
                            Snackbar.LENGTH_SHORT
                        ).show()
                    }
                    Status.ERROR -> {
                        binding.loading.visibility = View.GONE
                        binding.btnSubmit.visibility = View.VISIBLE
                        Snackbar.make(
                            requireView(),
                            "Failed to submit the complaint!",
                            Snackbar.LENGTH_SHORT
                        ).show()
                    }
                }
            })
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    companion object {
        private const val GALLERY_IMAGE_REQ_CODE = 102
    }
}