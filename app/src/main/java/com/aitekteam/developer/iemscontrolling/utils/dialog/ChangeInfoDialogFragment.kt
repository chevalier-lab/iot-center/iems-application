package com.aitekteam.developer.iemscontrolling.utils.dialog

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.DialogFragment
import com.aitekteam.developer.iemscontrolling.R
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.profile.UserInfo
import com.aitekteam.developer.iemscontrolling.core.utils.vo.Status
import com.aitekteam.developer.iemscontrolling.databinding.FragmentChangeInfoDialogBinding
import com.aitekteam.developer.iemscontrolling.ui.user.profile.account.info.AccountInfoActivity
import com.aitekteam.developer.iemscontrolling.ui.user.profile.account.info.AccountInfoViewModel
import com.aitekteam.developer.iemscontrolling.utils.helper.Constants.AUTH_TOKEN
import com.aitekteam.developer.iemscontrolling.utils.helper.SharedPrefsUtils
import com.google.gson.JsonObject
import org.koin.android.viewmodel.ext.android.viewModel
import timber.log.Timber

class ChangeInfoDialogFragment(
    private val activity: Activity,
    private val data: UserInfo
    ) : DialogFragment() {
    private var _binding: FragmentChangeInfoDialogBinding? = null
    private val binding get() = _binding!!
    private val viewModel: AccountInfoViewModel by viewModel()

    // utils
    private var ssidValid = false
    private var passwordValid = false
    private lateinit var sharedPref: SharedPrefsUtils

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentChangeInfoDialogBinding.inflate(inflater, container, false)
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog?.window?.requestFeature(Window.FEATURE_NO_TITLE)

        // init variables
        sharedPref = SharedPrefsUtils()
        sharedPref.start(requireActivity(), AUTH_TOKEN)

        // init ui
        initUI()

        // Inflate the layout for this fragment
        return binding.root
    }

    private fun initUI() {
        // set data
        binding.etSsid.setText(data.ssid)
        binding.etPassword.setText(data.ssidPassword)

        // valudate input
        binding.etSsid.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun afterTextChanged(p0: Editable?) {}

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                ssidValid = p0.toString().trim().isNotEmpty()
                inputCheck()
            }
        })

        binding.etPassword.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun afterTextChanged(p0: Editable?) {}

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                passwordValid = p0.toString().trim().isNotEmpty()
                inputCheck()
            }
        })

        binding.btnSubmit.setOnClickListener { showDialogToProcess() }
    }

    private fun showDialogToProcess() {
        CustomDialogFragment(
            getString(R.string.perhatian),
            getString(R.string.update_info_disclaimer)
        ) { processData() }.show(parentFragmentManager, "customDialog")
    }

    private fun processData() {
        val rawData = JsonObject().apply {
            addProperty("ssid_name", binding.etSsid.text.toString())
            addProperty("ssid_password", binding.etPassword.text.toString())
        }

        sharedPref.get(AUTH_TOKEN)?.let { token ->
            viewModel.changeUserInfo(token, rawData).observe(viewLifecycleOwner) {
                when (it.status) {
                    Status.LOADING -> {
                        binding.btnSubmit.visibility = View.GONE
                        binding.loading.visibility = View.VISIBLE
                    }
                    Status.SUCCESS -> {
                        Toast.makeText(
                            requireContext(),
                            "Success edit information!",
                            Toast.LENGTH_SHORT
                        ).show()
                        activity.finish()
                        startActivity(Intent(requireContext(), AccountInfoActivity::class.java))
                        dismiss()
                    }
                    Status.ERROR -> {
                        Timber.i("error => ${it.message}")
                        Toast.makeText(
                            requireContext(),
                            "Failed to update information:(",
                            Toast.LENGTH_SHORT
                        ).show()
                        dismiss()
                    }
                }
            }
        }
    }

    private fun inputCheck() {
        if (ssidValid && passwordValid) {
            binding.btnSubmit.isEnabled = true
            binding.btnSubmit.setBackgroundColor(
                ContextCompat.getColor(
                    requireContext(),
                    R.color.colorPrimary
                )
            )
        } else {
            binding.btnSubmit.isEnabled = false
            binding.btnSubmit.setBackgroundColor(
                ContextCompat.getColor(
                    requireContext(),
                    R.color.divider
                )
            )
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}