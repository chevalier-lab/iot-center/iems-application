package com.aitekteam.developer.iemscontrolling.ui.auth.register

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import com.aitekteam.developer.iemscontrolling.core.data.Resource
import com.aitekteam.developer.iemscontrolling.core.data.network.response.auth.Register
import com.aitekteam.developer.iemscontrolling.core.domain.usecase.AuthUseCase
import com.google.gson.JsonObject

class RegisterViewModel(
    private val authUseCase: AuthUseCase
) : ViewModel() {

    fun register(jsonObject: JsonObject): LiveData<Resource<Register>> =
        authUseCase.register(jsonObject).asLiveData()
}