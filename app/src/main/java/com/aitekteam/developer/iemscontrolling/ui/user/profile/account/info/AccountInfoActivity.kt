package com.aitekteam.developer.iemscontrolling.ui.user.profile.account.info

import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.aitekteam.developer.iemscontrolling.R
import com.aitekteam.developer.iemscontrolling.core.data.Resource
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.profile.UserInfo
import com.aitekteam.developer.iemscontrolling.core.utils.vo.Status
import com.aitekteam.developer.iemscontrolling.databinding.ActivityAccountInfoBinding
import com.aitekteam.developer.iemscontrolling.ui.operator.profile.ProfileOperatorActivity
import com.aitekteam.developer.iemscontrolling.ui.user.host.HostActivity
import com.aitekteam.developer.iemscontrolling.ui.user.profile.account.manage.ManageAccountActivity
import com.aitekteam.developer.iemscontrolling.utils.dialog.ChangeInfoDialogFragment
import com.aitekteam.developer.iemscontrolling.utils.helper.Constants.AUTH_TOKEN
import com.aitekteam.developer.iemscontrolling.utils.helper.SharedPrefsUtils
import com.bumptech.glide.Glide
import org.koin.android.viewmodel.ext.android.viewModel
import timber.log.Timber

class AccountInfoActivity : AppCompatActivity() {
    private lateinit var binding: ActivityAccountInfoBinding
    private val viewModel: AccountInfoViewModel by viewModel()

    // utils
    private var condition: String? = null
    private lateinit var sharedPrefs: SharedPrefsUtils

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAccountInfoBinding.inflate(layoutInflater)
        setContentView(binding.root)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.apply {
                addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                statusBarColor = Color.TRANSPARENT
                this.decorView.rootView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            }
        }

        binding.toolbar.apply {
            setSupportActionBar(this)
            supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_back_black)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setDisplayShowTitleEnabled(false)
        }

        // init var
        sharedPrefs = SharedPrefsUtils()
        sharedPrefs.start(this, AUTH_TOKEN)

        // get intent data
        val extra = intent.extras
        if (extra != null)
            condition = extra.getString(EXTRA_CONDITION)

        initUI()
    }

    private fun initUI() {
        sharedPrefs.get(AUTH_TOKEN)?.let { token ->
            // fetch profile info
            viewModel.getProfile(token).observe(this) {
                when (it.status) {
                    Status.LOADING -> {
                        binding.layoutMain.visibility = View.GONE
                        binding.loading.visibility = View.VISIBLE
                    }
                    Status.SUCCESS -> {
                        binding.layoutMain.visibility = View.VISIBLE
                        binding.loading.visibility = View.GONE

                        it.data?.let { data ->
                            // set UI
                            binding.tvName.text = data.fullName
                            binding.tvPhone.text = data.phoneNumber
                            binding.tvEmail.text = data.email

                            // set image picture
                            Glide.with(this)
                                .load(data.cover ?: R.drawable.avatar)
                                .circleCrop()
                                .into(binding.imageUser)

                            // set user type
                            if (data.type == "guest" && data.status == "not activate yet"){
                                visibleSsid(false)
                                binding.tvType.text = StringBuilder().append("Anonym")
                            }
                            else if (data.type == "guest" && data.status == "request activate") {
                                visibleSsid(false)
                                binding.tvType.text = StringBuilder().append("Anonym")
                            }
                            else if (data.type == "guest" && data.status == "activate") {
                                visibleSsid(false)
                                binding.tvType.text = StringBuilder().append("Guest")
                            }
                            else if (data.type == "user" && data.status == "activate") {
                                visibleSsid(true)
                                binding.tvType.text = StringBuilder().append("subscriber")
                                viewModel.getUserInfo(token).observe(this, ssidObserver)
                            }
                            else if (data.type == "user" && data.status == "banned") {
                                visibleSsid(false)
                                binding.tvType.text = StringBuilder().append("banned")
                            }
                            else if (data.type == "operator" && data.status == "activate") {
                                visibleSsid(false)
                                binding.tvType.text = StringBuilder().append("operator")
                            }
                        }

                        // edit info
                        binding.btnEditInfo.setOnClickListener {
                            if (condition != null) {
                                startActivity(
                                    Intent(this, ManageAccountActivity::class.java).putExtra(
                                        ManageAccountActivity.EXTRA_CONDITION, condition
                                    )
                                )
                                finish()
                            }
                        }
                    }
                    Status.ERROR -> {
                        binding.loading.visibility = View.GONE
                        binding.layoutMain.visibility = View.GONE
                        binding.layoutError.visibility = View.VISIBLE
                    }
                }
            }
        }
    }

    private val ssidObserver = Observer<Resource<UserInfo>> { res ->
        when (res.status) {
            Status.LOADING -> {
            }
            Status.SUCCESS -> {
                res.data?.let { data ->
                    binding.tvSsidName.text = data.ssid

                    // edit listener
                    binding.btnEditSsid.setOnClickListener {
                        ChangeInfoDialogFragment(this, data).show(supportFragmentManager, "Dialog")
                    }
                }
            }
            Status.ERROR -> {
                binding.layoutMain.visibility = View.GONE
                binding.layoutError.visibility = View.VISIBLE
                Timber.i("error => ${res.message}")
            }
        }
    }

    private fun visibleSsid(isVisible: Boolean) {
        if (isVisible) {
            binding.layoutTitleSsid.visibility = View.VISIBLE
            binding.cardSsid.visibility = View.VISIBLE
        } else {
            binding.layoutTitleSsid.visibility = View.GONE
            binding.cardSsid.visibility = View.GONE
        }
    }

    override fun onBackPressed() {
        when (condition) {
            "user" -> startActivity(
                Intent(this, HostActivity::class.java).putExtra(
                    HostActivity.EXTRA_MOVE_PROFILE,
                    true
                )
            )
            "operator" -> startActivity(
                Intent(this, ProfileOperatorActivity::class.java)
            )
        }
        finish()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                when (condition) {
                    "user" -> startActivity(
                        Intent(this, HostActivity::class.java).putExtra(
                            HostActivity.EXTRA_MOVE_PROFILE,
                            true
                        )
                    )
                    "operator" -> startActivity(
                        Intent(this, ProfileOperatorActivity::class.java)
                    )
                }
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    companion object {
        const val EXTRA_CONDITION = "extra_condition"
    }
}