package com.aitekteam.developer.iemscontrolling.ui.operator.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import com.aitekteam.developer.iemscontrolling.core.data.Resource
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.general.Device
import com.aitekteam.developer.iemscontrolling.core.domain.usecase.DeviceUseCase

class UserDetailViewModel(
    private val deviceUseCase: DeviceUseCase
) : ViewModel() {

    fun getListDevices(token: String): LiveData<Resource<List<Device>>> =
        deviceUseCase.getListDevice(token).asLiveData()
}