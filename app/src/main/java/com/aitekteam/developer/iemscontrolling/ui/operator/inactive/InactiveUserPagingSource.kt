package com.aitekteam.developer.iemscontrolling.ui.operator.inactive

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.profile.User
import com.aitekteam.developer.iemscontrolling.core.data.network.service.OperatorService
import retrofit2.HttpException
import java.io.IOException

class InactiveUserPagingSource(
    private val token: String,
    private val operatorService: OperatorService
) : PagingSource<Int, User>() {

    override fun getRefreshKey(state: PagingState<Int, User>): Int? = state.anchorPosition

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, User> = try {
        val currentPage = params.key ?: FIRST_PAGE_INDEX
        val response = operatorService.getListCustomerPaging(token, currentPage)
        val responseList = mutableListOf<User>()

        val data = response.body()?.data ?: emptyList()
        responseList.addAll(data)

        LoadResult.Page(
            data = responseList,
            prevKey = null,
            nextKey = if (data.isNotEmpty()) currentPage.plus(1)
            else null
        )
    } catch (exception: Exception) {
        LoadResult.Error(exception)
    } catch (exception: IOException) {
        LoadResult.Error(exception)
    } catch (exception: HttpException) {
        LoadResult.Error(exception)
    }

    companion object {
        private const val FIRST_PAGE_INDEX = 0
    }
}