package com.aitekteam.developer.iemscontrolling.ui.user.profile.user.access.detail

import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.RecyclerView
import com.aitekteam.developer.iemscontrolling.R
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.general.Device
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.profile.User
import com.aitekteam.developer.iemscontrolling.core.utils.adapter.AdapterCallback
import com.aitekteam.developer.iemscontrolling.core.utils.adapter.ReusablePagingAdapter
import com.aitekteam.developer.iemscontrolling.databinding.ActivityDetailGuestBinding
import com.aitekteam.developer.iemscontrolling.ui.user.profile.user.access.invite.AddDeviceGuestActivity
import com.aitekteam.developer.iemscontrolling.utils.helper.Constants.AUTH_TOKEN
import com.aitekteam.developer.iemscontrolling.utils.helper.SharedPrefsUtils
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_device.view.*
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.koin.android.viewmodel.ext.android.viewModel

class DetailGuestActivity : AppCompatActivity() {
    private lateinit var binding: ActivityDetailGuestBinding
    private val viewModel: DetailGuestViewModel by viewModel()

    // utils
    private lateinit var sharedPrefs: SharedPrefsUtils
    private lateinit var adapter: ReusablePagingAdapter<Device>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailGuestBinding.inflate(layoutInflater)
        setContentView(binding.root)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.apply {
                addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                statusBarColor = Color.TRANSPARENT
                this.decorView.rootView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            }
        }

        binding.toolbar.apply {
            setSupportActionBar(this)
            supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_back_black)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setDisplayShowTitleEnabled(false)
        }

        // init var
        adapter = ReusablePagingAdapter(this)
        sharedPrefs = SharedPrefsUtils()
        sharedPrefs.start(this, AUTH_TOKEN)

        // setup adapter
        setupAdapter(binding.rvDevice)

        // get intent data
        val extra = intent.extras
        if (extra != null) {
            val data = extra.getParcelable<User>(EXTRA_DATA)
            data?.let { initUI(it) }
        }
    }

    private fun setupAdapter(recyclerView: RecyclerView) {
        adapter.adapterCallback(adapterCallback)
            .setLayout(R.layout.item_device)
            .isVerticalView()
            .build(recyclerView)
    }

    private fun initUI(user: User) {
        Glide.with(this)
            .load(user.cover ?: R.drawable.avatar)
            .circleCrop()
            .into(binding.imageUser)

        binding.tvName.text = user.fullName
        binding.tvType.text = user.type
        binding.tvPhone.text = user.phoneNumber
        binding.tvEmail.text = user.email

        sharedPrefs.get(AUTH_TOKEN)?.let { token ->
            lifecycleScope.launch {
                viewModel.getDeviceList(token, user.phoneNumber!!).collect {
                    adapter.submitData(it)
                }
            }

            with(adapter) {
                // loading state
                addLoadStateListener { loadState ->
                    // handle initial loading
                    if (loadState.append.endOfPaginationReached) {
                        // handle if data is empty
                        if (adapter.itemCount < 1)
                            binding.tvConnectedDevice.visibility = View.GONE
                    }
                }
            }
        }

        // add device
        binding.btnAddDevice.setOnClickListener {
            startActivity(
                Intent(this, AddDeviceGuestActivity::class.java)
                    .putExtra(AddDeviceGuestActivity.EXTRA_DATA, user)
            )
            finish()
        }
    }

    private val adapterCallback = object : AdapterCallback<Device> {
        override fun initComponent(itemView: View, data: Device, itemIndex: Int) {
            itemView.tv_device_name.text = data.name
            itemView.tv_status.text = data.status

            if (data.status == "active") {
                itemView.tv_status.setTextColor(
                    ContextCompat.getColor(
                        this@DetailGuestActivity,
                        R.color.green
                    )
                )
                itemView.card_status.setCardBackgroundColor(
                    ContextCompat.getColor(
                        this@DetailGuestActivity,
                        R.color.greenTransparent
                    )
                )
            } else {
                itemView.tv_status.setTextColor(
                    ContextCompat.getColor(
                        this@DetailGuestActivity,
                        R.color.red
                    )
                )
                itemView.card_status.setCardBackgroundColor(
                    ContextCompat.getColor(
                        this@DetailGuestActivity,
                        R.color.redTransparent
                    )
                )
            }
        }

        override fun onItemClicked(itemView: View, data: Device, itemIndex: Int) {}

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    companion object {
        const val EXTRA_DATA = "extra_data"
    }
}