package com.aitekteam.developer.iemscontrolling.ui.operator.detail

import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.aitekteam.developer.iemscontrolling.R
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.general.Device
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.profile.User
import com.aitekteam.developer.iemscontrolling.core.utils.adapter.AdapterCallback
import com.aitekteam.developer.iemscontrolling.core.utils.adapter.ReusableAdapter
import com.aitekteam.developer.iemscontrolling.core.utils.vo.Status
import com.aitekteam.developer.iemscontrolling.databinding.ActivityUserDetailBinding
import com.aitekteam.developer.iemscontrolling.ui.operator.inactive.ActivateUserDialogFragment
import com.aitekteam.developer.iemscontrolling.ui.scanner.AddDeviceActivity
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_device.view.*
import org.koin.android.viewmodel.ext.android.viewModel

class UserDetailActivity : AppCompatActivity() {
    private lateinit var binding: ActivityUserDetailBinding
    private val viewModel: UserDetailViewModel by viewModel()

    // utils
    private lateinit var adapter: ReusableAdapter<Device>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityUserDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.apply {
                addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                statusBarColor = Color.TRANSPARENT
                this.decorView.rootView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            }
        }

        binding.toolbar.apply {
            setSupportActionBar(this)
            supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_back_black)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setDisplayShowTitleEnabled(false)
        }

        val extras = intent.extras
        if (extras != null) {
            val data = extras.getParcelable<User>(EXTRA_DATA)
            data?.let { initUI(it) }
        }
    }

    private fun initUI(user: User) {
        Glide.with(this)
            .load(user.cover ?: R.drawable.avatar)
            .circleCrop()
            .into(binding.imageUser)

        binding.tvName.text = user.fullName
        binding.tvType.text = user.type
        binding.tvPhone.text = user.phoneNumber
        binding.tvEmail.text = user.email

        if (user.status == "activate") {
            binding.tvStatus.text = getString(R.string.aktif)
            binding.tvStatus.setTextColor(ContextCompat.getColor(this, R.color.green))
            binding.cardStatus.setCardBackgroundColor(
                ContextCompat.getColor(
                    this,
                    R.color.greenTransparent
                )
            )

            // init adapter
            adapter = ReusableAdapter(this)
            setupAdapter(binding.rvDevice)

            // set visibility
            binding.btnActivateUser.visibility = View.GONE
            binding.layoutBottom.visibility = View.VISIBLE
            binding.tvConnectedDevice.visibility = View.VISIBLE
            binding.rvDevice.visibility = View.VISIBLE

            // fetch data
            viewModel.getListDevices(user.token!!).observe(this) {
                when (it.status) {
                    Status.LOADING -> {
                        with(binding) {
                            rvDevice.visibility = View.GONE
                            layoutEmpty.visibility = View.GONE
                            loading.visibility = View.VISIBLE
                        }
                    }
                    Status.SUCCESS -> {
                        with (binding) {
                            loading.visibility = View.GONE
                            layoutEmpty.visibility = View.GONE
                            rvDevice.visibility = View.VISIBLE
                        }

                        it.data?.let { data ->
                            if (data.isNotEmpty())
                                adapter.addData(data)
                            else {
                                with (binding) {
                                    loading.visibility = View.GONE
                                    rvDevice.visibility = View.GONE
                                    layoutEmpty.visibility = View.VISIBLE
                                }
                            }
                        }
                    }
                    Status.ERROR -> {
                        with (binding) {
                            layoutMain.visibility = View.GONE
                            layoutBottom.visibility = View.GONE
                            layoutError.visibility = View.VISIBLE
                        }
                    }
                }
            }

        } else {
            binding.tvStatus.text = getString(R.string.belum_aktif)
            binding.tvStatus.setTextColor(ContextCompat.getColor(this, R.color.red))
            binding.cardStatus.setCardBackgroundColor(
                ContextCompat.getColor(
                    this,
                    R.color.redTransparent
                )
            )

            // set visibility
            binding.btnActivateUser.visibility = View.VISIBLE
            binding.layoutBottom.visibility = View.GONE
            binding.tvConnectedDevice.visibility = View.GONE
            binding.rvDevice.visibility = View.GONE
        }

        // activate user
        binding.btnActivateUser.setOnClickListener {
            ActivateUserDialogFragment(user).show(supportFragmentManager, "Dialog")
        }

        // activate device
        binding.btnActivateDevice.setOnClickListener {
            startActivity(
                Intent(
                    this,
                    AddDeviceActivity::class.java
                ).putExtra(AddDeviceActivity.EXTRA_PHONE, user.phoneNumber)
            )
        }
    }

    private fun setupAdapter(recyclerView: RecyclerView) {
        adapter.adapterCallback(adapterCallback)
            .setLayout(R.layout.item_device)
            .isVerticalView()
            .build(recyclerView)
    }

    private val adapterCallback = object : AdapterCallback<Device> {
        override fun initComponent(itemView: View, data: Device, itemIndex: Int) {
            itemView.tv_device_name.text = data.name
            itemView.tv_status.text = data.status

            if (data.status == "active") {
                itemView.tv_status.setTextColor(
                    ContextCompat.getColor(
                        this@UserDetailActivity,
                        R.color.green
                    )
                )
                itemView.card_status.setCardBackgroundColor(
                    ContextCompat.getColor(
                        this@UserDetailActivity,
                        R.color.greenTransparent
                    )
                )
            } else {
                itemView.tv_status.setTextColor(
                    ContextCompat.getColor(
                        this@UserDetailActivity,
                        R.color.red
                    )
                )
                itemView.card_status.setCardBackgroundColor(
                    ContextCompat.getColor(
                        this@UserDetailActivity,
                        R.color.redTransparent
                    )
                )
            }
        }

        override fun onItemClicked(itemView: View, data: Device, itemIndex: Int) {}

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    companion object {
        const val EXTRA_DATA = "extra_data"
    }
}