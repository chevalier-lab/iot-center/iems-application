package com.aitekteam.developer.iemscontrolling.ui.user.notification

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import com.aitekteam.developer.iemscontrolling.core.data.Resource
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.notification.DeviceNotification
import com.aitekteam.developer.iemscontrolling.core.domain.usecase.DeviceUseCase

class NotificationViewModel(
    private val deviceUseCase: DeviceUseCase
) : ViewModel() {

    fun getDeviceUserNotification(token: String): LiveData<Resource<List<DeviceNotification>>> =
        deviceUseCase.getDeviceUserNotification(token).asLiveData()

    fun getDeviceGuestNotification(token: String): LiveData<Resource<List<DeviceNotification>>> =
        deviceUseCase.getDeviceGuestNotification(token).asLiveData()
}