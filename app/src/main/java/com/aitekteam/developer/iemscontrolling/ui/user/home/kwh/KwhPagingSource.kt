package com.aitekteam.developer.iemscontrolling.ui.user.home.kwh

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.general.Device
import com.aitekteam.developer.iemscontrolling.core.data.network.service.DeviceService
import okio.IOException
import retrofit2.HttpException

class KwhPagingSource(
    private val token: String,
    private val deviceService: DeviceService
) : PagingSource<Int, Device>() {
    override fun getRefreshKey(state: PagingState<Int, Device>): Int? = state.anchorPosition

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Device> = try {
        val currentPage = params.key ?: FIRST_PAGE_INDEX
        val response = deviceService.listKwhPaging(token, currentPage)
        val responseList = mutableListOf<Device>()

        val data = response.body()?.data ?: emptyList()
        responseList.addAll(data)

        LoadResult.Page(
            data = responseList,
            prevKey = null,
            nextKey = if (data.isNotEmpty()) currentPage.plus(1)
            else null
        )
    } catch (exception: Exception) {
        LoadResult.Error(exception)
    } catch (exception: IOException) {
        LoadResult.Error(exception)
    } catch (exception: HttpException) {
        LoadResult.Error(exception)
    }

    companion object {
        private const val FIRST_PAGE_INDEX = 0
    }
}

class KwhGroupPagingSource(
    private val token: String,
    private val groupId: Int,
    private val deviceService: DeviceService
) : PagingSource<Int, Device>() {
    override fun getRefreshKey(state: PagingState<Int, Device>): Int? = state.anchorPosition

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Device> = try {
        val currentPage = params.key ?: FIRST_PAGE_INDEX
        val response = deviceService.listKwhOnGroupPaging(token, groupId, currentPage)
        val responseList = mutableListOf<Device>()

        val data = response.body()?.data ?: emptyList()
        responseList.addAll(data)

        LoadResult.Page(
            data = responseList,
            prevKey = null,
            nextKey = if (data.isNotEmpty()) currentPage.plus(1)
            else null
        )
    } catch (exception: Exception) {
        LoadResult.Error(exception)
    } catch (exception: IOException) {
        LoadResult.Error(exception)
    } catch (exception: HttpException) {
        LoadResult.Error(exception)
    }

    companion object {
        private const val FIRST_PAGE_INDEX = 0
    }
}
