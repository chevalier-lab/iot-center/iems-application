package com.aitekteam.developer.iemscontrolling.ui.user.profile.account.info

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import com.aitekteam.developer.iemscontrolling.core.data.Resource
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.profile.User
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.profile.UserInfo
import com.aitekteam.developer.iemscontrolling.core.domain.usecase.UserUseCase
import com.google.gson.JsonObject

class AccountInfoViewModel(
    private val userUseCase: UserUseCase
) : ViewModel() {

    fun getProfile(authorization: String): LiveData<Resource<User>> =
        userUseCase.getData(authorization).asLiveData()

    fun changeUserInfo(token: String, jsonObject: JsonObject): LiveData<Resource<UserInfo>> =
        userUseCase.changeUserInfo(token, jsonObject).asLiveData()

    fun getUserInfo(token: String): LiveData<Resource<UserInfo>> =
        userUseCase.getUserInfo(token).asLiveData()
}