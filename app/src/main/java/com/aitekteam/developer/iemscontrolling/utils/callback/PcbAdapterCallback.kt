package com.aitekteam.developer.iemscontrolling.utils.callback

import android.content.Context
import android.content.Intent
import android.view.View
import android.widget.Toast
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.LifecycleOwner
import com.aitekteam.developer.iemscontrolling.R
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.general.Device
import com.aitekteam.developer.iemscontrolling.core.utils.adapter.AdapterCallback
import com.aitekteam.developer.iemscontrolling.core.utils.vo.Status
import com.aitekteam.developer.iemscontrolling.ui.user.home.HomeViewModel
import com.aitekteam.developer.iemscontrolling.ui.user.home.pcb.PcbActivity
import com.aitekteam.developer.iemscontrolling.utils.dialog.CustomDialogFragment
import com.bumptech.glide.Glide
import com.google.gson.JsonObject
import kotlinx.android.synthetic.main.item_pcb.view.*
import timber.log.Timber

class PcbAdapterCallback(
    private val context: Context,
    private val viewModel: HomeViewModel,
    private val lifecycleOwner: LifecycleOwner,
    private val fragmentManager: FragmentManager
) : AdapterCallback<Device> {

    override fun initComponent(itemView: View, data: Device, itemIndex: Int) {
        itemView.tv_pcb_name.text = data.name

        // set value
        data.token?.let { token ->
            viewModel.getLastPcb(token).observe(lifecycleOwner) {
                when (it.status) {
                    Status.LOADING -> {
                    }
                    Status.SUCCESS -> {
                        lateinit var sendMode: String

                        it.data?.let { pcb ->
                            if (pcb.mode == null) {
//                                        val params = itemView.layoutParams.apply {
//                                            width = 0
//                                            height = 0
//                                        }
//                                        itemView.visibility = View.GONE
//                                        itemView.layoutParams = params
                                sendMode = "ON"
                                Glide.with(context)
                                    .load(R.drawable.ic_off)
                                    .into(itemView.btn_power_pcb)
                            } else {
                                when (pcb.mode) {
                                    "ON" -> {
                                        sendMode = "OFF"
                                        Glide.with(context)
                                            .load(R.drawable.ic_on)
                                            .into(itemView.btn_power_pcb)
                                    }
                                    "OFF" -> {
                                        sendMode = "ON"
                                        Glide.with(context)
                                            .load(R.drawable.ic_off)
                                            .into(itemView.btn_power_pcb)
                                    }
                                    else -> Glide.with(context)
                                        .load(R.drawable.ic_off)
                                        .into(itemView.btn_power_pcb)
                                }
                            }
                        }

                        itemView.btn_power_pcb.setOnClickListener {
                            CustomDialogFragment(
                                context.getString(R.string.perhatian),
                                context.getString(R.string.push_pcb_disclaimer)
                            ) {
                                val payload =  JsonObject().apply {
                                    addProperty("mode", sendMode)
                                }

                                viewModel.pushDevice(token, payload).observe(lifecycleOwner) { res ->
                                    when (res.status) {
                                        Status.LOADING -> {}
                                        Status.SUCCESS -> {
                                            when (sendMode) {
                                                "ON" -> {
                                                    sendMode = "OFF"
                                                    Glide.with(context)
                                                        .load(R.drawable.ic_on)
                                                        .into(itemView.btn_power_pcb)
                                                }
                                                "OFF" -> {
                                                    sendMode = "ON"
                                                    Glide.with(context)
                                                        .load(R.drawable.ic_off)
                                                        .into(itemView.btn_power_pcb)
                                                }
                                            }
                                        }
                                        Status.ERROR -> {
                                            Toast.makeText(context, res.message, Toast.LENGTH_SHORT).show()
                                        }
                                    }
                                }
                            }.show(fragmentManager, "customDialog")
                        }
                    }
                    Status.ERROR -> {
                        Timber.i("error => ${it.message}")
                    }
                }
            }
        }
    }

    override fun onItemClicked(itemView: View, data: Device, itemIndex: Int) {
        context.startActivity(
            Intent(context, PcbActivity::class.java).apply {
                putExtra(PcbActivity.EXTRA_TOKEN, data.token)
            }
        )
    }
}