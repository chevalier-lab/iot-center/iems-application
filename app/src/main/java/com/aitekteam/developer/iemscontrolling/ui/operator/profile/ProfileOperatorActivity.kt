package com.aitekteam.developer.iemscontrolling.ui.operator.profile

import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import com.aitekteam.developer.iemscontrolling.R
import com.aitekteam.developer.iemscontrolling.core.utils.vo.Status
import com.aitekteam.developer.iemscontrolling.databinding.ActivityProfileOperatorBinding
import com.aitekteam.developer.iemscontrolling.ui.auth.login.MainActivity
import com.aitekteam.developer.iemscontrolling.ui.operator.OperatorActivity
import com.aitekteam.developer.iemscontrolling.ui.user.profile.account.info.AccountInfoActivity
import com.aitekteam.developer.iemscontrolling.ui.user.profile.terms.TermsAndConditionActivity
import com.aitekteam.developer.iemscontrolling.utils.helper.Constants.AUTH_TOKEN
import com.aitekteam.developer.iemscontrolling.utils.dialog.CustomDialogFragment
import com.aitekteam.developer.iemscontrolling.utils.helper.SharedPrefsUtils
import com.bumptech.glide.Glide
import org.koin.android.viewmodel.ext.android.viewModel

class ProfileOperatorActivity : AppCompatActivity() {
    private lateinit var binding: ActivityProfileOperatorBinding
    private val viewModel: ProfileOperatorViewModel by viewModel()

    // utils
    private lateinit var sharedPrefs: SharedPrefsUtils

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityProfileOperatorBinding.inflate(layoutInflater)
        setContentView(binding.root)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.apply {
                addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                statusBarColor = Color.TRANSPARENT
                this.decorView.rootView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            }
        }

        binding.toolbar.apply {
            setSupportActionBar(this)
            supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_back_black)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setDisplayShowTitleEnabled(false)
        }

        // init var
        sharedPrefs = SharedPrefsUtils()
        sharedPrefs.start(this, AUTH_TOKEN)

        initUI()
    }

    private fun initUI() {
        sharedPrefs.get(AUTH_TOKEN)?.let { token ->
            viewModel.getProfile(token).observe(this, {
                when (it.status) {
                    Status.LOADING -> {
                    }
                    Status.SUCCESS -> {

                        it.data?.let { user ->
                            binding.tvEmail.text = user.email
                            binding.tvName.text = user.fullName

                            Glide.with(this)
                                .load(user.cover ?: R.drawable.avatar)
                                .circleCrop()
                                .into(binding.imageUser)
                        }
                    }
                    Status.ERROR -> {
                    }
                }
            })
        }

        binding.btnAccountInfo.setOnClickListener {
            startActivity(
                Intent(this, AccountInfoActivity::class.java).putExtra(
                    AccountInfoActivity.EXTRA_CONDITION,
                    "operator"
                )
            )
            finish()
        }

        binding.btnTerms.setOnClickListener {
            startActivity(Intent(this, TermsAndConditionActivity::class.java))
        }

        binding.btnSignOut.setOnClickListener {
            showDialogToLogout()
        }
    }

    private fun showDialogToLogout() {
        CustomDialogFragment(
            getString(R.string.perhatian),
            getString(R.string.logout_disclaimer)
        ) { logout() }.show(supportFragmentManager, "cutomDialog")
    }

    private fun logout() {
        // clear shared prefs
        sharedPrefs.clear()

        val intent = Intent(this, MainActivity::class.java).apply {
            addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        }
        startActivity(intent)
    }

    override fun onBackPressed() {
        startActivity(Intent(this, OperatorActivity::class.java))
        finish()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                startActivity(Intent(this, OperatorActivity::class.java))
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}