package com.aitekteam.developer.iemscontrolling.ui.operator

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import com.aitekteam.developer.iemscontrolling.core.data.Resource
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.profile.User
import com.aitekteam.developer.iemscontrolling.core.domain.usecase.UserUseCase

class OperatorViewModel(
    private val userUseCase: UserUseCase
) : ViewModel() {

    fun getProfile(token: String): LiveData<Resource<User>> =
        userUseCase.getData(token).asLiveData()
}