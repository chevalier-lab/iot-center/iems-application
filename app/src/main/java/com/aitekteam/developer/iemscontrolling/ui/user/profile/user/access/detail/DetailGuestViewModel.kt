package com.aitekteam.developer.iemscontrolling.ui.user.profile.user.access.detail

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.general.Device
import com.aitekteam.developer.iemscontrolling.core.data.network.service.UserService
import kotlinx.coroutines.flow.Flow

class DetailGuestViewModel(
    private val userService: UserService
) : ViewModel() {

    fun getDeviceList(token: String, phoneNumber: String): Flow<PagingData<Device>> =
        Pager(PagingConfig(12)) {
            GuestDevicePagingSource(token, phoneNumber, userService)
        }.flow.cachedIn(viewModelScope)
}