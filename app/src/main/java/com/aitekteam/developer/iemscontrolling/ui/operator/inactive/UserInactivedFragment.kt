package com.aitekteam.developer.iemscontrolling.ui.operator.inactive

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import androidx.recyclerview.widget.RecyclerView
import com.aitekteam.developer.iemscontrolling.R
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.profile.User
import com.aitekteam.developer.iemscontrolling.core.utils.adapter.AdapterCallback
import com.aitekteam.developer.iemscontrolling.core.utils.adapter.ReusablePagingAdapter
import com.aitekteam.developer.iemscontrolling.databinding.FragmentUserInactivedBinding
import com.aitekteam.developer.iemscontrolling.ui.operator.detail.UserDetailActivity
import com.aitekteam.developer.iemscontrolling.utils.helper.Constants.AUTH_TOKEN
import com.aitekteam.developer.iemscontrolling.utils.helper.SharedPrefsUtils
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_user.view.*
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.koin.android.viewmodel.ext.android.viewModel

class UserInactivedFragment : Fragment() {
    private var _binding: FragmentUserInactivedBinding? = null
    private val binding get() = _binding!!
    private val viewModel: InactiveUserViewModel by viewModel()

    // utils
    private var updateJob: Job? = null
    private lateinit var sharedPrefs: SharedPrefsUtils
    private lateinit var adapter: ReusablePagingAdapter<User>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentUserInactivedBinding.inflate(inflater, container, false)

        // init utils
        adapter = ReusablePagingAdapter(requireContext())
        sharedPrefs = SharedPrefsUtils()
        sharedPrefs.start(requireActivity(), AUTH_TOKEN)

        // setup adapter
        setupAdapter(binding.rvUser)

        // init UI
        initUI()

        // refresh
        binding.swipeRefresh.setOnRefreshListener { initUI() }

        // Inflate the layout for this fragment
        return binding.root
    }

    private fun initUI() {
        sharedPrefs.get(AUTH_TOKEN)?.let { token ->
            updateJob?.cancel()
            updateJob = viewLifecycleOwner.lifecycleScope.launch {
                viewModel.getListCustomer(token).collect {
                    adapter.submitData(it)
                }
            }

            with(adapter) {
                // loading state
                addLoadStateListener { loadState ->
                    if (loadState.refresh is LoadState.Loading) {
                        binding.rvUser.visibility = View.GONE
                        binding.layoutEmpty.visibility = View.GONE
                        binding.layoutError.visibility = View.GONE
                        binding.shimmerLoading.rootLayout.visibility = View.VISIBLE
                    } else if (loadState.append.endOfPaginationReached) {
                        // handle if data is empty
                        if (adapter.itemCount < 1) {
                            binding.rvUser.visibility = View.GONE
                            binding.layoutError.visibility = View.GONE
                            binding.layoutEmpty.visibility = View.VISIBLE
                            binding.shimmerLoading.rootLayout.visibility = View.GONE

                            // swipe layout
                            binding.swipeRefresh.isRefreshing = false
                        }
                    } else {
                        // handle if data is exists
                        binding.rvUser.visibility = View.VISIBLE
                        binding.layoutError.visibility = View.GONE
                        binding.layoutEmpty.visibility = View.GONE
                        binding.shimmerLoading.rootLayout.visibility = View.GONE

                        // swipe layout
                        binding.swipeRefresh.isRefreshing = false

                        // get error
                        val error = when {
                            loadState.prepend is LoadState.Error -> loadState.prepend as LoadState.Error
                            loadState.append is LoadState.Error -> loadState.append as LoadState.Error
                            loadState.refresh is LoadState.Error -> loadState.refresh as LoadState.Error
                            else -> null
                        }

                        error?.let {
                            binding.rvUser.visibility = View.GONE
                            binding.layoutEmpty.visibility = View.GONE
                            binding.layoutError.visibility = View.VISIBLE
                            binding.shimmerLoading.rootLayout.visibility = View.GONE

                            // swipe layout
                            binding.swipeRefresh.isRefreshing = false
                        }
                    }
                }
            }
        }
    }

    private fun setupAdapter(recyclerView: RecyclerView) {
        adapter.adapterCallback(adapterCallback)
            .setLayout(R.layout.item_user)
            .isVerticalView()
            .build(recyclerView)
    }

    private val adapterCallback = object : AdapterCallback<User> {
        override fun initComponent(itemView: View, data: User, itemIndex: Int) {
            itemView.tv_name.text = data.fullName
            itemView.tv_email.text = data.email

            Glide.with(requireContext())
                .load(data.cover ?: R.drawable.avatar)
                .into(itemView.image_user)
        }

        override fun onItemClicked(itemView: View, data: User, itemIndex: Int) {
            startActivity(
                Intent(requireContext(), UserDetailActivity::class.java).putExtra(
                    UserDetailActivity.EXTRA_DATA,
                    data
                )
            )
        }

    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}