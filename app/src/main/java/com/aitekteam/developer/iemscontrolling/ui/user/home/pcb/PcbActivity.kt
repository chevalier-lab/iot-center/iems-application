package com.aitekteam.developer.iemscontrolling.ui.user.home.pcb

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.RecyclerView
import com.aitekteam.developer.iemscontrolling.R
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.general.Device
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.pcb.Pcb
import com.aitekteam.developer.iemscontrolling.core.utils.adapter.AdapterCallback
import com.aitekteam.developer.iemscontrolling.core.utils.adapter.ReusablePagingAdapter
import com.aitekteam.developer.iemscontrolling.core.utils.vo.Status
import com.aitekteam.developer.iemscontrolling.databinding.ActivityPcbBinding
import com.aitekteam.developer.iemscontrolling.utils.dialog.ChangeDeviceSsidDialogFragment
import com.aitekteam.developer.iemscontrolling.utils.dialog.CustomDialogFragment
import com.aitekteam.developer.iemscontrolling.utils.dialog.EditDeviceDialogFragment
import com.aitekteam.developer.iemscontrolling.utils.helper.Constants.AUTH_TOKEN
import com.aitekteam.developer.iemscontrolling.utils.helper.Helpers.dateFormat
import com.aitekteam.developer.iemscontrolling.utils.helper.SharedPrefsUtils
import com.aitekteam.developer.iemscontrolling.utils.listener.DialogListener
import com.google.gson.JsonObject
import kotlinx.android.synthetic.main.activity_manage_account.*
import kotlinx.android.synthetic.main.item_pcb_history.view.*
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.koin.android.viewmodel.ext.android.viewModel
import timber.log.Timber

class PcbActivity : AppCompatActivity(), DialogListener {
    private lateinit var binding: ActivityPcbBinding
    private val viewModel: PcbViewModel by viewModel()

    // utils
    private var deviceToken: String? = null
    private lateinit var sharedPrefs: SharedPrefsUtils
    private lateinit var dialog: EditDeviceDialogFragment
    private lateinit var adapter: ReusablePagingAdapter<Pcb>
    private lateinit var ssidDialog: ChangeDeviceSsidDialogFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPcbBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.toolbar.apply {
            setSupportActionBar(this)
            supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_back)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setDisplayShowTitleEnabled(false)
        }

        // init utils
        adapter = ReusablePagingAdapter(this)
        sharedPrefs = SharedPrefsUtils()
        sharedPrefs.start(this, AUTH_TOKEN)

        // setup adapter
        setupAdapter(binding.rvHistory)

        val extra = intent.extras
        if (extra != null) {
            deviceToken = extra.getString(EXTRA_TOKEN)
            deviceToken?.let { initUI(it) }
        }
    }

    private fun initUI(token: String) {
        // device info
        viewModel.getDeviceInfo(token).observe(this) {
            when (it.status) {
                Status.LOADING -> {
                    visibleUI(true)
                }
                Status.SUCCESS -> {
                    visibleUI(false)
                    it.data?.let { data ->
                        binding.layoutDeviceInfo.tvName.text = data.name
                        binding.layoutDeviceInfo.tvShortname.text = data.shortName
                        binding.layoutDeviceInfo.tvStatus.text = data.status

                        // set ssid
                        if (data.deviceSsid == null && data.devicePassword == null)
                            binding.layoutDeviceInfo.tvSsid.text = data.userSsid
                        else binding.layoutDeviceInfo.tvSsid.text = data.deviceSsid

                        // edit name
                        binding.layoutDeviceInfo.btnEdit.setOnClickListener {
                            dialog = EditDeviceDialogFragment(data.id!!, data.name!!, this)
                            dialog.show(supportFragmentManager, "editDialog")
                        }

                        // edit ssid
                        binding.layoutDeviceInfo.btnEditDeviceSsid.setOnClickListener {
                            ssidDialog = ChangeDeviceSsidDialogFragment(data, this)
                            ssidDialog.show(supportFragmentManager, "changeSsid")
                        }
                    }
                }
                Status.ERROR -> {
                    binding.layoutMain.visibility = View.GONE
                    binding.layoutShimmer.visibility = View.GONE
                    binding.layoutError.visibility = View.VISIBLE
                }
            }
        }

        // last data
        viewModel.getLastPcb(token).observe(this) {
            when (it.status) {
                Status.LOADING -> {
                    visibleUI(true)
                }
                Status.SUCCESS -> {
                    visibleUI(false)
                    it.data?.let { data ->
                        if (data.date != null && data.time != null) {
                            val dateTime = "${data.date} ${data.time}"
                            binding.layoutDeviceInfo.tvLastUpdate.text = dateTime.dateFormat()
                        } else
                            binding.layoutDeviceInfo.layoutLastUpdate.visibility = View.GONE


                        when (data.mode) {
                            "ON" -> {
                                binding.layoutDeviceInfo.tvStatus.text = data.mode
                                binding.layoutDeviceInfo.tvStatus.setTextColor(ContextCompat.getColor(this, R.color.white))
                                binding.layoutDeviceInfo.cardStatus.setCardBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary))
                            }
                            "OFF" -> {
                                binding.layoutDeviceInfo.tvStatus.text = data.mode
                                binding.layoutDeviceInfo.tvStatus.setTextColor(ContextCompat.getColor(this, R.color.colorPrimary))
                                binding.layoutDeviceInfo.cardStatus.strokeWidth = 1
                                binding.layoutDeviceInfo.cardStatus.strokeColor = ContextCompat.getColor(this, R.color.colorPrimary)
                                binding.layoutDeviceInfo.cardStatus.setCardBackgroundColor(ContextCompat.getColor(this, R.color.white))
                            }
                        }

                        // history data
                        lifecycleScope.launch {
                            viewModel.getHistoryPcb(token).collect { data ->
                                adapter.submitData(data)
                            }
                        }

                        // loading state
                        with(adapter) {
                            // loading state
                            addLoadStateListener { loadState ->
                                // handle initial loading
                                if (loadState.append.endOfPaginationReached) {
                                    // handle if data is empty
                                    if (adapter.itemCount < 1) {
                                        binding.rvHistory.visibility = View.GONE
                                        binding.layoutEmpty.visibility = View.VISIBLE
                                    }
                                } else {
                                    // handle if data is exists
                                    binding.rvHistory.visibility = View.VISIBLE
                                    binding.layoutEmpty.visibility = View.GONE
                                }
                            }
                        }
                    }
                }
                Status.ERROR -> {
                    binding.layoutMain.visibility = View.GONE
                    binding.layoutShimmer.visibility = View.GONE
                    binding.layoutError.visibility = View.VISIBLE
                    Timber.i("error => ${it.message}")
                }
            }
        }
    }

    override fun changeSsid(jsonObject: JsonObject, data: Device) {
        CustomDialogFragment(
            getString(R.string.perhatian),
            getString(R.string.update_info_disclaimer)
        ) {
            ssidDialog.dismiss()

            sharedPrefs.get(AUTH_TOKEN)?.let { userToken ->
                viewModel.changeDeviceSsid(userToken, data.id!!, jsonObject).observe(this) {
                    when (it.status) {
                        Status.LOADING -> {}
                        Status.SUCCESS -> {
                            initUI(deviceToken!!)
                            Toast.makeText(this, "Success update SSID!", Toast.LENGTH_SHORT)
                                .show()
                        }
                        Status.ERROR -> {
                            Timber.i("error => ${it.message}")
                            Toast.makeText(this, "Failed to update SSID!", Toast.LENGTH_SHORT)
                                .show()
                        }
                    }
                }
            }
        }.show(supportFragmentManager, "customDialog")
    }

    private fun setupAdapter(recyclerView: RecyclerView) {
        adapter.adapterCallback(adapterCallback)
            .setLayout(R.layout.item_pcb_history)
            .isVerticalView()
            .build(recyclerView)
    }

    private val adapterCallback = object : AdapterCallback<Pcb> {
        override fun initComponent(itemView: View, data: Pcb, itemIndex: Int) {
            val dateTime = "${data.date} ${data.time}"
            itemView.tv_date.text = dateTime.dateFormat()

            when (data.mode) {
                "ON" -> {
                    itemView.tv_status.text = data.mode
                    itemView.tv_status.setTextColor(ContextCompat.getColor(this@PcbActivity, R.color.white))
                    itemView.card_status.setCardBackgroundColor(ContextCompat.getColor(this@PcbActivity, R.color.colorPrimary))
                }
                "OFF" -> {
                    itemView.tv_status.text = data.mode
                    itemView.tv_status.setTextColor(ContextCompat.getColor(this@PcbActivity, R.color.colorPrimary))
                    itemView.card_status.strokeWidth = 1
                    itemView.card_status.strokeColor = ContextCompat.getColor(this@PcbActivity, R.color.colorPrimary)
                    itemView.card_status.setCardBackgroundColor(ContextCompat.getColor(this@PcbActivity, R.color.white))
                }
            }
        }

        override fun onItemClicked(itemView: View, data: Pcb, itemIndex: Int) {}

    }

    override fun updateDevice(id: Int, name: String) {
        CustomDialogFragment(
            getString(R.string.perhatian),
            getString(R.string.device_update_disclaimer)
        ) {
            dialog.dismiss()
            val rawData = JsonObject().apply {
                addProperty("device_name", name)
            }

            sharedPrefs.get(AUTH_TOKEN)?.let { userToken ->
                viewModel.updateDevice(userToken, id, rawData).observe(this, {
                    when (it.status) {
                        Status.LOADING -> {
                        }
                        Status.SUCCESS -> {
                            initUI(deviceToken!!)
                            Toast.makeText(this, "Berhasil update perangkat!", Toast.LENGTH_SHORT)
                                .show()
                        }
                        Status.ERROR -> {
                            Timber.i("error => ${it.message}")
                            Toast.makeText(this, "Gagal update perangkat!", Toast.LENGTH_SHORT)
                                .show()
                        }
                    }
                })
            }
        }.show(supportFragmentManager, "customDialog")
    }

    private fun visibleUI(isLoading: Boolean) {
        if (isLoading) {
            binding.layoutMain.visibility = View.GONE
            binding.layoutShimmer.visibility = View.VISIBLE
        } else {
            binding.layoutMain.visibility = View.VISIBLE
            binding.layoutShimmer.visibility = View.GONE
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    companion object {
        const val EXTRA_TOKEN = "extra_token"
    }
}