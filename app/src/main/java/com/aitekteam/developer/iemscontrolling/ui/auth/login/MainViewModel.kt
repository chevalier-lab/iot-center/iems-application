package com.aitekteam.developer.iemscontrolling.ui.auth.login

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import com.aitekteam.developer.iemscontrolling.core.data.Resource
import com.aitekteam.developer.iemscontrolling.core.data.network.response.auth.Login
import com.aitekteam.developer.iemscontrolling.core.domain.usecase.AuthUseCase
import com.google.gson.JsonObject

class MainViewModel(
    private val authUseCase: AuthUseCase
) : ViewModel() {

    fun login(jsonObject: JsonObject): LiveData<Resource<Login>> =
        authUseCase.login(jsonObject).asLiveData()
}