package com.aitekteam.developer.iemscontrolling.ui.user.home.sensor

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.RecyclerView
import com.aitekteam.developer.iemscontrolling.R
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.general.Device
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.sensor.Sensor
import com.aitekteam.developer.iemscontrolling.core.utils.adapter.AdapterCallback
import com.aitekteam.developer.iemscontrolling.core.utils.adapter.ReusablePagingAdapter
import com.aitekteam.developer.iemscontrolling.core.utils.vo.Status
import com.aitekteam.developer.iemscontrolling.databinding.ActivitySensorBinding
import com.aitekteam.developer.iemscontrolling.utils.dialog.ChangeDeviceSsidDialogFragment
import com.aitekteam.developer.iemscontrolling.utils.dialog.CustomDialogFragment
import com.aitekteam.developer.iemscontrolling.utils.dialog.EditDeviceDialogFragment
import com.aitekteam.developer.iemscontrolling.utils.helper.Constants.AUTH_TOKEN
import com.aitekteam.developer.iemscontrolling.utils.helper.Helpers.dateFormat
import com.aitekteam.developer.iemscontrolling.utils.helper.SharedPrefsUtils
import com.aitekteam.developer.iemscontrolling.utils.listener.DialogListener
import com.bumptech.glide.Glide
import com.google.gson.JsonObject
import kotlinx.android.synthetic.main.item_sensor_history.view.*
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.koin.android.viewmodel.ext.android.viewModel
import timber.log.Timber

class SensorActivity : AppCompatActivity(), DialogListener {
    private lateinit var binding: ActivitySensorBinding
    private val viewModel: SensorViewModel by viewModel()

    // utils
    private var deviceToken: String? = null
    private lateinit var sharedPrefs: SharedPrefsUtils
    private lateinit var dialog: EditDeviceDialogFragment
    private lateinit var adapter: ReusablePagingAdapter<Sensor>
    private lateinit var ssidDialog: ChangeDeviceSsidDialogFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySensorBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.toolbar.apply {
            setSupportActionBar(this)
            supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_back)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setDisplayShowTitleEnabled(false)
        }

        // init utils
        adapter = ReusablePagingAdapter(this)
        sharedPrefs = SharedPrefsUtils()
        sharedPrefs.start(this, AUTH_TOKEN)

        // setup adapter
        setupAdapter(binding.rvHistory)

        // get intent data
        val extra = intent.extras
        if (extra != null) {
            deviceToken = extra.getString(EXTRA_TOKEN)
            deviceToken?.let { initUI(it) }
        }
    }

    private fun initUI(token: String) {
        // device info
        viewModel.getDeviceInfo(token).observe(this) {
            when (it.status) {
                Status.LOADING -> {
                    visibleUI(true)
                }
                Status.SUCCESS -> {
                    visibleUI(false)
                    it.data?.let { data ->
                        binding.layoutDeviceInfo.tvName.text = data.name
                        binding.layoutDeviceInfo.tvShortname.text = data.shortName
                        binding.layoutDeviceInfo.tvStatus.text = data.status

                        // set ssid
                        if (data.deviceSsid == null && data.devicePassword == null)
                            binding.layoutDeviceInfo.tvSsid.text = data.userSsid
                        else binding.layoutDeviceInfo.tvSsid.text = data.deviceSsid

                        // edit name
                        binding.layoutDeviceInfo.btnEdit.setOnClickListener {
                            dialog = EditDeviceDialogFragment(data.id!!, data.name!!, this)
                            dialog.show(supportFragmentManager, "editDialog")
                        }

                        // edit ssid
                        binding.layoutDeviceInfo.btnEditDeviceSsid.setOnClickListener {
                            ssidDialog = ChangeDeviceSsidDialogFragment(data, this)
                            ssidDialog.show(supportFragmentManager, "changeSsid")
                        }
                    }

                }
                Status.ERROR -> {
                    binding.layoutMain.visibility = View.GONE
                    binding.layoutShimmer.visibility = View.GONE
                    binding.layoutError.visibility = View.VISIBLE
                    Timber.i("error => ${it.message}")
                }
            }
        }

        // last data
        viewModel.getLastSensor(token).observe(this) {
            when (it.status) {
                Status.LOADING -> {
                    visibleUI(true)
                }
                Status.SUCCESS -> {
                    visibleUI(false)
                    it.data?.let { data ->
                        val dateTime = "${data.date} ${data.time}"
                        binding.layoutDeviceInfo.tvLastUpdate.text =
                            StringBuilder().append(dateTime.dateFormat())

                        // check for null data
                        data.temp?.let { temp ->
                            Glide.with(this@SensorActivity)
                                .load(R.drawable.ic_temperature)
                                .into(binding.layoutSensor.imageTemp)

                            binding.layoutSensor.tvTemp.text =
                                StringBuilder().append(temp).append("\u00B0")
                        } ?: run {
                            Glide.with(this@SensorActivity)
                                .load(R.drawable.ic_temperature_off)
                                .into(binding.layoutSensor.imageTemp)

                            binding.layoutSensor.tvTemp.text = StringBuilder().append("N/A")
                        }

                        data.hum?.let { hum ->
                            Glide.with(this@SensorActivity)
                                .load(R.drawable.ic_humidity)
                                .into(binding.layoutSensor.imageHum)

                            binding.layoutSensor.tvHum.text =
                                StringBuilder().append(hum).append("%")
                        } ?: run {
                            Glide.with(this@SensorActivity)
                                .load(R.drawable.ic_humidity_off)
                                .into(binding.layoutSensor.imageHum)

                            binding.layoutSensor.tvHum.text = StringBuilder().append("N/A")
                        }

                        data.lux?.let { lux ->
                            Glide.with(this@SensorActivity)
                                .load(R.drawable.ic_lux)
                                .into(binding.layoutSensor.imageLux)

                            binding.layoutSensor.tvLux.text =
                                StringBuilder().append(lux).append("%")
                        } ?: run {
                            Glide.with(this@SensorActivity)
                                .load(R.drawable.ic_lux_off)
                                .into(binding.layoutSensor.imageLux)

                            binding.layoutSensor.tvLux.text =
                                StringBuilder().append("N/A")
                        }

                        data.pir?.let { pir ->
                            Glide.with(this@SensorActivity)
                                .load(R.drawable.ic_pir)
                                .into(binding.layoutSensor.imagePir)

                            binding.layoutSensor.tvPir.text = pir.toString()
                        } ?: run {
                            Glide.with(this@SensorActivity)
                                .load(R.drawable.ic_pir_off)
                                .into(binding.layoutSensor.imagePir)

                            binding.layoutSensor.tvPir.text =
                                StringBuilder().append("N/A")
                        }

                    }
                }
                Status.ERROR -> {
                    binding.layoutMain.visibility = View.GONE
                    binding.layoutShimmer.visibility = View.GONE
                    binding.layoutError.visibility = View.VISIBLE
                    Timber.i("error => ${it.message}")
                }
            }
        }

        // history data
        lifecycleScope.launch {
            viewModel.getHistorySensor(token).collect {
                adapter.submitData(it)
            }
        }

        // loading state
        with(adapter) {
            // loading state
            addLoadStateListener { loadState ->
                // handle initial loading
                if (loadState.append.endOfPaginationReached) {
                    // handle if data is empty
                    if (adapter.itemCount < 1) {
                        binding.rvHistory.visibility = View.GONE
                        binding.layoutEmpty.visibility = View.VISIBLE
                    }
                } else {
                    // handle if data is exists
                    binding.rvHistory.visibility = View.VISIBLE
                    binding.layoutEmpty.visibility = View.GONE
                }
            }
        }

    }

    override fun changeSsid(jsonObject: JsonObject, data: Device) {
        CustomDialogFragment(
            getString(R.string.perhatian),
            getString(R.string.update_info_disclaimer)
        ) {
            ssidDialog.dismiss()

            sharedPrefs.get(AUTH_TOKEN)?.let { userToken ->
                viewModel.changeDeviceSsid(userToken, data.id!!, jsonObject).observe(this) {
                    when (it.status) {
                        Status.LOADING -> {}
                        Status.SUCCESS -> {
                            initUI(deviceToken!!)
                            Toast.makeText(this, "Success update SSID!", Toast.LENGTH_SHORT)
                                .show()
                        }
                        Status.ERROR -> {
                            Timber.i("error => ${it.message}")
                            Toast.makeText(this, "Failed to update SSID!", Toast.LENGTH_SHORT)
                                .show()
                        }
                    }
                }
            }
        }.show(supportFragmentManager, "customDialog")
    }

    private fun setupAdapter(recyclerView: RecyclerView) {
        adapter.adapterCallback(adapterCallback)
            .setLayout(R.layout.item_sensor_history)
            .isVerticalView()
            .build(recyclerView)
    }

    private val adapterCallback = object : AdapterCallback<Sensor> {
        override fun initComponent(itemView: View, data: Sensor, itemIndex: Int) {
            val dateTime = "${data.date} ${data.time}"
            itemView.tv_date.text = StringBuilder().append(dateTime.dateFormat())

            // check for null data
            data.temp?.let { temp ->
                itemView.tv_temp.text = StringBuilder().append(temp).append("\u00B0")
            } ?: run {
                itemView.tv_temp.text = StringBuilder().append("N/A")
            }

            data.hum?.let { hum ->
                itemView.tv_hum.text = StringBuilder().append(hum).append("%")
            } ?: run {
                itemView.tv_hum.text = StringBuilder().append("N/A")
            }

            data.lux?.let { lux ->
                itemView.tv_lux.text = StringBuilder().append(lux).append("%")
            } ?: run {
                itemView.tv_lux.text = StringBuilder().append("N/A")
            }

            data.pir?.let { pir ->
                itemView.tv_pir.text = pir.toString()
            } ?: run {
                itemView.tv_pir.text = StringBuilder().append("N/A")
            }
        }

        override fun onItemClicked(itemView: View, data: Sensor, itemIndex: Int) {}

    }

    override fun updateDevice(id: Int, name: String) {
        CustomDialogFragment(
            getString(R.string.perhatian),
            getString(R.string.device_update_disclaimer)
        ) {
            dialog.dismiss()
            val rawData = JsonObject().apply {
                addProperty("device_name", name)
            }

            sharedPrefs.get(AUTH_TOKEN)?.let { userToken ->
                viewModel.updateDevice(userToken, id, rawData).observe(this, {
                    when (it.status) {
                        Status.LOADING -> {
                        }
                        Status.SUCCESS -> {
                            initUI(deviceToken!!)
                            Toast.makeText(this, "Berhasil update perangkat!", Toast.LENGTH_SHORT)
                                .show()
                        }
                        Status.ERROR -> {
                            Timber.i("error => ${it.message}")
                            Toast.makeText(this, "Gagal update perangkat!", Toast.LENGTH_SHORT)
                                .show()
                        }
                    }
                })
            }
        }.show(supportFragmentManager, "customDialog")
    }

    private fun visibleUI(isLoading: Boolean) {
        if (isLoading) {
            binding.layoutMain.visibility = View.GONE
            binding.layoutShimmer.visibility = View.VISIBLE
        } else {
            binding.layoutMain.visibility = View.VISIBLE
            binding.layoutShimmer.visibility = View.GONE
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    companion object {
        const val EXTRA_TOKEN = "extra_token"
    }
}