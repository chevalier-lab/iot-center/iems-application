package com.aitekteam.developer.iemscontrolling.ui.user.profile.user.access.invite

import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import androidx.recyclerview.widget.RecyclerView
import com.aitekteam.developer.iemscontrolling.R
import com.aitekteam.developer.iemscontrolling.core.data.Resource
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.general.Device
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.profile.User
import com.aitekteam.developer.iemscontrolling.core.utils.adapter.AdapterCallback
import com.aitekteam.developer.iemscontrolling.core.utils.adapter.ReusableAdapter
import com.aitekteam.developer.iemscontrolling.core.utils.adapter.ReusablePagingAdapter
import com.aitekteam.developer.iemscontrolling.core.utils.vo.Status
import com.aitekteam.developer.iemscontrolling.databinding.ActivityAddDeviceGuestBinding
import com.aitekteam.developer.iemscontrolling.ui.user.profile.user.access.detail.DetailGuestActivity
import com.aitekteam.developer.iemscontrolling.utils.dialog.CustomDialogFragment
import com.aitekteam.developer.iemscontrolling.utils.helper.Constants.AUTH_TOKEN
import com.aitekteam.developer.iemscontrolling.utils.helper.SharedPrefsUtils
import com.bumptech.glide.Glide
import com.google.gson.JsonObject
import kotlinx.android.synthetic.main.activity_manage_account.*
import kotlinx.android.synthetic.main.item_device_group.view.*
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.koin.android.viewmodel.ext.android.viewModel

class AddDeviceGuestActivity : AppCompatActivity() {
    private lateinit var binding: ActivityAddDeviceGuestBinding
    private val viewModel: AddDeviceGuestViewModel by viewModel()

    // utils
    private var user: User? = null
    private lateinit var sharedPrefs: SharedPrefsUtils
    private lateinit var adapter: ReusableAdapter<Device>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAddDeviceGuestBinding.inflate(layoutInflater)
        setContentView(binding.root)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.apply {
                addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                statusBarColor = Color.TRANSPARENT
                this.decorView.rootView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            }
        }

        binding.toolbar.apply {
            setSupportActionBar(this)
            supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_back_black)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setDisplayShowTitleEnabled(false)
        }

        // init var
        sharedPrefs = SharedPrefsUtils()
        sharedPrefs.start(this, AUTH_TOKEN)
        adapter = ReusableAdapter(this)

        // setup adapter
        setupAdapter(binding.rvDevice)

        // get intent data
        val extra = intent.extras
        if (extra != null)
            user = extra.getParcelable(EXTRA_DATA)


        // init UI
        initUI()
    }

    private fun initUI() {
        sharedPrefs.get(AUTH_TOKEN)?.let { token ->
            viewModel.getListDevices(token).observe(this) {
                when (it.status) {
                    Status.LOADING -> {
                        with(binding) {
                            rvDevice.visibility = View.GONE
                            layoutEmpty.visibility = View.GONE
                            layoutError.visibility = View.GONE
                            shimmerLoading.rootLayout.visibility = View.VISIBLE
                        }
                    }
                    Status.SUCCESS -> {
                        with (binding) {
                            rvDevice.visibility = View.VISIBLE
                            layoutEmpty.visibility = View.GONE
                            layoutError.visibility = View.GONE
                            shimmerLoading.rootLayout.visibility = View.GONE
                        }

                        it.data?.let { data ->
                            if (data.isNotEmpty())
                                adapter.addData(data)
                            else {
                                with(binding) {
                                    rvDevice.visibility = View.GONE
                                    layoutError.visibility = View.GONE
                                    layoutEmpty.visibility = View.VISIBLE
                                    shimmerLoading.rootLayout.visibility = View.GONE
                                }
                            }
                        }
                    }
                    Status.ERROR -> {
                        with (binding) {
                            rvDevice.visibility = View.GONE
                            layoutEmpty.visibility = View.GONE
                            layoutError.visibility = View.VISIBLE
                            shimmerLoading.rootLayout.visibility = View.GONE
                        }
                    }
                }
            }
        }
    }

    private fun setupAdapter(recyclerView: RecyclerView) {
        adapter.adapterCallback(adapterCallback)
            .setLayout(R.layout.item_device_group)
            .isVerticalView()
            .build(recyclerView)
    }

    private val adapterCallback = object : AdapterCallback<Device> {
        override fun initComponent(itemView: View, data: Device, itemIndex: Int) {
            itemView.tv_device_name.text = data.name

            // set icon
            when (data.type) {
                "pcb" -> {
                    Glide.with(this@AddDeviceGuestActivity)
                        .load(R.drawable.ic_pcb)
                        .into(itemView.image_device)
                }
                "slca" -> {
                    Glide.with(this@AddDeviceGuestActivity)
                        .load(R.drawable.ic_slca)
                        .into(itemView.image_device)
                }
                "sensors" -> {
                    Glide.with(this@AddDeviceGuestActivity)
                        .load(R.drawable.ic_sensor)
                        .into(itemView.image_device)
                }
                "kwh-1-phase" -> {
                    Glide.with(this@AddDeviceGuestActivity)
                        .load(R.drawable.ic_slca)
                        .into(itemView.image_device)
                }
                "kwh-3-phase" -> {
                    Glide.with(this@AddDeviceGuestActivity)
                        .load(R.drawable.ic_slca)
                        .into(itemView.image_device)
                }
            }

            // append device
            sharedPrefs.get(AUTH_TOKEN)?.let { token ->
                itemView.btn_append.setOnClickListener {
                    CustomDialogFragment(
                        getString(R.string.perhatian),
                        getString(R.string.add_device_guest_disclaimer)
                    ) {
                        val rawData = JsonObject().apply {
                            addProperty("phone_number", user?.phoneNumber)
                            addProperty("id_m_devices", data.id)
                        }
                        viewModel.addDeviceGuest(token, rawData).observe(this@AddDeviceGuestActivity, observer)
                    }.show(supportFragmentManager, "customDialog")
                }
            }
        }

        override fun onItemClicked(itemView: View, data: Device, itemIndex: Int) {}
    }

    private val observer = Observer<Resource<Device>> {
        when (it.status) {
            Status.LOADING -> {}
            Status.SUCCESS -> {
                Toast.makeText(this, "Successfully added access!", Toast.LENGTH_SHORT).show()
            }
            Status.ERROR -> {
                Toast.makeText(this, it.message, Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                startActivity(
                    Intent(this, DetailGuestActivity::class.java)
                        .putExtra(DetailGuestActivity.EXTRA_DATA, user)
                )
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onBackPressed() {
        startActivity(
            Intent(this, DetailGuestActivity::class.java)
                .putExtra(DetailGuestActivity.EXTRA_DATA, user)
        )
        finish()
    }

    companion object {
        const val EXTRA_DATA = "extra_data"
    }
}