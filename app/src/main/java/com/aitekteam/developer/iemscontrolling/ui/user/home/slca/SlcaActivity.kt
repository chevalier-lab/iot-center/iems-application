package com.aitekteam.developer.iemscontrolling.ui.user.home.slca

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.RecyclerView
import com.aitekteam.developer.iemscontrolling.R
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.general.Device
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.slca.Slca
import com.aitekteam.developer.iemscontrolling.core.utils.adapter.AdapterCallback
import com.aitekteam.developer.iemscontrolling.core.utils.adapter.ReusablePagingAdapter
import com.aitekteam.developer.iemscontrolling.core.utils.vo.Status
import com.aitekteam.developer.iemscontrolling.databinding.ActivitySlcaBinding
import com.aitekteam.developer.iemscontrolling.utils.dialog.AddSchedulingDialogFragment
import com.aitekteam.developer.iemscontrolling.utils.dialog.ChangeDeviceSsidDialogFragment
import com.aitekteam.developer.iemscontrolling.utils.dialog.CustomDialogFragment
import com.aitekteam.developer.iemscontrolling.utils.dialog.EditDeviceDialogFragment
import com.aitekteam.developer.iemscontrolling.utils.helper.Constants.AUTH_TOKEN
import com.aitekteam.developer.iemscontrolling.utils.helper.Helpers.dateFormat
import com.aitekteam.developer.iemscontrolling.utils.helper.SharedPrefsUtils
import com.aitekteam.developer.iemscontrolling.utils.listener.BottomSheetListener
import com.aitekteam.developer.iemscontrolling.utils.listener.DialogListener
import com.google.gson.JsonObject
import kotlinx.android.synthetic.main.item_slca_history.view.*
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.koin.android.viewmodel.ext.android.viewModel
import timber.log.Timber

class SlcaActivity : AppCompatActivity(), DialogListener, BottomSheetListener {
    private lateinit var binding: ActivitySlcaBinding
    private val viewModel: SlcaViewModel by viewModel()

    // utils
    private var deviceId: String? = null
    private var deviceToken: String? = null
    private lateinit var sharedPrefs: SharedPrefsUtils
    private lateinit var dialog: EditDeviceDialogFragment
    private lateinit var adapter: ReusablePagingAdapter<Slca>
    private lateinit var ssidDialog: ChangeDeviceSsidDialogFragment
    private lateinit var bottomSheet: BottomSheetScheduler

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySlcaBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.toolbar.apply {
            setSupportActionBar(this)
            supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_back)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setDisplayShowTitleEnabled(false)
        }

        // init utils
        adapter = ReusablePagingAdapter(this)
        sharedPrefs = SharedPrefsUtils()
        sharedPrefs.start(this, AUTH_TOKEN)

        // setup adapter
        setupAdapter(binding.rvHistory)

        val extra = intent.extras
        if (extra != null) {
            deviceToken = extra.getString(EXTRA_TOKEN)
            deviceToken?.let { initUI(it) }
        }
    }

    private fun initUI(token: String) {
        // device info
        viewModel.getDeviceInfo(token).observe(this) {
            when (it.status) {
                Status.LOADING -> {
                    visibleUI(true)
                }
                Status.SUCCESS -> {
                    visibleUI(false)
                    it.data?.let { data ->
                        data.id?.let { id -> deviceId = id.toString() }

                        binding.layoutDeviceInfo.tvName.text = data.name
                        binding.layoutDeviceInfo.tvShortname.text = data.shortName
                        binding.layoutDeviceInfo.tvStatus.text = data.status

                        // set ssid
                        if (data.deviceSsid == null && data.devicePassword == null)
                            binding.layoutDeviceInfo.tvSsid.text = data.userSsid
                        else binding.layoutDeviceInfo.tvSsid.text = data.deviceSsid

                        // edit name
                        binding.layoutDeviceInfo.btnEdit.setOnClickListener {
                            dialog = EditDeviceDialogFragment(data.id!!, data.name!!, this)
                            dialog.show(supportFragmentManager, "editDialog")
                        }

                        // edit ssid
                        binding.layoutDeviceInfo.btnEditDeviceSsid.setOnClickListener {
                            ssidDialog = ChangeDeviceSsidDialogFragment(data, this)
                            ssidDialog.show(supportFragmentManager, "changeSsid")
                        }
                    }
                }
                Status.ERROR -> {
                    binding.layoutMain.visibility = View.GONE
                    binding.layoutShimmer.visibility = View.GONE
                    binding.layoutError.visibility = View.VISIBLE
                    Timber.i("error => ${it.message}")
                }
            }
        }

        // last data
        viewModel.getLastSlca(token).observe(this) {
            when (it.status) {
                Status.LOADING -> {
                    visibleUI(true)
                }
                Status.SUCCESS -> {
                    visibleUI(false)
                    it.data?.let { data ->
                        if (data.date != null && data.time != null) {
                            val dateTime = "${data.date} ${data.time}"
                            binding.layoutDeviceInfo.tvLastUpdate.text = StringBuilder().append(dateTime.dateFormat())
                        } else binding.layoutDeviceInfo.layoutLastUpdate.visibility = View.GONE

                        if (data.iac == null)
                            binding.layoutSlca.tvIac.text = StringBuilder().append("N/A")
                        else
                            binding.layoutSlca.tvIac.text = data.iac.toString()

                        if (data.power == null)
                            binding.layoutSlca.tvPower.text = StringBuilder().append("N/A")
                        else
                            binding.layoutSlca.tvPower.text = data.power.toString()

                        // history data
                        lifecycleScope.launch {
                            viewModel.getHistorySlca(token).collect { data ->
                                adapter.submitData(data)
                            }
                        }

                        // loading state
                        with(adapter) {
                            // loading state
                            addLoadStateListener { loadState ->
                                // handle initial loading
                                if (loadState.append.endOfPaginationReached) {
                                    // handle if data is empty
                                    if (adapter.itemCount < 1) {
                                        binding.rvHistory.visibility = View.GONE
                                        binding.layoutEmpty.visibility = View.VISIBLE
                                    }
                                } else {
                                    // handle if data is exists
                                    binding.rvHistory.visibility = View.VISIBLE
                                    binding.layoutEmpty.visibility = View.GONE
                                }
                            }
                        }
                    }
                }
                Status.ERROR -> {
                    binding.layoutMain.visibility = View.GONE
                    binding.layoutShimmer.visibility = View.GONE
                    binding.layoutError.visibility = View.VISIBLE
                    Timber.i("error => ${it.message}")
                }
            }
        }
    }

    override fun changeSsid(jsonObject: JsonObject, data: Device) {
        CustomDialogFragment(
            getString(R.string.perhatian),
            getString(R.string.update_info_disclaimer)
        ) {
            ssidDialog.dismiss()

            sharedPrefs.get(AUTH_TOKEN)?.let { userToken ->
                viewModel.changeDeviceSsid(userToken, data.id!!, jsonObject).observe(this) {
                    when (it.status) {
                        Status.LOADING -> {}
                        Status.SUCCESS -> {
                            initUI(deviceToken!!)
                            Toast.makeText(this, "Success update SSID!", Toast.LENGTH_SHORT)
                                .show()
                        }
                        Status.ERROR -> {
                            Timber.i("error => ${it.message}")
                            Toast.makeText(this, "Failed to update SSID!", Toast.LENGTH_SHORT)
                                .show()
                        }
                    }
                }
            }
        }.show(supportFragmentManager, "customDialog")
    }

    private fun setupAdapter(recyclerView: RecyclerView) {
        adapter.adapterCallback(adapterCallback)
            .setLayout(R.layout.item_slca_history)
            .isVerticalView()
            .build(recyclerView)
    }

    private val adapterCallback = object : AdapterCallback<Slca> {
        override fun initComponent(itemView: View, data: Slca, itemIndex: Int) {
            val dateTime = "${data.date} ${data.time}"
            itemView.tv_date.text = StringBuilder().append(dateTime.dateFormat())
            itemView.tv_iac.text = data.iac.toString()
            itemView.tv_power.text = data.power.toString()
        }

        override fun onItemClicked(itemView: View, data: Slca, itemIndex: Int) {}

    }

    override fun updateDevice(id: Int, name: String) {
        CustomDialogFragment(
            getString(R.string.perhatian),
            getString(R.string.device_update_disclaimer)
        ) {
            dialog.dismiss()
            val rawData = JsonObject().apply {
                addProperty("device_name", name)
            }

            sharedPrefs.get(AUTH_TOKEN)?.let { userToken ->
                viewModel.updateDevice(userToken, id, rawData).observe(this, {
                    when (it.status) {
                        Status.LOADING -> {
                        }
                        Status.SUCCESS -> {
                            initUI(deviceToken!!)
                            Toast.makeText(this, "Berhasil update perangkat!", Toast.LENGTH_SHORT)
                                .show()
                        }
                        Status.ERROR -> {
                            Timber.i("error => ${it.message}")
                            Toast.makeText(this, "Gagal update perangkat!", Toast.LENGTH_SHORT)
                                .show()
                        }
                    }
                })
            }
        }.show(supportFragmentManager, "customDialog")
    }

    private fun getListSchedulingSLCA() {
        sharedPrefs.get(AUTH_TOKEN)?.let { token ->
            deviceId?.let { id ->
                viewModel.getListScheduler(token, id).observe(this) {
                    when (it.status) {
                        Status.LOADING -> {
                        }
                        Status.SUCCESS -> {
                            it.data?.let { list ->
                                bottomSheet = BottomSheetScheduler(this, id, list, this)
                                bottomSheet.showBottomSheet()
                            }
                        }
                        Status.ERROR -> {
                        }
                    }
                }
            }
        }
    }

    override fun scheduleDevice(deviceId: String) {
        bottomSheet.dismiss()
        AddSchedulingDialogFragment(deviceId, this).show(
            supportFragmentManager,
            "addScheduleDialog"
        )
    }

    override fun deleteScheduleDevice(scheduleId: String) {
        CustomDialogFragment(
            getString(R.string.perhatian),
            getString(R.string.delete_schedule_disclaimer)
        ) {
            sharedPrefs.get(AUTH_TOKEN)?.let { token ->
                viewModel.deleteScheduler(token, scheduleId).observe(this) {
                    when (it.status) {
                        Status.LOADING -> {}
                        Status.SUCCESS -> {
                            bottomSheet.dismiss()
                            Toast.makeText(this, "Sukses menghapus schedule!", Toast.LENGTH_SHORT)
                                .show()
                        }
                        Status.ERROR -> {
                            Timber.i("error => ${it.message}")
                            Toast.makeText(this, "Gagal menghapus schedule!", Toast.LENGTH_SHORT)
                                .show()
                        }
                    }
                }
            }
        }.show(supportFragmentManager, "customDialog")
    }

    override fun createSchedule(deviceId: String, time: String, state: String) {
        val rawData = JsonObject().apply {
            addProperty("action_time", time)
            addProperty("action", state)
        }

        sharedPrefs.get(AUTH_TOKEN)?.let { token ->
            viewModel.createScheduler(token, deviceId, rawData).observe(this) {
                when (it.status) {
                    Status.LOADING -> {}
                    Status.SUCCESS -> {
                        Toast.makeText(this, "Sukses menambahkan schedule!", Toast.LENGTH_SHORT)
                            .show()
                    }
                    Status.ERROR -> {
                        Timber.i("error => ${it.message}")
                        Toast.makeText(this, "Gagal menambahkan schedule!", Toast.LENGTH_SHORT)
                            .show()
                    }
                }
            }
        }
    }

    private fun visibleUI(isLoading: Boolean) {
        if (isLoading) {
            binding.layoutMain.visibility = View.GONE
            binding.layoutShimmer.visibility = View.VISIBLE
        } else {
            binding.layoutMain.visibility = View.VISIBLE
            binding.layoutShimmer.visibility = View.GONE
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        menu?.getItem(0)?.icon = ContextCompat.getDrawable(this, R.drawable.ic_time_white)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            R.id.overflow -> {
                getListSchedulingSLCA()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    companion object {
        const val EXTRA_TOKEN = "extra_token"
    }
}