package com.aitekteam.developer.iemscontrolling.ui.user.home.kwh

import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import androidx.recyclerview.widget.RecyclerView
import com.aitekteam.developer.iemscontrolling.R
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.general.Device
import com.aitekteam.developer.iemscontrolling.core.utils.adapter.AdapterCallback
import com.aitekteam.developer.iemscontrolling.core.utils.adapter.ReusablePagingAdapter
import com.aitekteam.developer.iemscontrolling.databinding.ActivityListKwhBinding
import com.aitekteam.developer.iemscontrolling.ui.user.home.kwh1.detail.PowerStatus1Activity
import com.aitekteam.developer.iemscontrolling.ui.user.home.kwh3.detail.PowerStatus3Activity
import com.aitekteam.developer.iemscontrolling.utils.helper.Constants.AUTH_TOKEN
import com.aitekteam.developer.iemscontrolling.utils.helper.SharedPrefsUtils
import kotlinx.android.synthetic.main.item_total_kwh.view.*
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.koin.android.viewmodel.ext.android.viewModel
import java.text.DecimalFormat

class ListKwhActivity : AppCompatActivity() {
    private lateinit var binding: ActivityListKwhBinding
    private val viewModel: ListKwhViewModel by viewModel()

    // utils
    private var groupId: Int? = null
    private lateinit var sharedPrefs: SharedPrefsUtils
    private lateinit var adapter: ReusablePagingAdapter<Device>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityListKwhBinding.inflate(layoutInflater)
        setContentView(binding.root)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.apply {
                addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                statusBarColor = Color.TRANSPARENT
                this.decorView.rootView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            }
        }

        binding.toolbar.apply {
            setSupportActionBar(this)
            supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_back_black)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setDisplayShowTitleEnabled(false)
        }

        // init var
        sharedPrefs = SharedPrefsUtils()
        sharedPrefs.start(this, AUTH_TOKEN)
        adapter = ReusablePagingAdapter(this)

        // setup adapter
        setupAdapter(binding.rvKwh)

        // get intent data
        val extra = intent.extras
        if (extra != null) {
            groupId = extra.getInt(EXTRA_ID)
        }

        // init ui
        initUI()
    }

    private fun initUI() {
        sharedPrefs.get(AUTH_TOKEN)?.let { token ->
            lifecycleScope.launch {
                groupId?.let {
                    viewModel.getListKwhOnGroup(token, it).collect { data ->
                        adapter.submitData(data)
                    }
                } ?: run {
                    viewModel.getListKwh(token).collect { data ->
                        adapter.submitData(data)
                    }
                }
            }
        }

        // loading state
        with(adapter) {
            // loading state
            addLoadStateListener { loadState ->
                if (loadState.refresh is LoadState.Loading) {
                    binding.rvKwh.visibility = View.GONE
                    binding.shimmerLoading.rootLayout.visibility = View.VISIBLE
                } else {
                    // handle if data is exists
                    binding.rvKwh.visibility = View.VISIBLE
                    binding.shimmerLoading.rootLayout.visibility = View.GONE

                    // get error
                    val error = when {
                        loadState.prepend is LoadState.Error -> loadState.prepend as LoadState.Error
                        loadState.append is LoadState.Error -> loadState.append as LoadState.Error
                        loadState.refresh is LoadState.Error -> loadState.refresh as LoadState.Error
                        else -> null
                    }

                    error?.let {
                        binding.rvKwh.visibility = View.VISIBLE
                        binding.layoutError.visibility = View.VISIBLE
                        binding.shimmerLoading.rootLayout.visibility = View.GONE
                    }
                }
            }
        }
    }

    private fun setupAdapter(recyclerView: RecyclerView) {
        adapter.adapterCallback(adapterCallback)
            .setLayout(R.layout.item_total_kwh)
            .isVerticalView()
            .build(recyclerView)
    }

    private val adapterCallback = object : AdapterCallback<Device> {
        override fun initComponent(itemView: View, data: Device, itemIndex: Int) {
            val df = DecimalFormat("#.###")
            itemView.tv_name.text = StringBuilder().append(data.name)
            itemView.tv_total_kwh.text = df.format(data.preview?.total)
            itemView.tv_total_cost.text = data.preview?.priceStr?.replace("Rp", "")
        }

        override fun onItemClicked(itemView: View, data: Device, itemIndex: Int) {
            if (data.type == "kwh-1-phase")
                startActivity(
                    Intent(
                        this@ListKwhActivity,
                        PowerStatus1Activity::class.java
                    ).putExtra(PowerStatus1Activity.EXTRA_TOKEN, data.token)
                )
            else if (data.type == "kwh-3-phase")
                startActivity(
                    Intent(
                        this@ListKwhActivity,
                        PowerStatus3Activity::class.java
                    ).putExtra(PowerStatus3Activity.EXTRA_TOKEN, data.token)
                )
        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    companion object {
        const val EXTRA_ID = "extra_id"
    }
}