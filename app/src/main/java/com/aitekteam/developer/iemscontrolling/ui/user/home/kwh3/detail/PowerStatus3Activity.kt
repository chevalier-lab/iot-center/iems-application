package com.aitekteam.developer.iemscontrolling.ui.user.home.kwh3.detail

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.aitekteam.developer.iemscontrolling.R
import com.aitekteam.developer.iemscontrolling.core.data.Resource
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.calculate.TotalKwh
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.general.Device
import com.aitekteam.developer.iemscontrolling.core.utils.vo.Status
import com.aitekteam.developer.iemscontrolling.databinding.ActivityPowerStatus3Binding
import com.aitekteam.developer.iemscontrolling.ui.user.home.kwh3.chart.WebViewChartKwh3Activity
import com.aitekteam.developer.iemscontrolling.utils.dialog.ChangeDeviceSsidDialogFragment
import com.aitekteam.developer.iemscontrolling.utils.helper.Constants.AUTH_TOKEN
import com.aitekteam.developer.iemscontrolling.utils.dialog.CustomDialogFragment
import com.aitekteam.developer.iemscontrolling.utils.dialog.EditDeviceDialogFragment
import com.aitekteam.developer.iemscontrolling.utils.helper.SharedPrefsUtils
import com.aitekteam.developer.iemscontrolling.utils.listener.DialogListener
import com.google.gson.JsonObject
import org.koin.android.viewmodel.ext.android.viewModel
import timber.log.Timber
import java.text.DecimalFormat

class PowerStatus3Activity : AppCompatActivity(), DialogListener {
    private lateinit var binding: ActivityPowerStatus3Binding
    private val viewModel: PowerStatus3ViewModel by viewModel()

    // utils
    private var deviceToken: String? = null
    private lateinit var sharedPrefs: SharedPrefsUtils
    private lateinit var dialog: EditDeviceDialogFragment
    private lateinit var ssidDialog: ChangeDeviceSsidDialogFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPowerStatus3Binding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.toolbar.apply {
            setSupportActionBar(this)
            supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_back)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setDisplayShowTitleEnabled(false)
        }

        // init vars
        sharedPrefs = SharedPrefsUtils()
        sharedPrefs.start(this, AUTH_TOKEN)

        val extra = intent.extras
        if (extra != null) {
            deviceToken = extra.getString(EXTRA_TOKEN)
            deviceToken?.let { initUI(it) }
        }
    }

    private fun initUI(token: String) {
        // go to chart
        binding.btnChart.setOnClickListener {
            startActivity(
                Intent(
                    this,
                    WebViewChartKwh3Activity::class.java
                ).putExtra(WebViewChartKwh3Activity.EXTRA_TOKEN, token)
            )
        }

        // get device info
        viewModel.getDeviceInfo(token).observe(this) {
            when (it.status) {
                Status.LOADING -> {
                    visibleUI(true)
                }
                Status.SUCCESS -> {
                    visibleUI(false)
                    it.data?.let { data ->
                        // set information
                        binding.tvName.text = data.name
                        binding.tvShortname.text = data.shortName
                        binding.tvStatus.text = data.status
                        binding.tvType.text = data.type

                        // set ssid
                        if (data.deviceSsid == null && data.devicePassword == null)
                            binding.tvSsid.text = data.userSsid
                        else binding.tvSsid.text = data.deviceSsid


                        // get last data
                        viewModel.getLastKwh3(token).observe(this) { lastData ->
                            when (lastData.status) {
                                Status.LOADING -> {
                                    visibleUI(true)
                                }
                                Status.SUCCESS -> {
                                    visibleUI(false)
                                    lastData.data?.let { data ->
                                        val kwh = data.ep?.toDouble()
                                        val power = data.pt?.toDouble()
                                        val freq = data.freq?.toDouble()
                                        val currentA = data.ia?.toDouble()
                                        val currentB = data.ib?.toDouble()
                                        val currentC = data.ic?.toDouble()
                                        val voltageA = data.va?.toDouble()
                                        val voltageB = data.vb?.toDouble()
                                        val voltageC = data.vc?.toDouble()
                                        val powerFactorA = data.pfa?.toDouble()
                                        val powerFactorB = data.pfb?.toDouble()
                                        val powerFactorC = data.pfc?.toDouble()

                                        val df = DecimalFormat("#.###")
                                        voltageA?.let {
                                            binding.tvVoltageA.text = StringBuilder().append(df.format(voltageA))
                                        } ?: run {
                                            binding.tvVoltageA.text = StringBuilder().append("N/A")
                                        }

                                        voltageB?.let {
                                            binding.tvVoltageB.text = StringBuilder().append(df.format(voltageB))
                                        } ?: run {
                                            binding.tvVoltageB.text = StringBuilder().append("N/A")
                                        }

                                        voltageC?.let {
                                            binding.tvVoltageC.text = StringBuilder().append(df.format(voltageC))
                                        } ?: run {
                                            binding.tvVoltageC.text = StringBuilder().append("N/A")
                                        }

                                        currentA?.let {
                                            binding.tvCurrentA.text = StringBuilder().append(df.format(currentA))
                                        } ?: run {
                                            binding.tvCurrentA.text = StringBuilder().append("N/A")
                                        }

                                        currentB?.let {
                                            binding.tvCurrentB.text = StringBuilder().append(df.format(currentB))
                                        } ?: run {
                                            binding.tvCurrentB.text = StringBuilder().append("N/A")
                                        }

                                        currentC?.let {
                                            binding.tvCurrentC.text = StringBuilder().append(df.format(currentC))
                                        } ?: run {
                                            binding.tvCurrentC.text = StringBuilder().append("N/A")
                                        }

                                        powerFactorA?.let {
                                            binding.tvPowerFactorA.text = StringBuilder().append(df.format(powerFactorA))
                                        } ?: run {
                                            binding.tvPowerFactorA.text = StringBuilder().append("N/A")
                                        }

                                        powerFactorB?.let {
                                            binding.tvPowerFactorB.text = StringBuilder().append(df.format(powerFactorB))
                                        } ?: run {
                                            binding.tvPowerFactorB.text = StringBuilder().append("N/A")
                                        }

                                        powerFactorC?.let {
                                            binding.tvPowerFactorC.text = StringBuilder().append(df.format(powerFactorC))
                                        } ?: run {
                                            binding.tvPowerFactorC.text = StringBuilder().append("N/A")
                                        }

                                        freq?.let {
                                            binding.tvFrequency3.text = StringBuilder().append(df.format(freq))
                                        } ?: run {
                                            binding.tvFrequency3.text = StringBuilder().append("N/A")
                                        }

                                        power?.let {
                                            binding.tvPower3.text = StringBuilder().append(df.format(power))
                                        } ?: run {
                                            binding.tvPower3.text = StringBuilder().append("N/A")
                                        }

                                        kwh?.let {
                                            binding.tvKwh3.text = StringBuilder().append(df.format(kwh))
                                        } ?: run {
                                            binding.tvKwh3.text = StringBuilder().append("N/A")
                                        }
                                    }

                                    sharedPrefs.get(AUTH_TOKEN)?.let { userToken ->
                                        viewModel.calculateKwh(userToken, token).observe(this, calculateObserver)
                                    }
                                }
                                Status.ERROR -> {
                                    binding.layoutMain.visibility = View.GONE
                                    binding.layoutBottom.visibility = View.GONE
                                    binding.layoutShimmer.visibility = View.GONE
                                    binding.layoutError.visibility = View.VISIBLE
                                    Timber.i("error => ${it.message}")
                                }
                            }
                        }
//                        data.ssid?.let { ssid -> binding.tvSsid.text = ssid }
//                            ?: run {
//                                binding.lineSsid.visibility = View.GONE
//                                binding.layoutSsid.visibility = View.GONE
//                            }

                        // edit name
                        binding.btnEdit.setOnClickListener {
                            dialog = EditDeviceDialogFragment(data.id!!, data.name!!, this)
                            dialog.show(supportFragmentManager, "editDialog")
                        }

                        // edit ssid
                        binding.btnEditDeviceSsid.setOnClickListener {
                            ssidDialog = ChangeDeviceSsidDialogFragment(data, this)
                            ssidDialog.show(supportFragmentManager, "changeSsid")
                        }
                    }
                }
                Status.ERROR -> {
                    binding.layoutMain.visibility = View.GONE
                    binding.layoutBottom.visibility = View.GONE
                    binding.layoutShimmer.visibility = View.GONE
                    binding.layoutError.visibility = View.VISIBLE
                    Timber.i("error => ${it.message}")
                }
            }
        }

    }

    private val calculateObserver = Observer<Resource<TotalKwh>> {
        when (it.status) {
            Status.LOADING -> {}
            Status.SUCCESS -> {
                it.data?.let { data ->
                    binding.tvCost3.text = data.priceStr?.replace("Rp", "")
                }
            }
            Status.ERROR -> {
                Timber.i("error => ${it.message}")
            }
        }
    }

    private fun visibleUI(isLoading: Boolean) {
        if (isLoading) {
            binding.layoutMain.visibility = View.GONE
            binding.layoutBottom.visibility = View.GONE
            binding.layoutShimmer.visibility = View.VISIBLE
        } else {
            binding.layoutMain.visibility = View.VISIBLE
            binding.layoutBottom.visibility = View.VISIBLE
            binding.layoutShimmer.visibility = View.GONE
        }
    }

    override fun updateDevice(id: Int, name: String) {
        CustomDialogFragment(
            getString(R.string.perhatian),
            getString(R.string.device_update_disclaimer)
        ) {
            dialog.dismiss()
            val rawData = JsonObject().apply {
                addProperty("device_name", name)
            }

            sharedPrefs.get(AUTH_TOKEN)?.let { userToken ->
                viewModel.updateDevice(userToken, id, rawData).observe(this, {
                    when (it.status) {
                        Status.LOADING -> {
                        }
                        Status.SUCCESS -> {
                            initUI(deviceToken!!)
                            Toast.makeText(this, "Berhasil update perangkat!", Toast.LENGTH_SHORT)
                                .show()
                        }
                        Status.ERROR -> {
                            Timber.i("error => ${it.message}")
                            Toast.makeText(this, "Gagal update perangkat!", Toast.LENGTH_SHORT)
                                .show()
                        }
                    }
                })
            }
        }.show(supportFragmentManager, "customDialog")
    }

    override fun changeSsid(jsonObject: JsonObject, data: Device) {
        CustomDialogFragment(
            getString(R.string.perhatian),
            getString(R.string.update_info_disclaimer)
        ) {
            ssidDialog.dismiss()

            sharedPrefs.get(AUTH_TOKEN)?.let { userToken ->
                viewModel.changeDeviceSsid(userToken, data.id!!, jsonObject).observe(this) {
                    when (it.status) {
                        Status.LOADING -> {}
                        Status.SUCCESS -> {
                            initUI(deviceToken!!)
                            Toast.makeText(this, "Success update SSID!", Toast.LENGTH_SHORT)
                                .show()
                        }
                        Status.ERROR -> {
                            Timber.i("error => ${it.message}")
                            Toast.makeText(this, "Failed to update SSID!", Toast.LENGTH_SHORT)
                                .show()
                        }
                    }
                }
            }
        }.show(supportFragmentManager, "customDialog")
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    companion object {
        const val EXTRA_TOKEN = "extra_token"
    }
}