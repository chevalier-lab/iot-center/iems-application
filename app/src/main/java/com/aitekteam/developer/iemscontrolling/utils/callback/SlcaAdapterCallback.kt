package com.aitekteam.developer.iemscontrolling.utils.callback

import android.content.Context
import android.content.Intent
import android.view.View
import android.widget.Toast
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.LifecycleOwner
import com.aitekteam.developer.iemscontrolling.R
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.general.Device
import com.aitekteam.developer.iemscontrolling.core.utils.adapter.AdapterCallback
import com.aitekteam.developer.iemscontrolling.core.utils.vo.Status
import com.aitekteam.developer.iemscontrolling.ui.user.home.HomeViewModel
import com.aitekteam.developer.iemscontrolling.ui.user.home.slca.SlcaActivity
import com.aitekteam.developer.iemscontrolling.utils.dialog.CustomDialogFragment
import com.bumptech.glide.Glide
import com.google.gson.JsonObject
import kotlinx.android.synthetic.main.item_slca.view.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import timber.log.Timber

class SlcaAdapterCallback(
    private val context: Context,
    private val viewModel: HomeViewModel,
    private val lifecycleOwner: LifecycleOwner,
    private val lifecycleScope: CoroutineScope,
    private val fragmentManager: FragmentManager
) : AdapterCallback<Device> {
    private lateinit var sendMode: String

    override fun initComponent(itemView: View, data: Device, itemIndex: Int) {
        itemView.tv_slca_name.text = data.name

        // set value
        data.token?.let { token ->
            viewModel.getLastSlca(token).observe(lifecycleOwner) {
                when (it.status) {
                    Status.LOADING -> {
                    }
                    Status.SUCCESS -> {

                        it.data?.let { slca ->
                            itemView.tv_iac.text = slca.iac.toString()
                            itemView.tv_power.text = slca.power.toString()

                            if (slca.iac == null || slca.power == null) {
                                sendMode = "ON"
                                itemView.tv_iac.text = StringBuilder().append("N/A")
                                itemView.tv_power.text = StringBuilder().append("N/A")

                                Glide.with(context)
                                    .load(R.drawable.ic_off)
                                    .into(itemView.btn_power_slca)
                            } else {
                                when (slca.status?.mode) {
                                    "ON" -> {
                                        sendMode = "OFF"
                                        Glide.with(context)
                                            .load(R.drawable.ic_on)
                                            .into(itemView.btn_power_slca)
                                    }
                                    "OFF" -> {
                                        sendMode = "ON"
                                        Glide.with(context)
                                            .load(R.drawable.ic_off)
                                            .into(itemView.btn_power_slca)
                                    }
                                    else -> Glide.with(context)
                                        .load(R.drawable.ic_off)
                                        .into(itemView.btn_power_slca)
                                }
                            }

                            itemView.btn_power_slca.setOnClickListener {
                                CustomDialogFragment(
                                    context.getString(R.string.perhatian),
                                    context.getString(R.string.push_slca_disclaimer, data.name)
                                ) {
                                    val payload = JsonObject().apply {
                                        addProperty("mode", sendMode)
                                    }
                                    viewModel.pushDevice(token, payload).observe(lifecycleOwner) { res ->
                                        when (res.status) {
                                            Status.LOADING -> {}
                                            Status.SUCCESS -> {
                                                when (sendMode) {
                                                    "ON" -> {
                                                        sendMode = "OFF"
                                                        Glide.with(context)
                                                            .load(R.drawable.ic_on)
                                                            .into(itemView.btn_power_slca)

                                                        lifecycleScope.launch {
                                                            delayFetchOn(token, itemView)
                                                        }
                                                    }
                                                    "OFF" -> {
                                                        sendMode = "ON"
                                                        Glide.with(context)
                                                            .load(R.drawable.ic_off)
                                                            .into(itemView.btn_power_slca)

                                                        lifecycleScope.launch {
                                                            delayFetchOff(token, itemView)
                                                        }
                                                    }
                                                }
                                            }
                                            Status.ERROR -> {
                                                Toast.makeText(context, res.message, Toast.LENGTH_SHORT).show()
                                            }
                                        }
                                    }
                                }.show(fragmentManager, "customDialog")
                            }
                        }
                    }
                    Status.ERROR -> {
                        Timber.i("error => ${it.message}")
                    }
                }
            }
        }
    }

    override fun onItemClicked(itemView: View, data: Device, itemIndex: Int) {
        context.startActivity(Intent(context, SlcaActivity::class.java).apply {
            putExtra(SlcaActivity.EXTRA_TOKEN, data.token)
        })
    }

    private suspend fun delayFetchOn(token: String, itemView: View) {
        Timber.i("Test thread : wait 1 minutes")
        delay(60000)

        viewModel.getLastSlca(token).observe(lifecycleOwner) { result ->
            when (result.status) {
                Status.LOADING ->  {}
                Status.SUCCESS ->  {
                    val data = result.data
                    Timber.i("Test thread : done with data ${result.data}")

                    if (data?.iac == null || data.power == null) {
                        sendMode = "ON"
                        itemView.tv_iac.text = StringBuilder().append("N/A")
                        itemView.tv_power.text = StringBuilder().append("N/A")

                        Glide.with(context)
                            .load(R.drawable.ic_off)
                            .into(itemView.btn_power_slca)
                    } else {
                        sendMode = "OFF"
                        itemView.tv_iac.text = StringBuilder().append(result.data?.iac)
                        itemView.tv_power.text = StringBuilder().append(result.data?.power)

                        Glide.with(context)
                            .load(R.drawable.ic_on)
                            .into(itemView.btn_power_slca)
                    }
                }
                Status.ERROR ->  {}
            }
        }
    }

    private suspend fun delayFetchOff(token: String, itemView: View) {
        Timber.i("Test thread : wait 1 minutes")
        delay(60000)

        viewModel.getLastSlca(token).observe(lifecycleOwner) { result ->
            when (result.status) {
                Status.LOADING ->  {}
                Status.SUCCESS ->  {
                    val data = result.data
                    Timber.i("Test thread : done with data ${result.data}")

                    if (data?.iac == null || data.power == null) {
                        sendMode = "ON"
                        itemView.tv_iac.text = StringBuilder().append("N/A")
                        itemView.tv_power.text = StringBuilder().append("N/A")

                        Glide.with(context)
                            .load(R.drawable.ic_off)
                            .into(itemView.btn_power_slca)
                    } else {
                        sendMode = "ON"
                        itemView.tv_iac.text = StringBuilder().append(result.data?.iac)
                        itemView.tv_power.text = StringBuilder().append(result.data?.power)

                        Glide.with(context)
                            .load(R.drawable.ic_off)
                            .into(itemView.btn_power_slca)
                    }
                }
                Status.ERROR ->  {}
            }
        }
    }
}