package com.aitekteam.developer.iemscontrolling.ui.operator.inactive

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.aitekteam.developer.iemscontrolling.core.data.Resource
import com.aitekteam.developer.iemscontrolling.core.data.network.response.operator.ActivationUser
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.profile.User
import com.aitekteam.developer.iemscontrolling.core.data.network.service.OperatorService
import com.aitekteam.developer.iemscontrolling.core.domain.usecase.OperatorUseCase
import com.google.gson.JsonObject
import kotlinx.coroutines.flow.Flow

class InactiveUserViewModel(
    private val operatorUseCase: OperatorUseCase,
    private val operatorService: OperatorService
) : ViewModel() {

    fun getListCustomer(token: String): Flow<PagingData<User>> = Pager(PagingConfig(12)) {
        InactiveUserPagingSource(token, operatorService)
    }.flow.cachedIn(viewModelScope)

    fun activateCustomer(token: String, jsonObject: JsonObject): LiveData<Resource<ActivationUser>> =
        operatorUseCase.activateCustomer(token, jsonObject).asLiveData()
}