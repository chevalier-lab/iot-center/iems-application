package com.aitekteam.developer.iemscontrolling.ui.user.home.kwh1.chart

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import com.aitekteam.developer.iemscontrolling.R
import com.aitekteam.developer.iemscontrolling.databinding.ActivityWebviewChartKwh1Binding
import com.aitekteam.developer.iemscontrolling.utils.helper.Constants.AUTH_TOKEN
import com.aitekteam.developer.iemscontrolling.utils.helper.SharedPrefsUtils
import com.aitekteam.developer.iemscontrolling.utils.listener.BottomSheetListener
import im.delight.android.webview.AdvancedWebView
import timber.log.Timber

class WebviewChartKwh1Activity : AppCompatActivity(), BottomSheetListener {
    private lateinit var binding: ActivityWebviewChartKwh1Binding

    // utils
    private lateinit var token: String
    private lateinit var userToken: String
    private lateinit var webUrl: String
    private lateinit var mWebView: AdvancedWebView
    private lateinit var sharedPrefs: SharedPrefsUtils

    private var filter = "Cumulative"
    private var filterType = "Energy"
    private var date = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityWebviewChartKwh1Binding.inflate(layoutInflater)
        setContentView(binding.root)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.apply {
                addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                statusBarColor = Color.TRANSPARENT
                this.decorView.rootView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            }
        }

        binding.toolbar.apply {
            setSupportActionBar(this)
            supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_back_black)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setDisplayShowTitleEnabled(false)
        }

        // init vars
        sharedPrefs = SharedPrefsUtils()
        sharedPrefs.start(this, AUTH_TOKEN)

        // get intent data
        val extras = intent.extras
        if (extras != null) {
            token = extras.getString(EXTRA_TOKEN).toString()
            initUI()
        }
    }

    private fun initUI() {
        sharedPrefs.get(AUTH_TOKEN)?.let { userToken ->
            this.userToken = userToken
            webUrl = StringBuilder().apply {
                append("http://213.190.4.40:2030/chartKWH?")
                append("Authorization=")
                append(userToken)
                append("&deviceToken=")
                append(token)
                append("&displayType=")
                append("last")
                append("&id=")
                append("cumulative")
                append("&displayChartType=")
                append("month")
                append("&date=")
                append("&param=kwh")
                append("&label=")
                append("Energy+Cumulative+Data")
            }.toString()

            // init webview
            initWebView()

            binding.btnFilter.setOnClickListener {
                BottomSheetFilterKwh1(this, filter, filterType, date, this).showBottomSheet()
            }
        }
    }

    private fun initWebView() {
        mWebView = binding.webview
        mWebView.apply {
            setListener(this@WebviewChartKwh1Activity, webviewListener)
            setMixedContentAllowed(false)
            loadUrl(webUrl)
        }
    }

    override fun filterChartKwh(filter: String, type: String, date: String) {
        // set global var
        this.filter = filter
        filterType = type
        this.date = date

        val typeOfFilter = when (type) {
            "Energy" -> "kwh"
            "Active Power" -> "pa"
            "Reactive Power" -> "q"
            "Apparent Power" -> "s"
            "Current" -> "i"
            "Voltage" -> "v"
            "Power Factor" -> "pf"
            else -> ""
        }

        when (filter) {
            "Cumulative" -> {
                webUrl = StringBuilder().apply {
                    append("http://213.190.4.40:2030/chartKWH?")
                    append("Authorization=")
                    append(userToken)
                    append("&deviceToken=")
                    append(token)
                    append("&displayType=")
                    append("last")
                    append("&id=")
                    append("cumulative")
                    append("&displayChartType=")
                    append("month")
                    append("&date=")
                    append("&param=")
                    append(typeOfFilter)
                    append("&label=")
                    append(type)
                    append("+Cumulative+Data")
                }.toString()
                mWebView.loadUrl(webUrl)
                Timber.i("testingWeb => $webUrl")
            }
            "Daily Total" -> {
                webUrl = StringBuilder().apply {
                    append("http://213.190.4.40:2030/chartKWH?")
                    append("Authorization=")
                    append(userToken)
                    append("&deviceToken=")
                    append(token)
                    append("&displayType=")
                    append("between")
                    append("&id=")
                    append("daily-total")
                    append("&displayChartType=")
                    append("month")
                    append("&date=")
                    append("&param=")
                    append(typeOfFilter)
                    append("&label=")
                    append(type)
                    append("+Daily+Total+Data")
                }.toString()
                mWebView.loadUrl(webUrl)
                Timber.i("testingWeb => $webUrl")
            }
            "Monthly Total" -> {
                webUrl = StringBuilder().apply {
                    append("http://213.190.4.40:2030/chartKWH?")
                    append("Authorization=")
                    append(userToken)
                    append("&deviceToken=")
                    append(token)
                    append("&displayType=")
                    append("last")
                    append("&id=")
                    append("montly-total")
                    append("&displayChartType=")
                    append("year")
                    append("&date=")
                    append("&param=")
                    append(typeOfFilter)
                    append("&label=")
                    append(type)
                    append("+Monthly+Total+Data")
                }.toString()
                mWebView.loadUrl(webUrl)
                Timber.i("testingWeb => $webUrl")
            }
            "Hourly Average" -> {
                webUrl = StringBuilder().apply {
                    append("http://213.190.4.40:2030/chartKWH?")
                    append("Authorization=")
                    append(userToken)
                    append("&deviceToken=")
                    append(token)
                    append("&displayType=")
                    append("avg")
                    append("&id=")
                    append("hourly-average")
                    append("&displayChartType=")
                    append("day")
                    append("&date=")
                    append(date)
                    append("&param=")
                    append(typeOfFilter)
                    append("&label=")
                    append(type)
                    append("+Hourly+Average+Data")
                }.toString()
                mWebView.loadUrl(webUrl)
                Timber.i("testingWeb => $webUrl")
            }
            "Daily Average" -> {
                webUrl = StringBuilder().apply {
                    append("http://213.190.4.40:2030/chartKWH?")
                    append("Authorization=")
                    append(userToken)
                    append("&deviceToken=")
                    append(token)
                    append("&displayType=")
                    append("avg")
                    append("&id=")
                    append("daily-average")
                    append("&displayChartType=")
                    append("month")
                    append("&date=")
                    append("&param=")
                    append(typeOfFilter)
                    append("&label=")
                    append(type)
                    append("+Daily+Average+Data")
                }.toString()
                mWebView.loadUrl(webUrl)
                Timber.i("testingWeb => $webUrl")
            }
            "Monthly Average" -> {
                webUrl = StringBuilder().apply {
                    append("http://213.190.4.40:2030/chartKWH?")
                    append("Authorization=")
                    append(userToken)
                    append("&deviceToken=")
                    append(token)
                    append("&displayType=")
                    append("avg")
                    append("&id=")
                    append("monthly-average")
                    append("&displayChartType=")
                    append("year")
                    append("&date=")
                    append("&param=")
                    append(typeOfFilter)
                    append("&label=")
                    append(type)
                    append("+Monthly+Average+Data")
                }.toString()
                mWebView.loadUrl(webUrl)
                Timber.i("testingWeb => $webUrl")
            }
            "Realtime" -> {
                webUrl = StringBuilder().apply {
                    append("http://213.190.4.40:2030/chartKWH?")
                    append("Authorization=")
                    append(userToken)
                    append("&deviceToken=")
                    append(token)
                    append("&displayType=")
                    append("last")
                    append("&id=")
                    append("realtime")
                    append("&displayChartType=")
                    append("day")
                    append("&date=")
                    append("&param=")
                    append(typeOfFilter)
                    append("&label=")
                    append(type)
                    append("+Realtime+Data")
                }.toString()
                mWebView.loadUrl(webUrl)
                Timber.i("testingWeb => $webUrl")
            }
            "Monthly" -> {
                webUrl = StringBuilder().apply {
                    append("http://213.190.4.40:2030/chartKWH?")
                    append("Authorization=")
                    append(userToken)
                    append("&deviceToken=")
                    append(token)
                    append("&displayType=")
                    append("last")
                    append("&id=")
                    append("monthly")
                    append("&displayChartType=")
                    append("year")
                    append("&date=")
                    append("&param=")
                    append(typeOfFilter)
                    append("&label=")
                    append(type)
                    append("+Monthly+Data")
                }.toString()
                mWebView.loadUrl(webUrl)
                Timber.i("testingWeb => $webUrl")
            }
            "Daily" -> {
                webUrl = StringBuilder().apply {
                    append("http://213.190.4.40:2030/chartKWH?")
                    append("Authorization=")
                    append(userToken)
                    append("&deviceToken=")
                    append(token)
                    append("&displayType=")
                    append("last")
                    append("&id=")
                    append("daily")
                    append("&displayChartType=")
                    append("month")
                    append("&date=")
                    append("&param=")
                    append(typeOfFilter)
                    append("&label=")
                    append(type)
                    append("+Daily+Data")
                }.toString()
                mWebView.loadUrl(webUrl)
                Timber.i("testingWeb => $webUrl")
            }
            "Hourly" -> {
                webUrl = StringBuilder().apply {
                    append("http://213.190.4.40:2030/chartKWH?")
                    append("Authorization=")
                    append(userToken)
                    append("&deviceToken=")
                    append(token)
                    append("&displayType=")
                    append("last")
                    append("&id=")
                    append("hourly")
                    append("&displayChartType=")
                    append("day")
                    append("&date=")
                    append(date)
                    append("&param=")
                    append(typeOfFilter)
                    append("&label=")
                    append(type)
                    append("+Hourly+Data")
                }.toString()
                mWebView.loadUrl(webUrl)
                Timber.i("testingWeb => $webUrl")
            }
        }
    }

    override fun onResume() {
        super.onResume()
        mWebView.onResume()
    }

    override fun onPause() {
        super.onPause()
        mWebView.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()
        mWebView.onDestroy()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        mWebView.onActivityResult(requestCode, resultCode, data)
    }

    private val webviewListener = object : AdvancedWebView.Listener {
        override fun onPageStarted(url: String?, favicon: Bitmap?) {
            binding.loading.visibility = View.VISIBLE
        }

        override fun onPageFinished(url: String?) {
            binding.loading.visibility = View.GONE
        }

        override fun onPageError(errorCode: Int, description: String?, failingUrl: String?) {}

        override fun onDownloadRequested(
            url: String?,
            suggestedFilename: String?,
            mimeType: String?,
            contentLength: Long,
            contentDisposition: String?,
            userAgent: String?
        ) {
        }

        override fun onExternalPageRequest(url: String?) {}

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    companion object {
        const val EXTRA_TOKEN = "extra_token"
    }

}