package com.aitekteam.developer.iemscontrolling.ui.user.home.pcb

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.aitekteam.developer.iemscontrolling.core.data.Resource
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.general.Device
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.pcb.Pcb
import com.aitekteam.developer.iemscontrolling.core.data.network.service.DeviceService
import com.aitekteam.developer.iemscontrolling.core.domain.usecase.DeviceUseCase
import com.google.gson.JsonObject
import kotlinx.coroutines.flow.Flow

class PcbViewModel(
    private val deviceUseCase: DeviceUseCase,
    private val deviceService: DeviceService
) : ViewModel() {

    fun getDeviceInfo(token: String): LiveData<Resource<Device>> =
        deviceUseCase.getDeviceInfo(token).asLiveData()

    fun getLastPcb(token: String): LiveData<Resource<Pcb>> =
        deviceUseCase.getLastPcb(token).asLiveData()

    fun getHistoryPcb(token: String): Flow<PagingData<Pcb>> = Pager(PagingConfig(12)) {
        PcbPagingSource(token, deviceService)
    }.flow.cachedIn(viewModelScope)

    fun updateDevice(token: String, id: Int, jsonObject: JsonObject): LiveData<Resource<Device>> =
        deviceUseCase.updateDevice(token, id, jsonObject).asLiveData()

    fun changeDeviceSsid(
        token: String,
        deviceId: Int,
        jsonObject: JsonObject
    ): LiveData<Resource<Device>> =
        deviceUseCase.changeDeviceSsid(token, deviceId, jsonObject).asLiveData()
}