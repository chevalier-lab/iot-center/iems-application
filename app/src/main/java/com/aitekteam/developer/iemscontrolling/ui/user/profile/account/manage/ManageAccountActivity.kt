package com.aitekteam.developer.iemscontrolling.ui.user.profile.account.manage

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import com.aitekteam.developer.iemscontrolling.R
import com.aitekteam.developer.iemscontrolling.core.data.Resource
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.profile.User
import com.aitekteam.developer.iemscontrolling.core.utils.vo.Status
import com.aitekteam.developer.iemscontrolling.databinding.ActivityManageAccountBinding
import com.aitekteam.developer.iemscontrolling.ui.user.profile.account.info.AccountInfoActivity
import com.aitekteam.developer.iemscontrolling.utils.dialog.CustomDialogFragment
import com.aitekteam.developer.iemscontrolling.utils.helper.Constants.AUTH_TOKEN
import com.aitekteam.developer.iemscontrolling.utils.helper.Constants.USER_COVER
import com.aitekteam.developer.iemscontrolling.utils.helper.Constants.USER_EMAIL
import com.aitekteam.developer.iemscontrolling.utils.helper.Constants.USER_NAME
import com.aitekteam.developer.iemscontrolling.utils.helper.Constants.USER_PHONE
import com.aitekteam.developer.iemscontrolling.utils.helper.Constants.USER_TYPE
import com.aitekteam.developer.iemscontrolling.utils.helper.SharedPrefsUtils
import com.bumptech.glide.Glide
import com.github.dhaval2404.imagepicker.ImagePicker
import com.google.android.material.snackbar.Snackbar
import com.google.gson.JsonObject
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import org.koin.android.viewmodel.ext.android.viewModel
import timber.log.Timber
import java.io.File

class ManageAccountActivity : AppCompatActivity() {
    private lateinit var binding: ActivityManageAccountBinding
    private val viewModel: ManageAccountViewModel by viewModel()

    companion object {
        const val EXTRA_CONDITION = "extra_condition"
        private const val GALLERY_IMAGE_REQ_CODE = 102
    }
    private var imgProfileFile: File? = null
    private var emailValid = false
    private var fullNameValid = false

    // utils
    private var condition: String? = null
    private lateinit var sharedPrefs: SharedPrefsUtils

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityManageAccountBinding.inflate(layoutInflater)
        setContentView(binding.root)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.apply {
                addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                statusBarColor = Color.TRANSPARENT
                this.decorView.rootView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            }
        }

        binding.toolbar.apply {
            setSupportActionBar(this)
            supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_back_black)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setDisplayShowTitleEnabled(false)
        }


        // init var
        sharedPrefs = SharedPrefsUtils()
        sharedPrefs.start(this, AUTH_TOKEN)

        // get intent data
        val extra = intent.extras
        if (extra != null)
            condition = extra.getString(EXTRA_CONDITION)

        initUI()
    }

    private fun initUI() {
        // validate input
        validateInput()

        // set user
        binding.etEmail.setText(sharedPrefs.get(USER_EMAIL))
        binding.etFullName.setText(sharedPrefs.get(USER_NAME))
        binding.etPhone.setText(sharedPrefs.get(USER_PHONE))
        binding.etType.setText(sharedPrefs.get(USER_TYPE))
        Glide.with(this)
            .load(sharedPrefs.get(USER_COVER) ?: R.drawable.avatar)
            .into(binding.imageUser)

        // pick image
        binding.btnChooseFoto.setOnClickListener {
            pickGalleryImage()
        }

        // submit button
        binding.btnSubmit.setOnClickListener {
            CustomDialogFragment(
                getString(R.string.perhatian),
                getString(R.string.update_profile_disclaimer)
            ) {
                if (imgProfileFile !== null) saveDataWithUpload()
                else saveData(null)
            }.show(supportFragmentManager, "customDialog")
        }

        // fetch data user
//        sharedPrefs.get(AUTH_TOKEN)?.let { token ->
//            viewModel.getProfile(token).observe(this, {
//                when (it.status) {
//                    Status.LOADING -> {
//                        binding.error.visibility = View.GONE
//                    }
//                    Status.SUCCESS -> {
//                        binding.layoutInfo.visibility = View.VISIBLE
//                        binding.error.visibility = View.GONE
//
//                        it.data?.let { user ->
//                            binding.etFullName.setText(user.fullName)
//                            binding.etEmail.setText(user.email)
//                            binding.etPhone.setText(user.phoneNumber)
//                            binding.etType.setText(user.type)
//
//                            Glide.with(this)
//                                .load(user.cover ?: R.drawable.avatar)
//                                .into(binding.imageUser)
//                        }
//                    }
//                    Status.ERROR -> {
//                        binding.layoutInfo.visibility = View.GONE
//                        binding.error.visibility = View.VISIBLE
//                    }
//                }
//            })
//        }
    }

    private fun validateInput() {
        binding.etEmail.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun afterTextChanged(p0: Editable?) {}

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                emailValid = p0.toString().trim().isNotEmpty()
                inputCheck()
                binding.tvEmail.text = binding.etEmail.text.toString()
            }
        })

        binding.etFullName.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun afterTextChanged(p0: Editable?) {}

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                fullNameValid = p0.toString().trim().isNotEmpty()
                inputCheck()
                binding.tvName.text = binding.etFullName.text.toString()
            }
        })
    }

    private fun inputCheck() {
        if (emailValid && fullNameValid) {
            binding.btnSubmit.isEnabled = true
            binding.btnSubmit.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary))
        } else {
            binding.btnSubmit.isEnabled = false
            binding.btnSubmit.setBackgroundColor(ContextCompat.getColor(this, R.color.divider))
        }
    }

    private fun pickGalleryImage() {
        ImagePicker.with(this)
            .cropSquare()
            .galleryOnly()
            .galleryMimeTypes(
                mimeTypes = arrayOf(
                    "image/png",
                    "image/jpg",
                    "image/jpeg"
                )
            )
            .maxResultSize(1080, 1920)
            .start(GALLERY_IMAGE_REQ_CODE)
    }

    private fun saveDataWithUpload() {
        if (imgProfileFile != null) {
            val image = imgProfileFile!!.asRequestBody()
            val dataImage = MultipartBody.Part.createFormData("photo", imgProfileFile?.name,  image)

            sharedPrefs.get(AUTH_TOKEN)?.let { token ->
                viewModel.createMedia(token, dataImage).observe(this) {
                    when (it.status) {
                        Status.LOADING -> {
                            binding.btnSubmit.visibility = View.GONE
                            binding.loadingBar.visibility = View.VISIBLE
                        }
                        Status.SUCCESS -> {
                            it.data?.let { data ->
                                saveData(data.id)
                            }
                        }
                        Status.ERROR -> {
                            binding.btnSubmit.visibility = View.VISIBLE
                            binding.loadingBar.visibility = View.GONE
                            Snackbar.make(
                                window.decorView.rootView,
                                "Failed to update your profile picture!",
                                Snackbar.LENGTH_SHORT
                            ).show()
                        }
                    }
                }
            }
        }
    }

    private val userObserver = Observer<Resource<User>> {
        when (it.status) {
            Status.LOADING -> {
                binding.btnSubmit.visibility = View.GONE
                binding.loadingBar.visibility = View.VISIBLE
            }
            Status.SUCCESS -> {
                binding.btnSubmit.visibility = View.VISIBLE
                binding.loadingBar.visibility = View.GONE

                it.data?.let { data ->
                    sharedPrefs.setString(USER_COVER, data.cover!!)
                }
            }
            Status.ERROR -> {
                binding.btnSubmit.visibility = View.VISIBLE
                binding.loadingBar.visibility = View.GONE
                Snackbar.make(
                    window.decorView.rootView,
                    "Failed to update your profile!",
                    Snackbar.LENGTH_SHORT
                ).show()
            }
        }
    }

    private fun saveData(coverId: String?) {
        val rawData = JsonObject().apply {
            addProperty("email", binding.etEmail.text.toString())
            addProperty("full_name", binding.etFullName.text.toString())
            if (coverId != null) addProperty("id_cover", coverId)
        }

        sharedPrefs.get(AUTH_TOKEN)?.let { token ->
            viewModel.updateProfile(token, rawData).observe(this) {
                when (it.status) {
                    Status.LOADING -> {
                        binding.btnSubmit.visibility = View.GONE
                        binding.loadingBar.visibility = View.VISIBLE
                    }
                    Status.SUCCESS -> {
                        binding.btnSubmit.visibility = View.VISIBLE
                        binding.loadingBar.visibility = View.GONE

                        it.data?.let { data ->
                            if (data.email != null)
                                sharedPrefs.setString(USER_EMAIL, data.email!!)
                            if (data.fullName != null)
                                sharedPrefs.setString(USER_NAME, data.fullName!!)
                        }

                        Snackbar.make(
                            window.decorView.rootView,
                            "Success update profile!",
                            Snackbar.LENGTH_SHORT
                        ).show()
                    }
                    Status.ERROR -> {
                        binding.btnSubmit.visibility = View.VISIBLE
                        binding.loadingBar.visibility = View.GONE
                        Snackbar.make(
                            window.decorView.rootView,
                            "Failed to update your profile!",
                            Snackbar.LENGTH_SHORT
                        ).show()
                    }
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Timber.i(data.toString())
        when (resultCode) {
            Activity.RESULT_OK -> {
                // File object will not be null for RESULT_OK
                val file = ImagePicker.getFile(data)!!
                when (requestCode) {
                    GALLERY_IMAGE_REQ_CODE -> {
                        imgProfileFile = file
                        Glide.with(this)
                            .load(file)
                            .into(binding.imageUser)
                    }
                }
            }
            ImagePicker.RESULT_ERROR -> {
                Toast.makeText(this, ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onBackPressed() {
        if (condition != null) {
            startActivity(
                Intent(this, AccountInfoActivity::class.java).putExtra(
                    AccountInfoActivity.EXTRA_CONDITION,
                    condition
                )
            )
            finish()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                if (condition != null) {
                    startActivity(
                        Intent(this, AccountInfoActivity::class.java).putExtra(
                            AccountInfoActivity.EXTRA_CONDITION,
                            condition
                        )
                    )
                    finish()
                }
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}