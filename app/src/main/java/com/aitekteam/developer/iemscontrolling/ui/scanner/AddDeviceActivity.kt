package com.aitekteam.developer.iemscontrolling.ui.scanner

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.aitekteam.developer.iemscontrolling.R
import com.aitekteam.developer.iemscontrolling.databinding.ActivityAddDeviceBinding
import com.aitekteam.developer.iemscontrolling.ui.scanner.dialog.ActivateDeviceDialogFragment
import com.aitekteam.developer.iemscontrolling.ui.scanner.dialog.InputTokenDialogFragment
import com.aitekteam.developer.iemscontrolling.ui.user.host.HostActivity
import com.aitekteam.developer.iemscontrolling.utils.helper.Constants.AUTH_TOKEN
import com.aitekteam.developer.iemscontrolling.utils.helper.Constants.USER_PHONE
import com.aitekteam.developer.iemscontrolling.utils.helper.SharedPrefsUtils
import com.google.zxing.Result
import me.dm7.barcodescanner.zxing.ZXingScannerView

class AddDeviceActivity : AppCompatActivity(), ZXingScannerView.ResultHandler {
    private lateinit var binding: ActivityAddDeviceBinding

    // utils
    private var userPhone: String? = null
    private lateinit var sharedPref: SharedPrefsUtils
    private lateinit var scannerView: ZXingScannerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAddDeviceBinding.inflate(layoutInflater)
        setContentView(binding.root)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.apply {
                addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                statusBarColor = Color.TRANSPARENT
                this.decorView.rootView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            }
        }

        binding.toolbar.apply {
            setSupportActionBar(this)
            supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_back_black)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setDisplayShowTitleEnabled(false)
        }

        // init variables
        sharedPref = SharedPrefsUtils()
        sharedPref.start(this, AUTH_TOKEN)

        // get intent data
        val extras = intent.extras
        if (extras != null)
            extras.getString(EXTRA_PHONE)?.let { userPhone = it }

        // re scan qr code
        binding.btnRescan.setOnClickListener {
            scannerView.resumeCameraPreview(this)
            it.visibility = View.GONE
        }

        // input token manually
        userPhone?.let { phone ->
            binding.btnEnterCode.setOnClickListener {
                InputTokenDialogFragment(phone).show(supportFragmentManager, "dialog")
            }
        } ?: run {
            sharedPref.get(USER_PHONE)?.let { phone ->
                binding.btnEnterCode.setOnClickListener {
                    InputTokenDialogFragment(phone).show(supportFragmentManager, "dialog")
                }
            }
        }

        // init view
        initScannerView()
    }

    private fun initScannerView() {
        scannerView = ZXingScannerView(this)
        scannerView.apply {
            setAutoFocus(true)
            setResultHandler(this@AddDeviceActivity)
        }
        binding.frameScanner.addView(scannerView)
    }

    override fun onStart() {
        scannerView.startCamera()
        doRequestPermission()
        super.onStart()
    }

    private fun doRequestPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(
                    this,
                    Manifest.permission.CAMERA
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(Manifest.permission.CAMERA),
                    100
                )
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            100 -> initScannerView()
            else -> {
            }
        }
    }

    override fun onPause() {
        super.onPause()
        scannerView.stopCamera()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                if (userPhone == null)
                    startActivity(Intent(this, HostActivity::class.java))
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onBackPressed() {
        if (userPhone == null)
            startActivity(Intent(this, HostActivity::class.java))
        finish()
    }

    override fun handleResult(rawResult: Result?) {
        userPhone?.let {
            ActivateDeviceDialogFragment(rawResult?.text!!, it).show(supportFragmentManager, "dialog")
        } ?: run {
            sharedPref.get(USER_PHONE)?.let {
                ActivateDeviceDialogFragment(rawResult?.text!!, it).show(supportFragmentManager, "dialog")
            }
        }

        binding.btnRescan.visibility = View.VISIBLE
    }

    companion object {
        const val EXTRA_PHONE = "extra_phone"
    }
}