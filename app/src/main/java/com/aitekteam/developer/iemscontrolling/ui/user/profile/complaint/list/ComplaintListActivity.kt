package com.aitekteam.developer.iemscontrolling.ui.user.profile.complaint.list

import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import androidx.recyclerview.widget.RecyclerView
import com.aitekteam.developer.iemscontrolling.R
import com.aitekteam.developer.iemscontrolling.core.data.network.response.ticketing.Ticketing
import com.aitekteam.developer.iemscontrolling.core.utils.adapter.AdapterCallback
import com.aitekteam.developer.iemscontrolling.core.utils.adapter.ReusablePagingAdapter
import com.aitekteam.developer.iemscontrolling.databinding.ActivityComplaintListBinding
import com.aitekteam.developer.iemscontrolling.ui.user.profile.complaint.detail.ComplaintDetailActivity
import com.aitekteam.developer.iemscontrolling.utils.helper.Constants.AUTH_TOKEN
import com.aitekteam.developer.iemscontrolling.utils.helper.Helpers.dateFormat
import com.aitekteam.developer.iemscontrolling.utils.helper.SharedPrefsUtils
import kotlinx.android.synthetic.main.item_complaint.view.*
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.koin.android.viewmodel.ext.android.viewModel

class ComplaintListActivity : AppCompatActivity() {
    private lateinit var binding: ActivityComplaintListBinding
    private val viewModel: ComplaintListViewModel by viewModel()

    // utils
    private lateinit var sharedPrefs: SharedPrefsUtils
    private lateinit var adapter: ReusablePagingAdapter<Ticketing>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityComplaintListBinding.inflate(layoutInflater)
        setContentView(binding.root)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.apply {
                addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                statusBarColor = Color.TRANSPARENT
                this.decorView.rootView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            }
        }

        binding.toolbar.apply {
            setSupportActionBar(this)
            supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_back_black)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setDisplayShowTitleEnabled(false)
        }

        // init var
        sharedPrefs = SharedPrefsUtils()
        sharedPrefs.start(this, AUTH_TOKEN)
        adapter = ReusablePagingAdapter(this)

        // setup adapter
        setupAdapter(binding.rvComplaint)

        // init ui
        initUI()
    }

    private fun initUI() {
        sharedPrefs.get(AUTH_TOKEN)?.let { token ->
            lifecycleScope.launch {
                viewModel.getListComplaint(token).collect {
                    adapter.submitData(it)
                }
            }

            with(adapter) {
                // loading state
                addLoadStateListener { loadState ->
                    // handle initial loading
                    if (loadState.refresh is LoadState.Loading) {
                        binding.rvComplaint.visibility = View.GONE
                        binding.layoutShimmer.visibility = View.VISIBLE
                    } else if (loadState.append.endOfPaginationReached) {
                        // handle if data is empty
                        if (adapter.itemCount < 1) {
                            binding.rvComplaint.visibility = View.GONE
                            binding.layoutShimmer.visibility = View.GONE
                            binding.layoutEmpty.visibility = View.VISIBLE
                        }
                    } else {
                        // handle if data is exists
                        binding.rvComplaint.visibility = View.VISIBLE
                        binding.layoutShimmer.visibility = View.GONE

                        // get error
                        val error = when {
                            loadState.prepend is LoadState.Error -> loadState.prepend as LoadState.Error
                            loadState.append is LoadState.Error -> loadState.append as LoadState.Error
                            loadState.refresh is LoadState.Error -> loadState.refresh as LoadState.Error
                            else -> null
                        }

                        error?.let {
                            binding.layoutShimmer.visibility = View.GONE
                            binding.rvComplaint.visibility = View.VISIBLE
                            binding.layoutError.visibility = View.VISIBLE
                        }
                    }
                }
            }
        }
    }

    private fun setupAdapter(recyclerView: RecyclerView) {
        adapter.adapterCallback(adapterCallback)
            .setLayout(R.layout.item_complaint)
            .isVerticalView()
            .build(recyclerView)
    }

    private val adapterCallback = object : AdapterCallback<Ticketing> {
        override fun initComponent(itemView: View, data: Ticketing, itemIndex: Int) {
            itemView.tv_title.text = data.title
            itemView.tv_date.text = StringBuilder().apply {
                append(data.date?.dateFormat())
                append(" (${data.status})")
            }
        }

        override fun onItemClicked(itemView: View, data: Ticketing, itemIndex: Int) {
            startActivity(
                Intent(
                    this@ComplaintListActivity,
                    ComplaintDetailActivity::class.java
                ).putExtra(ComplaintDetailActivity.EXTRA_DATA, data)
            )
        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}