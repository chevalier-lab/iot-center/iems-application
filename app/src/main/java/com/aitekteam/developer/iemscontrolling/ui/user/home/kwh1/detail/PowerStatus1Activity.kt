package com.aitekteam.developer.iemscontrolling.ui.user.home.kwh1.detail

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.aitekteam.developer.iemscontrolling.R
import com.aitekteam.developer.iemscontrolling.core.data.Resource
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.calculate.TotalKwh
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.general.Device
import com.aitekteam.developer.iemscontrolling.core.utils.vo.Status
import com.aitekteam.developer.iemscontrolling.databinding.ActivityPowerStatus1Binding
import com.aitekteam.developer.iemscontrolling.ui.user.home.kwh1.chart.WebviewChartKwh1Activity
import com.aitekteam.developer.iemscontrolling.utils.dialog.ChangeDeviceSsidDialogFragment
import com.aitekteam.developer.iemscontrolling.utils.dialog.CustomDialogFragment
import com.aitekteam.developer.iemscontrolling.utils.dialog.EditDeviceDialogFragment
import com.aitekteam.developer.iemscontrolling.utils.helper.Constants.AUTH_TOKEN
import com.aitekteam.developer.iemscontrolling.utils.helper.SharedPrefsUtils
import com.aitekteam.developer.iemscontrolling.utils.listener.DialogListener
import com.google.gson.JsonObject
import org.koin.android.viewmodel.ext.android.viewModel
import timber.log.Timber
import java.text.DecimalFormat

class PowerStatus1Activity : AppCompatActivity(), DialogListener {
    private lateinit var binding: ActivityPowerStatus1Binding
    private val viewModel: PowerStatus1ViewModel by viewModel()

    // utils
    private var deviceToken: String? = null
    private lateinit var sharedPrefs: SharedPrefsUtils
    private lateinit var dialog: EditDeviceDialogFragment
    private lateinit var ssidDialog: ChangeDeviceSsidDialogFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPowerStatus1Binding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.toolbar.apply {
            setSupportActionBar(this)
            supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_back)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setDisplayShowTitleEnabled(false)
        }

        // init vars
        sharedPrefs = SharedPrefsUtils()
        sharedPrefs.start(this, AUTH_TOKEN)

        val extra = intent.extras
        if (extra != null) {
            deviceToken = extra.getString(EXTRA_TOKEN)
            deviceToken?.let { initUI(it) }
        }
    }

    private fun initUI(token: String) {
        // go to chart
        binding.btnChart.setOnClickListener {
            startActivity(
                Intent(
                    this,
                    WebviewChartKwh1Activity::class.java
                ).putExtra(WebviewChartKwh1Activity.EXTRA_TOKEN, token)
            )
        }

        // get device info
        viewModel.getDeviceInfo(token).observe(this) {
            when (it.status) {
                Status.LOADING -> {
                    visibleUI(true)
                }
                Status.SUCCESS -> {
                    visibleUI(false)
                    it.data?.let { data ->
                        // set information
                        binding.tvName.text = data.name
                        binding.tvShortname.text = data.shortName
                        binding.tvStatus.text = data.status
                        binding.tvType.text = data.type

                        // set ssid
                        if (data.deviceSsid == null && data.devicePassword == null)
                            binding.tvSsid.text = data.userSsid
                        else binding.tvSsid.text = data.deviceSsid

//                        data.ssid?.let { ssid -> binding.tvSsid.text = ssid }
//                            ?: run {
//                                binding.lineSsid.visibility = View.GONE
//                                binding.layoutSsid.visibility = View.GONE
//                            }

                        // edit name
                        binding.btnEdit.setOnClickListener {
                            dialog = EditDeviceDialogFragment(data.id!!, data.name!!, this)
                            dialog.show(supportFragmentManager, "editDialog")
                        }

                        // edit ssid
                        binding.btnEditDeviceSsid.setOnClickListener {
                            ssidDialog = ChangeDeviceSsidDialogFragment(data, this)
                            ssidDialog.show(supportFragmentManager, "changeSsid")
                        }
                    }
                }
                Status.ERROR -> {
                    binding.layoutMain.visibility = View.GONE
                    binding.layoutBottom.visibility = View.GONE
                    binding.layoutShimmer.visibility = View.GONE
                    binding.layoutError.visibility = View.VISIBLE
                    Timber.i("error => ${it.message}")
                }
            }
        }

        // get data last
        viewModel.getLastKwh1(token).observe(this) {
            when (it.status) {
                Status.LOADING -> {
                    visibleUI(true)
                }
                Status.SUCCESS -> {
                    visibleUI(false)
                    it.data?.let { data ->
                        val df = DecimalFormat("#.###")
                        binding.tvVoltage.text = StringBuilder().append(df.format(data.voltage?.toDouble()))
                        binding.tvCurrent.text = StringBuilder().append(df.format(data.current?.toDouble()))
                        binding.tvFrequency.text = StringBuilder().append(df.format(data.frequency?.toDouble()))
                        binding.tvPowerKwh.text = StringBuilder().append(df.format(data.power?.toDouble()))
                        binding.tvPowerFactor.text = StringBuilder().append(df.format(data.powerFactor?.toDouble()))
                        binding.tvKwh.text = StringBuilder().append(df.format(data.kwh?.toDouble()))
                        binding.tvRecPower.text = StringBuilder().append(df.format(data.q?.toDouble()))
                        binding.tvApparentPower.text = StringBuilder().append(df.format(data.s?.toDouble()))
                    }

                    sharedPrefs.get(AUTH_TOKEN)?.let { userToken ->
                        viewModel.calculateKwh(userToken, token).observe(this, calculateObserver)
                    }
                }
                Status.ERROR -> {
                    binding.layoutMain.visibility = View.GONE
                    binding.layoutBottom.visibility = View.GONE
                    binding.layoutShimmer.visibility = View.GONE
                    binding.layoutError.visibility = View.VISIBLE
                    Timber.i("error => ${it.message}")
                }
            }
        }
    }

    private val calculateObserver = Observer<Resource<TotalKwh>> {
        when (it.status) {
            Status.LOADING -> {}
            Status.SUCCESS -> {
                it.data?.let { data ->
                    val df = DecimalFormat("#.###")
                    binding.tvCurrentEnergy.text = StringBuilder().append(df.format(data.total))
                    binding.tvCost.text = data.priceStr?.replace("Rp", "")
                }
            }
            Status.ERROR -> {
                Timber.i("error => ${it.message}")
            }
        }
    }

    private fun visibleUI(isLoading: Boolean) {
        if (isLoading) {
            binding.layoutMain.visibility = View.GONE
            binding.layoutBottom.visibility = View.GONE
            binding.layoutShimmer.visibility = View.VISIBLE
        } else {
            binding.layoutMain.visibility = View.VISIBLE
            binding.layoutBottom.visibility = View.VISIBLE
            binding.layoutShimmer.visibility = View.GONE
        }
    }

    override fun updateDevice(id: Int, name: String) {
        CustomDialogFragment(
            getString(R.string.perhatian),
            getString(R.string.device_update_disclaimer)
        ) {
            dialog.dismiss()
            val rawData = JsonObject().apply {
                addProperty("device_name", name)
            }

            sharedPrefs.get(AUTH_TOKEN)?.let { userToken ->
                viewModel.updateDevice(userToken, id, rawData).observe(this, {
                    when (it.status) {
                        Status.LOADING -> {
                        }
                        Status.SUCCESS -> {
                            initUI(deviceToken!!)
                            Toast.makeText(this, "Success update device!", Toast.LENGTH_SHORT)
                                .show()
                        }
                        Status.ERROR -> {
                            Timber.i("error => ${it.message}")
                            Toast.makeText(this, "Failed to update device!", Toast.LENGTH_SHORT)
                                .show()
                        }
                    }
                })
            }
        }.show(supportFragmentManager, "customDialog")
    }

    override fun changeSsid(jsonObject: JsonObject, data: Device) {
        CustomDialogFragment(
            getString(R.string.perhatian),
            getString(R.string.update_info_disclaimer)
        ) {
            ssidDialog.dismiss()

            sharedPrefs.get(AUTH_TOKEN)?.let { userToken ->
                viewModel.changeDeviceSsid(userToken, data.id!!, jsonObject).observe(this) {
                    when (it.status) {
                        Status.LOADING -> {}
                        Status.SUCCESS -> {
                            initUI(deviceToken!!)
                            Toast.makeText(this, "Success update SSID!", Toast.LENGTH_SHORT)
                                .show()
                        }
                        Status.ERROR -> {
                            Timber.i("error => ${it.message}")
                            Toast.makeText(this, "Failed to update SSID!", Toast.LENGTH_SHORT)
                                .show()
                        }
                    }
                }
            }
        }.show(supportFragmentManager, "customDialog")
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    companion object {
        const val EXTRA_TOKEN = "extra_token"
    }
}