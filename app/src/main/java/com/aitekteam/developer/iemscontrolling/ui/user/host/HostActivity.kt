package com.aitekteam.developer.iemscontrolling.ui.user.host

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.aitekteam.developer.iemscontrolling.R
import com.aitekteam.developer.iemscontrolling.core.utils.vo.Status
import com.aitekteam.developer.iemscontrolling.databinding.ActivityHostBinding
import com.aitekteam.developer.iemscontrolling.ui.user.notification.NotificationActivity
import com.aitekteam.developer.iemscontrolling.utils.helper.Constants.AUTH_TOKEN
import com.aitekteam.developer.iemscontrolling.utils.helper.SharedPrefsUtils
import org.koin.android.viewmodel.ext.android.viewModel

class HostActivity : AppCompatActivity() {
    private lateinit var binding: ActivityHostBinding
    private val viewModel: HostViewModel by viewModel()

    // utils
    private lateinit var sharedPrefs: SharedPrefsUtils

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHostBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.toolbar.apply {
            setSupportActionBar(this)
            supportActionBar?.setDisplayShowTitleEnabled(false)
        }

        // init var
        sharedPrefs = SharedPrefsUtils()
        sharedPrefs.start(this, AUTH_TOKEN)

        // init UI
        initUI()
    }

    private fun initUI() {
        // init nav view
        val navView = binding.navView
        val navController = findNavController(R.id.nav_host_fragment)
        navView.setupWithNavController(navController)

        // get intent data
        val extra = intent.extras
        if (extra != null) {
            val moveProfile = extra.getBoolean(EXTRA_MOVE_PROFILE, false)
            if (moveProfile)
                navController.navigate(R.id.navigation_profile)
        }

        sharedPrefs.get(AUTH_TOKEN)?.let { token ->
            viewModel.getProfile(token).observe(this) {
                when (it.status) {
                    Status.LOADING -> {
                    }
                    Status.SUCCESS -> {
                        it.data?.let { data ->
                            if (data.type == "guest" && data.status == "not activate yet") {
                                navView.menu.removeItem(R.id.navigation_complain)
                                navView.menu.removeItem(R.id.navigation_add_device)
                            } else if (data.type == "guest" && data.status == "request activate") {
                                navView.menu.removeItem(R.id.navigation_complain)
                                navView.menu.removeItem(R.id.navigation_add_device)
                            } else if (data.type == "guest" && data.status == "activate") {
                                navView.menu.removeItem(R.id.navigation_add_device)
                            } else if (data.type == "user" && data.status == "banned") {
                                navView.menu.removeItem(R.id.navigation_complain)
                                navView.menu.removeItem(R.id.navigation_add_device)
                            }
                        }
                    }
                    Status.ERROR -> {
                        Toast.makeText(this, it.message, Toast.LENGTH_SHORT).show()
                    }
                }
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.overflow -> {
                startActivity(Intent(this, NotificationActivity::class.java))
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    companion object {
        const val EXTRA_MOVE_PROFILE = "extra_move_profile"
    }
}