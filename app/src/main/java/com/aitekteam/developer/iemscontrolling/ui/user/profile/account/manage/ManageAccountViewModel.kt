package com.aitekteam.developer.iemscontrolling.ui.user.profile.account.manage

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import com.aitekteam.developer.iemscontrolling.core.data.Resource
import com.aitekteam.developer.iemscontrolling.core.data.network.response.media.Media
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.profile.User
import com.aitekteam.developer.iemscontrolling.core.domain.usecase.UserUseCase
import com.google.gson.JsonObject
import okhttp3.MultipartBody

class ManageAccountViewModel(
    private val userUseCase: UserUseCase
) : ViewModel() {

    fun getProfile(authorization: String): LiveData<Resource<User>> =
        userUseCase.getData(authorization).asLiveData()

    fun createMedia(token: String, photo: MultipartBody.Part): LiveData<Resource<Media>> =
        userUseCase.createMedia(token, photo).asLiveData()

    fun updateProfile(authorization: String, jsonObject: JsonObject): LiveData<Resource<User>> =
        userUseCase.updateProfile(authorization, jsonObject).asLiveData()

    fun updatePhotoProfile(authorization: String, coverId: String): LiveData<Resource<User>> =
        userUseCase.updatePhotoProfile(authorization, coverId).asLiveData()
}