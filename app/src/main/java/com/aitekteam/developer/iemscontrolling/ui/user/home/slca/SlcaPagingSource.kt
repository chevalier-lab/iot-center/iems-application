package com.aitekteam.developer.iemscontrolling.ui.user.home.slca

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.slca.Slca
import com.aitekteam.developer.iemscontrolling.core.data.network.service.DeviceService
import okio.IOException
import retrofit2.HttpException

class SlcaPagingSource(
    private val token: String,
    private val deviceService: DeviceService
) : PagingSource<Int, Slca>() {

    override fun getRefreshKey(state: PagingState<Int, Slca>): Int? = state.anchorPosition

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Slca> = try {
        val currentPage = params.key ?: FIRST_PAGE_INDEX
        val response = deviceService.getHistorySlcaPaging(token, currentPage)
        val responseList = mutableListOf<Slca>()

        val data = response.body()?.data ?: emptyList()
        responseList.addAll(data)

        LoadResult.Page(
            data = responseList,
            prevKey = null,
            nextKey = if (data.isNotEmpty()) currentPage.plus(1)
            else null
        )
    } catch (exception: Exception) {
        LoadResult.Error(exception)
    } catch (exception: IOException) {
        LoadResult.Error(exception)
    } catch (exception: HttpException) {
        LoadResult.Error(exception)
    }

    companion object {
        private const val FIRST_PAGE_INDEX = 0
    }
}