package com.aitekteam.developer.iemscontrolling.utils.dialog

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.core.content.ContextCompat
import androidx.fragment.app.DialogFragment
import com.aitekteam.developer.iemscontrolling.R
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.general.Device
import com.aitekteam.developer.iemscontrolling.databinding.FragmentChangeDeviceSsidDialogBinding
import com.aitekteam.developer.iemscontrolling.utils.helper.Constants.AUTH_TOKEN
import com.aitekteam.developer.iemscontrolling.utils.helper.SharedPrefsUtils
import com.aitekteam.developer.iemscontrolling.utils.listener.DialogListener
import com.google.gson.JsonObject

class ChangeDeviceSsidDialogFragment(
    private val data: Device,
    private val listener: DialogListener
) : DialogFragment() {
    private var _binding: FragmentChangeDeviceSsidDialogBinding? = null
    private val binding get() = _binding!!

    // utils
    private var ssidValid = false
    private var passwordValid = false
    private lateinit var sharedPref: SharedPrefsUtils

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentChangeDeviceSsidDialogBinding.inflate(inflater, container, false)
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog?.window?.requestFeature(Window.FEATURE_NO_TITLE)

        // init variables
        sharedPref = SharedPrefsUtils()
        sharedPref.start(requireActivity(), AUTH_TOKEN)

        // init UI
        initUI()

        // Inflate the layout for this fragment
        return binding.root
    }

    private fun initUI() {
        // set data
        if (data.deviceSsid == null && data.devicePassword == null) {
            binding.etSsid.setText(data.userSsid)
            binding.etPassword.setText(data.userPassword)
        } else {
            binding.etSsid.setText(data.deviceSsid)
            binding.etPassword.setText(data.devicePassword)
        }

        // valudate input
        binding.etSsid.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun afterTextChanged(p0: Editable?) {}

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                ssidValid = p0.toString().trim().isNotEmpty()
                inputCheck()
            }
        })

        binding.etPassword.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun afterTextChanged(p0: Editable?) {}

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                passwordValid = p0.toString().trim().isNotEmpty()
                inputCheck()
            }
        })

        binding.btnSubmit.setOnClickListener {
            val rawData = JsonObject().apply {
                addProperty("ssid_name_device", binding.etSsid.text.toString())
                addProperty("ssid_password_device", binding.etPassword.text.toString())
            }
            listener.changeSsid(rawData, data)
        }
    }

    private fun inputCheck() {
        if (ssidValid && passwordValid) {
            binding.btnSubmit.isEnabled = true
            binding.btnSubmit.setBackgroundColor(
                ContextCompat.getColor(
                    requireContext(),
                    R.color.colorPrimary
                )
            )
        } else {
            binding.btnSubmit.isEnabled = false
            binding.btnSubmit.setBackgroundColor(
                ContextCompat.getColor(
                    requireContext(),
                    R.color.divider
                )
            )
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}