package com.aitekteam.developer.iemscontrolling.ui.user.home.kwh

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.general.Device
import com.aitekteam.developer.iemscontrolling.core.data.network.service.DeviceService
import kotlinx.coroutines.flow.Flow

class ListKwhViewModel(
    private val deviceService: DeviceService
) : ViewModel() {

    fun getListKwh(token: String): Flow<PagingData<Device>> = Pager(PagingConfig(12)) {
        KwhPagingSource(token, deviceService)
    }.flow.cachedIn(viewModelScope)

    fun getListKwhOnGroup(token: String, groupId: Int): Flow<PagingData<Device>> =
        Pager(PagingConfig(12)) {
            KwhGroupPagingSource(token, groupId, deviceService)
        }.flow.cachedIn(viewModelScope)
}