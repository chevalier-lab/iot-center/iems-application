package com.aitekteam.developer.iemscontrolling.ui.user.profile.user.invite

import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.aitekteam.developer.iemscontrolling.R
import com.aitekteam.developer.iemscontrolling.core.data.Resource
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.invite.Invite
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.profile.User
import com.aitekteam.developer.iemscontrolling.core.utils.vo.Status
import com.aitekteam.developer.iemscontrolling.databinding.ActivityDetailInviteUserBinding
import com.aitekteam.developer.iemscontrolling.ui.user.profile.user.access.UserAccessActivity
import com.aitekteam.developer.iemscontrolling.utils.dialog.CustomDialogFragment
import com.aitekteam.developer.iemscontrolling.utils.helper.Constants.AUTH_TOKEN
import com.aitekteam.developer.iemscontrolling.utils.helper.SharedPrefsUtils
import com.bumptech.glide.Glide
import com.google.gson.JsonObject
import org.koin.android.viewmodel.ext.android.viewModel
import timber.log.Timber

class DetailInviteUserActivity : AppCompatActivity() {
    private lateinit var binding: ActivityDetailInviteUserBinding
    private val viewModel: DetailInviteUserViewModel by viewModel()

    // utils
    private lateinit var sharedPrefs: SharedPrefsUtils

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailInviteUserBinding.inflate(layoutInflater)
        setContentView(binding.root)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.apply {
                addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                statusBarColor = Color.TRANSPARENT
                this.decorView.rootView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            }
        }

        binding.toolbar.apply {
            setSupportActionBar(this)
            supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_back_black)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setDisplayShowTitleEnabled(false)
        }

        // init utils
        sharedPrefs = SharedPrefsUtils()
        sharedPrefs.start(this, AUTH_TOKEN)

        // get extras
        val extra = intent.extras
        if (extra != null) {
            val data = extra.getParcelable<User>(EXTRA_DATA)
            data?.let { initUI(it) }
        }
    }

    private fun initUI(data: User) {
        // set utils
        binding.tvName.text = data.fullName
        binding.tvType.text = data.type
        binding.tvPhone.text = data.phoneNumber
        binding.tvEmail.text = data.email

        // set profile picture
        Glide.with(this)
            .load(data.cover ?: R.drawable.avatar)
            .circleCrop()
            .into(binding.imageUser)

        sharedPrefs.get(AUTH_TOKEN)?.let { token ->
            binding.btnInvite.setOnClickListener {
                CustomDialogFragment(
                    getString(R.string.perhatian),
                    getString(R.string.invite_disclaimer)
                ) {
                    val rawData = JsonObject().apply {
                        addProperty("phone_number", data.phoneNumber)
                    }

                    viewModel.inviteGuest(token, rawData).observe(this, observer)
                }.show(supportFragmentManager, "customDialog")
            }
        }
    }

    private val observer = Observer<Resource<Invite>> {
        when (it.status) {
            Status.LOADING -> {
            }
            Status.SUCCESS -> {
                Toast.makeText(
                    this,
                    "Success invite this user!",
                    Toast.LENGTH_SHORT
                ).show()
                startActivity(
                    Intent(this@DetailInviteUserActivity, UserAccessActivity::class.java).apply {
                        putExtra(UserAccessActivity.EXTRA_CONDITION, "addUser")
                        addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    }
                )
            }
            Status.ERROR -> {
                Timber.i("error => ${it.message}")
                Toast.makeText(
                    this,
                    "Failed to invite this user!",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    companion object {
        const val EXTRA_DATA = "extra_data"
    }
}