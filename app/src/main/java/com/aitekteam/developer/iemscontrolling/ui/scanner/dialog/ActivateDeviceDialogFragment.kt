package com.aitekteam.developer.iemscontrolling.ui.scanner.dialog

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.DialogFragment
import com.aitekteam.developer.iemscontrolling.R
import com.aitekteam.developer.iemscontrolling.core.utils.vo.Status
import com.aitekteam.developer.iemscontrolling.databinding.FragmentActivateDeviceDialogBinding
import com.aitekteam.developer.iemscontrolling.ui.scanner.AddDeviceViewModel
import com.aitekteam.developer.iemscontrolling.utils.helper.Constants.AUTH_TOKEN
import com.aitekteam.developer.iemscontrolling.utils.dialog.CustomDialogFragment
import com.aitekteam.developer.iemscontrolling.utils.helper.SharedPrefsUtils
import com.google.gson.JsonObject
import org.koin.android.viewmodel.ext.android.viewModel
import timber.log.Timber

class ActivateDeviceDialogFragment(
    private val deviceToken: String,
    private val userPhone: String
    ) : DialogFragment() {
    private var _binding: FragmentActivateDeviceDialogBinding? = null
    private val binding get() = _binding!!
    private val viewModel: AddDeviceViewModel by viewModel()

    // utils
    private var nameValid = false
    private lateinit var sharedPref: SharedPrefsUtils

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentActivateDeviceDialogBinding.inflate(inflater, container, false)
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog?.window?.requestFeature(Window.FEATURE_NO_TITLE)

        // init variables
        sharedPref = SharedPrefsUtils()
        sharedPref.start(requireActivity(), AUTH_TOKEN)

        // setup UI
        setupUI()

        Timber.i("user phone => $userPhone")

        // Inflate the layout for this fragment
        return binding.root
    }

    private fun setupUI() {
        viewModel.getDeviceInfo(deviceToken).observe(viewLifecycleOwner) {
            when (it.status) {
                Status.LOADING -> {}
                Status.SUCCESS -> {
                    it.data?.let { data ->
                        binding.tvShortName.text = data.shortName
                        binding.tvToken.text = deviceToken
                        binding.tvType.text = data.type
                        binding.etName.setText(data.name)
                        nameValid = true
                        inputCheck()
                    }
                }
                Status.ERROR -> {
                    dismiss()
                    Timber.i("error => ${it.message}")
                    Toast.makeText(requireContext(), "Invalid device token!", Toast.LENGTH_SHORT).show()
                }
            }
        }

        binding.etName.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { }

            override fun afterTextChanged(p0: Editable?) {
                nameValid = p0.toString().trim().isNotEmpty()
                inputCheck()
            }
        })

        binding.btnSubmit.setOnClickListener { showDialogToProcess() }
    }

    private fun showDialogToProcess() {
        CustomDialogFragment(
            getString(R.string.perhatian),
            getString(R.string.device_activation_disclaimer)
        ) { processData() }.show(parentFragmentManager, "customDialog")
    }

    private fun processData() {
        val rawData = JsonObject().apply {
            addProperty("phone_number", userPhone)
            addProperty("device_token", deviceToken)
            addProperty("device_name", binding.etName.text.toString())
        }

        sharedPref.get(AUTH_TOKEN)?.let { token ->
            viewModel.activateDevice(token, rawData).observe(viewLifecycleOwner) {
                when (it.status) {
                    Status.LOADING -> {
                        binding.btnSubmit.visibility = View.GONE
                        binding.loading.visibility = View.VISIBLE
                    }
                    Status.SUCCESS -> {
                        Toast.makeText(
                            requireContext(),
                            "Success activate device!",
                            Toast.LENGTH_SHORT
                        ).show()
                        dismiss()
                    }
                    Status.ERROR -> {
                        binding.btnSubmit.visibility = View.VISIBLE
                        binding.loading.visibility = View.GONE
                        Toast.makeText(requireContext(), "Failed to activate device!", Toast.LENGTH_SHORT).show()
                    }
                }
            }
        }
    }

    private fun inputCheck() {
        if (nameValid) {
            binding.btnSubmit.isEnabled = true
            binding.btnSubmit.setBackgroundColor(
                ContextCompat.getColor(
                    requireContext(),
                    R.color.colorPrimary
                )
            )
        } else {
            binding.btnSubmit.isEnabled = false
            binding.btnSubmit.setBackgroundColor(
                ContextCompat.getColor(
                    requireContext(),
                    R.color.divider
                )
            )
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}