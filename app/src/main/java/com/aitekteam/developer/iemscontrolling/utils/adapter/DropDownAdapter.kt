package com.aitekteam.developer.iemscontrolling.utils.adapter

import android.content.Context
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView

class DropDownAdapter<T>(
    private val context: Context,
    private val dropdown: AutoCompleteTextView,
    private val design: Int,
    private val list: List<T>,
    private val callback: (ArrayAdapter<T>) -> Unit
) {
    fun show() {
        ArrayAdapter(context, design, list).apply {
            dropdown.setAdapter(this)
            // callback fun
            callback(this)
        }
    }
}