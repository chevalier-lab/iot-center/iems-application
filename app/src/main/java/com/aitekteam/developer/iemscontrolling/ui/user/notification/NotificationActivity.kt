package com.aitekteam.developer.iemscontrolling.ui.user.notification

import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import com.aitekteam.developer.iemscontrolling.R
import com.aitekteam.developer.iemscontrolling.core.data.Resource
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.notification.DeviceNotification
import com.aitekteam.developer.iemscontrolling.core.utils.adapter.AdapterCallback
import com.aitekteam.developer.iemscontrolling.core.utils.adapter.ReusableAdapter
import com.aitekteam.developer.iemscontrolling.core.utils.vo.Status
import com.aitekteam.developer.iemscontrolling.databinding.ActivityNotificationBinding
import com.aitekteam.developer.iemscontrolling.utils.helper.Constants.AUTH_TOKEN
import com.aitekteam.developer.iemscontrolling.utils.helper.Constants.USER_STATUS
import com.aitekteam.developer.iemscontrolling.utils.helper.Constants.USER_TYPE
import com.aitekteam.developer.iemscontrolling.utils.helper.Helpers.dateFormat
import com.aitekteam.developer.iemscontrolling.utils.helper.SharedPrefsUtils
import kotlinx.android.synthetic.main.item_notification.view.*
import org.koin.android.viewmodel.ext.android.viewModel

class NotificationActivity : AppCompatActivity() {
    private lateinit var binding: ActivityNotificationBinding
    private val viewModel: NotificationViewModel by viewModel()

    // utils
    private var type: String? = null
    private var status: String? = null
    private lateinit var sharedPref: SharedPrefsUtils
    private lateinit var adapter: ReusableAdapter<DeviceNotification>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityNotificationBinding.inflate(layoutInflater)
        setContentView(binding.root)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.apply {
                addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                statusBarColor = Color.TRANSPARENT
                this.decorView.rootView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            }
        }

        binding.toolbar.apply {
            setSupportActionBar(this)
            supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_back_black)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setDisplayShowTitleEnabled(false)
        }

        // init utils
        sharedPref = SharedPrefsUtils()
        sharedPref.start(this, AUTH_TOKEN)
        adapter = ReusableAdapter(this)

        // setup adapter
        setupAdapter(binding.rvNotification)

        // get status user
        sharedPref.get(USER_TYPE)?.let { type = it }
        sharedPref.get(USER_STATUS)?.let { status = it }

        initUI()
    }

    private fun initUI() {
        sharedPref.get(AUTH_TOKEN)?.let { token ->
            if (type == "user" && status == "activate")
                viewModel.getDeviceUserNotification(token).observe(this, observer)
            else if (type == "guest" && status == "activate")
                viewModel.getDeviceGuestNotification(token).observe(this, observer)
            else {
                binding.rvNotification.visibility = View.GONE
                binding.layoutEmpty.visibility = View.VISIBLE
            }
        }
    }

    private val observer = Observer<Resource<List<DeviceNotification>>> {
        when (it.status) {
            Status.LOADING -> {
                binding.rvNotification.visibility = View.GONE
                binding.shimmerNotification.rootLayout.visibility = View.VISIBLE
            }
            Status.SUCCESS -> {
                binding.layoutError.visibility = View.GONE
                binding.rvNotification.visibility = View.VISIBLE
                binding.shimmerNotification.rootLayout.visibility = View.GONE

                it.data?.let { data ->
                    if (data.isNotEmpty())
                        adapter.addData(data)
                    else {
                        binding.rvNotification.visibility = View.GONE
                        binding.layoutEmpty.visibility = View.VISIBLE
                    }
                }
            }
            Status.ERROR -> {
                binding.layoutError.visibility = View.VISIBLE
                binding.rvNotification.visibility = View.GONE
                binding.shimmerNotification.rootLayout.visibility = View.GONE
            }
        }
    }

    private fun setupAdapter(recyclerView: RecyclerView) {
        adapter.adapterCallback(adapterCallback)
            .setLayout(R.layout.item_notification)
            .isVerticalView()
            .build(recyclerView)
    }

    private val adapterCallback = object : AdapterCallback<DeviceNotification> {
        override fun initComponent(itemView: View, data: DeviceNotification, itemIndex: Int) {
            itemView.tv_title.text = data.title
            itemView.tv_content.text = data.description
            itemView.tv_time.text = data.date?.dateFormat()
        }

        override fun onItemClicked(itemView: View, data: DeviceNotification, itemIndex: Int) {}

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}