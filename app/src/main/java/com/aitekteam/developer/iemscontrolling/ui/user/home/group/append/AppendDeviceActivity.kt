package com.aitekteam.developer.iemscontrolling.ui.user.home.group.append

import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import androidx.paging.PagingData
import androidx.recyclerview.widget.RecyclerView
import com.aitekteam.developer.iemscontrolling.R
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.general.Device
import com.aitekteam.developer.iemscontrolling.core.utils.adapter.AdapterCallback
import com.aitekteam.developer.iemscontrolling.core.utils.adapter.ReusablePagingAdapter
import com.aitekteam.developer.iemscontrolling.core.utils.vo.Status
import com.aitekteam.developer.iemscontrolling.databinding.ActivityAppendDeviceBinding
import com.aitekteam.developer.iemscontrolling.ui.user.home.group.detail.DetailGroupActivity
import com.aitekteam.developer.iemscontrolling.utils.dialog.CustomDialogFragment
import com.aitekteam.developer.iemscontrolling.utils.helper.Constants.AUTH_TOKEN
import com.aitekteam.developer.iemscontrolling.utils.helper.SharedPrefsUtils
import com.aitekteam.developer.iemscontrolling.utils.listener.BottomSheetListener
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_device_group.view.*
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.koin.android.viewmodel.ext.android.viewModel
import timber.log.Timber
import kotlin.properties.Delegates

class AppendDeviceActivity : AppCompatActivity(), BottomSheetListener {
    private lateinit var binding: ActivityAppendDeviceBinding
    private val viewModel: AppendDeviceViewModel by viewModel()

    // utils
    private var currentType = ""
    private var currentStatus = "normal"
    private var updateJob: Job? = null
    private var title: String? = null
    private var groupId by Delegates.notNull<Int>()
    private lateinit var sharedPrefs: SharedPrefsUtils
    private lateinit var adapter: ReusablePagingAdapter<Device>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAppendDeviceBinding.inflate(layoutInflater)
        setContentView(binding.root)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.apply {
                addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                statusBarColor = Color.TRANSPARENT
                this.decorView.rootView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            }
        }

        binding.toolbar.apply {
            setSupportActionBar(this)
            supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_back_black)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setDisplayShowTitleEnabled(false)
        }

        // init var
        sharedPrefs = SharedPrefsUtils()
        sharedPrefs.start(this, AUTH_TOKEN)
        adapter = ReusablePagingAdapter(this)

        // setup adapter
        setupAdapter(binding.rvDevice)

        // get intent data
        val extra = intent.extras
        if (extra != null) {
            groupId = extra.getInt(EXTRA_ID)
            title = extra.getString(EXTRA_NAME)
            initUI()
        }
    }

    private fun initUI() {
        sharedPrefs.get(AUTH_TOKEN)?.let { token ->
            updateJob?.cancel()
            updateJob = lifecycleScope.launch {
                viewModel.filterDeviceGroup(token, groupId, "", "0", "normal", "ASC").collect {
                    adapter.submitData(it)
                }
            }

            with(adapter) {
                // loading state
                addLoadStateListener { loadState ->
                    // handle initial loading
                    if (loadState.refresh is LoadState.Loading) {
                        binding.rvDevice.visibility = View.GONE
                        binding.layoutEmpty.visibility = View.GONE
                        binding.layoutError.visibility = View.GONE
                        binding.shimmerLoading.rootLayout.visibility = View.VISIBLE
                    } else if (loadState.append.endOfPaginationReached) {
                        // handle if data is empty
                        if (adapter.itemCount < 1) {
                            binding.rvDevice.visibility = View.GONE
                            binding.layoutEmpty.visibility = View.VISIBLE
                            binding.shimmerLoading.rootLayout.visibility = View.GONE
                        }
                    } else {
                        // handle if data is exists
                        binding.rvDevice.visibility = View.VISIBLE
                        binding.layoutEmpty.visibility = View.GONE
                        binding.layoutError.visibility = View.GONE
                        binding.shimmerLoading.rootLayout.visibility = View.GONE

                        // get error
                        val error = when {
                            loadState.prepend is LoadState.Error -> loadState.prepend as LoadState.Error
                            loadState.append is LoadState.Error -> loadState.append as LoadState.Error
                            loadState.refresh is LoadState.Error -> loadState.refresh as LoadState.Error
                            else -> null
                        }

                        error?.let {
                            binding.rvDevice.visibility = View.GONE
                            binding.layoutEmpty.visibility = View.GONE
                            binding.layoutError.visibility = View.VISIBLE
                            binding.shimmerLoading.rootLayout.visibility = View.GONE
                        }
                    }
                }
            }

            // search device
            binding.etSearch.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

                override fun afterTextChanged(p0: Editable?) {}

                override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                    if (p0?.trim() == "") {
                        adapter.submitData(lifecycle, PagingData.empty())
                        adapter.notifyDataSetChanged()
                    } else {
                        adapter.submitData(lifecycle, PagingData.empty())
                        adapter.notifyDataSetChanged()

                        updateJob?.cancel()
                        updateJob = lifecycleScope.launch {
                            viewModel.filterDeviceGroup(
                                token,
                                groupId,
                                p0.toString(),
                                "0",
                                "normal",
                                "ASC"
                            ).collect {
                                adapter.submitData(it)
                            }
                        }
                    }
                }
            })

            // filter
            binding.btnFilter.setOnClickListener {
                BottomSheetFilterDevice(this, currentType, currentStatus, this).showBottomSheet()
            }
        }
    }

    private fun setupAdapter(recyclerView: RecyclerView) {
        adapter.adapterCallback(adapterCallback)
            .setLayout(R.layout.item_device_group)
            .isVerticalView()
            .build(recyclerView)
    }

    private val adapterCallback = object : AdapterCallback<Device> {
        override fun initComponent(itemView: View, data: Device, itemIndex: Int) {
            itemView.tv_device_name.text = data.name

            // set icon
            when (data.type) {
                "pcb" -> {
                    Glide.with(this@AppendDeviceActivity)
                        .load(R.drawable.ic_pcb)
                        .into(itemView.image_device)
                }
                "slca" -> {
                    Glide.with(this@AppendDeviceActivity)
                        .load(R.drawable.ic_slca)
                        .into(itemView.image_device)
                }
                "sensors" -> {
                    Glide.with(this@AppendDeviceActivity)
                        .load(R.drawable.ic_sensor)
                        .into(itemView.image_device)
                }
                "kwh-1-phase" -> {
                    Glide.with(this@AppendDeviceActivity)
                        .load(R.drawable.ic_slca)
                        .into(itemView.image_device)
                }
                "kwh-3-phase" -> {
                    Glide.with(this@AppendDeviceActivity)
                        .load(R.drawable.ic_slca)
                        .into(itemView.image_device)
                }
            }

            // append/remove
            if (data.isAdded == "yes") {
                Glide.with(this@AppendDeviceActivity)
                    .load(R.drawable.ic_minus)
                    .into(itemView.btn_append)
            } else if (data.isAdded == "no") {
                Glide.with(this@AppendDeviceActivity)
                    .load(R.drawable.ic_plus)
                    .into(itemView.btn_append)
            }

            // append device
            sharedPrefs.get(AUTH_TOKEN)?.let { token ->
                itemView.btn_append.setOnClickListener {
                    if (data.isAdded == "yes") {
                        CustomDialogFragment(
                            getString(R.string.perhatian),
                            getString(R.string.remove_device_disclaimer)
                        ) {
                            viewModel.removeDevice(token, groupId, data.id!!)
                                .observe(this@AppendDeviceActivity) {
                                    when (it.status) {
                                        Status.LOADING -> {
                                        }
                                        Status.SUCCESS -> initUI()
                                        Status.ERROR -> {
                                            Toast.makeText(
                                                this@AppendDeviceActivity,
                                                "Failed to remove device:(",
                                                Toast.LENGTH_SHORT
                                            ).show()
                                        }
                                    }
                                }
                        }.show(supportFragmentManager, "customDialog")
                    } else if (data.isAdded == "no") {
                        CustomDialogFragment(
                            getString(R.string.perhatian),
                            getString(R.string.append_device_disclaimer)
                        ) {
                            viewModel.appendDevice(token, groupId, data.id!!)
                                .observe(this@AppendDeviceActivity) {
                                    when (it.status) {
                                        Status.LOADING -> {
                                        }
                                        Status.SUCCESS -> initUI()
                                        Status.ERROR -> {
                                            Timber.e("error nih => ${it.message}")
                                            Toast.makeText(
                                                this@AppendDeviceActivity,
                                                "Failed to append device:(",
                                                Toast.LENGTH_SHORT
                                            ).show()
                                        }
                                    }
                                }
                        }.show(supportFragmentManager, "customDialog")
                    }
                }
            }
        }

        override fun onItemClicked(itemView: View, data: Device, itemIndex: Int) {}

    }

    override fun filterDevice(type: String, status: String) {
        sharedPrefs.get(AUTH_TOKEN)?.let { token ->
            currentType = type
            currentStatus = status

            updateJob?.cancel()
            updateJob = lifecycleScope.launch {
                val search = binding.etSearch.text.toString()
                viewModel.filterDeviceGroup(
                    token,
                    groupId,
                    search,
                    currentType,
                    currentStatus,
                    "ASC"
                ).collect {
                    adapter.submitData(it)
                }
            }

        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                startActivity(
                    Intent(this, DetailGroupActivity::class.java)
                        .putExtra(DetailGroupActivity.EXTRA_ID, groupId)
                        .putExtra(DetailGroupActivity.EXTRA_NAME, title)
                )
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onBackPressed() {
        startActivity(
            Intent(this, DetailGroupActivity::class.java)
                .putExtra(DetailGroupActivity.EXTRA_ID, groupId)
                .putExtra(DetailGroupActivity.EXTRA_NAME, title)
        )
        finish()
    }

    companion object {
        const val EXTRA_ID = "extra_id"
        const val EXTRA_NAME = "extra_name"
    }
}