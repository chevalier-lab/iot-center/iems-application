package com.aitekteam.developer.iemscontrolling.ui.user.profile.user.access

import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import androidx.recyclerview.widget.RecyclerView
import com.aitekteam.developer.iemscontrolling.R
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.profile.User
import com.aitekteam.developer.iemscontrolling.core.utils.adapter.AdapterCallback
import com.aitekteam.developer.iemscontrolling.core.utils.adapter.ReusablePagingAdapter
import com.aitekteam.developer.iemscontrolling.databinding.ActivityUserAccessBinding
import com.aitekteam.developer.iemscontrolling.ui.user.host.HostActivity
import com.aitekteam.developer.iemscontrolling.ui.user.profile.user.access.detail.DetailGuestActivity
import com.aitekteam.developer.iemscontrolling.utils.helper.Constants.AUTH_TOKEN
import com.aitekteam.developer.iemscontrolling.utils.helper.Constants.REQUEST_TYPE
import com.aitekteam.developer.iemscontrolling.utils.helper.SharedPrefsUtils
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_manage_account.*
import kotlinx.android.synthetic.main.item_user.view.*
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.koin.android.viewmodel.ext.android.viewModel

class UserAccessActivity : AppCompatActivity() {
    private lateinit var binding: ActivityUserAccessBinding
    private val viewModel: UserAccessViewModel by viewModel()

    // utils
    private var userSize: Int = 0
    private var condition: String? = null
    private lateinit var sharedPrefs: SharedPrefsUtils
    private lateinit var adapter: ReusablePagingAdapter<User>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityUserAccessBinding.inflate(layoutInflater)
        setContentView(binding.root)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.apply {
                addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                statusBarColor = Color.TRANSPARENT
                this.decorView.rootView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            }
        }

        binding.toolbar.apply {
            setSupportActionBar(this)
            supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_back_black)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setDisplayShowTitleEnabled(false)
        }

        // init utils
        adapter = ReusablePagingAdapter(this)
        sharedPrefs = SharedPrefsUtils()
        sharedPrefs.start(this, AUTH_TOKEN)

        // setup adapter
        setupAdapter(binding.rvUser)

        // get intent extra
        val extra = intent.extras
        if (extra != null) {
            val data = extra.getString(EXTRA_CONDITION)
            data?.let { condition = it }
        }

        // init UI
        initUI()
    }

    private fun initUI() {
        sharedPrefs.get(AUTH_TOKEN)?.let { token ->
            lifecycleScope.launch {
                viewModel.getListGuest(token).collect {
                    adapter.submitData(it)
                }
            }

            with(adapter) {
                // loading state
                addLoadStateListener { loadState ->
                    // handle initial loading
                    if (loadState.refresh is LoadState.Loading) {
                        binding.rvUser.visibility = View.GONE
                        binding.layoutShimmer.visibility = View.VISIBLE
                    } else if (loadState.append.endOfPaginationReached) {
                        // handle if data is empty
                        if (adapter.itemCount < 1) {
                            binding.rvUser.visibility = View.GONE
                            binding.layoutShimmer.visibility = View.GONE
                            binding.layoutEmpty.visibility = View.VISIBLE
                        }
                    } else {
                        // handle if data is exists
                        binding.rvUser.visibility = View.VISIBLE
                        binding.layoutShimmer.visibility = View.GONE

                        // set user size
                        userSize = adapter.itemCount

                        // get error
                        val error = when {
                            loadState.prepend is LoadState.Error -> loadState.prepend as LoadState.Error
                            loadState.append is LoadState.Error -> loadState.append as LoadState.Error
                            loadState.refresh is LoadState.Error -> loadState.refresh as LoadState.Error
                            else -> null
                        }

                        error?.let {
                            binding.rvUser.visibility = View.VISIBLE
                            binding.layoutShimmer.visibility = View.GONE
                            binding.layoutError.visibility = View.VISIBLE
                        }
                    }
                }
            }
        }
    }

    private fun setupAdapter(recyclerView: RecyclerView) {
        adapter.adapterCallback(adapterCallback)
            .setLayout(R.layout.item_user)
            .isVerticalView()
            .build(recyclerView)
    }

    private val adapterCallback = object : AdapterCallback<User> {
        override fun initComponent(itemView: View, data: User, itemIndex: Int) {
            itemView.tv_name.text = data.fullName
            itemView.tv_email.text = data.email

            Glide.with(this@UserAccessActivity)
                .load(data.cover ?: R.drawable.avatar)
                .into(itemView.image_user)
        }

        override fun onItemClicked(itemView: View, data: User, itemIndex: Int) {
            startActivity(
                Intent(this@UserAccessActivity, DetailGuestActivity::class.java)
                    .putExtra(DetailGuestActivity.EXTRA_DATA, data)
            )
        }

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        menu?.getItem(0)?.icon = ContextCompat.getDrawable(this, R.drawable.ic_add)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                if (condition == "addUser") {
                    finish()
                    startActivity(Intent(this, HostActivity::class.java))
                } else finish()
                true
            }
            R.id.overflow -> {
                sharedPrefs.get(REQUEST_TYPE)?.let { rt ->
                    when (rt) {
                        "home" -> {
                            if (userSize == 5)
                                Toast.makeText(
                                    this,
                                    "U have reached the maximum limit for adding access",
                                    Toast.LENGTH_SHORT
                                ).show()
                            else
                                InviteUserDialogFragment().show(supportFragmentManager, "Dialog")
                        }
                        "building" -> InviteUserDialogFragment().show(supportFragmentManager, "Dialog")
                    }
                }
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onBackPressed() {
        if (condition == "addUser") {
            finish()
            startActivity(Intent(this, HostActivity::class.java))
        } else finish()
    }

    companion object {
        const val EXTRA_CONDITION = "extra_condition"
    }
}