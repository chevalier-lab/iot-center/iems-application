package com.aitekteam.developer.iemscontrolling.ui.user.profile.user.invite

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import com.aitekteam.developer.iemscontrolling.core.data.Resource
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.invite.Invite
import com.aitekteam.developer.iemscontrolling.core.domain.usecase.UserUseCase
import com.google.gson.JsonObject

class DetailInviteUserViewModel(
    private val userUseCase: UserUseCase
) : ViewModel() {

    fun inviteGuest(token: String, jsonObject: JsonObject): LiveData<Resource<Invite>> =
        userUseCase.inviteGuest(token, jsonObject).asLiveData()
}