package com.aitekteam.developer.iemscontrolling.ui.user.home

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import androidx.recyclerview.widget.RecyclerView
import com.aitekteam.developer.iemscontrolling.R
import com.aitekteam.developer.iemscontrolling.core.data.Resource
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.calculate.TotalKwh
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.general.Device
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.group.Group
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.profile.User
import com.aitekteam.developer.iemscontrolling.core.utils.adapter.AdapterCallback
import com.aitekteam.developer.iemscontrolling.core.utils.adapter.ReusableAdapter
import com.aitekteam.developer.iemscontrolling.core.utils.adapter.ReusablePagingAdapter
import com.aitekteam.developer.iemscontrolling.core.utils.vo.Status
import com.aitekteam.developer.iemscontrolling.databinding.FragmentHomeBinding
import com.aitekteam.developer.iemscontrolling.ui.user.home.group.detail.DetailGroupActivity
import com.aitekteam.developer.iemscontrolling.ui.user.home.kwh.ListKwhActivity
import com.aitekteam.developer.iemscontrolling.ui.user.home.kwh1.detail.PowerStatus1Activity
import com.aitekteam.developer.iemscontrolling.ui.user.home.kwh3.detail.PowerStatus3Activity
import com.aitekteam.developer.iemscontrolling.ui.user.home.sensor.SensorActivity
import com.aitekteam.developer.iemscontrolling.ui.user.home.slca.SlcaActivity
import com.aitekteam.developer.iemscontrolling.utils.callback.PcbAdapterCallback
import com.aitekteam.developer.iemscontrolling.utils.callback.SensorAdapterCallback
import com.aitekteam.developer.iemscontrolling.utils.callback.SlcaAdapterCallback
import com.aitekteam.developer.iemscontrolling.utils.helper.Constants.AUTH_TOKEN
import com.aitekteam.developer.iemscontrolling.utils.helper.Constants.REQUEST_TYPE
import com.aitekteam.developer.iemscontrolling.utils.helper.Constants.USER_COVER
import com.aitekteam.developer.iemscontrolling.utils.helper.Constants.USER_EMAIL
import com.aitekteam.developer.iemscontrolling.utils.helper.Constants.USER_NAME
import com.aitekteam.developer.iemscontrolling.utils.helper.Constants.USER_PHONE
import com.aitekteam.developer.iemscontrolling.utils.helper.Constants.USER_STATUS
import com.aitekteam.developer.iemscontrolling.utils.helper.Constants.USER_TYPE
import com.aitekteam.developer.iemscontrolling.utils.dialog.CreateGroupDialogFragment
import com.aitekteam.developer.iemscontrolling.utils.dialog.CustomDialogFragment
import com.aitekteam.developer.iemscontrolling.utils.helper.SharedPrefsUtils
import com.aitekteam.developer.iemscontrolling.utils.listener.BottomSheetListener
import com.aitekteam.developer.iemscontrolling.utils.listener.DialogListener
import com.bumptech.glide.Glide
import com.google.gson.JsonObject
import kotlinx.android.synthetic.main.activity_host.*
import kotlinx.android.synthetic.main.item_group.view.*
import kotlinx.android.synthetic.main.item_kwh_1.view.*
import kotlinx.android.synthetic.main.item_kwh_3.view.*
import kotlinx.android.synthetic.main.item_pcb.view.*
import kotlinx.android.synthetic.main.item_sensor.view.*
import kotlinx.android.synthetic.main.item_slca.view.*
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.koin.android.viewmodel.ext.android.viewModel
import timber.log.Timber
import java.text.DecimalFormat

class HomeFragment : Fragment(), BottomSheetListener, DialogListener {
    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!
    private val viewModel: HomeViewModel by viewModel()

    // utils
    private var isLoading = true
    private var deviceCollapsed = true
    private lateinit var dialog: CreateGroupDialogFragment
    private lateinit var sharedPrefs: SharedPrefsUtils
    private lateinit var pcbAdapter: ReusableAdapter<Device>
    private lateinit var sensorAdapter: ReusableAdapter<Device>
    private lateinit var slcaAdapter: ReusableAdapter<Device>
    private lateinit var kwh1Adapter: ReusableAdapter<Device>
    private lateinit var kwh3Adapter: ReusableAdapter<Device>
    private lateinit var groupAdapter: ReusablePagingAdapter<Group>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        activity?.toolbar_title?.text = getString(R.string.home_title)
        activity?.toolbar?.elevation = 0.0F

        // init var
        sharedPrefs = SharedPrefsUtils()
        sharedPrefs.start(requireActivity(), AUTH_TOKEN)
        pcbAdapter = ReusableAdapter(requireContext())
        sensorAdapter = ReusableAdapter(requireContext())
        slcaAdapter = ReusableAdapter(requireContext())
        kwh1Adapter = ReusableAdapter(requireContext())
        kwh3Adapter = ReusableAdapter(requireContext())
        groupAdapter = ReusablePagingAdapter(requireContext())

        // setup adapter
        setupAdapter.pcb(binding.rvPcb)
        setupAdapter.sensor(binding.rvSensor)
        setupAdapter.slca(binding.rvSlca)
        setupAdapter.kwh1(binding.rvKwh1)
        setupAdapter.kwh3(binding.rvKwh3)
        setupAdapter.group(binding.rvGroup)

        initUI()

        // Inflate the layout for this fragment
        return binding.root
    }

    private fun initUI() {
        // collapsed toggle
        collapsedToggle()

        // fetch data
        sharedPrefs.get(AUTH_TOKEN)?.let { token ->
            viewModel.getProfile(token).observe(viewLifecycleOwner, observer.profile(token))

            // activate user
            binding.btnActivateUser.setOnClickListener {
                BottomSheetSubscribe(
                    requireActivity(),
                    this
                ).showBottomSheet()
            }
        }
    }

    private val observer = object {
        fun profile(token: String) = Observer<Resource<User>> {
            when (it.status) {
                Status.LOADING -> {
                    isLoading = true
                    visibleUI(isLoading, "home")
                }
                Status.SUCCESS -> {
                    isLoading = false
                    visibleUI(isLoading, "home")
                    it.data?.let { data ->
                        // set shared prefs
                        sharedPrefs.setString(USER_NAME, data.fullName!!)
                        sharedPrefs.setString(USER_EMAIL, data.email!!)
                        sharedPrefs.setString(USER_PHONE, data.phoneNumber!!)
                        sharedPrefs.setString(USER_TYPE, data.type!!)
                        sharedPrefs.setString(USER_STATUS, data.status!!)
                        data.cover?.let { cover -> sharedPrefs.setString(USER_COVER, cover) }

                        // set request type
                        data.requestType?.let { rt ->
                            sharedPrefs.setString(REQUEST_TYPE, rt)
                        } ?: run {
                            sharedPrefs.setString(REQUEST_TYPE, "guest")
                        }

                        // set utils
                        binding.tvName.text = data.fullName
                        binding.tvEmail.text = data.email
                        binding.tvPhone.text = data.phoneNumber

                        // set status
                        // not activate (new user)
                        if (data.type == "guest" && data.status == "not activate yet") {
                            binding.tvStatus.text = StringBuilder().append("Anonym")
                            binding.layoutKwh.visibility = View.GONE
                            binding.layoutDevice.visibility = View.GONE
                            binding.layoutWaitingRequest.visibility = View.GONE
                            binding.layoutActivation.visibility = View.VISIBLE
                        } // activation requested
                        else if (data.type == "guest" && data.status == "request activate") {
                            binding.tvStatus.text = StringBuilder().append("Anonym")
                            binding.layoutKwh.visibility = View.GONE
                            binding.layoutDevice.visibility = View.GONE
                            binding.layoutActivation.visibility = View.GONE
                            binding.layoutWaitingRequest.visibility = View.VISIBLE
                        } // active guest
                        else if (data.type == "guest" && data.status == "activate") {
                            binding.tvStatus.text = StringBuilder().append("Guest")
                            viewModel.getListDeviceGuest(token).observe(viewLifecycleOwner, devices)
                        } // active user (home)
                        else if (data.type == "user" && data.status == "activate" && data.requestType == "home") {
                            binding.tvStatus.text = StringBuilder().append("subscriber")
                            viewModel.getListDevices(token).observe(viewLifecycleOwner, devices)
                        }
                        else if (data.type == "user" && data.status == "activate" && data.requestType == "building") {
                            binding.tvStatus.text = StringBuilder().append("subscriber")
                            viewModel.totalKwh(token).observe(viewLifecycleOwner, totalKwh)

                            viewLifecycleOwner.lifecycleScope.launch {
                                viewModel.getListGroup(token).collect { group ->
                                    groupAdapter.submitData(group)
                                }
                            }

                            // loading state
                            with(groupAdapter) {
                                // loading state
                                addLoadStateListener { loadState ->
                                    // handle initial loading
                                    if (loadState.refresh is LoadState.Loading) {
                                        isLoading = true
                                        visibleUI(isLoading, "building")
                                    } else {
                                        // handle if data is exists
                                        isLoading = false
                                        visibleUI(isLoading, "building")

                                        // add group
                                        binding.btnNewGroup.setOnClickListener {
                                            dialog = CreateGroupDialogFragment(this@HomeFragment)
                                            dialog.show(parentFragmentManager, "createGroup")
                                        }

                                        // get error
                                        val error = when {
                                            loadState.prepend is LoadState.Error -> loadState.prepend as LoadState.Error
                                            loadState.append is LoadState.Error -> loadState.append as LoadState.Error
                                            loadState.refresh is LoadState.Error -> loadState.refresh as LoadState.Error
                                            else -> null
                                        }

                                        error?.let {
                                            isLoading = false
                                            visibleUI(isLoading, "error")
                                        }
                                    }
                                }
                            }

                        } // banned user
                        else if (data.type == "user" && data.status == "banned")
                            binding.tvStatus.text = StringBuilder().append("banned")

                        // set avatar
                        Glide.with(requireContext())
                            .load(data.cover ?: R.drawable.avatar)
                            .circleCrop()
                            .into(binding.imageUser)
                    }
                }
                Status.ERROR -> {
                    isLoading = false
                    visibleUI(isLoading, "error")
                }
            }
        }

        val activate = Observer<Resource<User>> {
            when (it.status) {
                Status.LOADING -> {
                    isLoading = true
                    visibleUI(isLoading, "home")
                    binding.layoutActivation.visibility = View.GONE
                }
                Status.SUCCESS -> {
                    isLoading = false
                    visibleUI(isLoading, "home")
                    sharedPrefs.get(AUTH_TOKEN)?.let { token ->
                        viewModel.getProfile(token).observe(viewLifecycleOwner, profile(token))
                    }
                }
                Status.ERROR -> {
                    isLoading = false
                    visibleUI(isLoading, "error")
                }
            }
        }

        val devices = Observer<Resource<List<Device>>> {
            when (it.status) {
                Status.LOADING -> {
                    isLoading = true
                    visibleUI(isLoading, "home")
                }
                Status.SUCCESS -> {
                    isLoading = false
                    visibleUI(isLoading, "home")
                    val listPcb = mutableListOf<Device>()
                    val listSensor = mutableListOf<Device>()
                    val listSlca = mutableListOf<Device>()
                    val listKwh1 = mutableListOf<Device>()
                    val listKwh3 = mutableListOf<Device>()

                    it.data?.let { data ->
                        data.map { device ->
                            when (device.type) {
                                "pcb" -> listPcb.add(device)
                                "sensors" -> listSensor.add(device)
                                "slca" -> listSlca.add(device)
                                "kwh-1-phase" -> listKwh1.add(device)
                                "kwh-3-phase" -> listKwh3.add(device)
                                else -> null
                            }
                        }

                        pcbAdapter.addData(listPcb)
                        sensorAdapter.addData(listSensor)
                        slcaAdapter.addData(listSlca)
                        kwh1Adapter.addData(listKwh1)
                        kwh3Adapter.addData(listKwh3)

                        // set visible layout
                        if (listPcb.isEmpty())
                            binding.rvPcb.visibility = View.GONE
                        if (listSensor.isEmpty())
                            binding.rvSensor.visibility = View.GONE
                        if (listSlca.isEmpty())
                            binding.rvSlca.visibility = View.GONE
                        if (listKwh1.isEmpty())
                            binding.rvKwh1.visibility = View.GONE
                        if (listKwh3.isEmpty())
                            binding.rvKwh3.visibility = View.GONE
                        if (listPcb.isEmpty() && listSensor.isEmpty() && listSlca.isEmpty())
                            binding.groupDevice.visibility = View.GONE
                    }

                }
                Status.ERROR -> {
                    isLoading = false
                    visibleUI(isLoading, "error")
                }
            }
        }

        val totalKwh = Observer<Resource<TotalKwh>> {
            when (it.status) {
                Status.LOADING -> {
                    isLoading = true
                    visibleUI(isLoading, "building")
                }
                Status.SUCCESS -> {
                    isLoading = false
                    visibleUI(isLoading, "building")

                    it.data?.let { data ->
                        val df = DecimalFormat("#.###")
                        binding.totalKwh.tvTotalKwh.text = df.format(data.total)
                        binding.totalKwh.tvTotalCost.text = data.priceStr?.replace("Rp", "")

                        binding.totalKwh.rootLayout.setOnClickListener {
                            startActivity(Intent(requireContext(), ListKwhActivity::class.java))
                        }
                    }
                }
                Status.ERROR -> {
                    isLoading = false
                    visibleUI(isLoading, "error")
                }
            }
        }
    }

    private val setupAdapter = object {
        fun pcb(recyclerView: RecyclerView) {
            val adapterCallback = PcbAdapterCallback(
                requireContext(),
                viewModel,
                viewLifecycleOwner,
                parentFragmentManager
            )

            pcbAdapter.adapterCallback(adapterCallback)
                .setLayout(R.layout.item_pcb)
                .isVerticalView()
                .build(recyclerView)
        }

        fun sensor(recyclerView: RecyclerView) {
            val adapterCallback = SensorAdapterCallback(
                requireContext(),
                viewModel,
                viewLifecycleOwner
            )

            sensorAdapter.adapterCallback(adapterCallback)
                .setLayout(R.layout.item_sensor)
                .isVerticalView()
                .build(recyclerView)
        }

        fun slca(recyclerView: RecyclerView) {
            val adapterCallback = SlcaAdapterCallback(
                requireContext(),
                viewModel,
                viewLifecycleOwner,
                lifecycleScope,
                parentFragmentManager
            )

            slcaAdapter.adapterCallback(adapterCallback)
                .setLayout(R.layout.item_slca)
                .isVerticalView()
                .build(recyclerView)
        }

        fun kwh1(recyclerView: RecyclerView) {
            kwh1Adapter.adapterCallback(adapterCallback.kwh1)
                .setLayout(R.layout.item_kwh_1)
                .isVerticalView()
                .build(recyclerView)
        }

        fun kwh3(recyclerView: RecyclerView) {
            kwh3Adapter.adapterCallback(adapterCallback.kwh3)
                .setLayout(R.layout.item_kwh_3)
                .isVerticalView()
                .build(recyclerView)
        }

        fun group(recyclerView: RecyclerView) {
            groupAdapter.adapterCallback(adapterCallback.group)
                .setLayout(R.layout.item_group)
                .isGridView(2)
                .build(recyclerView)
        }
    }

    private val adapterCallback = object {
        val kwh1 = object : AdapterCallback<Device> {
            override fun initComponent(itemView: View, data: Device, itemIndex: Int) {
                itemView.tv_name.text = StringBuilder().append("Power Status - ${data.name}")

                data.token?.let { token ->
                    viewModel.getLastKwh1(token).observe(viewLifecycleOwner) {
                        when (it.status) {
                            Status.LOADING -> {
                                isLoading = true
                                visibleUI(isLoading, "home")
                            }
                            Status.SUCCESS -> {
                                isLoading = false
                                visibleUI(isLoading, "home")

                                it.data?.let { data ->
                                    if (data.kwh == null || data.voltage == null
                                        || data.current == null || data.frequency == null
                                        || data.power == null || data.powerFactor == null
                                    ) {
                                        val params = itemView.layoutParams.apply {
                                            width = 0
                                            height = 0
                                        }
                                        itemView.visibility = View.GONE
                                        itemView.layoutParams = params
                                    }
                                    else {
                                        val df = DecimalFormat("#.###")
                                        itemView.tv_voltage.text = StringBuilder().append(df.format(data.voltage?.toDouble()))
                                        itemView.tv_current.text = StringBuilder().append(df.format(data.current?.toDouble()))
                                        itemView.tv_frequency.text = StringBuilder().append(df.format(data.frequency?.toDouble()))
                                        itemView.tv_power_kwh.text = StringBuilder().append(df.format(data.power?.toDouble()))
                                        itemView.tv_power_factor.text = StringBuilder().append(df.format(data.powerFactor?.toDouble()))
                                        itemView.tv_kwh.text = StringBuilder().append(df.format(data.kwh?.toDouble()))
                                        itemView.tv_rec_power.text = StringBuilder().append(df.format(data.q?.toDouble()))
                                        itemView.tv_apparent_power.text = StringBuilder().append(df.format(data.s?.toDouble()))
                                    }
                                }

                                sharedPrefs.get(AUTH_TOKEN)?.let { userToken ->
                                    viewModel.calculateKwh(userToken, token).observe(viewLifecycleOwner) { res ->
                                        when (res.status) {
                                            Status.LOADING -> {}
                                            Status.SUCCESS -> {
                                                res.data?.let { dataKwh ->
                                                    val df = DecimalFormat("#.###")
                                                    itemView.tv_current_energy.text = StringBuilder().append(df.format(dataKwh.total))
                                                    itemView.tv_cost_kwh1.text = dataKwh.priceStr?.replace("Rp", "")
                                                }
                                            }
                                            Status.ERROR -> {
                                                Timber.i("error => ${it.message}")
                                            }
                                        }
                                    }

                                }
                            }
                            Status.ERROR -> {
                                isLoading = false
                                visibleUI(isLoading, "home")
                                Timber.i("error => ${it.message}")
                            }
                        }
                    }
                }
            }

            override fun onItemClicked(itemView: View, data: Device, itemIndex: Int) {
                startActivity(
                    Intent(
                        requireContext(),
                        PowerStatus1Activity::class.java
                    ).putExtra(PowerStatus1Activity.EXTRA_TOKEN, data.token)
                )
            }

        }

        val kwh3 = object : AdapterCallback<Device> {
            override fun initComponent(itemView: View, data: Device, itemIndex: Int) {
                itemView.tv_name_3.text = StringBuilder().append("Power Status - ${data.name}")

                data.token?.let { token ->
                    viewModel.getLastKwh3(token).observe(viewLifecycleOwner) {
                        when (it.status) {
                            Status.LOADING -> {
                                isLoading = true
                                visibleUI(isLoading, "home")
                            }
                            Status.SUCCESS -> {
                                isLoading = false
                                visibleUI(isLoading, "home")

                                it.data?.let { data ->
                                    if (data.id == null || data.deviceId == null) {
                                        val params = itemView.layoutParams.apply {
                                            width = 0
                                            height = 0
                                        }
                                        itemView.visibility = View.GONE
                                        itemView.layoutParams = params
                                    }
                                    else {
                                        val kwh = data.ep?.toDouble()
                                        val power = data.pt?.toDouble()
                                        val freq = data.freq?.toDouble()
                                        val currentA = data.ia?.toDouble()
                                        val currentB = data.ib?.toDouble()
                                        val currentC = data.ic?.toDouble()
                                        val voltageA = data.va?.toDouble()
                                        val voltageB = data.vb?.toDouble()
                                        val voltageC = data.vc?.toDouble()
                                        val powerFactorA = data.pfa?.toDouble()
                                        val powerFactorB = data.pfb?.toDouble()
                                        val powerFactorC = data.pfc?.toDouble()

                                        val df = DecimalFormat("#.###")
                                        itemView.tv_voltage_a.text = StringBuilder().append(df.format(voltageA))
                                        itemView.tv_voltage_b.text = StringBuilder().append(df.format(voltageB))
                                        itemView.tv_voltage_c.text = StringBuilder().append(df.format(voltageC))
                                        itemView.tv_current_a.text = StringBuilder().append(df.format(currentA))
                                        itemView.tv_current_b.text = StringBuilder().append(df.format(currentB))
                                        itemView.tv_current_c.text = StringBuilder().append(df.format(currentC))
                                        itemView.tv_power_factor_a.text = StringBuilder().append(df.format(powerFactorA))
                                        itemView.tv_power_factor_b.text = StringBuilder().append(df.format(powerFactorB))
                                        itemView.tv_power_factor_c.text = StringBuilder().append(df.format(powerFactorC))
                                        itemView.tv_frequency_3.text = StringBuilder().append(df.format(freq))
                                        itemView.tv_power_3.text = StringBuilder().append(df.format(power))
                                        itemView.tv_kwh_3.text = StringBuilder().append(df.format(kwh))
                                    }
                                }
                            }
                            Status.ERROR -> {
                                isLoading = false
                                visibleUI(isLoading, "home")
                                Timber.i("error => ${it.message}")
                            }
                        }
                    }
                 }
            }

            override fun onItemClicked(itemView: View, data: Device, itemIndex: Int) {
                startActivity(
                    Intent(
                        requireContext(),
                        PowerStatus3Activity::class.java
                    ).putExtra(PowerStatus3Activity.EXTRA_TOKEN, data.token)
                )
            }

        }

        val group = object : AdapterCallback<Group> {
            override fun initComponent(itemView: View, data: Group, itemIndex: Int) {
                itemView.tv_group_name.text = data.label
                itemView.tv_connected_device.text = StringBuilder().append("Connected device : ${data.total}")
            }

            override fun onItemClicked(itemView: View, data: Group, itemIndex: Int) {
                startActivity(
                    Intent(requireContext(), DetailGroupActivity::class.java)
                    .putExtra(DetailGroupActivity.EXTRA_ID, data.id?.toInt())
                    .putExtra(DetailGroupActivity.EXTRA_NAME, data.label)
                )
                activity?.finish()
            }

        }
    }

    override fun selectSubscribe(requestType: String) {
        sharedPrefs.get(AUTH_TOKEN)?.let { token ->
            CustomDialogFragment(
                getString(R.string.perhatian),
                getString(R.string.activation_disclaimer)
            ) {
                viewModel.requestActivation(token, requestType)
                    .observe(viewLifecycleOwner, observer.activate)
            }.show(parentFragmentManager, "customDialog")
        }
    }

    override fun createGroup(name: String) {
        val rawData = JsonObject().apply {
            addProperty("group_label", name)
        }

        sharedPrefs.get(AUTH_TOKEN)?.let { token ->
            CustomDialogFragment(
                getString(R.string.perhatian),
                getString(R.string.create_group_disclaimer)
            ) {
                viewModel.createGroup(token, rawData).observe(viewLifecycleOwner) {
                    when (it.status) {
                        Status.LOADING -> {}
                        Status.SUCCESS -> {
                            dialog.dismiss()
                            initUI()
                        }
                        Status.ERROR -> {
                            Toast.makeText(requireContext(), it.message, Toast.LENGTH_SHORT).show()
                        }
                    }
                }
            }.show(parentFragmentManager, "customDialog")
        }
    }

    private fun visibleUI(isLoading: Boolean, type: String) {
        if (isLoading) {
            binding.layoutKwh.visibility = View.GONE
            binding.backgroundTop.visibility = View.GONE
            binding.layoutCardProfile.visibility = View.GONE
            binding.shimmer.rootLayout.visibility = View.VISIBLE
            binding.layoutDevice.visibility = View.GONE
            binding.layoutTotalKwh.visibility = View.GONE
            binding.layoutGroup.visibility = View.GONE
        } else if (!isLoading && type == "home") {
            binding.layoutKwh.visibility = View.VISIBLE
            binding.backgroundTop.visibility = View.VISIBLE
            binding.layoutCardProfile.visibility = View.VISIBLE
            binding.shimmer.rootLayout.visibility = View.GONE
            binding.layoutDevice.visibility = View.VISIBLE
            binding.layoutTotalKwh.visibility = View.GONE
            binding.layoutGroup.visibility = View.GONE
        } else if (!isLoading && type == "building") {
            binding.layoutKwh.visibility = View.GONE
            binding.backgroundTop.visibility = View.VISIBLE
            binding.layoutCardProfile.visibility = View.VISIBLE
            binding.shimmer.rootLayout.visibility = View.GONE
            binding.layoutDevice.visibility = View.GONE
            binding.layoutTotalKwh.visibility = View.VISIBLE
            binding.layoutGroup.visibility = View.VISIBLE
        } else if (!isLoading && type == "error") {
            binding.layoutKwh.visibility = View.GONE
            binding.backgroundTop.visibility = View.GONE
            binding.layoutCardProfile.visibility = View.GONE
            binding.shimmer.rootLayout.visibility = View.GONE
            binding.layoutError.visibility = View.VISIBLE
            binding.layoutDevice.visibility = View.GONE
            binding.layoutTotalKwh.visibility = View.GONE
            binding.layoutGroup.visibility = View.GONE
        }
    }

    private fun collapsedToggle() {
        binding.groupDevice.setOnClickListener {
            deviceCollapsed = !deviceCollapsed

            if (deviceCollapsed) {
                binding.rvPcb.visibility = View.VISIBLE
                binding.rvSlca.visibility = View.VISIBLE
                binding.rvSensor.visibility = View.VISIBLE
                binding.imageCollapseDevice.setImageResource(R.drawable.ic_up)
            } else {
                binding.rvPcb.visibility = View.GONE
                binding.rvSlca.visibility = View.GONE
                binding.rvSensor.visibility = View.GONE
                binding.imageCollapseDevice.setImageResource(R.drawable.ic_down)
            }
        }
    }

    override fun onStart() {
        super.onStart()
        doRequestPermission()
    }

    private fun doRequestPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(
                    requireContext(),
                    Manifest.permission.CAMERA
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                ActivityCompat.requestPermissions(
                    requireActivity(),
                    arrayOf(Manifest.permission.CAMERA),
                    100
                )
            }

            if (ContextCompat.checkSelfPermission(
                    requireContext(),
                    Manifest.permission.CAMERA
                ) == PackageManager.PERMISSION_DENIED
            ) {
                ActivityCompat.requestPermissions(
                    requireActivity(),
                    arrayOf(Manifest.permission.CAMERA),
                    100
                )
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}