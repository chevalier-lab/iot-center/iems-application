package com.aitekteam.developer.iemscontrolling

import android.app.Application
import com.aitekteam.developer.iemscontrolling.core.di.dataSourceModule
import com.aitekteam.developer.iemscontrolling.core.di.networkModule
import com.aitekteam.developer.iemscontrolling.core.di.repositoryModule
import com.aitekteam.developer.iemscontrolling.di.useCaseModule
import com.aitekteam.developer.iemscontrolling.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level
import timber.log.Timber

class MyApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger(Level.NONE)
            androidContext(this@MyApplication)
            modules(
                listOf(
                    networkModule,
                    dataSourceModule,
                    repositoryModule,
                    useCaseModule,
                    viewModelModule
                )
            )
        }

        if (BuildConfig.DEBUG)
            Timber.plant(Timber.DebugTree())
    }
}