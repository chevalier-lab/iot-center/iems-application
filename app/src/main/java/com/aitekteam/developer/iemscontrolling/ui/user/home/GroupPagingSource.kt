package com.aitekteam.developer.iemscontrolling.ui.user.home

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.group.Group
import com.aitekteam.developer.iemscontrolling.core.data.network.service.UserService
import okio.IOException
import retrofit2.HttpException

class GroupPagingSource(
    private val token: String,
    private val userService: UserService
) : PagingSource<Int, Group>() {

    override fun getRefreshKey(state: PagingState<Int, Group>): Int? = state.anchorPosition

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Group> = try {
        val currentPage = params.key ?: FIRST_PAGE_INDEX
        val response = userService.getListGroupPaging(token, currentPage)
        val responseList = mutableListOf<Group>()

        val data = response.body()?.data ?: emptyList()
        responseList.addAll(data)

        LoadResult.Page(
            data = responseList,
            prevKey = null,
            nextKey = if (data.isNotEmpty()) currentPage.plus(1)
            else null
        )
    } catch (exception: Exception) {
        LoadResult.Error(exception)
    } catch (exception: IOException) {
        LoadResult.Error(exception)
    } catch (exception: HttpException) {
        LoadResult.Error(exception)
    }

    companion object {
        private const val FIRST_PAGE_INDEX = 0
    }
}