package com.aitekteam.developer.iemscontrolling.ui.user.home

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import com.aitekteam.developer.iemscontrolling.R
import com.aitekteam.developer.iemscontrolling.utils.listener.BottomSheetListener
import com.google.android.material.bottomsheet.BottomSheetDialog
import kotlinx.android.synthetic.main.bottom_sheet_subscribe.*
import kotlinx.android.synthetic.main.bottom_sheet_subscribe.view.*

class BottomSheetSubscribe(
    activity: Activity,
    private val listener: BottomSheetListener
) : BottomSheetDialog(activity, R.style.BottomSheetDialogTheme) {

    fun showBottomSheet() {
        val view = LayoutInflater.from(context)
            .inflate(R.layout.bottom_sheet_subscribe, subscribe_container)
        setContentView(view)
        show()

        setupUI(view)
    }

    private fun setupUI(view: View) {
        var requestType = "home"
        view.radio_home.isChecked = true

        view.home_usage.setOnClickListener {
            requestType = "home"
            view.radio_home.isChecked = true
        }

        view.building_usage.setOnClickListener {
            requestType = "building"
            view.radio_building.isChecked = true
        }

        view.btn_submit.setOnClickListener {
            listener.selectSubscribe(requestType)
            dismiss()
        }
    }
}