package com.aitekteam.developer.iemscontrolling.ui.user.profile.user.access

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.aitekteam.developer.iemscontrolling.core.data.Resource
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.invite.Invite
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.profile.User
import com.aitekteam.developer.iemscontrolling.core.data.network.service.UserService
import com.aitekteam.developer.iemscontrolling.core.domain.usecase.UserUseCase
import com.google.gson.JsonObject
import kotlinx.coroutines.flow.Flow

class UserAccessViewModel(
    private val userUseCase: UserUseCase,
    private val userService: UserService
) : ViewModel() {

    fun getListGuest(token: String): Flow<PagingData<User>> = Pager(PagingConfig(12)) {
        UserAccessPagingSource(token, userService)
    }.flow.cachedIn(viewModelScope)

    fun getDetailGuest(token: String, phoneNumber: String): LiveData<Resource<User>> =
        userUseCase.detailGuest(token, phoneNumber).asLiveData()
}