package com.aitekteam.developer.iemscontrolling.utils.listener

import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.general.Device
import com.google.gson.JsonObject

interface DialogListener {

    fun updateDevice(id: Int, name: String) {}

    fun createGroup(name: String) {}

    fun updateGroup(name: String) {}

    fun changeSsid(jsonObject: JsonObject, data: Device) {}

    fun createSchedule(deviceId: String, time: String, state: String) {}
}