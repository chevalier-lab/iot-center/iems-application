package com.aitekteam.developer.iemscontrolling.utils.dialog

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.core.content.ContextCompat
import androidx.fragment.app.DialogFragment
import com.aitekteam.developer.iemscontrolling.R
import com.aitekteam.developer.iemscontrolling.databinding.FragmentEditDeviceDialogBinding
import com.aitekteam.developer.iemscontrolling.utils.listener.DialogListener

class EditDeviceDialogFragment(
    private val deviceId: Int,
    private val deviceName: String,
    private val listener: DialogListener
) : DialogFragment() {
    private var _binding: FragmentEditDeviceDialogBinding? = null
    private val binding get() = _binding!!

    // utils
    private var nameValid = true

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentEditDeviceDialogBinding.inflate(inflater, container, false)
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog?.window?.requestFeature(Window.FEATURE_NO_TITLE)

        binding.etName.setText(deviceName)
        binding.etName.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun afterTextChanged(p0: Editable?) {}

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                nameValid = p0.toString().trim().isNotEmpty()
                inputCheck()
            }
        })

        binding.btnUpdate.setOnClickListener {
            listener.updateDevice(deviceId, binding.etName.text.toString())
        }

        // Inflate the layout for this fragment
        return binding.root
    }

    private fun inputCheck() {
        if (nameValid) {
            binding.btnUpdate.isEnabled = true
            binding.btnUpdate.setBackgroundColor(
                ContextCompat.getColor(
                    requireContext(),
                    R.color.colorPrimary
                )
            )
        } else {
            binding.btnUpdate.isEnabled = false
            binding.btnUpdate.setBackgroundColor(
                ContextCompat.getColor(
                    requireContext(),
                    R.color.divider
                )
            )
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}