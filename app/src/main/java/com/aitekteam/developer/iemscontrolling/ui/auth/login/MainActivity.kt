package com.aitekteam.developer.iemscontrolling.ui.auth.login

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.aitekteam.developer.iemscontrolling.R
import com.aitekteam.developer.iemscontrolling.core.utils.vo.Status
import com.aitekteam.developer.iemscontrolling.databinding.ActivityMainBinding
import com.aitekteam.developer.iemscontrolling.ui.user.host.HostActivity
import com.aitekteam.developer.iemscontrolling.ui.operator.OperatorActivity
import com.aitekteam.developer.iemscontrolling.ui.auth.register.RegisterActivity
import com.aitekteam.developer.iemscontrolling.utils.helper.Constants.AUTH_TOKEN
import com.aitekteam.developer.iemscontrolling.utils.helper.Constants.USER_TYPE
import com.aitekteam.developer.iemscontrolling.utils.helper.SharedPrefsUtils
import com.google.android.material.snackbar.Snackbar
import com.google.gson.JsonObject
import org.koin.android.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private val viewModel: MainViewModel by viewModel()

    // utils
    private lateinit var sharedPref: SharedPrefsUtils
    private var phoneValid = false
    private var passwordValid = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // change background status bar
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.apply {
                addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                statusBarColor = Color.TRANSPARENT
                this.decorView.rootView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            }
        }

        // init variables
        sharedPref = SharedPrefsUtils()
        sharedPref.start(this, AUTH_TOKEN)

        // auth check
        if (sharedPref.get(AUTH_TOKEN) != null) {
            if (sharedPref.get(USER_TYPE) == "user" || sharedPref.get(USER_TYPE) == "guest")
                startActivity(getLaunchHome(this))
            else if (sharedPref.get(USER_TYPE) == "operator")
                startActivity(getLaunchOperator(this))
        }

        // setup UI
        setupUI()
    }

    private fun setupUI() {
        // validate input
        binding.etPhone.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun afterTextChanged(p0: Editable?) {}

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                phoneValid = p0.toString().trim().isNotEmpty()
                inputCheck()
            }
        })

        binding.etPassword.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun afterTextChanged(p0: Editable?) {}

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                passwordValid = p0.toString().trim().isNotEmpty()
                inputCheck()
            }
        })

        // login
        binding.btnLogin.setOnClickListener { login() }

        binding.tvRegister.setOnClickListener {
            startActivity(Intent(this, RegisterActivity::class.java))
        }
    }

    private fun inputCheck() {
        if (phoneValid && passwordValid) {
            binding.btnLogin.isEnabled = true
            binding.btnLogin.setBackgroundColor(ContextCompat.getColor(this, R.color.green))
        } else {
            binding.btnLogin.isEnabled = false
            binding.btnLogin.setBackgroundColor(ContextCompat.getColor(this, R.color.divider))
        }
    }

    private fun login() {
        val dataRaw = JsonObject().apply {
            addProperty("phone_number", binding.etPhone.text.toString())
            addProperty("password", binding.etPassword.text.toString())
        }

        viewModel.login(dataRaw).observe(this, {
            when (it.status) {
                Status.LOADING -> {
                    binding.loading.visibility = View.VISIBLE
                    binding.btnLogin.visibility = View.GONE
                }
                Status.SUCCESS -> {
                    it.data?.let { data ->
                        sharedPref.setString(AUTH_TOKEN, data.token!!)
                        sharedPref.setString(USER_TYPE, data.type!!)

                        if (data.type == "user" || data.type == "guest")
                            startActivity(getLaunchHome(this))
                        else if (data.type == "operator")
                            startActivity(getLaunchOperator(this))
                    }
                }
                Status.ERROR -> {
                    binding.loading.visibility = View.GONE
                    binding.btnLogin.visibility = View.VISIBLE
                    Snackbar.make(window.decorView.rootView, "Incorrect username or password!", Snackbar.LENGTH_SHORT)
                        .show()
                }
            }
        })
    }

    companion object {
        fun getLaunchHome(from: Context) = Intent(from, HostActivity::class.java).apply {
            addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        }

        fun getLaunchOperator(from: Context) = Intent(from, OperatorActivity::class.java).apply {
            addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        }
    }
}