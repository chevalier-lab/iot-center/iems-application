package com.aitekteam.developer.iemscontrolling.utils.dialog

import android.app.TimePickerDialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.TimePicker
import androidx.fragment.app.DialogFragment
import com.aitekteam.developer.iemscontrolling.R
import com.aitekteam.developer.iemscontrolling.databinding.FragmentAddSchedulingDialogBinding
import com.aitekteam.developer.iemscontrolling.utils.adapter.DropDownAdapter
import com.aitekteam.developer.iemscontrolling.utils.listener.DialogListener
import java.util.*

class AddSchedulingDialogFragment(
    private val deviceId: String,
    private val listener: DialogListener
) : DialogFragment(), TimePickerDialog.OnTimeSetListener {
    private var hour = 0
    private var minute = 0

    private var time = ""
    private var state = "Choose Action"

    private var _binding: FragmentAddSchedulingDialogBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentAddSchedulingDialogBinding.inflate(inflater, container, false)
        dialog?.window?.apply {
            setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            requestFeature(Window.FEATURE_NO_TITLE)
        }

        binding.time.setOnClickListener {
            getDateTime()

            val timePicker = TimePickerDialog(context, this, hour, minute, true)
            timePicker.apply {
                show()
                getButton(TimePickerDialog.BUTTON_POSITIVE).setTextColor(Color.BLACK)
                getButton(TimePickerDialog.BUTTON_NEGATIVE).setTextColor(Color.BLACK)
            }
        }
        setDropDown()

        binding.btnAddSchedule.setOnClickListener { inputCheck() }
        return binding.root
    }

    private fun setDropDown() {
        val defaultValue = "Choose Action"
        binding.filter.setText(defaultValue)

        val listState = listOf("Choose Action", "ON", "OFF")
        DropDownAdapter(requireContext(), binding.filter, R.layout.option_item, listState) {
            binding.filter.setOnItemClickListener { _, _, pos, _ ->
                when (pos) {
                    0 -> state = defaultValue
                    1 -> state = "ON"
                    2 -> state = "OFF"
                }
            }
        }.show()
    }

    private fun getDateTime() {
        val calendar = Calendar.getInstance()
        hour = calendar.get(Calendar.HOUR_OF_DAY)
        minute = calendar.get(Calendar.MINUTE)
    }

    override fun onTimeSet(p: TimePicker?, hourOfDay: Int, min: Int) {
        val hourString = if (hourOfDay < 10) "0$hourOfDay"
        else "$hourOfDay"

        val minString = if (min < 10) "0$min"
        else "$min"

        time = StringBuilder().apply {
            append(hourString).append(":$minString")
        }.toString()

        binding.time.setText(time)
    }

    private fun inputCheck() {
        when {
            time == "" -> {
                binding.time.apply {
                    error = "Choose time!"
                    requestFocus()
                }
            }
            state == "Choose Action" -> {
                binding.filter.apply {
                    error = "Choose state!"
                    requestFocus()
                }
            }
            else -> {
                listener.createSchedule(deviceId, time, state)
                dismiss()
            }
        }
    }
}