package com.aitekteam.developer.iemscontrolling.ui.user.profile.user.access

import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.DialogFragment
import com.aitekteam.developer.iemscontrolling.R
import com.aitekteam.developer.iemscontrolling.core.utils.vo.Status
import com.aitekteam.developer.iemscontrolling.databinding.FragmentInviteUserDialogBinding
import com.aitekteam.developer.iemscontrolling.ui.user.profile.user.invite.DetailInviteUserActivity
import com.aitekteam.developer.iemscontrolling.utils.helper.Constants.AUTH_TOKEN
import com.aitekteam.developer.iemscontrolling.utils.helper.SharedPrefsUtils
import org.koin.android.viewmodel.ext.android.viewModel
import timber.log.Timber

class InviteUserDialogFragment : DialogFragment() {
    private var _binding: FragmentInviteUserDialogBinding? = null
    private val binding get() = _binding!!
    private val viewModel: UserAccessViewModel by viewModel()

    // utils
    private var phoneValid = false
    private lateinit var sharedPref: SharedPrefsUtils

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentInviteUserDialogBinding.inflate(inflater, container, false)

        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog?.window?.requestFeature(Window.FEATURE_NO_TITLE)

        // init variables
        sharedPref = SharedPrefsUtils()
        sharedPref.start(requireActivity(), AUTH_TOKEN)

        // init ui
        initUI()

        return binding.root
    }

    private fun initUI() {
        // validate input
        binding.etPhone.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun afterTextChanged(p0: Editable?) {
                phoneValid = p0.toString().trim().isNotEmpty()
                inputCheck()
            }
        })

        binding.btnSubmit.setOnClickListener { processData() }
    }

    private fun processData() {
        sharedPref.get(AUTH_TOKEN)?.let { token ->
            viewModel.getDetailGuest(token, binding.etPhone.text.toString())
                .observe(viewLifecycleOwner) {
                    when (it.status) {
                        Status.LOADING -> {
                            binding.loading.visibility = View.VISIBLE
                            binding.btnSubmit.visibility = View.GONE
                        }
                        Status.SUCCESS -> {
                            it.data?.let { data ->
                                if (data.status == "activate")
                                    Toast.makeText(
                                        requireContext(),
                                        "This account has been activated!",
                                        Toast.LENGTH_SHORT
                                    ).show()
                                else
                                    startActivity(
                                        Intent(
                                            requireContext(),
                                            DetailInviteUserActivity::class.java
                                        )
                                            .putExtra(DetailInviteUserActivity.EXTRA_DATA, data)
                                    )
                                dismiss()
                            }
                        }
                        Status.ERROR -> {
                            binding.loading.visibility = View.GONE
                            binding.btnSubmit.visibility = View.VISIBLE
                            Timber.i("error => ${it.message}")
                            Toast.makeText(
                                requireContext(),
                                "Incorrect phone number!",
                                Toast.LENGTH_SHORT
                            ).show()
                            dismiss()
                        }
                    }
                }
        }
    }

    private fun inputCheck() {
        if (phoneValid) {
            binding.btnSubmit.isEnabled = true
            binding.btnSubmit.setBackgroundColor(
                ContextCompat.getColor(
                    requireContext(),
                    R.color.colorPrimary
                )
            )
        } else {
            binding.btnSubmit.isEnabled = false
            binding.btnSubmit.setBackgroundColor(
                ContextCompat.getColor(
                    requireContext(),
                    R.color.divider
                )
            )
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}