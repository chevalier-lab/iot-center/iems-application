package com.aitekteam.developer.iemscontrolling.ui.user.home.group.append

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import androidx.core.content.ContextCompat
import com.aitekteam.developer.iemscontrolling.R
import com.aitekteam.developer.iemscontrolling.utils.adapter.DropDownAdapter
import com.aitekteam.developer.iemscontrolling.utils.listener.BottomSheetListener
import com.google.android.material.bottomsheet.BottomSheetDialog
import kotlinx.android.synthetic.main.bottom_sheet_filter_device.*
import kotlinx.android.synthetic.main.bottom_sheet_filter_device.view.*

class BottomSheetFilterDevice(
    activity: Activity,
    private val deviceType: String,
    private val deviceStatus: String,
    private val listener: BottomSheetListener
) : BottomSheetDialog(activity, R.style.BottomSheetDialogTheme) {

    private var type = ""
    private var status = "normal"

    fun showBottomSheet() {
        val view = LayoutInflater.from(context).inflate(R.layout.bottom_sheet_filter_device, filter_container)
        setContentView(view)
        show()

        setupUI(view)
    }

    private fun setupUI(view: View) {
        type = deviceType
        status = deviceStatus

        // set current type
        when (type) {
            "" -> view.type.setText(StringBuilder().append("All Device"))
            "pcb" -> view.type.setText(StringBuilder().append("Circuit Breaker"))
            "sensors" -> view.type.setText(StringBuilder().append("Sensor"))
            "slca" -> view.type.setText(StringBuilder().append("SLCA"))
            "kwh-1-phase" -> view.type.setText(StringBuilder().append("kWh 1 Phase"))
            "kwh-3-phase" -> view.type.setText(StringBuilder().append("kWh 3 Phase"))
        }

        // set current status
        when (status) {
            "normal" -> view.status.setText(StringBuilder().append("All Status"))
            "yes" -> view.status.setText(StringBuilder().append("Already Added"))
            "no" -> view.status.setText(StringBuilder().append("Not Added"))
        }

        val typeOptions = listOf("All Device", "Circuit Breaker", "Sensor", "SLCA", "kWh 1 Phase", "kWh 3 Phase")
        DropDownAdapter(context, view.type, R.layout.option_item, typeOptions) {
            // filter listener
            view.type.setOnItemClickListener { _, _, pos, _ ->
                when (pos) {
                    0 -> type = ""
                    1 -> type = "pcb"
                    2 -> type = "sensors"
                    3 -> type = "slca"
                    4 -> type = "kwh-1-phase"
                    5 -> type = "kwh-3-phase"
                }
            }
        }.show()

        val statusOptions = listOf("All Status", "Already Added", "Not Added")
        DropDownAdapter(context, view.status, R.layout.option_item, statusOptions) {
            // status listener
            view.status.setOnItemClickListener { _, _, pos, _ ->
                when (pos) {
                    0 -> status = "normal"
                    1 -> status = "yes"
                    2 -> status = "no"
                }
            }
        }.show()

        // submit
        view.btn_submit.setOnClickListener {
            listener.filterDevice(type, status)
            dismiss()
        }
    }

}