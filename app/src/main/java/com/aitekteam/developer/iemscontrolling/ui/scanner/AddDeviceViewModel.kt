package com.aitekteam.developer.iemscontrolling.ui.scanner

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import com.aitekteam.developer.iemscontrolling.core.data.Resource
import com.aitekteam.developer.iemscontrolling.core.data.network.response.operator.ActivationDevice
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.general.Device
import com.aitekteam.developer.iemscontrolling.core.domain.usecase.DeviceUseCase
import com.aitekteam.developer.iemscontrolling.core.domain.usecase.OperatorUseCase
import com.google.gson.JsonObject

class AddDeviceViewModel(
    private val deviceUseCase: DeviceUseCase,
    private val operatorUseCase: OperatorUseCase
) : ViewModel() {

    fun getDeviceInfo(token: String): LiveData<Resource<Device>> =
        deviceUseCase.previewDevice(token).asLiveData()

    fun activateDevice(token: String, jsonObject: JsonObject): LiveData<Resource<ActivationDevice>> =
        operatorUseCase.activateDevice(token, jsonObject).asLiveData()
}