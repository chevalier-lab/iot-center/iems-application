package com.aitekteam.developer.iemscontrolling.utils.helper

object Helpers {

    fun Int.convertDayMonth(): String {
        return if (this < 10)
            StringBuilder().append("0$this").toString()
        else
            StringBuilder().append(this).toString()
    }

    fun String.dateFormat(): String {
        val splitDateTime = this.split(" ")
        val splitDate = splitDateTime[0].split("-")
        val splitTime = splitDateTime[1].split(":")

        return StringBuilder().apply {
            append("${splitTime[0]}:${splitTime[1]}, ")
            append(formatAccumulated(splitDateTime[0]))
            append(" ${splitDate[0]}")
        }.toString()
    }

    val formatMonth: (String) -> String = {
        val splitDate = it.split("-")

        when (splitDate[1]) {
            "01" -> "Jan"
            "02" -> "Feb"
            "03" -> "Mar"
            "04" -> "Apr"
            "05" -> "May"
            "06" -> "Jun"
            "07" -> "Jul"
            "08" -> "Aug"
            "09" -> "Sep"
            "10" -> "Okt"
            "11" -> "Nov"
            "12" -> "Dec"
            else -> ""
        }
    }

    val formatAccumulated: (String) -> String = {
        val splitDate = it.split("-")
        val day = splitDate[2]

        when (splitDate[1]) {
            "01" -> "Jan $day"
            "02" -> "Feb $day"
            "03" -> "Mar $day"
            "04" -> "Apr $day"
            "05" -> "May $day"
            "06" -> "Jun $day"
            "07" -> "Jul $day"
            "08" -> "Aug $day"
            "09" -> "Sep $day"
            "10" -> "Okt $day"
            "11" -> "Nov $day"
            "12" -> "Dec $day"
            else -> ""
        }
    }

    val formatSpesificDate: (String) -> String = {
        val hour = it.toInt()

        if (hour < 13) {
            "$it AM"
        } else {
            when (hour) {
                13 -> "01 PM"
                14 -> "02 PM"
                15 -> "03 PM"
                16 -> "04 PM"
                17 -> "05 PM"
                18 -> "06 PM"
                19 -> "07 PM"
                20 -> "08 PM"
                21 -> "09 PM"
                22 -> "10 PM"
                23 -> "11 PM"
                24 -> "12 PM"
                else -> ""
            }
        }
    }
}