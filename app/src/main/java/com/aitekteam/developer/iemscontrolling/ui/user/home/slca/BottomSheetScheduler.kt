package com.aitekteam.developer.iemscontrolling.ui.user.home.slca

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.aitekteam.developer.iemscontrolling.R
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.slca.Scheduler
import com.aitekteam.developer.iemscontrolling.core.utils.adapter.AdapterCallback
import com.aitekteam.developer.iemscontrolling.core.utils.adapter.ReusableAdapter
import com.aitekteam.developer.iemscontrolling.utils.listener.BottomSheetListener
import com.google.android.material.bottomsheet.BottomSheetDialog
import kotlinx.android.synthetic.main.bottom_sheet_scheduler.*
import kotlinx.android.synthetic.main.bottom_sheet_scheduler.view.*
import kotlinx.android.synthetic.main.item_scheduler.view.*

class BottomSheetScheduler(
    activity: Activity,
    private val deviceId: String,
    private val list: List<Scheduler>,
    private val listener: BottomSheetListener
) : BottomSheetDialog(activity, R.style.BottomSheetDialogTheme) {
    private lateinit var adapter: ReusableAdapter<Scheduler>

    fun showBottomSheet() {
        val view = LayoutInflater.from(context)
            .inflate(R.layout.bottom_sheet_scheduler, scheduler_container)
        setContentView(view)
        show()

        adapter = ReusableAdapter(view.context)
        setupUI(view)
    }

    private fun setupUI(view: View) {
        if (list.isEmpty()) {
            view.layout_empty.visibility = View.VISIBLE
            view.rv_schedule.visibility = View.GONE
        } else {
            view.layout_empty.visibility = View.GONE
            view.rv_schedule.visibility = View.VISIBLE
        }

        setupAdapter(view.rv_schedule)
        adapter.addData(list)

        btn_add_schedule.setOnClickListener { listener.scheduleDevice(deviceId) }
    }

    private val adapterCallback = object : AdapterCallback<Scheduler> {
        override fun initComponent(itemView: View, data: Scheduler, itemIndex: Int) {
            itemView.apply {
                this.tv_action.text = data.action
                this.tv_time.text = data.action_time

                this.btn_delete.setOnClickListener {
                    data.id?.let { listener.deleteScheduleDevice(it) }
                }
            }
        }

        override fun onItemClicked(itemView: View, data: Scheduler, itemIndex: Int) {}
    }

    private fun setupAdapter(recyclerView: RecyclerView) {
        adapter.adapterCallback(adapterCallback)
            .setLayout(R.layout.item_scheduler)
            .isVerticalView()
            .build(recyclerView)
    }
}