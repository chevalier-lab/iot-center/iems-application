package com.aitekteam.developer.iemscontrolling.ui.user.home.kwh1.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import com.aitekteam.developer.iemscontrolling.core.data.Resource
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.calculate.TotalKwh
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.general.Device
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.kwh1.Kwh1
import com.aitekteam.developer.iemscontrolling.core.domain.usecase.DeviceUseCase
import com.google.gson.JsonObject
import kotlinx.coroutines.flow.Flow

class PowerStatus1ViewModel(
    private val deviceUseCase: DeviceUseCase
) : ViewModel() {

    fun getDeviceInfo(token: String): LiveData<Resource<Device>> =
        deviceUseCase.getDeviceInfo(token).asLiveData()

    fun getLastKwh1(token: String): LiveData<Resource<Kwh1>> =
        deviceUseCase.getLastKwh1(token).asLiveData()

    fun updateDevice(token: String, id: Int, jsonObject: JsonObject): LiveData<Resource<Device>> =
        deviceUseCase.updateDevice(token, id, jsonObject).asLiveData()

    fun calculateKwh(token: String, deviceToken: String): LiveData<Resource<TotalKwh>> =
        deviceUseCase.calculateKwh(token, deviceToken).asLiveData()

    fun changeDeviceSsid(
        token: String,
        deviceId: Int,
        jsonObject: JsonObject
    ): LiveData<Resource<Device>> =
        deviceUseCase.changeDeviceSsid(token, deviceId, jsonObject).asLiveData()
}