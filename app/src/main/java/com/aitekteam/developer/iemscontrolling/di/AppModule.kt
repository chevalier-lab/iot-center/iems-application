package com.aitekteam.developer.iemscontrolling.di

import com.aitekteam.developer.iemscontrolling.core.domain.interactor.*
import com.aitekteam.developer.iemscontrolling.core.domain.usecase.*
import com.aitekteam.developer.iemscontrolling.ui.auth.login.MainViewModel
import com.aitekteam.developer.iemscontrolling.ui.auth.register.RegisterViewModel
import com.aitekteam.developer.iemscontrolling.ui.operator.OperatorViewModel
import com.aitekteam.developer.iemscontrolling.ui.operator.active.ActiveUserViewModel
import com.aitekteam.developer.iemscontrolling.ui.operator.detail.UserDetailViewModel
import com.aitekteam.developer.iemscontrolling.ui.operator.inactive.InactiveUserViewModel
import com.aitekteam.developer.iemscontrolling.ui.operator.profile.ProfileOperatorViewModel
import com.aitekteam.developer.iemscontrolling.ui.scanner.AddDeviceViewModel
import com.aitekteam.developer.iemscontrolling.ui.user.complaint.ComplainViewModel
import com.aitekteam.developer.iemscontrolling.ui.user.home.HomeViewModel
import com.aitekteam.developer.iemscontrolling.ui.user.home.group.append.AppendDeviceViewModel
import com.aitekteam.developer.iemscontrolling.ui.user.home.group.detail.DetailGroupViewModel
import com.aitekteam.developer.iemscontrolling.ui.user.home.kwh.ListKwhViewModel
import com.aitekteam.developer.iemscontrolling.ui.user.home.kwh1.detail.PowerStatus1ViewModel
import com.aitekteam.developer.iemscontrolling.ui.user.home.kwh3.detail.PowerStatus3ViewModel
import com.aitekteam.developer.iemscontrolling.ui.user.home.pcb.PcbViewModel
import com.aitekteam.developer.iemscontrolling.ui.user.home.sensor.SensorViewModel
import com.aitekteam.developer.iemscontrolling.ui.user.home.slca.SlcaViewModel
import com.aitekteam.developer.iemscontrolling.ui.user.host.HostViewModel
import com.aitekteam.developer.iemscontrolling.ui.user.notification.NotificationViewModel
import com.aitekteam.developer.iemscontrolling.ui.user.profile.ProfileViewModel
import com.aitekteam.developer.iemscontrolling.ui.user.profile.account.info.AccountInfoViewModel
import com.aitekteam.developer.iemscontrolling.ui.user.profile.account.manage.ManageAccountViewModel
import com.aitekteam.developer.iemscontrolling.ui.user.profile.complaint.detail.ComplaintDetailViewModel
import com.aitekteam.developer.iemscontrolling.ui.user.profile.complaint.list.ComplaintListViewModel
import com.aitekteam.developer.iemscontrolling.ui.user.profile.user.access.UserAccessViewModel
import com.aitekteam.developer.iemscontrolling.ui.user.profile.user.access.detail.DetailGuestViewModel
import com.aitekteam.developer.iemscontrolling.ui.user.profile.user.access.invite.AddDeviceGuestViewModel
import com.aitekteam.developer.iemscontrolling.ui.user.profile.user.invite.DetailInviteUserViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val useCaseModule = module {
    factory<AuthUseCase> { AuthInteractor(get()) }
    factory<UserUseCase> { UserInteractor(get()) }
    factory<ChartUseCase> { ChartInteractor(get()) }
    factory<DeviceUseCase> { DeviceInteractor(get()) }
    factory<OperatorUseCase> { OperatorInteractor(get()) }
    factory<TicketingUseCase> { TicketingInteractor(get()) }
}

val viewModelModule = module {
    viewModel { HostViewModel(get()) }
    viewModel { MainViewModel(get()) }
    viewModel { ListKwhViewModel(get()) }
    viewModel { ProfileViewModel(get()) }
    viewModel { RegisterViewModel(get()) }
    viewModel { OperatorViewModel(get()) }
    viewModel { PcbViewModel(get(), get()) }
    viewModel { UserDetailViewModel(get()) }
    viewModel { ActiveUserViewModel(get()) }
    viewModel { SlcaViewModel(get(), get()) }
    viewModel { DetailGuestViewModel(get()) }
    viewModel { AccountInfoViewModel(get()) }
    viewModel { PowerStatus1ViewModel(get()) }
    viewModel { PowerStatus3ViewModel(get()) }
    viewModel { NotificationViewModel(get()) }
    viewModel { SensorViewModel(get(), get()) }
    viewModel { ComplaintListViewModel(get()) }
    viewModel { ManageAccountViewModel(get()) }
    viewModel { ComplaintDetailViewModel(get()) }
    viewModel { ComplainViewModel(get(), get()) }
    viewModel { ProfileOperatorViewModel(get()) }
    viewModel { DetailInviteUserViewModel(get()) }
    viewModel { AddDeviceViewModel(get(), get()) }
    viewModel { UserAccessViewModel(get(), get()) }
    viewModel { HomeViewModel(get(), get(), get()) }
    viewModel { DetailGroupViewModel(get(), get()) }
    viewModel { InactiveUserViewModel(get(), get()) }
    viewModel { AppendDeviceViewModel(get(), get()) }
    viewModel { AddDeviceGuestViewModel(get(), get()) }
}