package com.aitekteam.developer.iemscontrolling.utils.listener

interface BottomSheetListener {

    fun filterChartKwh(filter: String, type: String, date: String) {}

    fun filterChartKwh3(filter: String, type: String, value: String, date: String) {}

    fun selectSubscribe(requestType: String) {}

    fun filterDevice(type: String, status: String) {}

    fun scheduleDevice(deviceId: String) {}

    fun deleteScheduleDevice(scheduleId: String) {}
}

