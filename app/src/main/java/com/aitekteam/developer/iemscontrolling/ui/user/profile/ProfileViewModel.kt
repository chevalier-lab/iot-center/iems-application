package com.aitekteam.developer.iemscontrolling.ui.user.profile

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import com.aitekteam.developer.iemscontrolling.core.data.Resource
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.profile.User
import com.aitekteam.developer.iemscontrolling.core.domain.usecase.UserUseCase

class ProfileViewModel(
    private val userUseCase: UserUseCase
) : ViewModel() {

    fun getProfile(authorization: String): LiveData<Resource<User>> =
        userUseCase.getData(authorization).asLiveData()
}