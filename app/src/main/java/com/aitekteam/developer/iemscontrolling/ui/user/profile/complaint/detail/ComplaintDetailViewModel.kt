package com.aitekteam.developer.iemscontrolling.ui.user.profile.complaint.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import com.aitekteam.developer.iemscontrolling.core.data.Resource
import com.aitekteam.developer.iemscontrolling.core.data.network.response.ticketing.ReplyTicket
import com.aitekteam.developer.iemscontrolling.core.domain.usecase.TicketingUseCase
import com.google.gson.JsonObject

class ComplaintDetailViewModel(
    private val ticketingUseCase: TicketingUseCase
) : ViewModel() {

    fun replyComplaint(
        token: String,
        ticketId: Int,
        jsonObject: JsonObject
    ): LiveData<Resource<ReplyTicket>> =
        ticketingUseCase.replyComplaint(token, ticketId, jsonObject).asLiveData()

    fun getListReply(
        token: String,
        ticketId: Int
    ): LiveData<Resource<List<ReplyTicket>>> =
        ticketingUseCase.getListReply(token, ticketId).asLiveData()
}