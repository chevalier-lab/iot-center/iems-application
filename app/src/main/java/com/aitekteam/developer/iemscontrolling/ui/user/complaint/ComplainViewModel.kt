package com.aitekteam.developer.iemscontrolling.ui.user.complaint

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import com.aitekteam.developer.iemscontrolling.core.data.Resource
import com.aitekteam.developer.iemscontrolling.core.data.network.response.media.Media
import com.aitekteam.developer.iemscontrolling.core.data.network.response.ticketing.CreateTicketing
import com.aitekteam.developer.iemscontrolling.core.domain.usecase.TicketingUseCase
import com.aitekteam.developer.iemscontrolling.core.domain.usecase.UserUseCase
import com.google.gson.JsonObject
import okhttp3.MultipartBody

class ComplainViewModel(
    private val userUseCase: UserUseCase,
    private val ticketingUseCase: TicketingUseCase
) : ViewModel() {

    fun createComplain(
        authorization: String,
        jsonObject: JsonObject
    ): LiveData<Resource<CreateTicketing>> =
        ticketingUseCase.createComplain(authorization, jsonObject).asLiveData()

    fun createMedia(token: String, photo: MultipartBody.Part): LiveData<Resource<Media>> =
        userUseCase.createMedia(token, photo).asLiveData()
}