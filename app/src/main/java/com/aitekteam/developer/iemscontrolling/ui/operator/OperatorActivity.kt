package com.aitekteam.developer.iemscontrolling.ui.operator

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.aitekteam.developer.iemscontrolling.R
import com.aitekteam.developer.iemscontrolling.core.utils.vo.Status
import com.aitekteam.developer.iemscontrolling.databinding.ActivityOperatorBinding
import com.aitekteam.developer.iemscontrolling.ui.operator.profile.ProfileOperatorActivity
import com.aitekteam.developer.iemscontrolling.utils.adapter.SectionAdapter
import com.aitekteam.developer.iemscontrolling.utils.helper.Constants.AUTH_TOKEN
import com.aitekteam.developer.iemscontrolling.utils.helper.Constants.USER_COVER
import com.aitekteam.developer.iemscontrolling.utils.helper.Constants.USER_EMAIL
import com.aitekteam.developer.iemscontrolling.utils.helper.Constants.USER_NAME
import com.aitekteam.developer.iemscontrolling.utils.helper.Constants.USER_PHONE
import com.aitekteam.developer.iemscontrolling.utils.helper.Constants.USER_STATUS
import com.aitekteam.developer.iemscontrolling.utils.helper.Constants.USER_TYPE
import com.aitekteam.developer.iemscontrolling.utils.helper.SharedPrefsUtils
import com.bumptech.glide.Glide
import com.google.android.material.tabs.TabLayoutMediator
import org.koin.android.viewmodel.ext.android.viewModel
import timber.log.Timber

class OperatorActivity : AppCompatActivity() {
    private lateinit var binding: ActivityOperatorBinding
    private val viewModel: OperatorViewModel by viewModel()

    // utils
    private lateinit var sharedPrefs: SharedPrefsUtils

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityOperatorBinding.inflate(layoutInflater)
        setContentView(binding.root)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.apply {
                addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                statusBarColor = Color.TRANSPARENT
                this.decorView.rootView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            }
        }

        // init utils
        sharedPrefs = SharedPrefsUtils()
        sharedPrefs.start(this, AUTH_TOKEN)

        doRequestPermission()

        fetchData()

        // init utils
        val adapter = SectionAdapter(supportFragmentManager, lifecycle)
        binding.viewPager.apply {
            this.adapter = adapter
        }

        val title = listOf("User Baru", "User Aktif")
        TabLayoutMediator(binding.tabLayout, binding.viewPager) { tab, pos ->
            tab.text = title[pos]
        }.attach()
    }

    private fun fetchData() {
        binding.btnSetting.setOnClickListener {
            startActivity(Intent(this, ProfileOperatorActivity::class.java))
            finish()
        }

        sharedPrefs.get(AUTH_TOKEN)?.let { token ->
            viewModel.getProfile(token).observe(this) {
                when (it.status) {
                    Status.LOADING -> {}
                    Status.SUCCESS -> {
                        it.data?.let { data ->
                            binding.tvName.text = data.fullName
                            binding.tvEmail.text = data.email
                            binding.tvPhone.text = data.phoneNumber

                            // set shared prefs
                            sharedPrefs.setString(USER_NAME, data.fullName!!)
                            sharedPrefs.setString(USER_EMAIL, data.email!!)
                            sharedPrefs.setString(USER_TYPE, data.type!!)
                            sharedPrefs.setString(USER_COVER, data.cover!!)
                            sharedPrefs.setString(USER_PHONE, data.phoneNumber!!)
                            sharedPrefs.setString(USER_STATUS, data.status!!)

                            // set image
                            Glide.with(this)
                                .load(data.cover ?: R.drawable.avatar)
                                .circleCrop()
                                .into(binding.imageUser)
                        }
                    }
                    Status.ERROR -> {
                        Timber.i("error => ${it.message}")
                    }
                }
            }
        }
    }

    private fun doRequestPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(
                    this,
                    Manifest.permission.CAMERA
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(Manifest.permission.CAMERA),
                    100
                )
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        menu?.getItem(0)?.icon = ContextCompat.getDrawable(this, R.drawable.ic_profile)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.overflow -> {
                startActivity(Intent(this, ProfileOperatorActivity::class.java))
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}