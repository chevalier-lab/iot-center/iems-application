package com.aitekteam.developer.iemscontrolling.utils.callback

import android.content.Context
import android.content.Intent
import android.view.View
import androidx.lifecycle.LifecycleOwner
import com.aitekteam.developer.iemscontrolling.R
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.general.Device
import com.aitekteam.developer.iemscontrolling.core.utils.adapter.AdapterCallback
import com.aitekteam.developer.iemscontrolling.core.utils.vo.Status
import com.aitekteam.developer.iemscontrolling.ui.user.home.HomeViewModel
import com.aitekteam.developer.iemscontrolling.ui.user.home.sensor.SensorActivity
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_sensor.view.*
import timber.log.Timber

class SensorAdapterCallback(
    private val context: Context,
    private val viewModel: HomeViewModel,
    private val lifecycleOwner: LifecycleOwner
) : AdapterCallback<Device> {

    override fun initComponent(itemView: View, data: Device, itemIndex: Int) {
        itemView.tv_sensor_name.text = data.name

        // set value
        data.token?.let { token ->
            viewModel.getLastSensor(token).observe(lifecycleOwner) {
                when (it.status) {
                    Status.LOADING -> {
                    }
                    Status.SUCCESS -> {
                        it.data?.let { sensor ->
                            sensor.temp?.let { temp ->
                                Glide.with(context)
                                    .load(R.drawable.ic_temperature)
                                    .into(itemView.image_temp)

                                itemView.tv_temp.text =
                                    StringBuilder().append(temp).append("\u00B0")
                            } ?: run {
                                Glide.with(context)
                                    .load(R.drawable.ic_temperature_off)
                                    .into(itemView.image_temp)

                                itemView.tv_temp.text =
                                    StringBuilder().append("N/A")
                            }

                            sensor.hum?.let { hum ->
                                Glide.with(context)
                                    .load(R.drawable.ic_humidity)
                                    .into(itemView.image_hum)

                                itemView.tv_hum.text =
                                    StringBuilder().append(hum).append("%")
                            } ?: run {
                                Glide.with(context)
                                    .load(R.drawable.ic_humidity_off)
                                    .into(itemView.image_hum)

                                itemView.tv_hum.text =
                                    StringBuilder().append("N/A")
                            }

                            sensor.lux?.let { lux ->
                                Glide.with(context)
                                    .load(R.drawable.ic_lux)
                                    .into(itemView.image_lux)

                                itemView.tv_lux.text =
                                    StringBuilder().append(lux).append("%")
                            } ?: run {
                                Glide.with(context)
                                    .load(R.drawable.ic_lux_off)
                                    .into(itemView.image_lux)

                                itemView.tv_lux.text =
                                    StringBuilder().append("N/A")
                            }

                            sensor.pir?.let { pir ->
                                Glide.with(context)
                                    .load(R.drawable.ic_pir)
                                    .into(itemView.image_pir)

                                itemView.tv_pir.text = pir.toString()
                            } ?: run {
                                Glide.with(context)
                                    .load(R.drawable.ic_pir_off)
                                    .into(itemView.image_pir)

                                itemView.tv_pir.text =
                                    StringBuilder().append("N/A")
                            }
                        }
                    }
                    Status.ERROR -> {
                        Timber.i("error => ${it.message}")
                    }
                }
            }
        }
    }

    override fun onItemClicked(itemView: View, data: Device, itemIndex: Int) {
        context.startActivity(
            Intent(context, SensorActivity::class.java).apply {
                putExtra(SensorActivity.EXTRA_TOKEN, data.token)
            }
        )
    }
}