package com.aitekteam.developer.iemscontrolling.ui.user.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.aitekteam.developer.iemscontrolling.core.data.Resource
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.calculate.TotalKwh
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.general.Device
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.kwh1.Kwh1
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.kwh3.Kwh3
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.pcb.Pcb
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.sensor.Sensor
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.slca.Slca
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.group.Group
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.profile.User
import com.aitekteam.developer.iemscontrolling.core.data.network.service.UserService
import com.aitekteam.developer.iemscontrolling.core.domain.usecase.DeviceUseCase
import com.aitekteam.developer.iemscontrolling.core.domain.usecase.UserUseCase
import com.google.gson.JsonObject
import kotlinx.coroutines.flow.Flow

class HomeViewModel(
    private val userUseCase: UserUseCase,
    private val userService: UserService,
    private val deviceUseCase: DeviceUseCase
) : ViewModel() {

    fun getProfile(token: String): LiveData<Resource<User>> =
        userUseCase.getData(token).asLiveData()

    fun requestActivation(token: String, requestType: String): LiveData<Resource<User>> =
        userUseCase.requestActivate(token, requestType).asLiveData()

    fun getListDevices(token: String): LiveData<Resource<List<Device>>> =
        deviceUseCase.getListDevice(token).asLiveData()

    fun getListDeviceGuest(token: String): LiveData<Resource<List<Device>>> =
        deviceUseCase.getListDeviceGuest(token).asLiveData()

    fun getLastPcb(token: String): LiveData<Resource<Pcb>> =
        deviceUseCase.getLastPcb(token).asLiveData()

    fun getLastSensor(token: String): LiveData<Resource<Sensor>> =
        deviceUseCase.getLastSensor(token).asLiveData()

    fun getLastSlca(token: String): LiveData<Resource<Slca>> =
        deviceUseCase.getLastSlca(token).asLiveData()

    fun getLastKwh1(token: String): LiveData<Resource<Kwh1>> =
        deviceUseCase.getLastKwh1(token).asLiveData()

    fun getLastKwh3(token: String): LiveData<Resource<Kwh3>> =
        deviceUseCase.getLastKwh3(token).asLiveData()

    fun pushDevice(token: String, jsonObject: JsonObject): LiveData<Resource<Device>> =
        deviceUseCase.pushDevice(token, jsonObject).asLiveData()

    fun totalKwh(token: String): LiveData<Resource<TotalKwh>> =
        deviceUseCase.totalKwh(token).asLiveData()

    fun getListGroup(token: String): Flow<PagingData<Group>> = Pager(PagingConfig(12)) {
        GroupPagingSource(token, userService)
    }.flow.cachedIn(viewModelScope)

    fun createGroup(token: String, jsonObject: JsonObject): LiveData<Resource<Group>> =
        userUseCase.createGroup(token, jsonObject).asLiveData()

    fun calculateKwh(token: String, deviceToken: String): LiveData<Resource<TotalKwh>> =
        deviceUseCase.calculateKwh(token, deviceToken).asLiveData()
}