package com.aitekteam.developer.iemscontrolling.ui.user.home.group.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import com.aitekteam.developer.iemscontrolling.core.data.Resource
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.calculate.TotalKwh
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.general.Device
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.pcb.Pcb
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.sensor.Sensor
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.slca.Slca
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.group.Group
import com.aitekteam.developer.iemscontrolling.core.domain.usecase.DeviceUseCase
import com.aitekteam.developer.iemscontrolling.core.domain.usecase.UserUseCase
import com.google.gson.JsonObject

class DetailGroupViewModel(
    private val userUseCase: UserUseCase,
    private val deviceUseCase: DeviceUseCase
) : ViewModel() {

    fun getDetailGroup(token: String, groupId: Int): LiveData<Resource<List<Device>>> =
        userUseCase.getDetailGroup(token, groupId).asLiveData()

    fun getLastPcb(token: String): LiveData<Resource<Pcb>> =
        deviceUseCase.getLastPcb(token).asLiveData()

    fun getLastSensor(token: String): LiveData<Resource<Sensor>> =
        deviceUseCase.getLastSensor(token).asLiveData()

    fun getLastSlca(token: String): LiveData<Resource<Slca>> =
        deviceUseCase.getLastSlca(token).asLiveData()

    fun pushDevice(token: String, jsonObject: JsonObject): LiveData<Resource<Device>> =
        deviceUseCase.pushDevice(token, jsonObject).asLiveData()

    fun totalKwhOnGroup(token: String, groupId: Int): LiveData<Resource<TotalKwh>> =
        deviceUseCase.totalKwhOnGroup(token, groupId).asLiveData()

    fun updateGroup(
        token: String,
        groupId: Int,
        jsonObject: JsonObject
    ): LiveData<Resource<Group>> = userUseCase.updateGroup(token, groupId, jsonObject).asLiveData()
}