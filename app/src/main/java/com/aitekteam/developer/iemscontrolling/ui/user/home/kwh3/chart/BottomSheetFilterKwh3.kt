package com.aitekteam.developer.iemscontrolling.ui.user.home.kwh3.chart

import android.app.Activity
import android.app.DatePickerDialog
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.widget.ArrayAdapter
import android.widget.DatePicker
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.aitekteam.developer.iemscontrolling.R
import com.aitekteam.developer.iemscontrolling.utils.helper.Helpers.convertDayMonth
import com.aitekteam.developer.iemscontrolling.utils.listener.BottomSheetListener
import com.google.android.material.bottomsheet.BottomSheetDialog
import kotlinx.android.synthetic.main.bottom_sheet_chart_kwh.*
import kotlinx.android.synthetic.main.bottom_sheet_filter_kwh3.view.*
import java.util.*

class BottomSheetFilterKwh3(
    activity: Activity,
    private val currentFilter: String,
    private val currentType: String,
    private val currentValue: String,
    private val currentDate: String,
    private val listener: BottomSheetListener
) : BottomSheetDialog(activity, R.style.BottomSheetDialogTheme),
    DatePickerDialog.OnDateSetListener {

    private lateinit var view: View

    private var day = 0
    private var month = 0
    private var year = 0

    private var filter = ""
    private var type = ""
    private var value = ""
    private var date = ""

    fun showBottomSheet() {
        view =
            LayoutInflater.from(context).inflate(R.layout.bottom_sheet_filter_kwh3, chart_container)
        setContentView(view)
        show()

        setupUI(view)
    }

    private fun setupUI(view: View) {
        // set first value
        view.filter.setText(currentFilter)
        view.type.setText(currentType)
        view.value.setText(currentValue)

        // set utils
        filter = currentFilter
        type = currentType
        value = currentValue
        date = currentDate

        // set visibility of date
        if (currentDate == "")
            view.date.setText(context.getString(R.string.date))
        else view.date.setText(currentDate)

        // set visibility of type
        if (currentType == "Energy")
            view.layout_value.visibility = View.GONE
        else
            view.layout_value.visibility = View.VISIBLE


        // set visibility of filter
        if (currentFilter == "Hourly Average" || currentFilter == "Hourly")
            enableDateTime(true)
        else enableDateTime(false)

        // set dropdown
        val filterEnergyOption =
            listOf("Choose Collecting Data", "Cumulative", "Daily Total", "Monthly Total")
        val filterPowerOption =
            listOf(
                "Choose Collecting Data",
                "Hourly Average",
                "Daily Average",
                "Monthly Average",
                "Realtime"
            )
        val filterOption =
            listOf("Choose Collecting Data", "Realtime", "Monthly", "Daily", "Hourly")
        val typeOption = listOf(
            "Energy",
            "Active Power",
            "Reactive Power",
            "Apparent Power",
            "Current",
            "Voltage",
            "Power Factor"
        )
        val valueOption = listOf("Choose Value Option", "R", "S", "T", "Total")
        val valueOption2 = listOf("Choose Value Option", "R", "S", "T")

        // init adapter
        val typeAdapter = ArrayAdapter(context, R.layout.option_item, typeOption)
        var filterAdapter = ArrayAdapter(context, R.layout.option_item, filterOption)
        var valueAdapter = ArrayAdapter(context, R.layout.option_item, valueOption)

        // set adapter
        view.type.setAdapter(typeAdapter)
        view.filter.setAdapter(filterAdapter)
        view.value.setAdapter(valueAdapter)

        // set dropdown type
        view.type.setOnItemClickListener { _, _, pos, _ ->
            filter = "Choose Collecting Data"
            value = "Choose Value Option"
            view.filter.setText(filter)
            view.value.setText(value)

            // set visibility value
            if (pos == 1 || pos == 2 || pos == 3) {
                view.layout_value.visibility = View.VISIBLE
                valueAdapter = ArrayAdapter(context, R.layout.option_item, valueOption)
                view.value.setAdapter(valueAdapter)
            } else if (pos == 4 || pos == 5 || pos == 6) {
                view.layout_value.visibility = View.VISIBLE
                valueAdapter = ArrayAdapter(context, R.layout.option_item, valueOption2)
                view.value.setAdapter(valueAdapter)
            } else {
                view.layout_value.visibility = View.GONE
            }

            // set filter value
            if (pos == 0) {
                enableDateTime(false)
                filterAdapter = ArrayAdapter(context, R.layout.option_item, filterEnergyOption)
                view.filter.setAdapter(filterAdapter)
            } else if (pos == 1 || pos == 2 || pos == 3) {
                filterAdapter = ArrayAdapter(context, R.layout.option_item, filterPowerOption)
                view.filter.setAdapter(filterAdapter)
            } else if (pos == 4 || pos == 5 || pos == 6) {
                filterAdapter = ArrayAdapter(context, R.layout.option_item, filterOption)
                view.filter.setAdapter(filterAdapter)
            }

            type = typeAdapter.getItem(pos).toString()
        }

        // set dropdown filter
        view.filter.setOnItemClickListener { _, _, pos, _ ->
            val value = filterAdapter.getItem(pos).toString()

            if (value == "Hourly Average" || value == "Hourly")
                enableDateTime(true)
            else enableDateTime(false)

            // set filter
            filter = filterAdapter.getItem(pos).toString()
        }

        // set dropdown value
        view.value.setOnItemClickListener { _, _, pos, _ ->
            value = valueAdapter.getItem(pos).toString()
        }

        // date picker listener
        view.date.setOnClickListener {
            getDateTimeCalendar()
            val datePicker = DatePickerDialog(context, this, year, month, day)
            datePicker.show()
            datePicker.getButton(DatePickerDialog.BUTTON_POSITIVE)
                .setTextColor(Color.BLACK)
            datePicker.getButton(DatePickerDialog.BUTTON_NEGATIVE)
                .setTextColor(Color.BLACK)
        }

        // button listener
        view.btn_submit.setOnClickListener {
            when {
                filter == "Choose Collecting Data" -> Toast.makeText(
                    context,
                    "Please choose collecting data first!",
                    Toast.LENGTH_SHORT
                )
                    .show()
                value == "Choose Value Option" -> Toast.makeText(
                    context,
                    "Please choose value option first!",
                    Toast.LENGTH_SHORT
                )
                    .show()
                else -> {
                    listener.filterChartKwh3(filter, type, value, date)
                    dismiss()
                }
            }
        }
    }

    private fun getDateTimeCalendar() {
        val cal = Calendar.getInstance()
        day = cal.get(Calendar.DAY_OF_MONTH)
        month = cal.get(Calendar.MONTH)
        year = cal.get(Calendar.YEAR)
    }

    override fun onDateSet(p0: DatePicker?, p1: Int, p2: Int, p3: Int) {
        date = StringBuilder().apply {
            append(year)
            append("-${(month + 1).convertDayMonth()}")
            append("-${day.convertDayMonth()}")
        }.toString()

        view.date.setText(date)
    }

    private fun enableDateTime(isEnable: Boolean) {
        if (isEnable) {
            view.date.isEnabled = true
            view.date.setBackgroundColor(Color.WHITE)
        } else {
            view.date.isEnabled = false
            view.date.setBackgroundColor(ContextCompat.getColor(context, R.color.disable))
        }
    }
}