package com.aitekteam.developer.iemscontrolling.ui.user.profile.complaint.list

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.aitekteam.developer.iemscontrolling.core.data.network.response.ticketing.Ticketing
import com.aitekteam.developer.iemscontrolling.core.data.network.service.TicketingService
import kotlinx.coroutines.flow.Flow

class ComplaintListViewModel(
    private val ticketingService: TicketingService
) : ViewModel() {

    fun getListComplaint(token: String): Flow<PagingData<Ticketing>> = Pager(PagingConfig(12)) {
        ComplaintPagingSource(token, ticketingService)
    }.flow.cachedIn(viewModelScope)
}