package com.aitekteam.developer.iemscontrolling.utils.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.aitekteam.developer.iemscontrolling.ui.operator.active.UserActivedFragment
import com.aitekteam.developer.iemscontrolling.ui.operator.inactive.UserInactivedFragment

class SectionAdapter(fm: FragmentManager, lifecycle: Lifecycle): FragmentStateAdapter(fm, lifecycle) {

    private val fragments: List<Fragment> = listOf(
        UserInactivedFragment(),
        UserActivedFragment()
    )

    override fun getItemCount(): Int = fragments.size

    override fun createFragment(position: Int): Fragment {
        return fragments[position]
    }
}