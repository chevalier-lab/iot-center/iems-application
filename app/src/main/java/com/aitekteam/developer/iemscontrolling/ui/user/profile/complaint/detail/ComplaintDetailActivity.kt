package com.aitekteam.developer.iemscontrolling.ui.user.profile.complaint.detail

import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.aitekteam.developer.iemscontrolling.R
import com.aitekteam.developer.iemscontrolling.core.data.network.response.ticketing.ReplyTicket
import com.aitekteam.developer.iemscontrolling.core.data.network.response.ticketing.Ticketing
import com.aitekteam.developer.iemscontrolling.core.utils.adapter.AdapterCallback
import com.aitekteam.developer.iemscontrolling.core.utils.adapter.ReusableAdapter
import com.aitekteam.developer.iemscontrolling.core.utils.vo.Status
import com.aitekteam.developer.iemscontrolling.databinding.ActivityComplaintDetailBinding
import com.aitekteam.developer.iemscontrolling.utils.helper.Constants.AUTH_TOKEN
import com.aitekteam.developer.iemscontrolling.utils.helper.Constants.USER_PHONE
import com.aitekteam.developer.iemscontrolling.utils.helper.Helpers.dateFormat
import com.aitekteam.developer.iemscontrolling.utils.helper.SharedPrefsUtils
import com.bumptech.glide.Glide
import com.google.gson.JsonObject
import kotlinx.android.synthetic.main.item_message.view.*
import org.koin.android.viewmodel.ext.android.viewModel

class ComplaintDetailActivity : AppCompatActivity() {
    private lateinit var binding: ActivityComplaintDetailBinding
    private val viewModel: ComplaintDetailViewModel by viewModel()

    // utils
    private lateinit var sharedPrefs: SharedPrefsUtils
    private lateinit var adapter: ReusableAdapter<ReplyTicket>
    private lateinit var tempDataList: MutableList<ReplyTicket>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityComplaintDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.apply {
                addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                statusBarColor = Color.TRANSPARENT
                this.decorView.rootView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            }
        }

        binding.toolbar.apply {
            setSupportActionBar(this)
            supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_back_black)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setDisplayShowTitleEnabled(false)
        }

        // init var
        tempDataList = mutableListOf()
        sharedPrefs = SharedPrefsUtils()
        sharedPrefs.start(this, AUTH_TOKEN)
        adapter = ReusableAdapter(this)

        // setup adapter
        setupAdapter(binding.rvMessage)

        // get intent data
        val extras = intent.extras
        if (extras != null) {
            val data = extras.getParcelable<Ticketing>(EXTRA_DATA)
            data?.let { initUI(it) }
        }
    }

    private fun initUI(data: Ticketing) {
        binding.tvTitle.text = data.title
        binding.tvDescription.text = data.description
        binding.tvDate.text = StringBuilder().append(data.date?.dateFormat())

        if (data.uri == null)
            binding.imageComplaint.visibility = View.GONE
        else
            Glide.with(this)
                .load(data.uri)
                .into(binding.imageComplaint)

        // fetch reply
        sharedPrefs.get(AUTH_TOKEN)?.let { token ->
            viewModel.getListReply(token, data.id!!.toInt()).observe(this) {
                when (it.status) {
                    Status.LOADING -> {
                        binding.rvMessage.visibility = View.GONE
                        binding.loading.visibility = View.VISIBLE
                    }
                    Status.SUCCESS -> {
                        it.data?.let { data ->
                            binding.loading.visibility = View.GONE

                            if (data.isNotEmpty()) {
                                adapter.addData(data.reversed())
                                tempDataList.addAll(data.reversed())
                                binding.rvMessage.visibility = View.VISIBLE
                                binding.layoutEmpty.visibility = View.GONE
                            } else {
                                binding.rvMessage.visibility = View.GONE
                                binding.layoutEmpty.visibility = View.VISIBLE
                            }
                            binding.rvMessage.scrollToPosition(adapter.itemCount - 1)

                        }
                    }
                    Status.ERROR -> {
                        with (binding) {
                            layoutMain.visibility = View.GONE
                            layoutReply.visibility = View.GONE
                            layoutError.visibility = View.VISIBLE
                        }
                    }
                }
            }

            // reply messge
            binding.btnReply.setOnClickListener {
                reply(token, data.id!!.toInt())
            }
        }
    }

    private fun reply(token: String, ticketId: Int) {
        val reply = binding.etReply.text.toString().trim()
        val data = JsonObject().apply {
            addProperty("reply", reply)
        }

        if (reply != "")
            viewModel.replyComplaint(token, ticketId, data).observe(this) {
                when (it.status) {
                    Status.LOADING -> {
                        binding.btnReply.isEnabled = false
                    }
                    Status.SUCCESS -> {
                        binding.btnReply.isEnabled = true
                        binding.layoutEmpty.visibility = View.GONE
                        binding.rvMessage.visibility = View.VISIBLE
                        it.data?.let { data ->
                            tempDataList.add(data)
                            adapter.addMoreData(tempDataList)
                            binding.etReply.setText("")
                            binding.rvMessage.scrollToPosition(adapter.itemCount - 1)
                        }
                    }
                    Status.ERROR -> {
                        binding.btnReply.isEnabled = true
                        Toast.makeText(this, "Failed to send reply!", Toast.LENGTH_SHORT).show()
                    }
                }
            }

    }

    private fun setupAdapter(recyclerView: RecyclerView) {
        adapter.adapterCallback(adapterCallback)
            .setLayout(R.layout.item_message)
            .isVerticalView()
            .build(recyclerView)
    }

    private val adapterCallback = object : AdapterCallback<ReplyTicket> {
        override fun initComponent(itemView: View, data: ReplyTicket, itemIndex: Int) {
            sharedPrefs.get(USER_PHONE)?.let { phone ->
                if (data.phoneNumber == phone) {
                    itemView.chat_admin.visibility = View.GONE
                    itemView.chat_user.visibility = View.VISIBLE

                    itemView.tv_content.text = data.reply
                    itemView.tv_time.text = data.date?.dateFormat()
                } else {
                    itemView.chat_admin.visibility = View.VISIBLE
                    itemView.chat_user.visibility = View.GONE

                    itemView.tv_content_admin.text = data.reply
                    itemView.tv_time_admin.text = data.date?.dateFormat()
                }
            }
        }

        override fun onItemClicked(itemView: View, data: ReplyTicket, itemIndex: Int) {}

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    companion object {
        const val EXTRA_DATA = "extra_data"
    }
}