package com.aitekteam.developer.iemscontrolling.ui.user.home.slca

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.aitekteam.developer.iemscontrolling.core.data.Resource
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.general.Device
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.slca.Scheduler
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.slca.SingleSchedulerResponse
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.slca.Slca
import com.aitekteam.developer.iemscontrolling.core.data.network.service.DeviceService
import com.aitekteam.developer.iemscontrolling.core.domain.usecase.DeviceUseCase
import com.google.gson.JsonObject
import kotlinx.coroutines.flow.Flow

class SlcaViewModel(
    private val deviceUseCase: DeviceUseCase,
    private val deviceService: DeviceService
) : ViewModel() {

    fun getDeviceInfo(token: String): LiveData<Resource<Device>> =
        deviceUseCase.getDeviceInfo(token).asLiveData()

    fun getLastSlca(token: String): LiveData<Resource<Slca>> =
        deviceUseCase.getLastSlca(token).asLiveData()

    fun getHistorySlca(token: String): Flow<PagingData<Slca>> = Pager(PagingConfig(12)) {
        SlcaPagingSource(token, deviceService)
    }.flow.cachedIn(viewModelScope)

    fun updateDevice(token: String, id: Int, jsonObject: JsonObject): LiveData<Resource<Device>> =
        deviceUseCase.updateDevice(token, id, jsonObject).asLiveData()

    fun changeDeviceSsid(
        token: String,
        deviceId: Int,
        jsonObject: JsonObject
    ): LiveData<Resource<Device>> =
        deviceUseCase.changeDeviceSsid(token, deviceId, jsonObject).asLiveData()

    fun getListScheduler(
        token: String,
        deviceId: String
    ): LiveData<Resource<List<Scheduler>>> =
        deviceUseCase.getListScheduler(token, deviceId).asLiveData()

    fun createScheduler(
        token: String,
        deviceId: String,
        jsonObject: JsonObject
    ): LiveData<Resource<Scheduler>> =
        deviceUseCase.createScheduler(token, deviceId, jsonObject).asLiveData()

    fun deleteScheduler(
        token: String,
        deviceId: String
    ): LiveData<Resource<SingleSchedulerResponse>> =
        deviceUseCase.deleteScheduler(token, deviceId).asLiveData()
}