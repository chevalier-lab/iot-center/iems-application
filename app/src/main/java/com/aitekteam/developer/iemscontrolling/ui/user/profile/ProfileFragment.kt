package com.aitekteam.developer.iemscontrolling.ui.user.profile

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.aitekteam.developer.iemscontrolling.R
import com.aitekteam.developer.iemscontrolling.core.data.Resource
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.profile.User
import com.aitekteam.developer.iemscontrolling.core.utils.vo.Status
import com.aitekteam.developer.iemscontrolling.databinding.FragmentProfileBinding
import com.aitekteam.developer.iemscontrolling.ui.auth.login.MainActivity
import com.aitekteam.developer.iemscontrolling.ui.user.profile.complaint.list.ComplaintListActivity
import com.aitekteam.developer.iemscontrolling.ui.user.profile.user.access.UserAccessActivity
import com.aitekteam.developer.iemscontrolling.ui.user.profile.account.info.AccountInfoActivity
import com.aitekteam.developer.iemscontrolling.ui.user.profile.terms.TermsAndConditionActivity
import com.aitekteam.developer.iemscontrolling.utils.helper.Constants.AUTH_TOKEN
import com.aitekteam.developer.iemscontrolling.utils.dialog.CustomDialogFragment
import com.aitekteam.developer.iemscontrolling.utils.helper.SharedPrefsUtils
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_host.*
import org.koin.android.viewmodel.ext.android.viewModel

class ProfileFragment : Fragment() {
    private var _binding: FragmentProfileBinding? = null
    private val binding get() = _binding!!
    private val viewModel: ProfileViewModel by viewModel()

    // utils
    private lateinit var sharedPrefs: SharedPrefsUtils

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentProfileBinding.inflate(inflater, container, false)

        // init var
        sharedPrefs = SharedPrefsUtils()
        sharedPrefs.start(requireActivity(), AUTH_TOKEN)

        activity?.toolbar_title?.text = getString(R.string.profile_title)

        initUI()

        // Inflate the layout for this fragment
        return binding.root
    }

    private fun initUI() {
        // fetch user
        sharedPrefs.get(AUTH_TOKEN)?.let { token ->
            viewModel.getProfile(token).observe(viewLifecycleOwner, profileObserver)
        }

        binding.btnAccountInfo.setOnClickListener {
            startActivity(
                Intent(requireContext(), AccountInfoActivity::class.java).putExtra(
                    AccountInfoActivity.EXTRA_CONDITION,
                    "user"
                )
            )
            activity?.finish()
        }

        binding.btnUserAccess.setOnClickListener {
            startActivity(Intent(requireContext(), UserAccessActivity::class.java))
        }

        binding.btnComplain.setOnClickListener {
            startActivity(Intent(requireContext(), ComplaintListActivity::class.java))
        }

        binding.btnTerms.setOnClickListener {
            startActivity(Intent(requireContext(), TermsAndConditionActivity::class.java))
        }

        binding.btnAbout.setOnClickListener {
            AboutDialogFragment().show(parentFragmentManager, "customDialog")
        }

        binding.btnSignOut.setOnClickListener { showDialogToLogout() }
    }

    private val profileObserver = Observer<Resource<User>> {
        when (it.status) {
            Status.LOADING -> {
            }
            Status.SUCCESS -> {
                it.data?.let { user ->
                    binding.tvEmail.text = user.email
                    binding.tvName.text = user.fullName

                    Glide.with(requireContext())
                        .load(user.cover ?: R.drawable.avatar)
                        .circleCrop()
                        .into(binding.imageUser)

                    // check access
                    if (user.type == "guest" && user.status == "activate") {
                        binding.btnComplain.visibility = View.VISIBLE
                        binding.lineAccountInfo.visibility = View.VISIBLE
                    } else if (user.type == "user" && user.status == "activate") {
                        binding.btnComplain.visibility = View.VISIBLE
                        binding.lineAccountInfo.visibility = View.VISIBLE
                        binding.btnUserAccess.visibility = View.VISIBLE
                        binding.lineComplaint.visibility = View.VISIBLE
                    }
                }
            }
            Status.ERROR -> {
            }
        }
    }

    private fun showDialogToLogout() {
        CustomDialogFragment(
            getString(R.string.perhatian),
            getString(R.string.logout_disclaimer)
        ) { logout() }.show(parentFragmentManager, "customDialog")
    }

    private fun logout() {
        // clear shared prefs
        sharedPrefs.clear()

        val intent = Intent(requireContext(), MainActivity::class.java).apply {
            addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        }
        startActivity(intent)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}