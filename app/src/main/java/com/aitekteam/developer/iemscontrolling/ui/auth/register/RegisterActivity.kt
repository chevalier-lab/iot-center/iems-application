package com.aitekteam.developer.iemscontrolling.ui.auth.register

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.aitekteam.developer.iemscontrolling.R
import com.aitekteam.developer.iemscontrolling.core.utils.vo.Status
import com.aitekteam.developer.iemscontrolling.databinding.ActivityRegisterBinding
import com.aitekteam.developer.iemscontrolling.ui.user.host.HostActivity
import com.aitekteam.developer.iemscontrolling.utils.helper.Constants.AUTH_TOKEN
import com.aitekteam.developer.iemscontrolling.utils.dialog.CustomDialogFragment
import com.aitekteam.developer.iemscontrolling.utils.helper.SharedPrefsUtils
import com.google.android.material.snackbar.Snackbar
import com.google.gson.JsonObject
import org.koin.android.viewmodel.ext.android.viewModel

class RegisterActivity : AppCompatActivity() {
    private lateinit var binding: ActivityRegisterBinding
    private val viewModel: RegisterViewModel by viewModel()

    //utils
    private var fullNameValid = false
    private var phoneValid = false
    private var emailValid = false
    private var passwordValid = false
    private var passwordConfirmValid = false
    private lateinit var sharedPref: SharedPrefsUtils

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRegisterBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // change background status bar
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.apply {
                addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                statusBarColor = Color.TRANSPARENT
                this.decorView.rootView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            }
        }

        // init variables
        sharedPref = SharedPrefsUtils()
        sharedPref.start(this, AUTH_TOKEN)

        // auth check
        if (sharedPref.get(AUTH_TOKEN) != null)
            startActivity(getLaunchHome(this))

        // setup UI
        setupUI()
    }

    private fun setupUI() {
        // validate input
        validateInput()

        // register
        binding.btnRegister.setOnClickListener { showDialogToProcess() }
        binding.tvLogin.setOnClickListener { finish() }
    }

    private fun validateInput() {
        binding.etFullName.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun afterTextChanged(p0: Editable?) {}

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                fullNameValid = p0.toString().trim().isNotEmpty()
                inputCheck()
            }
        })

        binding.etPhone.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun afterTextChanged(p0: Editable?) {}

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                phoneValid = p0.toString().trim().isNotEmpty()
                inputCheck()
            }
        })

        binding.etEmail.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun afterTextChanged(p0: Editable?) {}

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                emailValid = p0.toString().trim().isNotEmpty()
                inputCheck()
            }
        })

        binding.etPassword.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun afterTextChanged(p0: Editable?) {}

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                passwordValid = p0.toString().trim().isNotEmpty()
                inputCheck()
            }
        })

        binding.etPasswordConfirm.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun afterTextChanged(p0: Editable?) {}

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                passwordConfirmValid = p0.toString() == binding.etPassword.text.toString()
                inputCheck()
            }
        })
    }

    private fun inputCheck() {
        if (fullNameValid && phoneValid && emailValid && passwordValid && passwordConfirmValid) {
            binding.btnRegister.isEnabled = true
            binding.btnRegister.setBackgroundColor(ContextCompat.getColor(this, R.color.green))
        } else {
            binding.btnRegister.isEnabled = false
            binding.btnRegister.setBackgroundColor(ContextCompat.getColor(this, R.color.divider))
        }
    }

    private fun showDialogToProcess() {
        CustomDialogFragment(
            getString(R.string.perhatian),
            getString(R.string.register_disclaimer)
        ) { register() }.show(supportFragmentManager, "cutomDialog")
    }

    private fun register() {
        val dataRaw = JsonObject().apply {
            addProperty("phone_number", binding.etPhone.text.toString())
            addProperty("password", binding.etPassword.text.toString())
            addProperty("full_name", binding.etFullName.text.toString())
            addProperty("email", binding.etEmail.text.toString())
        }

        viewModel.register(dataRaw).observe(this, {
            when (it.status) {
                Status.LOADING -> {
                    binding.loading.visibility = View.VISIBLE
                    binding.btnRegister.visibility = View.GONE
                }
                Status.SUCCESS -> {
                    it.data?.let { data ->
                        sharedPref.setString(AUTH_TOKEN, data.token!!)
                    }
                    startActivity(getLaunchHome(this))
                }
                Status.ERROR -> {
                    binding.loading.visibility = View.GONE
                    binding.btnRegister.visibility = View.VISIBLE
                    Snackbar.make(window.decorView.rootView, "Gagal registrasi!", Snackbar.LENGTH_SHORT)
                        .show()
                }
            }
        })
    }

    companion object {
        fun getLaunchHome(from: Context) = Intent(from, HostActivity::class.java).apply {
            addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        }
    }
}