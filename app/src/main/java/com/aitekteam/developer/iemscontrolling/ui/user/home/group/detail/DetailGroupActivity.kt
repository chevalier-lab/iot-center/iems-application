package com.aitekteam.developer.iemscontrolling.ui.user.home.group.detail

import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.RecyclerView
import com.aitekteam.developer.iemscontrolling.R
import com.aitekteam.developer.iemscontrolling.core.data.Resource
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.calculate.TotalKwh
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.general.Device
import com.aitekteam.developer.iemscontrolling.core.utils.adapter.AdapterCallback
import com.aitekteam.developer.iemscontrolling.core.utils.adapter.ReusableAdapter
import com.aitekteam.developer.iemscontrolling.core.utils.vo.Status
import com.aitekteam.developer.iemscontrolling.databinding.ActivityDetailGroupBinding
import com.aitekteam.developer.iemscontrolling.ui.user.home.group.append.AppendDeviceActivity
import com.aitekteam.developer.iemscontrolling.ui.user.home.kwh.ListKwhActivity
import com.aitekteam.developer.iemscontrolling.ui.user.home.pcb.PcbActivity
import com.aitekteam.developer.iemscontrolling.ui.user.home.sensor.SensorActivity
import com.aitekteam.developer.iemscontrolling.ui.user.home.slca.SlcaActivity
import com.aitekteam.developer.iemscontrolling.ui.user.host.HostActivity
import com.aitekteam.developer.iemscontrolling.utils.helper.Constants.AUTH_TOKEN
import com.aitekteam.developer.iemscontrolling.utils.dialog.CustomDialogFragment
import com.aitekteam.developer.iemscontrolling.utils.dialog.EditGroupDialogFragment
import com.aitekteam.developer.iemscontrolling.utils.helper.SharedPrefsUtils
import com.aitekteam.developer.iemscontrolling.utils.listener.DialogListener
import com.bumptech.glide.Glide
import com.google.gson.JsonObject
import kotlinx.android.synthetic.main.item_pcb.view.*
import kotlinx.android.synthetic.main.item_sensor.view.*
import kotlinx.android.synthetic.main.item_slca.view.*
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import timber.log.Timber
import org.koin.android.viewmodel.ext.android.viewModel
import java.text.DecimalFormat
import kotlin.properties.Delegates

class DetailGroupActivity : AppCompatActivity(), DialogListener {
    private lateinit var binding: ActivityDetailGroupBinding
    private val viewModel: DetailGroupViewModel by viewModel()

    // utils
    private var deviceCollapsed = true
    private var title: String? = null
    private var groupId by Delegates.notNull<Int>()
    private lateinit var sharedPrefs: SharedPrefsUtils
    private lateinit var dialog: EditGroupDialogFragment
    private lateinit var pcbAdapter: ReusableAdapter<Device>
    private lateinit var sensorAdapter: ReusableAdapter<Device>
    private lateinit var slcaAdapter: ReusableAdapter<Device>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailGroupBinding.inflate(layoutInflater)
        setContentView(binding.root)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.apply {
                addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                statusBarColor = Color.TRANSPARENT
                this.decorView.rootView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            }
        }

        binding.toolbar.apply {
            setSupportActionBar(this)
            supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_back_black)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setDisplayShowTitleEnabled(false)
        }

        // init var
        sharedPrefs = SharedPrefsUtils()
        sharedPrefs.start(this, AUTH_TOKEN)
        pcbAdapter = ReusableAdapter(this)
        sensorAdapter = ReusableAdapter(this)
        slcaAdapter = ReusableAdapter(this)

        // setup adapter
        setupAdapter.pcb(binding.rvPcb)
        setupAdapter.sensor(binding.rvSensor)
        setupAdapter.slca(binding.rvSlca)

        // get intent data
        val extra = intent.extras
        if (extra != null) {
            groupId = extra.getInt(EXTRA_ID)
            title = extra.getString(EXTRA_NAME)
            title?.let { initUI(it) }
        }

    }

    private fun initUI(title: String) {
        // collapsed toggle
        collapsedToggle()

        // toolbar title
        binding.toolbarTitle.text = title

        // fetch data
        sharedPrefs.get(AUTH_TOKEN)?.let { token ->
            viewModel.getDetailGroup(token, groupId).observe(this, observer.device(token))
        }

        // append device
        binding.fab.setOnClickListener {
            startActivity(
                Intent(this, AppendDeviceActivity::class.java)
                    .putExtra(AppendDeviceActivity.EXTRA_ID, groupId)
                    .putExtra(AppendDeviceActivity.EXTRA_NAME, title)
            )
            finish()
        }
    }

    private val observer = object {
        val totalKwh = Observer<Resource<TotalKwh>> {
            when (it.status) {
                Status.LOADING -> {
                    visibleUI(true)
                }
                Status.SUCCESS -> {
                    visibleUI(false)
                    it.data?.let { data ->
                        if (data.total == null && data.price == null)
                            binding.totalKwh.rootLayout.visibility = View.GONE
                        else {
                            binding.totalKwh.rootLayout.visibility = View.VISIBLE
                            val df = DecimalFormat("#.###")
                            binding.totalKwh.tvTotalKwh.text = df.format(data.total)
                            binding.totalKwh.tvTotalCost.text = data.priceStr?.replace("Rp", "")

                            // onclick
                            binding.totalKwh.rootLayout.setOnClickListener {
                                startActivity(
                                    Intent(this@DetailGroupActivity, ListKwhActivity::class.java)
                                        .putExtra(ListKwhActivity.EXTRA_ID, groupId)
                                )
                            }
                        }
                    }
                }
                Status.ERROR -> {
                    visibleUI(false)
                    binding.fab.visibility = View.GONE
                    binding.layoutError.visibility = View.VISIBLE
                    binding.totalKwh.rootLayout.visibility = View.GONE
                    Timber.i("error => ${it.message}")
                }
            }
        }

        fun device(token: String) = Observer<Resource<List<Device>>> {
            when (it.status) {
                Status.LOADING -> {
                    visibleUI(true)
                }
                Status.SUCCESS -> {
                    visibleUI(false)
                    val listPcb = mutableListOf<Device>()
                    val listSensor = mutableListOf<Device>()
                    val listSlca = mutableListOf<Device>()

                    it.data?.let { data ->
                        if (data.isEmpty()) {
                            binding.layoutMain.visibility = View.GONE
                            binding.layoutEmpty.visibility = View.VISIBLE
                        } else {
//                          // if data device is exists
                            viewModel.totalKwhOnGroup(token, groupId).observe(this@DetailGroupActivity, totalKwh)

                            data.map { device ->
                                when (device.type) {
                                    "pcb" -> listPcb.add(device)
                                    "sensors" -> listSensor.add(device)
                                    "slca" -> listSlca.add(device)
                                    else -> null
                                }
                            }

                            // append data to adapter
                            pcbAdapter.addData(listPcb)
                            sensorAdapter.addData(listSensor)
                            slcaAdapter.addData(listSlca)

                            // set visible layout
                            if (listPcb.isEmpty())
                                binding.rvPcb.visibility = View.GONE
                            if (listSensor.isEmpty())
                                binding.rvSensor.visibility = View.GONE
                            if (listSlca.isEmpty())
                                binding.rvSlca.visibility = View.GONE
                            if (listPcb.isEmpty() && listSensor.isEmpty() && listSlca.isEmpty())
                                binding.groupDevice.visibility = View.GONE
                        }
                    }
                }
                Status.ERROR -> {
                    visibleUI(false)
                    binding.fab.visibility = View.GONE
                    binding.layoutError.visibility = View.VISIBLE
                    Timber.i("error => ${it.message}")
                }
            }
        }
    }

    private fun visibleUI(isLoading: Boolean) {
        if (isLoading) {
            binding.fab.visibility = View.GONE
            binding.layoutMain.visibility = View.GONE
            binding.layoutEmpty.visibility = View.GONE
            binding.shimmerLoading.rootLayout.visibility = View.VISIBLE
        } else {
            binding.fab.visibility = View.VISIBLE
            binding.layoutMain.visibility = View.VISIBLE
            binding.layoutEmpty.visibility = View.GONE
            binding.shimmerLoading.rootLayout.visibility = View.GONE
        }
    }

    private val setupAdapter = object {
        fun pcb(recyclerView: RecyclerView) {
            pcbAdapter.adapterCallback(adapterCallback.pcb)
                .setLayout(R.layout.item_pcb)
                .isVerticalView()
                .build(recyclerView)
        }

        fun sensor(recyclerView: RecyclerView) {
            sensorAdapter.adapterCallback(adapterCallback.sensor)
                .setLayout(R.layout.item_sensor)
                .isVerticalView()
                .build(recyclerView)
        }

        fun slca(recyclerView: RecyclerView) {
            slcaAdapter.adapterCallback(adapterCallback.slca)
                .setLayout(R.layout.item_slca)
                .isVerticalView()
                .build(recyclerView)
        }
    }

    private val adapterCallback = object {

        val pcb = object : AdapterCallback<Device> {
            override fun initComponent(itemView: View, data: Device, itemIndex: Int) {
                itemView.tv_pcb_name.text = data.name

                // set value
                data.token?.let { token ->
                    viewModel.getLastPcb(token).observe(this@DetailGroupActivity) {
                        when (it.status) {
                            Status.LOADING -> {
                            }
                            Status.SUCCESS -> {
                                lateinit var sendMode: String

                                it.data?.let { data ->
                                    if (data.mode == null) {
                                        val params = itemView.layoutParams.apply {
                                            width = 0
                                            height = 0
                                        }
                                        itemView.visibility = View.GONE
                                        itemView.layoutParams = params
                                    } else {
                                        when (data.mode) {
                                            "ON" -> {
                                                sendMode = "OFF"
                                                Glide.with(this@DetailGroupActivity)
                                                    .load(R.drawable.ic_on)
                                                    .into(itemView.btn_power_pcb)
                                            }
                                            "OFF" -> {
                                                sendMode = "ON"
                                                Glide.with(this@DetailGroupActivity)
                                                    .load(R.drawable.ic_off)
                                                    .into(itemView.btn_power_pcb)
                                            }
                                            else -> Glide.with(this@DetailGroupActivity)
                                                .load(R.drawable.ic_off)
                                                .into(itemView.btn_power_pcb)
                                        }
                                    }
                                }

                                itemView.btn_power_pcb.setOnClickListener {
                                    CustomDialogFragment(
                                        getString(R.string.perhatian),
                                        getString(R.string.push_pcb_disclaimer)
                                    ) {
                                        val payload = JsonObject().apply {
                                            addProperty("mode", sendMode)
                                        }

                                        viewModel.pushDevice(token, payload)
                                            .observe(this@DetailGroupActivity) { res ->
                                                when (res.status) {
                                                    Status.LOADING -> {
                                                    }
                                                    Status.SUCCESS -> {
                                                        when (sendMode) {
                                                            "ON" -> {
                                                                sendMode = "OFF"
                                                                Glide.with(this@DetailGroupActivity)
                                                                    .load(R.drawable.ic_on)
                                                                    .into(itemView.btn_power_pcb)
                                                            }
                                                            "OFF" -> {
                                                                sendMode = "ON"
                                                                Glide.with(this@DetailGroupActivity)
                                                                    .load(R.drawable.ic_off)
                                                                    .into(itemView.btn_power_pcb)
                                                            }
                                                        }
                                                    }
                                                    Status.ERROR -> {
                                                        Toast.makeText(
                                                            this@DetailGroupActivity,
                                                            res.message,
                                                            Toast.LENGTH_SHORT
                                                        ).show()
                                                    }
                                                }
                                            }
                                    }.show(supportFragmentManager, "customDialog")
                                }
                            }
                            Status.ERROR -> {
                                Timber.i("error => ${it.message}")
                            }
                        }
                    }
                }
            }

            override fun onItemClicked(itemView: View, data: Device, itemIndex: Int) {
                startActivity(
                    Intent(this@DetailGroupActivity, PcbActivity::class.java).apply {
                        putExtra(PcbActivity.EXTRA_TOKEN, data.token)
                    }
                )
            }
        }

        val sensor = object : AdapterCallback<Device> {
            override fun initComponent(itemView: View, data: Device, itemIndex: Int) {
                itemView.tv_sensor_name.text = data.name

                // set value
                data.token?.let { token ->
                    viewModel.getLastSensor(token).observe(this@DetailGroupActivity) {
                        when (it.status) {
                            Status.LOADING -> {
                            }
                            Status.SUCCESS -> {
                                it.data?.let { data ->
                                    data.temp?.let { temp ->
                                        Glide.with(this@DetailGroupActivity)
                                            .load(R.drawable.ic_temperature)
                                            .into(itemView.image_temp)

                                        itemView.tv_temp.text =
                                            StringBuilder().append(temp).append("\u00B0")
                                    } ?: run {
                                        Glide.with(this@DetailGroupActivity)
                                            .load(R.drawable.ic_temperature_off)
                                            .into(itemView.image_temp)

                                        itemView.tv_temp.text =
                                            StringBuilder().append("N/A")
                                    }

                                    data.hum?.let { hum ->
                                        Glide.with(this@DetailGroupActivity)
                                            .load(R.drawable.ic_humidity)
                                            .into(itemView.image_hum)

                                        itemView.tv_hum.text =
                                            StringBuilder().append(hum).append("%")
                                    } ?: run {
                                        Glide.with(this@DetailGroupActivity)
                                            .load(R.drawable.ic_humidity_off)
                                            .into(itemView.image_hum)

                                        itemView.tv_hum.text =
                                            StringBuilder().append("N/A")
                                    }

                                    data.lux?.let { lux ->
                                        Glide.with(this@DetailGroupActivity)
                                            .load(R.drawable.ic_lux)
                                            .into(itemView.image_lux)

                                        itemView.tv_lux.text =
                                            StringBuilder().append(lux).append("%")
                                    } ?: run {
                                        Glide.with(this@DetailGroupActivity)
                                            .load(R.drawable.ic_lux_off)
                                            .into(itemView.image_lux)

                                        itemView.tv_lux.text =
                                            StringBuilder().append("N/A")
                                    }

                                    data.pir?.let { pir ->
                                        Glide.with(this@DetailGroupActivity)
                                            .load(R.drawable.ic_pir)
                                            .into(itemView.image_pir)

                                        itemView.tv_pir.text = pir.toString()
                                    } ?: run {
                                        Glide.with(this@DetailGroupActivity)
                                            .load(R.drawable.ic_pir_off)
                                            .into(itemView.image_pir)

                                        itemView.tv_pir.text =
                                            StringBuilder().append("N/A")
                                    }
                                }
                            }
                            Status.ERROR -> {
                                Timber.i("error => ${it.message}")
                            }
                        }
                    }
                }
            }

            override fun onItemClicked(itemView: View, data: Device, itemIndex: Int) {
                startActivity(
                    Intent(this@DetailGroupActivity, SensorActivity::class.java).apply {
                        putExtra(SensorActivity.EXTRA_TOKEN, data.token)
                    }
                )
            }
        }

        val slca = object : AdapterCallback<Device> {
            private lateinit var sendMode: String

            override fun initComponent(itemView: View, data: Device, itemIndex: Int) {
                itemView.tv_slca_name.text = data.name

                // set value
                data.token?.let { token ->
                    viewModel.getLastSlca(token).observe(this@DetailGroupActivity) {
                        when (it.status) {
                            Status.LOADING -> {
                            }
                            Status.SUCCESS -> {
                                it.data?.let { data ->
                                    itemView.tv_iac.text = data.iac.toString()
                                    itemView.tv_power.text = data.power.toString()

                                    if (data.iac == null || data.power == null) {
                                        sendMode = "ON"
                                        itemView.tv_iac.text = StringBuilder().append("N/A")
                                        itemView.tv_power.text = StringBuilder().append("N/A")

                                        Glide.with(this@DetailGroupActivity)
                                            .load(R.drawable.ic_off)
                                            .into(itemView.btn_power_slca)
                                    } else {
                                        when (data.status?.mode) {
                                            "ON" -> {
                                                sendMode = "OFF"
                                                Glide.with(this@DetailGroupActivity)
                                                    .load(R.drawable.ic_on)
                                                    .into(itemView.btn_power_slca)
                                            }
                                            "OFF" -> {
                                                sendMode = "ON"
                                                Glide.with(this@DetailGroupActivity)
                                                    .load(R.drawable.ic_off)
                                                    .into(itemView.btn_power_slca)
//                                                itemView.tv_iac.text = StringBuilder().append(0)
//                                                itemView.tv_power.text = StringBuilder().append(0)
                                            }
                                            else -> Glide.with(this@DetailGroupActivity)
                                                .load(R.drawable.ic_off)
                                                .into(itemView.btn_power_slca)
                                        }
                                    }

                                    itemView.btn_power_slca.setOnClickListener {
                                        CustomDialogFragment(
                                            getString(R.string.perhatian),
                                            getString(R.string.push_slca_disclaimer)
                                        ) {
                                            val payload = JsonObject().apply {
                                                addProperty("mode", sendMode)
                                            }
                                            viewModel.pushDevice(token, payload).observe(this@DetailGroupActivity) { res ->
                                                when (res.status) {
                                                    Status.LOADING -> {}
                                                    Status.SUCCESS -> {
                                                        when (sendMode) {
                                                            "ON" -> {
                                                                sendMode = "OFF"
                                                                Glide.with(this@DetailGroupActivity)
                                                                    .load(R.drawable.ic_on)
                                                                    .into(itemView.btn_power_slca)
//                                                                itemView.tv_iac.text = data.iac.toString()
//                                                                itemView.tv_power.text = data.power.toString()

                                                                lifecycleScope.launch {
                                                                    delayFetchOn(token, itemView)
                                                                }
                                                            }
                                                            "OFF" -> {
                                                                sendMode = "ON"
                                                                Glide.with(this@DetailGroupActivity)
                                                                    .load(R.drawable.ic_off)
                                                                    .into(itemView.btn_power_slca)
//                                                                itemView.tv_iac.text = StringBuilder().append(0)
//                                                                itemView.tv_power.text = StringBuilder().append(0)

                                                                lifecycleScope.launch {
                                                                    delayFetchOff(token, itemView)
                                                                }
                                                            }
                                                        }
                                                    }
                                                    Status.ERROR -> {
                                                        Toast.makeText(this@DetailGroupActivity, res.message, Toast.LENGTH_SHORT).show()
                                                    }
                                                }
                                            }
                                        }.show(supportFragmentManager, "customDialog")
                                    }
                                }
                            }
                            Status.ERROR -> {
                                Timber.i("error => ${it.message}")
                            }
                        }
                    }
                }
            }

            override fun onItemClicked(itemView: View, data: Device, itemIndex: Int) {
                startActivity(Intent(this@DetailGroupActivity, SlcaActivity::class.java).apply {
                    putExtra(SlcaActivity.EXTRA_TOKEN, data.token)
                })
            }

            private suspend fun delayFetchOn(token: String, itemView: View) {
                Timber.i("Test thread : wait 1 minutes")
                delay(60000)

                viewModel.getLastSlca(token).observe(this@DetailGroupActivity) { result ->
                    when (result.status) {
                        Status.LOADING ->  {}
                        Status.SUCCESS ->  {
                            val data = result.data
                            Timber.i("Test thread : done with data ${result.data}")

                            if (data?.iac == null || data.power == null) {
                                sendMode = "ON"
                                itemView.tv_iac.text = StringBuilder().append("N/A")
                                itemView.tv_power.text = StringBuilder().append("N/A")

                                Glide.with(this@DetailGroupActivity)
                                    .load(R.drawable.ic_off)
                                    .into(itemView.btn_power_slca)
                            } else {
                                sendMode = "OFF"
                                itemView.tv_iac.text = StringBuilder().append(result.data?.iac)
                                itemView.tv_power.text = StringBuilder().append(result.data?.power)

                                Glide.with(this@DetailGroupActivity)
                                    .load(R.drawable.ic_on)
                                    .into(itemView.btn_power_slca)
                            }
                        }
                        Status.ERROR ->  {}
                    }
                }
            }

            private suspend fun delayFetchOff(token: String, itemView: View) {
                Timber.i("Test thread : wait 1 minutes")
                delay(60000)

                viewModel.getLastSlca(token).observe(this@DetailGroupActivity) { result ->
                    when (result.status) {
                        Status.LOADING ->  {}
                        Status.SUCCESS ->  {
                            val data = result.data
                            Timber.i("Test thread : done with data ${result.data}")

                            if (data?.iac == null || data.power == null) {
                                sendMode = "ON"
                                itemView.tv_iac.text = StringBuilder().append("N/A")
                                itemView.tv_power.text = StringBuilder().append("N/A")

                                Glide.with(this@DetailGroupActivity)
                                    .load(R.drawable.ic_off)
                                    .into(itemView.btn_power_slca)
                            } else {
                                sendMode = "ON"
                                itemView.tv_iac.text = StringBuilder().append(result.data?.iac)
                                itemView.tv_power.text = StringBuilder().append(result.data?.power)

                                Glide.with(this@DetailGroupActivity)
                                    .load(R.drawable.ic_off)
                                    .into(itemView.btn_power_slca)
                            }
                        }
                        Status.ERROR ->  {}
                    }
                }
            }
        }
    }

    private fun collapsedToggle() {
        binding.groupDevice.setOnClickListener {
            deviceCollapsed = !deviceCollapsed

            if (deviceCollapsed) {
                binding.rvPcb.visibility = View.VISIBLE
                binding.rvSlca.visibility = View.VISIBLE
                binding.rvSensor.visibility = View.VISIBLE
                binding.imageCollapseDevice.setImageResource(R.drawable.ic_up)
            } else {
                binding.rvPcb.visibility = View.GONE
                binding.rvSlca.visibility = View.GONE
                binding.rvSensor.visibility = View.GONE
                binding.imageCollapseDevice.setImageResource(R.drawable.ic_down)
            }
        }
    }

    override fun updateGroup(name: String) {
        val rawData = JsonObject().apply {
            addProperty("group_label", name)
        }

        sharedPrefs.get(AUTH_TOKEN)?.let { token ->
            CustomDialogFragment(
                getString(R.string.perhatian),
                getString(R.string.update_group_disclaimer)
            ) {
                viewModel.updateGroup(token, groupId, rawData).observe(this) {
                    when (it.status) {
                        Status.LOADING -> {}
                        Status.SUCCESS -> {
                            dialog.dismiss()
                            initUI(name)
                        }
                        Status.ERROR -> {
                            Toast.makeText(this, it.message, Toast.LENGTH_SHORT).show()
                        }
                    }
                }
            }.show(supportFragmentManager, "customDialog")
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        menu?.getItem(0)?.icon = ContextCompat.getDrawable(this, R.drawable.ic_edit)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                startActivity(Intent(this, HostActivity::class.java))
                finish()
                true
            }
            R.id.overflow -> {
                title?.let {
                    dialog = EditGroupDialogFragment(it, this)
                    dialog.show(supportFragmentManager, "editGroup")
                }
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onBackPressed() {
        startActivity(Intent(this, HostActivity::class.java))
        finish()
    }

    companion object {
        const val EXTRA_ID = "extra_id"
        const val EXTRA_NAME = "extra_name"
    }
}