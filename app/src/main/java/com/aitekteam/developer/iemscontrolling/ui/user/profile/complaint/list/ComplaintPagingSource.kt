package com.aitekteam.developer.iemscontrolling.ui.user.profile.complaint.list

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.aitekteam.developer.iemscontrolling.core.data.network.response.ticketing.Ticketing
import com.aitekteam.developer.iemscontrolling.core.data.network.service.TicketingService
import okio.IOException
import retrofit2.HttpException

class ComplaintPagingSource(
    private val token: String,
    private val ticketingService: TicketingService
) : PagingSource<Int, Ticketing>() {

    override fun getRefreshKey(state: PagingState<Int, Ticketing>): Int? = state.anchorPosition

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Ticketing> = try {
        val currentPage = params.key ?: FIRST_PAGE_INDEX
        val response = ticketingService.getListComplainPaging(token, "DESC", currentPage)
        val responseList = mutableListOf<Ticketing>()

        val data = response.body()?.data ?: emptyList()
        responseList.addAll(data)

        LoadResult.Page(
            data = responseList,
            prevKey = null,
            nextKey = if (data.isNotEmpty()) currentPage.plus(1)
            else null
        )
    } catch (exception: Exception) {
        LoadResult.Error(exception)
    } catch (exception: IOException) {
        LoadResult.Error(exception)
    } catch (exception: HttpException) {
        LoadResult.Error(exception)
    }

    companion object {
        private const val FIRST_PAGE_INDEX = 0
    }
}