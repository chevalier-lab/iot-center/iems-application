package com.aitekteam.developer.iemscontrolling.utils.helper

object Constants {
    const val AUTH_TOKEN = "AUTHORIZATION_TOKEN"
    const val USER_TYPE = "USER_TYPE"
    const val USER_NAME = "USER_NAME"
    const val USER_EMAIL = "USER_EMAIL"
    const val USER_PHONE = "USER_PHONE"
    const val USER_STATUS = "USER_STATUS"
    const val USER_COVER = "USER_COVER"
    const val REQUEST_TYPE = "REQUEST_TYPE"
    const val TERMS_AND_CONDITION_URL = "http://213.190.4.40/iems/iems-dashboard-guest/terms.php"
}