package com.aitekteam.developer.iemscontrolling.ui.user.profile.user.access.detail

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.general.Device
import com.aitekteam.developer.iemscontrolling.core.data.network.service.UserService
import retrofit2.HttpException
import java.io.IOException

class GuestDevicePagingSource(
    private val token: String,
    private val phoneNumber: String,
    private val userService: UserService
) : PagingSource<Int, Device>() {

    override fun getRefreshKey(state: PagingState<Int, Device>): Int? = state.anchorPosition

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Device> = try {
        val currentPage = params.key ?: FIRST_PAGE_INDEX
        val response = userService.getDeviceGuestPaging(token, phoneNumber, currentPage)
        val responseList = mutableListOf<Device>()

        val data = response.body()?.data ?: emptyList()
        responseList.addAll(data)

        LoadResult.Page(
            data = responseList,
            prevKey = null,
            nextKey = if (data.isNotEmpty()) currentPage.plus(1)
            else null
        )
    } catch (exception: Exception) {
        LoadResult.Error(exception)
    } catch (exception: IOException) {
        LoadResult.Error(exception)
    } catch (exception: HttpException) {
        LoadResult.Error(exception)
    }

    companion object {
        private const val FIRST_PAGE_INDEX = 0
    }
}