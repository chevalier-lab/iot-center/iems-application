package com.aitekteam.developer.iemscontrolling.ui.user.home.group.append

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.aitekteam.developer.iemscontrolling.core.data.Resource
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.general.Device
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.group.Group
import com.aitekteam.developer.iemscontrolling.core.data.network.service.UserService
import com.aitekteam.developer.iemscontrolling.core.domain.usecase.UserUseCase
import kotlinx.coroutines.flow.Flow

class AppendDeviceViewModel(
    private val userUseCase: UserUseCase,
    private val userService: UserService
) : ViewModel() {

    fun filterDeviceGroup(
        token: String,
        groupId: Int,
        search: String,
        type: String,
        isAdded: String,
        direction: String
    ): Flow<PagingData<Device>> = Pager(PagingConfig(12)) {
        AppendDevicePagingSource(token, groupId, search, type, isAdded, direction, userService)
    }.flow.cachedIn(viewModelScope)

    fun appendDevice(token: String, groupId: Int, deviceId: Int): LiveData<Resource<Group>> =
        userUseCase.appendDevice(token, groupId, deviceId).asLiveData()

    fun removeDevice(token: String, groupId: Int, deviceId: Int): LiveData<Resource<Group>> =
        userUseCase.removeDevice(token, groupId, deviceId).asLiveData()
}