package com.aitekteam.developer.iemscontrolling.ui.user.home.kwh1.chart

import android.app.Activity
import android.app.DatePickerDialog
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.widget.ArrayAdapter
import android.widget.DatePicker
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.aitekteam.developer.iemscontrolling.R
import com.aitekteam.developer.iemscontrolling.utils.helper.Helpers.convertDayMonth
import com.aitekteam.developer.iemscontrolling.utils.listener.BottomSheetListener
import com.google.android.material.bottomsheet.BottomSheetDialog
import kotlinx.android.synthetic.main.bottom_sheet_chart_kwh.*
import kotlinx.android.synthetic.main.bottom_sheet_filter_kwh1.view.*
import java.util.*

class BottomSheetFilterKwh1(
    activity: Activity,
    private val currentFilter: String,
    private val currentType: String,
    private val currentDate: String,
    private val listener: BottomSheetListener
) : BottomSheetDialog(activity, R.style.BottomSheetDialogTheme),
    DatePickerDialog.OnDateSetListener {

    private lateinit var view: View

    private var day = 0
    private var month = 0
    private var year = 0

    private var filter = ""
    private var type = ""
    private var date = ""

    fun showBottomSheet() {
        view =
            LayoutInflater.from(context).inflate(R.layout.bottom_sheet_filter_kwh1, chart_container)
        setContentView(view)
        show()

        setupUI(view)
    }

    private fun setupUI(view: View) {
        // set first value
        view.filter.setText(currentFilter)
        view.type.setText(currentType)

        if (currentDate == "")
            view.date.setText(context.getString(R.string.date))
        else view.date.setText(currentDate)

        // set utils
        filter = currentFilter
        type = currentType
        date = currentDate

        // enable / disable date
        if (currentFilter == "Hourly Average" || currentFilter == "Hourly")
            enableDateTime(true)
        else enableDateTime(false)

        // set dropdown
        val filterEnergyOption = listOf("Choose Collecting Data", "Cumulative", "Daily Total", "Monthly Total")
        val filterPowerOption =
            listOf("Choose Collecting Data", "Hourly Average", "Daily Average", "Monthly Average", "Realtime")
        val filterOption = listOf("Choose Collecting Data", "Realtime", "Monthly", "Daily", "Hourly")
        val typeOption = listOf(
            "Energy",
            "Active Power",
            "Reactive Power",
            "Apparent Power",
            "Current",
            "Voltage",
            "Power Factor"
        )

        // init adapter
        val typeAdapter = ArrayAdapter(context, R.layout.option_item, typeOption)
        var filterAdapter = ArrayAdapter(context, R.layout.option_item, filterOption)

        // set adapter
        view.type.setAdapter(typeAdapter)
        view.filter.setAdapter(filterAdapter)

        // set dropdown type
        view.type.setOnItemClickListener { _, _, pos, _ ->
            filter = "Choose Collecting Data"
            view.filter.setText(filter)

            // set filter value
            if (pos == 0) {
                enableDateTime(false)
                filterAdapter = ArrayAdapter(context, R.layout.option_item, filterEnergyOption)
                view.filter.setAdapter(filterAdapter)
            } else if (pos == 1 || pos == 2 || pos == 3) {
                filterAdapter = ArrayAdapter(context, R.layout.option_item, filterPowerOption)
                view.filter.setAdapter(filterAdapter)
            } else if (pos == 4 || pos == 5 || pos == 6) {
                filterAdapter = ArrayAdapter(context, R.layout.option_item, filterOption)
                view.filter.setAdapter(filterAdapter)
            }

            type = typeAdapter.getItem(pos).toString()
        }

        // set dropdown filter
        view.filter.setOnItemClickListener { _, _, pos, _ ->
            val value = filterAdapter.getItem(pos).toString()

            if (value == "Hourly Average" || value == "Hourly")
                enableDateTime(true)
            else enableDateTime(false)

            // set filter
            filter = filterAdapter.getItem(pos).toString()
        }

        // date picker listener
        view.date.setOnClickListener {
            getDateTimeCalendar()
            val datePicker = DatePickerDialog(context, this, year, month, day)
            datePicker.show()
            datePicker.getButton(DatePickerDialog.BUTTON_POSITIVE)
                .setTextColor(Color.BLACK)
            datePicker.getButton(DatePickerDialog.BUTTON_NEGATIVE)
                .setTextColor(Color.BLACK)
        }

        // button listener
        view.btn_submit.setOnClickListener {
            if (filter == "Choose Collecting Data")
                Toast.makeText(context, "Please choose collecting data first!", Toast.LENGTH_SHORT).show()
            else {
                listener.filterChartKwh(filter, type, date)
                dismiss()
            }
        }
    }

    private fun getDateTimeCalendar() {
        val cal = Calendar.getInstance()
        day = cal.get(Calendar.DAY_OF_MONTH)
        month = cal.get(Calendar.MONTH)
        year = cal.get(Calendar.YEAR)
    }

    override fun onDateSet(p0: DatePicker?, p1: Int, p2: Int, p3: Int) {
        date = StringBuilder().apply {
            append(year)
            append("-${(month + 1).convertDayMonth()}")
            append("-${day.convertDayMonth()}")
        }.toString()

        view.date.setText(date)
    }

    private fun enableDateTime(isEnable: Boolean) {
        if (isEnable) {
            view.date.isEnabled = true
            view.date.setBackgroundColor(Color.WHITE)
        } else {
            view.date.isEnabled = false
            view.date.setBackgroundColor(ContextCompat.getColor(context, R.color.disable))
        }
    }
}