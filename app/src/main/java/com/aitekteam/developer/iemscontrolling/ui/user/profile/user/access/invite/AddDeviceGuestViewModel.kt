package com.aitekteam.developer.iemscontrolling.ui.user.profile.user.access.invite

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import com.aitekteam.developer.iemscontrolling.core.data.Resource
import com.aitekteam.developer.iemscontrolling.core.data.network.response.user.device.general.Device
import com.aitekteam.developer.iemscontrolling.core.domain.usecase.DeviceUseCase
import com.aitekteam.developer.iemscontrolling.core.domain.usecase.UserUseCase
import com.google.gson.JsonObject

class AddDeviceGuestViewModel(
    private val userUseCase: UserUseCase,
    private val deviceUseCase: DeviceUseCase
) : ViewModel() {

    fun getListDevices(token: String): LiveData<Resource<List<Device>>> =
        deviceUseCase.getListDevice(token).asLiveData()

    fun addDeviceGuest(token: String, jsonObject: JsonObject): LiveData<Resource<Device>> =
        userUseCase.addDeviceGuest(token, jsonObject).asLiveData()
}